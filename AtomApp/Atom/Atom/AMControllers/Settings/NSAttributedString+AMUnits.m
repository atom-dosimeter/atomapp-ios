//
//  NSAttributedString+AMUnits.m
//  Atom
//
//  Created by denis svinarchuk on 20.07.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "NSAttributedString+AMUnits.h"
#import "AMUIPreferences.h"
#import "AMSettings.h"
#import <AtomMeasurerSDK/AMDoseUnit.h>
#import <AtomMeasurerSDK/AMControllerManager.h>

@implementation NSAttributedString (AMUnits)

+ (NSMutableAttributedString*) __stringWithDose:(float)dose withBaseFontSize:(CGFloat)fontSize{
    
    static AMDoseUnit *unit = nil; if(!unit) unit = [AMDoseUnit sharedInstance];    
    //static NSNumberFormatter *floatFormater = nil; if(!floatFormater) floatFormater = unit.floatFormatter;
    
    NSString *stringDoseRate = [unit localeStringFromFloat:dose] ;
    
    NSMutableAttributedString *_dosestring = [[NSMutableAttributedString alloc] initWithString: stringDoseRate];
    
    UIFont *font = [UIFont fontWithName:AM_UI_BOLD_FONT size:fontSize];
    UIFont *sfont = [UIFont fontWithName:AM_UI_FONT size:fontSize*.6];
    
    [_dosestring addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, _dosestring.length)];
    [_dosestring appendAttributedString:[[NSAttributedString alloc] initWithString:@" "]];
    
    NSMutableAttributedString *_extention = [[NSMutableAttributedString alloc] initWithString:[unit currentSymbol]];    
    [_extention addAttribute:NSFontAttributeName value:sfont range:NSMakeRange(0, _extention.length)];
    
    [_dosestring appendAttributedString:_extention];
    
    return _dosestring;
}

+ (NSAttributedString*) stringWithDoseInCurrentUnit:(float)dose withBaseFontSize:(CGFloat)fontSize{
    return [self __stringWithDose:dose withBaseFontSize:fontSize];
}

+ (NSAttributedString*) stringWithDoseRateInCurrentUnit:(float)dose withBaseFontSize:(CGFloat)fontSize{   
    NSMutableAttributedString *_doserate = [NSMutableAttributedString __stringWithDose:dose withBaseFontSize:fontSize];
    NSMutableAttributedString *_extention = [[NSMutableAttributedString alloc] initWithString:AM_UNIT_PERHOUR];    
    [_extention addAttribute:NSFontAttributeName value:[UIFont fontWithName:AM_UI_FONT size:fontSize*.6] range:NSMakeRange(0, _extention.length)];
    [_doserate appendAttributedString:_extention];
    return _doserate ;
}

+ (NSMutableAttributedString*) __coloredStringWithDose:(float)doseRate withBaseFontSize:(CGFloat)fontSize{
    return [self __coloredStringWithDose:doseRate withThresholdDoseRate:[[AMControllerManager sharedInstance] defaultController].settings.thresholdDoseRate withBaseFontSize:fontSize];
}

+ (NSMutableAttributedString*) __coloredStringWithDose:(float)doseRate withThresholdDoseRate:(NSNumber*)thresholdDoseRate withBaseFontSize:(CGFloat)fontSize{
    static NSShadow *shadow = nil;
    
    if (!shadow) {
        shadow = [[NSShadow alloc] init];
        shadow.shadowBlurRadius = 3.0;
        shadow.shadowOffset = CGSizeMake(0.0, 0.5);
        shadow.shadowColor =  [UIColor colorWithCGColor:CGColorCreateCopyWithAlpha([UIColor blackColor].CGColor, 0.5)];
    }
    
    UIFont *font = [UIFont fontWithName:AM_UI_BOLD_FONT size:fontSize] ;    
    static AMDoseUnit *unit = nil; if(!unit) unit =[AMDoseUnit sharedInstance];
    
    NSMutableAttributedString *der = [[NSMutableAttributedString alloc]
                                      initWithString:[unit localeStringFromFloat: doseRate]
                                      ];
    
    static UIColor *highcolor = nil; if (!highcolor) highcolor = AM_UI_DER_HIGHCOLOR;
    static UIColor *lowcolor = nil; if (!lowcolor) lowcolor = AM_UI_DER_LOWCOLOR;
    UIColor *color;
    //NSNumber *thresholdDoseRate=[[AMControllerManager sharedInstance] defaultController].settings.thresholdDoseRate;
    if (doseRate>=thresholdDoseRate.floatValue) {
        color = highcolor;
    }
    else{
        color = lowcolor;
    }
    [der setAttributes:@{
                         NSForegroundColorAttributeName: color,
                         NSFontAttributeName: font,
                         NSShadowAttributeName: shadow
                         } range:NSMakeRange(0, der.length)];
    return der;
}


+ (NSAttributedString*) coloredStringWithDose:(float)doseRate withBaseFontSize:(CGFloat)fontSize{
    return [self __coloredStringWithDose:doseRate withBaseFontSize:fontSize];
}

+ (NSAttributedString*) attributedString:(NSString*)string withExtention:(NSString*)extention withBaseFontSize:(CGFloat)fontSize{
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString  alloc] initWithString:string];
    NSMutableAttributedString *_extention = [[NSMutableAttributedString alloc] initWithString:extention];
    
    UIFont *font_base = [UIFont fontWithName:AM_UI_BOLD_FONT size:fontSize];
    UIFont *font = [UIFont fontWithName:AM_UI_FONT size:fontSize*.6];
    
    [attributedString addAttribute:NSFontAttributeName value:font_base range:NSMakeRange(0, attributedString.length)];
    [_extention addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, _extention.length)];

//    static NSMutableParagraphStyle *paragraphStyle = nil;
//    if (!paragraphStyle) {
//        paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//        paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
//        paragraphStyle.alignment = NSTextAlignmentCenter;
//    }
//    
//    [_extention addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, _extention.length)];

    [attributedString appendAttributedString:_extention];
    return attributedString;
}

+ (NSAttributedString*) coloredStringWithDoseInCurrentUnit:(float)doseRate withBaseFontSize:(CGFloat)fontSize{
    return [self coloredStringWithDoseInCurrentUnit:doseRate withThresholdDoseRate:[[AMControllerManager sharedInstance] defaultController].settings.thresholdDoseRate withBaseFontSize:fontSize];
}

+ (NSAttributedString*) coloredStringWithDoseInCurrentUnit:(float)doseRate withThresholdDoseRate:(NSNumber*)thresholdDoseRate withBaseFontSize:(CGFloat)fontSize{
    NSMutableAttributedString *_doserate = [NSMutableAttributedString  __coloredStringWithDose:doseRate withThresholdDoseRate:thresholdDoseRate  withBaseFontSize:fontSize];
    static AMDoseUnit *unit = nil; if(!unit) unit = [AMDoseUnit sharedInstance];    
    
    NSMutableAttributedString *_extention = [[NSMutableAttributedString alloc] initWithString:[unit currentSymbol]];    
    
    UIFont *font = [UIFont fontWithName:AM_UI_FONT size:fontSize*.6];
    
    static NSMutableAttributedString *_extention2 = nil;
    
    if (!_extention2) {
        _extention2 = [[NSMutableAttributedString alloc] initWithString:AM_UNIT_PERHOUR];    
        [_extention2 addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, _extention2.length)];                
    }
    
    [_extention addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, _extention.length)];
    
    static NSAttributedString *space = nil; if (!space) space = [[NSAttributedString alloc] initWithString:@" "];
    [_doserate appendAttributedString:space];
    [_doserate appendAttributedString:_extention];
    [_doserate appendAttributedString:_extention2];
    return _doserate ;
}


+ (NSAttributedString*) durationStringFromStartTime:(NSTimeInterval)starttime withBaseFontSize:(CGFloat)fontSize{
    
    static NSCalendar *calendar = nil; if(!calendar) calendar =[[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];

    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:starttime];
    NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:[NSDate timeIntervalSinceReferenceDate]];
    static unsigned int unitFlags = NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    NSDateComponents *components = [calendar components:unitFlags fromDate:startDate  toDate:endDate  options:0];        
    
    NSString *timestr_h = [NSString stringWithFormat:@"%02li", (long)components.hour];
    NSString *timestr_m = [NSString stringWithFormat:@"%02li", (long)components.minute];
    NSString *timestr_s = [NSString stringWithFormat:@"%02li", (long)components.second];
    
    static NSMutableAttributedString *colon = nil; 
    if(!colon){
        colon = [[NSMutableAttributedString alloc] initWithString:@":"];    
        [colon addAttribute:NSFontAttributeName value:[UIFont fontWithName:AM_UI_FONT size:fontSize*.9] range:NSMakeRange(0, colon.length)];    
    }
    
    NSMutableAttributedString *timeattrstr=[[NSMutableAttributedString alloc] initWithString:timestr_h
                                                                                  attributes:@{
                                                                                               NSFontAttributeName: [UIFont fontWithName:AM_UI_FONT size:fontSize]
                                                                                               }];
    NSMutableAttributedString *timeattrstr_m=[[NSMutableAttributedString alloc] initWithString:timestr_m
                                                                                  attributes:@{
                                                                                               NSFontAttributeName: [UIFont fontWithName:AM_UI_FONT size:fontSize]
                                                                                               }];
    NSMutableAttributedString *timeattrstr_s=[[NSMutableAttributedString alloc] initWithString:timestr_s
                                                                                  attributes:@{
                                                                                               NSFontAttributeName: [UIFont fontWithName:AM_UI_FONT size:fontSize]
                                                                                               }];
    
    [timeattrstr appendAttributedString:colon];
    [timeattrstr appendAttributedString:timeattrstr_m];
    [timeattrstr appendAttributedString:colon];
    [timeattrstr appendAttributedString:timeattrstr_s];
    
    return timeattrstr;
}

+ (NSAttributedString*) dash{
    static NSAttributedString *_dash = nil;
    if (!_dash) {
        _dash=[[NSAttributedString alloc] initWithString: @"-"];
    }
    return _dash;
}

+ (NSAttributedString*) empty{
    static NSAttributedString *_empty = nil;
    if (!_empty) {
        _empty=[[NSAttributedString alloc] initWithString: @""];
    }
    return _empty;
}

@end
