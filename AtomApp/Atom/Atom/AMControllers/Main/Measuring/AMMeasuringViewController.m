//
//  AMMeasuringViewController.m
//  Atom
//
//  Created by denis svinarchuk on 20/10/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMMeasuringViewController.h"
#import "AMUIPreferences.h"

@interface AMMeasuringViewController ()
@end

@implementation AMMeasuringViewController

- (void)viewDidLoad {
    self.hasResetButton = YES;
    self.hasIntervalPicker = NO;

    [super viewDidLoad];
    
    [self awakeWithMotion];

    // Do any additional setup after loading the view.
    
    self.screenView.displayView.hasParticlesDisplay = YES;
    self.titledView.title = NSLocalizedString(@"Measuring", @"Title of measuring localized string");
    self.screenView.displayView.sigmaIsHidden = NO;
    self.screenView.displayView.chartView.hidden = YES;
}

- (void) didDosimeterUpdate:(AMDosimeter *)dosimeter{

    if (!self.isActive) {
        return;
    }
    
    float progress = (float)dosimeter.particlesEquivalentCount / (float)AM_UI_MINIMUM_PARTICLES_TO_VIEW_COMPUTE;
    float doseRate = dosimeter.doseEquivalentRate;
    
    if (dosimeter.particlesEquivalentCount<AM_UI_MINIMUM_PARTICLES_TO_VIEW_COMPUTE) {
        doseRate = -1.0;
    }
    
    float sigma    = dosimeter.statErrorEquivalentRate;
    NSTimeInterval startTime = dosimeter.measuringStartTime;
    
    self.screenView.displayView.particles = dosimeter.particlesEquivalentCount;
    [self updateWithDoseRate:doseRate withSigma:sigma withStartTime:startTime withProgress:progress];
    
}


@end
