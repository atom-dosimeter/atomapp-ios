//
//  AMVibrateOrSound.h
//  Atom
//
//  Created by denis svinarchuk on 15/09/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AMVibrateOrSound : NSObject

@property (nonatomic,readonly) BOOL isPlaing;

+(id) sharedInstance;
- (void) play;
- (void) pause;
@end
