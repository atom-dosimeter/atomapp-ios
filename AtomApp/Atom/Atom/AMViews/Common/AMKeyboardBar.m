//
//  AMKeyboardBar.m
//  Atom
//
//  Created by denis svinarchuk on 28.07.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMKeyboardBar.h"
#import "AMUIPreferences.h"
#import "AMSettings.h"


@implementation AMKeyboardBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self __init__];
    }
    return self;
}


- (id) init{
    self = [super init];
    if (self) {
        [self sizeToFit];
        [self __init__];
    }
    return self;
}


- (void) __init__{
    self.tintColor = self.barTintColor = [UIColor colorWithRed:AM_UI_COLOR(90) green:AM_UI_COLOR(90) blue:AM_UI_COLOR(90) alpha:1.0];
    
    self.backgroundColor = [UIColor blackColor];
    self.clipsToBounds = YES;
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.borderColor = [UIColor blackColor].CGColor;
    bottomBorder.borderWidth = 1;
    bottomBorder.frame = CGRectMake(0, self.frame.size.height-.5, self.frame.size.width, 0.5);
    bottomBorder.opacity = 0.5;
    bottomBorder.shadowRadius = 1.;
    [self.layer addSublayer:bottomBorder];
    
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *flex_right = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    flex_right.width=10.;
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"Button Done")
                                                                   style: UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(doneClicked:)];
    
    
    doneButton.width = 70;
    doneButton.tintColor = [UIColor whiteColor];
    [self setItems:@[flex, doneButton]];
}

- (void)doneClicked:(id)sender
{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:AM_SETTINGS_CHANGED_NOTIFICATION object:self];;
}


@end
