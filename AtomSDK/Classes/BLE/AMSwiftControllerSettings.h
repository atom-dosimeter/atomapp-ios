//
//  AMSwiftControllerSettings.h
//  Pods
//
//  Created by denis svinarchuk on 20.11.16.
//
//

#import "AMController.h"


/*
 В первую очередь: 
 1. три порога по мощности дозы и дозе,
 2. вкл-выкл акселерометра, 
 3. изменение отклика на частицы (щелчки-писк-вибрация)
 */

@interface AMSwiftControllerSettings : AMControllerSettings
@property (strong,nonatomic) AMBoolean *hasSoundOnImpulse;
@property (strong,nonatomic) AMBoolean *clicksMode; // 1:1 1:10
@property (strong,nonatomic) AMBoolean *accelerometerMode; // off/on
@property (strong,nonatomic) AMBoolean *isEnabled;

- (id) initWithAMTagDevice:(AMTagDevice *)tagDevice;

@end


@interface AMFastControllerSettings : AMControllerSettings
@property (strong,nonatomic) AMBoolean *hasSoundOnImpulse;
@property (strong,nonatomic) AMBoolean *hasVibrationOnImpulse;
@property (strong,nonatomic) AMBoolean *clicksMode; // 1:1 1:10
@property (strong,nonatomic) AMBoolean *accelerometerMode; // off/on
@property (strong,nonatomic) AMBoolean *isEnabled;

- (id) initWithAMTagDevice:(AMTagDevice *)tagDevice;

@end

