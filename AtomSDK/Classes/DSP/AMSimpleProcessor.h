//
//  AMSimpleProcessor.h
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 10/09/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMSimpleAudio.h"


@class AMSimpleProcessor;

@protocol AMSimpleProcessorProtocol <NSObject>
@required

/**
 *  Indicate that "AM" has power to supply dosimeter or not. Phisicaly "AM" produces meander with frequency has value about 40-150Hz with an amplitude of 5-25% of the maximum (gain?).
 *
 *  @param processor the processor object
 *  @param lowePower YES - we need to top up level of modulating signal
 *
 */
- (void) amSimpleProcessor:(AMSimpleProcessor*)processor hasLowPower:(BOOL)lowPower;

/**
 *  Indicate that "AM" received local peaks - number of triggering duirng measurement.
 *
 *  @param processor the processor object
 *  @param count     local peaks recived from device during one measure. One measure performs on the interval which the device provides.
 *
 */
- (void) amSimpleProcessor:(AMSimpleProcessor*)processor hasLocalPeaks:(NSInteger)count;

@end


@interface AMSimpleProcessor : AMSimpleAudio
@property (nonatomic,assign) uint32_t fftNumberOfInputSamples; // accomulate proper count of input samples for launch FFT
@property (nonatomic,readonly) float  volume;
@property (nonatomic) id<AMSimpleProcessorProtocol> delegate;
@property (nonatomic,assign) NSTimeInterval  powerStartUpTime; // 1 sec
+ (id) sharedInstance;
@end
