//
//  AMController.h
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 26/08/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//


typedef enum {
    AM_CONTROLLER_POWER_ENOUGH,
    AM_CONTROLLER_POWER_NOT_ENOUGH,     // enough to work but it has reason to alert user
    AM_CONTROLLER_POWER_CUT_DISCHARGE,  // device can not work
    AM_CONTROLLER_POWER_CHARGE_PLUGGED, // device plugged
    AM_CONTROLLER_POWER_UNKNOWN         // device can not work
} AMControllerPowerState;

#import <Foundation/Foundation.h>
#import "AMErrorHandlerProtocol.h"
#import "AMDosimeter.h"
#import "AMControllerSettings.h"

@class AMController;

/**
 * Atom device controller protocol which delegate device connection, updates and value state changes.
 *
 */
@protocol AMControllerProtocol <NSObject>
@required

/**
 *  Handle when host can't, lost or has connection to dosimeter
 *
 *  @param dosimeter the instance of dosimeter object
 *  @param available YES/NO
 *
 */
- (void) amController:(AMController*)controller didUpdateConnectionState:(BOOL)available;

/**
 *  Indicate that "AM" has power to supply dosimeter or not. Phisicaly "AM" produces meander with frequency has value about 40-150Hz with an amplitude of 5-25% of the maximum (gain?).
 *
 *  @param processor the processor object
 *  @param powerState detect power when is not enough.
 *
 */
- (void) amController:(AMController*)controller didChangePowerState:(AMControllerPowerState)powerState;

/**
 *  Tells the delegate object that the dosimeter connected to the i-device is about to be ready during the next approxTime.
 *
 *  @param controller the dosimeter controller instance.
 *  @param approxTime time to be ready
 *
 */
- (void) amController:(AMController *)controller willReadyAfterTime:(NSTimeInterval)approxTime;

/**
 *  Tells the delegate object that the dosimeter connected to the i-device was ready or not ready to detect radiation.
 *
 *  @param controller the dosimeter controller instance
 *  @param ready ready-state YES/NO
 *
 */
- (void) amController:(AMController *)controller didReady:(BOOL)ready;

/**
 *  Tells the delegate that i-devices has changed absolute sound volume, which uses to power jack-connected dozimeter (Atom Simple). In case Atom BT connection controller should not care about it.
 *
 *  @param controller the dosimeter controller instance
 *  @param volume     volume value is between 0.0-1.0
 
 */
- (void) amController:(AMController *)controller didChangePowerVolume:(float)volume;

/**
 *  Indicate that "AM" received local peaks - number of triggering duirng measurement.
 *
 *  @param processor the dosimeter controller instance
 *  @param count     local peaks recived from device during one measure. One measure performs on the interval which the device provides.
 *
 */
- (void) amController:(AMController*)controller didDetectParticles:(NSInteger)count;

/**
 *  Indicate that settings of the device was changed.
 *
 *  @param controller the dosimeter controller instance
 *  @param settings   controller settings
 *
 */
- (void) amController:(AMController*)controller didUpdateSettings:(AMControllerSettings*)settings;

@optional
/**
 *  Device controller can initiate changing of the device preferences or settings. This delegate inform that the process has been started but not completed yet.
 *
 *  @param controller the dosimeter controller instance
 *
 */
- (void) amControllerWillCommitChanges:(AMController *)controller;
/**
 *  Device controller can initiate changing of the device preferences or settings. This delegate inform that the process has been completed.
 *
 *  @param controller the dosimeter controller instance
 *
 */
- (void) amControllerDidCommitChanges:(AMController *)controller;

/**
 *  Device name can be changed. This delegate inform that name was changed.
 *
 *  @param controller the dosimeter controller instance
 *  @param fromName   old name
 *  @param toName     new name
 *
 */
- (void) amController:(AMController *)controller didChangeFromName:(NSString*)fromName toName:(NSString*)toName;

- (void) amController:(AMController *)controller didWarn:(NSString*)warnString;

@end

/**
 *  Atom device controller is abstract layer which provides common interface to different physical devices that application can connect.
 *
 * The concrete device controller should implements the follow behavior:
 * 1. Checks hardware connection between i-device and dosimeter
 * 2. Detect dosimeter signals
 * 3. Create tone generator to supply power for a Atom Simple (jack-connected) or subscribe BLE notification to Atom Tag (Bluetooth/BTE4 supports)
 * 4. Check dosimeter states
 * 5. Handle particles detection
 * 6. Provide error handling (connection breaks, device off, low power to supply dosimeter, etc...)
 *
 */
@interface AMController : NSObject
/**
 *  Create Atom device controller.
 *
 *  @param delegate delegate
 *
 *  @return object instance
 *
 */
- (id) initWithDelegate:(id<AMControllerProtocol>)delegate;

/**
 *  Controller delegate manages events which dosimeter generate.
 */
@property (nonatomic,strong) id<AMControllerProtocol> delegate;

/**
 *  Error handler.
 *
 */
@property (nonatomic,strong) id<AMErrorHandlerProtocol> errorHandler;

/**
 *  DEvice specific settings;
 */
@property (nonatomic,readonly) AMControllerSettings *settings;

/**
 *  Battery or output power volume has value from 0.0 ot 1.0;
 *
 */
@property (nonatomic,readonly) float powerVolume;

/**
 *  Dosimeter low powered.
 *
 */
@property (nonatomic,readonly) AMControllerPowerState powerState;

/**
 *  Dosimeter calculator instance. It computes dose rate and other utility properties.
 *
 */
@property (nonatomic,readonly) AMDosimeter       *dosimeter;

/**
 *  Dosimeter has connection with the device.
 *
 */
@property (nonatomic,readonly) BOOL connectionState;

/**
 *  Dosimeter can have connection, but is not ready.
 */
@property (nonatomic,readonly) BOOL isReady;

/**
 *  Start controller handling.
 *
 */
- (void) start;

/**
 *  Stop controller handling.
 *
 */
- (void) stop;

- (void) resetDosimeter;

@end
