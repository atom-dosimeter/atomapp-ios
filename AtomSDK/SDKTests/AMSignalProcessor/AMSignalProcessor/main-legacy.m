//
//  main.m
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 24/06/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMToneGenerator.h"
#import "AMSimpleController.h"
#import "AMTagDeviceManager.h"

#define AM_TRANSFER_SERVICE_UUID        @"63462A4A-C28C-4FFD-87A4-2D23A1C72581"
#define AM_TRANSFER_CHARACTERISTIC_UUID @"70BC767E-7A1A-4304-81ED-14B9AF54F7BD"

@interface AMDosimeterController : NSObject <AMControllerProtocol, AMDosimeterProtocol>
@end

@implementation AMDosimeterController

- (void) amController:(AMSimpleController *)controller didUpdateConnectionState:(BOOL)available{
    NSLog(@"AMDosimeterController: has connection %i", available);
}

- (void) amController:(AMSimpleController *)controller didDetectParticles:(NSInteger)count{
    //NSLog(@"AMDosimeterController total peacks = %llu last peacks=%ld", controller.processor.peakCount, (long)count);
    [controller.dosimeter addParticles:count];
}

- (void) amController:(AMSimpleController *)controller didChangePowerVolume:(BOOL)lowPower{
    if (lowPower) {
        NSLog(@"AMDosimeterController has LOW    power! ");
    }
    else{
        NSLog(@"AMDosimeterController has ENOUGH power! ");
    }
}

- (void) amController:(AMSimpleController *)controller willReadyAfterTime:(NSTimeInterval)approxTime{
    NSLog(@"AMDosimeterController willReadyAfter to measure %f", approxTime);
}

- (void) amController:(AMSimpleController *)controller didReady:(BOOL)ready{
    NSLog(@"AMDosimeterController isReady to measure %i", ready);
    
    if (ready) {
        [controller.dosimeter start];
    }
    else{
        [controller.dosimeter stop];
    }
    
}

- (void) amDosimeterValuesChanched:(AMDosimeter *)dosimeter{
    NSLog(@"AMDosimeterController total = %lu searched = %lu MADER=%4.3f | SADER=%4.3f   S_SIGMA=%f | M_SIGMA=%f search time=%f", 
          (unsigned long)dosimeter.particlesEquivalentCount,
          (unsigned long)dosimeter.particlesSearchedCount,
          dosimeter.doseEquivalentRate, 
          dosimeter.doseSearchedRate, 
          dosimeter.statErrorEquivalentRate, 
          dosimeter.statErrorSearchedRate, 
          dosimeter.searchingStartTime);
}

@end

int main(int argc, const char * argv[])
{

    @autoreleasepool {

        AMTagDeviceManager *bleCentral = [AMTagDeviceManager sharedInstance];
        [bleCentral start];
        
                
        atexit_b(^{
            [bleCentral stop]; 
        });
        
//        AMToneGenerator *g = [[AMToneGenerator alloc] init];
//        g.frequency = 1000.0;
//        g.type = AM_SIGNAL_TYPE_SINE;        
//        [g start];
        
        sleep(1000);
        
//        AMDosimeterController *dcontroller = [[AMDosimeterController alloc] init];
//        AMController *controller = [[AMController alloc] initWithDelegate:dcontroller];
//        controller.delegate = dcontroller;
//        
//        controller.dosimeter.searchingModeThreshold = kAMDosimeterSearchModePrecisely;
//        controller.dosimeter.delegate = dcontroller;
//        controller.generator.frequency = 20000.0;
//        
//        [controller start];
//        
//        sleep(10000);
//        
//        [controller stop];
        
    }
        
    return 0;
}

