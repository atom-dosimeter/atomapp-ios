//
//  AMBaseDosimeterView.h
//  Atom
//
//  Created by denis svinarchuk on 16/10/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AMThermometerView.h"
#import "AMBaseDosimeterDisplayView.h"

@interface AMBaseDosimeterScreenView : UIView
@property (nonatomic,readonly) UIView                     *infoBaseView;
@property (nonatomic,readonly) AMBaseDosimeterDisplayView *displayView;
@property (nonatomic,readonly) AMThermometerView          *thermometerView;
@end
