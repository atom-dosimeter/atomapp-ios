//
//  AMController.m
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 26/08/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMController.h"

@implementation AMController

- (void) assertDelegate{
    NSAssert([self.delegate respondsToSelector:@selector(amController:didChangePowerState:)], @"AMController: - amController:didChangePowerState: does not define.");
    NSAssert([self.delegate respondsToSelector:@selector(amController:didUpdateConnectionState:)], @"AMController: - amController:didUpdateConnectionState: does not define.");
    NSAssert([self.delegate respondsToSelector:@selector(amController:didChangePowerVolume:)], @"AMController: - amController:didChangePowerVolume: does not define.");
    NSAssert([self.delegate respondsToSelector:@selector(amController:didDetectParticles:)], @"AMController: - amController:didDetectParticles: does not define.");
    NSAssert([self.delegate respondsToSelector:@selector(amController:didReady:)], @"AMController: - amController:didReady: does not define.");
    NSAssert([self.delegate respondsToSelector:@selector(amController:willReadyAfterTime:)], @"AMController: - amController:willReadyAfterTime: does not define.");
};

- (id) initWithDelegate:(id<AMControllerProtocol>)delegate{
    self = [super init];
    
    if (self) {
        _dosimeter = [[AMDosimeter alloc] init];
        _delegate = delegate;
        [self assertDelegate];
    }
    
    return self;
}

- (void) setDelegate:(id<AMControllerProtocol>)delegate{
    _delegate = delegate;
    [self assertDelegate];
}

- (void) start{
    self.dosimeter.UUIDString = self.settings.UUIDString;
}

- (void) stop{}

- (void) resetDosimeter{
}

@end
