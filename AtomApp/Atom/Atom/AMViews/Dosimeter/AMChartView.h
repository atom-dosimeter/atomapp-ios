//
//  AMChartView.h
//  Atom
//
//  Created by denis svinarchuk on 31/07/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "MESChartView.h"
#import <AtomMeasurerSDK/AMDosimeter.h>

@interface AMChartView : MESChartView
@property (nonatomic,assign) BOOL isActive;
@property (nonatomic,weak) AMDosimeter *currentDosimeter;
@property (nonatomic,strong) NSNumber *thresholdDoseRate;
@end
