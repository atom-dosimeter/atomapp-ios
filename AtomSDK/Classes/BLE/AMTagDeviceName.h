//
//  AMTagDeviceName.h
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 29/09/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMBLEProtocol.h"

@interface AMTagDeviceName : NSObject <AMBLEProtocol>
    /**
     *  Device name.
     */
    @property (strong,nonatomic) NSString *name;
    @property (readonly,nonatomic) NSData *data;
    @property (readonly,nonatomic) BOOL isReadOnly;
@end
