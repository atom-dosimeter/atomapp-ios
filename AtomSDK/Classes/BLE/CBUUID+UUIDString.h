//
//  CBUUID+UUIDString.h
//  Atom
//
//  Created by denis svinarchuk on 23.08.14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import <CoreFoundation/CoreFoundation.h>

#if TARGET_OS_IPHONE
#import <CoreBluetooth/CoreBluetooth.h>
#elif TARGET_OS_MAC
#import <IOBluetooth/IOBluetooth.h>
#endif

@interface CBUUID (UUIDString)
- (NSString *)UUIDString;
@end
