//
//  AMToneGenerator.m
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 24/06/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMToneGenerator.h"
#import "AMConstants.h"
#import <AVFoundation/AVFoundation.h>
#if TARGET_OS_IPHONE
#import <MediaPlayer/MediaPlayer.h>
#endif

typedef struct wavePlayer
{
	AudioUnit outputUnit;
	double startingFrameCount;
    
} AMWavePlayer;


@interface AMToneGenerator()
@property(nonatomic,assign) double cycleLength;
@end

@implementation AMToneGenerator
{
    AMWavePlayer player;
    Float32 *sineLeftBuffer, *sineRightBuffer;
    int      sineBufferSize;
}

@synthesize frequency=_frequency, sampleRate=_sampleRate, cycleLength=_cycleLength, type=_type;

- (id) init{
    self = [super init];
    
    if (self) {
        _isRunning = NO;
        _sampleRate = kAMSampleRate;
        _frequency = kAMSineToneGenratorFreq;
        _type = AM_SIGNAL_TYPE_SINE;
        sineLeftBuffer = sineRightBuffer = NULL;
        sineBufferSize = 0;
        [self configureDefaultDeviceID];
    }
    
    return self;
}

- (void) setType:(AMSignalType)type{
    //
    //
    //
    switch (type) {
        case AM_SIGNAL_TYPE_SINE:
        case AM_SIGNAL_TYPE_SQUARE:
            _type = type;
            break;
        default:
            NSAssert(NO, @"AMToneGenerator: Unknown generator.type: %d", self.type);
            return;
    }
}

- (AMSignalType) type{
    return _type;
}

- (void) setFrequency:(Float32)frequency{
    _frequency=frequency;
    _cycleLength = _sampleRate/_frequency;
}

- (Float32) frequency{
    return _frequency;
}

- (void) setSampleRate:(Float32)sampleRate{
    _sampleRate=sampleRate;
    _cycleLength = _sampleRate/_frequency;
}

- (Float32) sampleRate{
    return _sampleRate;
}

OSStatus waveRenderProc(void *inRefCon,
                        AudioUnitRenderActionFlags *ioActionFlags,
                        const AudioTimeStamp *inTimeStamp,
                        UInt32 inBusNumber,
                        UInt32 inNumberFrames,
                        AudioBufferList * ioData)
{
    AMToneGenerator *generator = (__bridge AMToneGenerator *)inRefCon;
        
	double j = generator->player.startingFrameCount;
	double cycleLength = generator->_cycleLength;
    AMSignalType wtype = generator->_type;
    
    Float32 *sineLeftBuffer = generator->sineLeftBuffer;
    Float32 *sineRightBuffer = generator->sineRightBuffer;
    int      sineBufferSize = generator->sineBufferSize;
    
    Float32 *buffer_left = (Float32 *)ioData->mBuffers[0].mData;
    Float32 *buffer_right = (Float32 *)ioData->mBuffers[1].mData;
    
    
    if (sineLeftBuffer == NULL || sineRightBuffer == NULL) {
        //
        // prepare sine once... 
        //
        generator->sineBufferSize = sineBufferSize = inNumberFrames;
        generator->sineLeftBuffer = sineLeftBuffer = malloc(sizeof(Float32)*sineBufferSize);
        generator->sineRightBuffer = sineRightBuffer = malloc(sizeof(Float32)*sineBufferSize);
                
        for (int frame = 0; frame < inNumberFrames; ++frame)
        {
            Float32 sample = 0;
            
            switch (wtype) {
                case AM_SIGNAL_TYPE_SINE:
                    
                    sample = (Float32) sin (2 * M_PI * (j / cycleLength));
                    break;
                    
                case AM_SIGNAL_TYPE_SQUARE:
                    
                    sample = 1.0;
                    if (j<cycleLength/2.)
                        sample = 0.0;
                    break;
                    
                default:
                    return kAudio_ParamError;
            }
            
            sineLeftBuffer[frame] = buffer_left[frame] = sample;
            sineRightBuffer[frame] = buffer_right[frame] =-sample;
            
            j += 1.0;
            if (j > cycleLength)
                j -= cycleLength;
        }                
    }
    else{
        memcpy(ioData->mBuffers[0].mData, sineLeftBuffer, sineBufferSize*sizeof(Float32));
        memcpy(ioData->mBuffers[1].mData, sineRightBuffer, sineBufferSize*sizeof(Float32));
    }
    
    generator->player.startingFrameCount = j;

	return noErr;
}

- (void) initOutputUnit{
    
    //  10.6 and later: generate description that will match out output device (speakers)
	AudioComponentDescription outputcd = {0}; // 10.6 version
    
	outputcd.componentType = kAudioUnitType_Output;
    outputcd.componentFlags = 0;
    outputcd.componentFlagsMask = 0;
	outputcd.componentManufacturer = kAudioUnitManufacturer_Apple;
    
#if TARGET_OS_IPHONE
    outputcd.componentSubType = kAudioUnitSubType_RemoteIO;
#else
	outputcd.componentSubType = kAudioUnitSubType_DefaultOutput;
#endif
    
	
	AudioComponent comp = AudioComponentFindNext (NULL, &outputcd);
	if (comp == NULL) {
		printf ("can't get output unit");
		exit (-1);
	}
    
    memset(&(self->player), 0, sizeof(self->player));
    
    [self checkError:AudioComponentInstanceNew(comp, &(self->player).outputUnit) operation:NSLocalizedString(@"Couldn't open component for outputUnit", @"")];
    
	// register render callback
    
	AURenderCallbackStruct input;
    input.inputProc = waveRenderProc;
	input.inputProcRefCon = (__bridge void *)(self);
    
    [self checkError:AudioUnitSetProperty(player.outputUnit,
                                          kAudioUnitProperty_SetRenderCallback,
                                          kAudioUnitScope_Input,
                                          0,
                                          &input,
                                          sizeof(input))
           operation:NSLocalizedString(@"AudioUnitSetProperty failed", @"")];
    
    
    // Set the format to 32 bit, single channel, floating point, linear PCM
	AudioStreamBasicDescription streamFormat;
	streamFormat.mSampleRate = self.sampleRate;
	streamFormat.mFormatID = kAudioFormatLinearPCM;
	streamFormat.mFormatFlags =
    kAudioFormatFlagsNativeFloatPacked | kAudioFormatFlagIsNonInterleaved;
	streamFormat.mBytesPerPacket = 4;
	streamFormat.mFramesPerPacket = 1;
	streamFormat.mBytesPerFrame = 4;
	streamFormat.mChannelsPerFrame = 2;
	streamFormat.mBitsPerChannel = streamFormat.mBytesPerPacket * 8;
    
    [self checkError:AudioUnitSetProperty (player.outputUnit,
                                           kAudioUnitProperty_StreamFormat,
                                           kAudioUnitScope_Input,
                                           0,
                                           &streamFormat,
                                           sizeof(AudioStreamBasicDescription))
           operation:NSLocalizedString(@"Couldn't initialize output stream properties", @"")];
    
    // initialize unit
    [self checkError:AudioUnitInitialize(player.outputUnit) operation:NSLocalizedString(@"Couldn't initialize output unit", @"")];
}


- (void)start{
    
    NSLog(@"AMToneGenerator.... %i",self.isRunning);
    
    if (self.isRunning) {
        return;
    }
    _isRunning = YES;
    // init output unit
    [self initOutputUnit];
    // start tone generator
    [self checkError:AudioOutputUnitStart(player.outputUnit) operation:NSLocalizedString(@"Couldn't start output unit", @"")];
}

- (void)stop{
    
    if (self.isRunning) {
        _isRunning = NO;
        AudioOutputUnitStop(player.outputUnit);
        AudioUnitUninitialize(player.outputUnit);
        AudioComponentInstanceDispose(player.outputUnit);                
    }
}

- (void) dealloc{
    [self stop];
    if (sineRightBuffer) {
        free(sineRightBuffer);
    }
    if (sineLeftBuffer) {
        free(sineLeftBuffer);
    }
}

@end

#pragma - utilities
