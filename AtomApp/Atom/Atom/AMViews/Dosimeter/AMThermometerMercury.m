//
//  AMThermometerMercury.m
//  Atom
//
//  Created by denis svinarchuk on 21/07/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMThermometerMercury.h"
#import "AMUIPreferences.h"
#import "AMSettings.h"
#import <AtomMeasurerSDK/AMControllerManager.h>

@interface AMThermometerMercury()

@property (nonatomic,strong) UIView *mercuryView;
@property (nonatomic,strong) CAShapeLayer *trustFactorWindowLayer;
@property (nonatomic,strong) CAGradientLayer *gradientLayer;
@property (nonatomic,strong) AMSettings *settings;
@property (nonatomic,readonly) AMControllerSettings *defaultControllerSettings;

@end

@implementation AMThermometerMercury

- (id) initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _baseFrame = frame;
        self.backgroundColor = [UIColor clearColor];
        [self gradientLayer];
    }
    return self;
}

- (AMControllerSettings*) defaultControllerSettings{
    return [[AMControllerManager sharedInstance] defaultController].settings;
}

- (AMSettings*) settings{
    if(!_settings) _settings = [AMSettings sharedInstance];
    return _settings;
}

- (void) setBaseFrame:(CGRect)baseFrame{
    _baseFrame = baseFrame;
    self.frame = _baseFrame;
    self.value = self.value;
    self.sigma = self.sigma;
}

- (CAGradientLayer*) gradientLayer{
    if (!_gradientLayer) {
        _gradientLayer = [CAGradientLayer layer];
        _gradientLayer.colors = @[
                                  (id)[AM_UI_HIGHERCOLOR CGColor],
                                  (id)[AM_UI_HIGHCOLOR CGColor],
                                  (id)[AM_UI_LOWCOLOR CGColor],
                                  (id)[AM_UI_LOWERCOLOR CGColor],
                                  (id)[AM_UI_LOWERCOLOR CGColor]
                                  ];
        _gradientLayer.drawsAsynchronously = YES;
        _gradientLayer.frame = self.mercuryView.bounds;
        _gradientLayer.shadowColor = AM_UI_LOWERCOLOR.CGColor;
        _gradientLayer.shadowOffset = CGSizeMake(.0, -.5);
        _gradientLayer.shadowRadius = 1.0;
        _gradientLayer.shadowOpacity = .2;
        
        [self.layer insertSublayer:_gradientLayer atIndex:0];
    }
    return _gradientLayer;
}

- (CAShapeLayer*) trustFactorWindowLayer{
    if (!_trustFactorWindowLayer) {
        _trustFactorWindowLayer = [CAShapeLayer layer];

        _trustFactorWindowLayer.drawsAsynchronously = YES;
        
        CGRect frame = CGRectInset(self.gradientLayer.bounds, -AM_THERMOMETER_OFSET_WIDTH-AM_MERCURY_OFSET_WIDTH, 0);
        frame.size.height = 10.;
        
        _trustFactorWindowLayer.frame = frame;
        _trustFactorWindowLayer.borderWidth = 0.5;
        _trustFactorWindowLayer.borderColor = CGColorCreateCopyWithAlpha([UIColor lightGrayColor].CGColor,0.7);
        _trustFactorWindowLayer.backgroundColor = CGColorCreateCopyWithAlpha([UIColor lightGrayColor].CGColor, 0.2);
        
        _trustFactorWindowLayer.opacity = 1.0;
        _trustFactorWindowLayer.shadowColor = [UIColor blackColor].CGColor;
        _trustFactorWindowLayer.shadowOffset = CGSizeMake(0.0, 1.0);
        _trustFactorWindowLayer.shadowRadius = 1.0;
        _trustFactorWindowLayer.shadowOpacity = 0.8;
        
        [self.gradientLayer addSublayer:_trustFactorWindowLayer];
    }
    return _trustFactorWindowLayer;
}

- (UIView*) mercuryView{
    if (!_mercuryView) {
        _mercuryView = [[UIView alloc] initWithFrame:self.bounds];
        [self addSubview:_mercuryView];
        _mercuryView.backgroundColor = [UIColor clearColor];
        
        _mercuryView.layer.shadowColor = [UIColor blackColor].CGColor;
        _mercuryView.layer.shadowOffset = CGSizeMake(-1., .5);
        _mercuryView.layer.shadowRadius = 3.;
        _mercuryView.layer.shadowOpacity = .4;
    }
    
    return _mercuryView;
}

- (void) setNeedsDisplay{
    [super setNeedsDisplay];
    self.value = self.value;
}

- (void) setValue:(float)value{
    
    [super setValue:value];
    
    if (value<=0.) {
        self.sigma = 0.0;
    }
    
    CGRect frame = self.bounds;
    float  height_log = 0;
    if (self.value>=0) {
        
        height_log = self.log10Value * frame.size.height;
        
        if (height_log<=0) {
            height_log = 0.0;
            frame.origin.y = self.baseFrame.size.height;
            frame.size.height = 0.0;
        }
        else{
            frame.origin.y = self.baseFrame.size.height-height_log;
            frame.size.height = height_log;
        }
    }
    else {
        frame.origin.y = self.baseFrame.size.height;
        frame.size.height = 0.0;
    }
    
    if (frame.size.height>=self.baseFrame.size.height) {
        frame.size.height = self.baseFrame.size.height;
    }
    
    
    float y_thresh_log = [self log10f:self.defaultControllerSettings.thresholdDoseRate.floatValue] * self.baseFrame.size.height;
    
    float high_gradient_part=0.0;
    float low_gradient_part = 0.0;
    if (height_log>y_thresh_log) {
        high_gradient_part = (height_log-y_thresh_log)/height_log;
        low_gradient_part  = y_thresh_log/height_log;
    }

    [CATransaction begin];
    [CATransaction setAnimationDuration:AM_UI_ANIMATE_DURATION];
    [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    
    self.gradientLayer.locations = @[
                                     [NSNumber numberWithDouble:high_gradient_part/4.],
                                     [NSNumber numberWithDouble:high_gradient_part],
                                     @0.0,//[NSNumber numberWithDouble:low_gradient_part],
                                     @1.0f,
                                     @0.0f//[NSNumber numberWithDouble:low_gradient_part],
                                     ];
    
    self.gradientLayer.frame = frame;
    
    [CATransaction commit];
}

- (void) setSigma:(float)sigma{
    _sigma = sigma;
    
    if (sigma<0) {
        return;
    }
    
    CGRect frame = self.trustFactorWindowLayer.frame;
    //
    // Hs=[D-sigma*Kp,D+sigma*Kp]
    //
    float td = _sigma/100.*self.trustFactor*self.value;
    float y1 = self.value-td;
    float y2 = self.value+td;
    float y1_log = [self log10f:y1>1.?y1:1.0] * self.baseFrame.size.height;
    float y2_log = [self log10f:y2>1.?y2:1.0] * self.baseFrame.size.height;
    float hs = y2_log-y1_log;
        
    if (hs<3.0) {
        hs=3.0;
    }
    else if (hs>=self.gradientLayer.bounds.size.height){
        hs = self.gradientLayer.bounds.size.height;
    }
    
    frame.size.height = hs;
    frame.origin.y = -frame.size.height/2.;
    
    if (frame.origin.y+self.gradientLayer.frame.origin.y<=0.) {
        frame.origin.y+=fabs(frame.origin.y+self.gradientLayer.frame.origin.y);
    }
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:AM_UI_ANIMATE_DURATION*1.5];
    self.trustFactorWindowLayer.frame = frame;
    [CATransaction commit];

}

@end

