//
//  AMTextUnitField.m
//  Atom
//
//  Created by denis svinarchuk on 02/09/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMDoseUnitField.h"
#import <AtomMeasurerSDK/AMDoseUnit.h>
#import "AMUIPreferences.h"
#import "AMKeyboardBar.h"
#import "NSAttributedString+AMUnits.h"
#import "NSString+AMUtils.h"

@interface AMDoseUnitField() <UITextFieldDelegate>
@property (strong,nonatomic) UILabel  *textLabel;
@property (readonly,nonatomic) AMDoseUnit *unit;
@property (strong,nonatomic) UITextField *textField;
@property (nonatomic,strong) NSNumberFormatter *floatFormatter;
@end

@implementation AMDoseUnitField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

@synthesize value=_value;

- (AMDoseUnit*) unit{
    return [AMDoseUnit sharedInstance];
}

- (void) setValue:(NSNumber *)value{
    
    _value = value;
    
    if (!self.isUpper) {
        if (self.isDoseRate) {
            self.textLabel.attributedText = [NSAttributedString stringWithDoseRateInCurrentUnit:_value.floatValue withBaseFontSize:self.textLabel.font.pointSize];
        }
        else{
            self.textLabel.attributedText = [NSAttributedString stringWithDoseInCurrentUnit:_value.floatValue withBaseFontSize:self.textLabel.font.pointSize];
        }
    }
    else{
        AMDoseUnitType type;
        [NSNumber numberWithFloat:[self.unit fromValueToUpper:self.value.floatValue unit:&type]];
        
        NSMutableAttributedString *_dosestring = [[NSMutableAttributedString alloc] initWithString: [self valueToString]];
        
        UIFont *font = self.textLabel.font;
        
        [_dosestring addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, _dosestring.length)];
        [_dosestring appendAttributedString:[[NSAttributedString alloc] initWithString:@""]];
        
        NSMutableAttributedString *_extention = [[NSMutableAttributedString alloc] initWithString:[AMDoseUnit unitSymbol:type]];
        [_extention addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, _extention.length)];
        
        [_dosestring appendAttributedString:_extention];
        
        self.textLabel.attributedText = _dosestring;
    }
}

- (NSNumber*) value{
    return _value;
}

- (void) setTextAlignment:(NSTextAlignment)textAlignment{
    _textAlignment=textAlignment;
    self.textLabel.textAlignment = self.textField.textAlignment = textAlignment;
}

- (UITextField*) textField{
    if (!_textField) {
        _textField = [[UITextField alloc] initWithFrame:self.textLabel.frame];
        _textField.autoresizingMask = ~ UIViewAutoresizingNone;
        _textField.textColor = self.textLabel.textColor;
        _textField.backgroundColor = [UIColor clearColor];
        _textField.userInteractionEnabled = YES;
        _textField.keyboardType = UIKeyboardTypeDecimalPad;
        _textField.keyboardAppearance = UIKeyboardAppearanceDark;
        _textField.textAlignment = self.textLabel.textAlignment;
        _textField.adjustsFontSizeToFitWidth = YES;
        _textField.minimumFontSize = 12;
        [self insertSubview:_textField aboveSubview:self.textLabel];
    }
    _textField.delegate = self;
    return _textField;
}

- (UILabel*) textLabel {
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] initWithFrame:self.bounds];
        _textLabel.autoresizingMask = ~ UIViewAutoresizingNone;
        
        _textLabel.adjustsFontSizeToFitWidth = YES;
        _textLabel.minimumScaleFactor = 0.6;
        _textLabel.textColor = [UIColor whiteColor];
        _textLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:_textLabel];
    }
    return _textLabel;
}

- (void) layoutSubviews{
    [super layoutSubviews];
    
    self.textLabel.frame = self.bounds;
    self.textField.frame = self.bounds;
    
}

- (void) setFontPointSize:(float)fontPointSize{
    _fontPointSize = fontPointSize;
    float fontSize = _fontPointSize;
    if (fontPointSize<=0.0) {
        fontSize = _textLabel.bounds.size.height*.9;
    }
    self.textLabel.font = [UIFont fontWithName:AM_UI_BOLD_FONT size:fontSize];
    self.textField.font = self.textLabel.font;
    self.value = _value;
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSMutableString *_text=[NSMutableString stringWithString:textField.text];
    
    [_text replaceCharactersInRange:range withString:string];
    return YES;
};

- (void) textFieldDidEndEditing:(UITextField *)textField{
    NSNumber *value=[self.textField.text floatNumber];
    self.value= self.isUpper?[self.unit fromUpperNumberToBase:value]:[self.unit fromNumberToBase:value];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(amDoseUnitFieldDidEndEditing:)]) {
        [self.delegate amDoseUnitFieldDidEndEditing:self];
    }
    
    [UIView animateWithDuration:AM_UI_ANIMATE_DURATION
                     animations:^{
                         self.textLabel.alpha = 1.0;
                         self.textField.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         textField.text = @"";
                         textField.alpha = 1.0;
                     }
     ];
}

- (NSString*) valueToString{
        NSNumberFormatter *formatter = self.unit.floatFormatter;
    
        AMDoseUnitType type = AM_DOSE_UNIT_NNSV;
        NSNumber *value = self.isUpper?[NSNumber numberWithFloat:[self.unit fromValueToUpper:self.value.doubleValue unit:&type]]:[self.unit convertNumberToCurrent:self.value];
    
        if (self.isUpper) {
            if (type == AM_DOSE_UNIT_RN)
                formatter = [self.unit formatterWithPrecision:2];
            else
                formatter = self.floatFormatter;
        }
        return  [formatter stringFromNumber:value];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(amDoseUnitFieldWillEdit:)]) {
        [self.delegate amDoseUnitFieldWillEdit:self];
    }
    
    textField.alpha = 0.0;
    
    textField.text = [self valueToString];
    
    [UIView animateWithDuration:AM_UI_ANIMATE_DURATION animations:^{
        self.textLabel.alpha = 0.0;
        self.textField.alpha = 1.0;
    }];
    
    if (self.textField.inputAccessoryView==nil && UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        self.textField.inputAccessoryView = [[AMKeyboardBar alloc] init];
    }
    
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [self endEditing:YES];
    return YES;
}

- (NSNumberFormatter*) floatFormatter{
    if (!_floatFormatter) {
        
        _floatFormatter = [[NSNumberFormatter alloc] init];
        
        [_floatFormatter allowsFloats];
        [_floatFormatter setLocale:[NSLocale currentLocale]];
        [_floatFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [_floatFormatter setAlwaysShowsDecimalSeparator:NO];
        
        NSInteger precision = 4;
        
        [_floatFormatter setMaximumFractionDigits:precision];
        [_floatFormatter setMinimumFractionDigits:precision];
    }
    
    return _floatFormatter;
}

@end
