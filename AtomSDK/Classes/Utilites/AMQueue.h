//
//  GIQueue.h
//  MEinfoCXConnector
//
//  Created by denis svinarchuk on 21.03.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Queue object.
 */
@interface AMQueue : NSObject
/**
 *  Queue size.
 */
@property(readonly) NSUInteger size;

/**
 *  Max queue depth. Forward мessages will be delete if max size is achieved.
 */
@property(assign) NSUInteger maxSize;

/**
 *  Get object placed at index.
 *
 *  @param index index
 *
 *  @return object.
 */
- (id) objectAtIndex:(NSUInteger) index;

/**
 *  Pop next object and delete.
 *  
 *  @return  object.
 */
- (id) pop;

/**
 *  Push new object.
 *  
 *  @param object.
 */
- (void) push: (id)object;

/**
 *  Clear queue.
 */
- (void) flush;

/**
 *  Get last.
 *
 *  @return object.
 */
- (id) lastObject;

///**
// *  Set max queue depth. Forward мessages will be delete if max size is achieved.
// *  
// *  @param size max size.
// */
//- (void) setMaxSize: (NSUInteger) size;

/**
 *  Get current queue depth.
 *  
 *  @return queue depth.
 */
- (NSUInteger) maxSize;
@end
