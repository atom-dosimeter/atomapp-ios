//
//  AMTagSettings.m
//  Atom
//
//  Created by denis svinarchuk on 23.08.14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMTagDevicePreferences.h"

@interface AMTagDevicePreferences()
@property (readonly,assign) uint8_t flags;
@end

@implementation AMTagDevicePreferences

- (instancetype) copyWithZone:(NSZone *)zone{
    AMTagDevicePreferences *p  = [super copyWithZone:zone];
    p->_hasAlarmOnImpulse      = p.hasAlarmOnImpulse;
    p->_hasAlarmOnThresholdAll = p.hasAlarmOnThresholdAll;
    p->_hasAlarmOnThresholdDosePriority1 = self.hasAlarmOnThresholdDosePriority1;
    p->_hasAlarmOnThresholdDosePriority2 = self.hasAlarmOnThresholdDosePriority2;
    p->_hasAlarmOnThresholdDosePriority3 = self.hasAlarmOnThresholdDosePriority3;
    p->_soundImpulseDuration      = self.soundImpulseDuration;
    p->_soundOnImpulseFrequency   = self.soundOnImpulseFrequency;
    p->_vibrateOnImpulseDuration  = self.vibrateOnImpulseDuration;
    return p;
}

- (void) readData:(NSData *)data{
    
    [super readData:data];
    
    AMDeviceSettingsData *input_data = (AMDeviceSettingsData *)[data bytes];
        
    memcpy(&_soundImpulseDuration, input_data->parameter1, sizeof(input_data->parameter1));
    _soundImpulseDuration = CFSwapInt16LittleToHost(_soundImpulseDuration);
    
    memcpy(&_soundOnImpulseFrequency, input_data->parameter2, sizeof(input_data->parameter2));
    _soundOnImpulseFrequency = CFSwapInt16LittleToHost(_soundOnImpulseFrequency);
    
    memcpy(&_vibrateOnImpulseDuration, input_data->parameter3, sizeof(input_data->parameter3));
    _vibrateOnImpulseDuration = CFSwapInt16LittleToHost(_vibrateOnImpulseDuration);
    
    super.flags = input_data->flags;
    _hasAlarmOnThresholdDosePriority1= !((input_data->flags&(0x01))!=0);
    _hasAlarmOnThresholdDosePriority2= !((input_data->flags&(0x01<<1))!=0);
    _hasAlarmOnThresholdDosePriority3= !((input_data->flags&(0x01<<2))!=0);
    _hasAlarmOnThresholdAll = !((input_data->flags&(0x01<<3))!=0);
    _hasAlarmOnImpulse = !((input_data->flags&(0x01<<6))!=0);
    _hasAlarmsOnAll= !((input_data->flags&(0x01<<7))!=0);
}

- (NSString*) description{
    
    return [NSString stringWithFormat: @"\n\
            flags                  = %x\n\
            priority1 on           = %i\n\
            priority1 on           = %i\n\
            priority1 on           = %i\n\
            all priority on        = %i\n\
            all alarms on          = %i\n\
            all alarms on impulse  = %i\n\
            command                = %x\n\
            soundDuration          = %u\n\
            soundFrequency         = %u\n\
            vibroDuration          = %u\n\
            parameter1             = %x\n\
            parameter2             = %x\n\
            parameter3             = %x\n",
            self.flags,
            self.hasAlarmOnThresholdDosePriority1,
            self.hasAlarmOnThresholdDosePriority2,
            self.hasAlarmOnThresholdDosePriority3,
            self.hasAlarmOnThresholdAll,
            self.hasAlarmOnImpulse,
            self.hasAlarmsOnAll,
            self.command,
            self.soundImpulseDuration,
            self.soundOnImpulseFrequency,
            self.vibrateOnImpulseDuration,
            self.parameter1,
            self.parameter2,
            self.parameter3
            ];
}

@end
