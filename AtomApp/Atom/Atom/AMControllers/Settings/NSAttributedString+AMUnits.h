//
//  NSAttributedString+AMUnits.h
//  Atom
//
//  Created by denis svinarchuk on 20.07.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <Foundation/Foundation.h>

#define AM_UNIT_PERHOUR NSLocalizedString(@"/h", @"Per hour")

@interface NSAttributedString (AMUnits)
+ (NSAttributedString*) durationStringFromStartTime:(NSTimeInterval)starttime withBaseFontSize:(CGFloat)fontSize;
+ (NSAttributedString*) stringWithDoseInCurrentUnit:(float)dose withBaseFontSize:(CGFloat)fontSize;
+ (NSAttributedString*) stringWithDoseRateInCurrentUnit:(float)dose withBaseFontSize:(CGFloat)fontSize;
+ (NSAttributedString*) coloredStringWithDoseInCurrentUnit:(float)doseRate withBaseFontSize:(CGFloat)fontSize;
+ (NSAttributedString*) coloredStringWithDose:(float)doseRate withBaseFontSize:(CGFloat)fontSize;
+ (NSAttributedString*) coloredStringWithDoseInCurrentUnit:(float)doseRate withThresholdDoseRate:(NSNumber*)thresholdDoseRate withBaseFontSize:(CGFloat)fontSize;
+ (NSAttributedString*) attributedString:(NSString*)string withExtention:(NSString*)extention withBaseFontSize:(CGFloat)fontSize;
+ (NSAttributedString*) dash;
+ (NSAttributedString*) empty;
@end
