//
//  AMSignalProcessor.m
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 26/06/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMConstants.h"
#import "AMSignalProcessor.h"
#import "AEFloatConverter.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import <Accelerate/Accelerate.h>


@interface AMSignalProcessor()
@property (nonatomic,assign) float highSignalAmplitude;
@property (atomic,assign)     BOOL isProcessing;
@end

@implementation AMSignalProcessor
{
    
    /// Audio Graph and Input/Output Units
    AudioUnit microphoneInput;
    AudioStreamBasicDescription streamFormat;
    
    /// Audio converter buffer Buffers
    float           **floatBuffers;
    
    /// To avoid accumulating error computation we use buffer has constant length.
    AudioBufferList *microphoneInputBuffer;
    
    /// Input samples buffer (microphione) converter. It produces output buffers with samples sutable for processing.
    
    AEFloatConverter *samplesCoverter;
    
    /// Fourier frequancies transform uses to accelerate osx/ios engine
    BOOL          isFftSetup;
    FFTSetup      fftSetup;
    float         *fftWindow;
    vDSP_Length   fftBufferSize;
    COMPLEX_SPLIT fftA;
    
    NSInteger lowPowerAlarmCount;
    
    BOOL   lowPowerSignalDetected;
    UInt64 peaksAfterLastLowPower;
    UInt64 frameCount, prevLowPowerFrame;
}

- (id) init{
    self = [super init];
    
    if (self) {
        
        _samplesGroupLength = kAMsamplesGroupLength;
        _highSignalAmplitude = kAMHighInputAmplitude;
        _lowPassAmplitude  = kAMLowPassAmplitude;
        _lowPowerSignalAmplitude = kAMLowPowerSignalAmplitude;
        _peakSignalAmplitude = kAMPeakSignalAmplitude;
        _lowPowerFalseAlarmCount = kAMLowPowerFalseAlarmCount;
        _powerStartUpTime = kAMDosimeterStartUpTime;
        
        peaksAfterLastLowPower = 0;
    }
    
    return self;
}

- (void) start{
    
    NSAssert(self.delegate, @"AMSignalProcessor: AMSignalProcessor has not defined delegate object...");
    NSAssert([self.delegate respondsToSelector:@selector(amSignalProcessor:hasLocalPeaks:)], @"AMSignalProcessor: AMSignalProcessor has not defined - processor:(AMSignalProcessor*)processor hasLocalPeaks:(NSInteger)count delegate");
    NSAssert([self.delegate respondsToSelector:@selector(amSignalProcessor:hasLowPower:)], @"AMSignalProcessor: AMSignalProcessor has not defined - processor:(AMSignalProcessor*)processor hasLowPower:(BOOL)lowePower delegate");
    
    NSLog(@"AMSignalProcessor.... %i",self.isProcessing);

    if( !self.isProcessing ){
        
        [self createInputUnit];
        
        // Start fetching input
        [self checkError:AudioOutputUnitStart(microphoneInput)
               operation:@"Microphone failed to start fetching audio"];
        
        _isProcessing = YES;
    }
}

- (void) stop{
    if( self.isProcessing ){
        
        _isProcessing = NO;
        
        [self checkError:AudioOutputUnitStop(microphoneInput)
               operation:@"Microphone failed to stop fetching audio"];
        
        [self destroyUnitInput];
    }
}


- (void) setuptFFT:(UInt32)bufferSize{
    
    fftBufferSize = log2f(bufferSize);
    fftSetup = vDSP_create_fftsetup(fftBufferSize, FFT_RADIX2);
    fftWindow = malloc((sizeof(float) * bufferSize));
    vDSP_hamm_window(fftWindow, bufferSize, 0);
    
    int nOver2 = bufferSize/2;
    
    fftA.realp = (float*) malloc(nOver2*sizeof(float));
    fftA.imagp = (float*) malloc(nOver2*sizeof(float));
}

- (void) deallocFft{
    if (isFftSetup) {
        vDSP_destroy_fftsetup(fftSetup);
        fftSetup = NULL;
        free(fftWindow);
        fftWindow = NULL;
        
        free(fftA.imagp);
        free(fftA.realp);
    }
    isFftSetup = NO;
}

static float renderLowPowerSpectrum(AMSignalProcessor *processor, float* samples, UInt32 numSamples){
    
    if (! processor->_isProcessing)  return 0.0;
    
    int   fft_samples = numSamples/2;
    
    vDSP_vmul(samples, 1, processor->fftWindow, 1, samples, 1, fft_samples);
    
    COMPLEX_SPLIT A=processor->fftA;
    
    // Pack samples:
    // C(re) -> A[n], C(im) -> A[n+1]
    vDSP_ctoz((COMPLEX*)samples, 2, &A, 1, fft_samples);
    
    //Perform a forward FFT using fftSetup and A
    //Results are returned in A
    vDSP_fft_zrip(processor->fftSetup, &A, 1, processor->fftBufferSize, FFT_FORWARD);
    
    // The output signal is now in a split real form. Use the vDSP_ztoc to get
    // a split real vector.
    // vDSP_ztoc(&A, 1, (COMPLEX *)outputBuffer, 2, fft_samples);
    
    Float32 freq_step = processor->_deviceSampleRate/(Float32)(numSamples);
    Float32 freq=freq_step;
    Float32 spectrum_amplitude = 0.0;
    float   volume = processor.volume;
    for (int i=0;
         
         i<numSamples
         &&
         freq<=kAMFFTHighFrequency
         &&
         freq<=kAMLowPowerFrequencyMax;
         
         i++, freq=freq+freq_step) {
        
        if (freq>=kAMFFTLowFrequency && freq>=kAMLowPowerFrequencyMin){
            //
            // detect spectrum fro 40-150Hz inpulses only
            //
            spectrum_amplitude += sqrtf(A.realp[i]*A.realp[i]+A.imagp[i]*A.imagp[i])/volume;            
        }
    }
    
    float absolute_impulse_amplitude = spectrum_amplitude/fft_samples;
    
    if  (absolute_impulse_amplitude>=kAMLowPowerAmplitudeMinLevel && absolute_impulse_amplitude<=kAMLowPowerAmplitudeMaxLevel){
        
        if (processor->lowPowerAlarmCount>=processor->_lowPowerFalseAlarmCount) {
            
            processor->lowPowerAlarmCount=0;
            
            if (!(processor->lowPowerSignalDetected)) {
                
                [processor.delegate amSignalProcessor:processor hasLowPower:YES];
                processor->lowPowerSignalDetected = YES;
                processor->peaksAfterLastLowPower=0;
            }
        }
        else
            processor->lowPowerAlarmCount++;
    }
    else
        return 0.0;
    
    return absolute_impulse_amplitude;
}

//static void movingAvg(float* x, UInt32 length, int window){
//    float avg = 0;
//    for(int i = 0; i!=length;i++){
//        avg = 0;
//        for(int j=0; j!= window; j++){
//            if( (i+j) < length) {
//                avg += x[i + j];
//            }
//        }
//        x[i] = (avg / window);
//    }
//}


static OSStatus renderCallback (
                                void *							inRefCon,
                                AudioUnitRenderActionFlags*	    ioActionFlags,
                                const AudioTimeStamp *			inTimeStamp,
                                UInt32							inBusNumber,
                                UInt32							inNumberFrames,
                                AudioBufferList *				ioData) {
    
	AMSignalProcessor *processor = (__bridge AMSignalProcessor*) inRefCon;
    if (! processor->_isProcessing)  return 0.0;
    
    OSStatus      result     = noErr;
    // Render audio into buffer
    result = AudioUnitRender(processor->microphoneInput,
                             ioActionFlags,
                             inTimeStamp,
                             inBusNumber,
                             inNumberFrames,
                             processor->microphoneInputBuffer);
    
    if( !result ){
        
        if (!(processor->isFftSetup)) {
            [processor setuptFFT:inNumberFrames];
            processor->isFftSetup = YES;
        }
        
        AEFloatConverterToFloat(processor->samplesCoverter,
                                processor->microphoneInputBuffer,
                                processor->floatBuffers,
                                inNumberFrames);
        
        Float32 *buffer = (Float32 *) processor->floatBuffers[0];
        int peaks = 0;
        
        //
        // Normalize frame
        //
        
        int   current_samples_count = 0;
        
        float highAmp = processor->_highSignalAmplitude;
        float lowpass = processor->_lowPassAmplitude;
        float peakAmp = processor->_peakSignalAmplitude;
        
        for (int frame = 0, prev_frame=-1; frame < inNumberFrames; ++frame){
            //Float32 sample = fabsf(buffer[frame]);
            Float32 sample = buffer[frame];
            
            // normilize signal
            //if ( sample > highAmp) {
            //    buffer[frame] = sample = 1.0;
            //}
            //else {
            //    buffer[frame] = sample = sample/highAmp;
            //}
            
            // filtering low-pass trash
            //if (sample < lowpass) {
            //   buffer[frame] = sample = 0.0;
            //}
            
            if (sample>=peakAmp) {
                current_samples_count++;
                if (prev_frame+1==frame) {
                    // find one impulse
                    if (peaks==0) {
                        peaks = 1;
                    }
                }
                else{
                    // find next impulse
                    peaks++;
                    current_samples_count = 0;
                }
                prev_frame = frame;
            }
        }
        
        //
        // Filtering by average
        //
        // movingAvg(buffer, inNumberFrames, kAMMovingAvgFilterWindow);
        
        if (peaks>0 && current_samples_count<=kAMDetectorImpulsDuration) {
            if (processor->peaksAfterLastLowPower==0) {
                if (processor->lowPowerSignalDetected) {
                    //
                    // TODO: in case low power and peaks should we do something?
                    //
                }
            }
            processor->_peakCount+=peaks;
            processor->peaksAfterLastLowPower+=peaks;
            [processor.delegate amSignalProcessor:processor hasLocalPeaks:peaks];
        }
        else {
            float spectrum = renderLowPowerSpectrum(processor, processor->floatBuffers[0], inNumberFrames);
            if (processor->lowPowerSignalDetected && spectrum <= 0.1) {
                //
                // we need some delay to detect low power has gone, i.e. low power already is detected and spectrum the next frame packet has not any low power impulses
                // time beetwen check when spectrum from low power with 0 impulses indicate low power ~ powerStartUpTime (sec.)
                //
                
                float theta = processor->_powerStartUpTime;
                float delta = (processor->frameCount-processor->prevLowPowerFrame)*inNumberFrames/kAMSampleRate/kAMLowPowerFalseAlarmCount;
                
                //printf("AMSignalProcessor: LOW POWER SIGNAL NOOOT DETECTED %llu  prev=%llu theta = %f  delta = %f  spectrum=%f!\n", processor->frameCount, processor->prevLowPowerFrame, theta, delta, spectrum);
                
                if (delta>theta) {
                    //
                    // low power impulses has stoped
                    //
                    [processor.delegate amSignalProcessor:processor hasLowPower:NO];
                    processor->lowPowerSignalDetected = NO;
                }
            }
            if (spectrum>0) {
                processor->prevLowPowerFrame=processor->frameCount;
            }
        }
        
        
    }
    processor->frameCount++;
    return noErr;
}


#pragma mark - Configure The Input Unit

-(void) createInputUnit {
    
    // Get component description for input
    AudioComponentDescription inputComponentDescription = [self getInputAudioComponentDescription];
    
    // Get the input component
    AudioComponent inputComponent = [self getInputComponentWithAudioComponentDescription:inputComponentDescription];
    
    // Create a new instance of the component and store it for internal use
    [self createNewInstanceForInputComponent:inputComponent withUnit: &microphoneInput];
    
    // Enable Input Scope
    [self enableInputScopeForInput:microphoneInput];
    
    // Disable Output Scope
    [self disableOutputScopeForinput:microphoneInput];
    
    // Get the default device if we need to (OSX only, iOS uses RemoteIO)
    [self configureDefaultDeviceForInput:microphoneInput];
    
    // Configure device and pull hardware specific sampling rate (default = 44.1 kHz)
    _deviceSampleRate = [self configureDeviceSampleRateWithDefault:kAMSampleRate];
    
    // Configure device and pull hardware specific buffer duration (default = 0.0232)
    _deviceBufferDuration = [self configureDeviceBufferDurationWithDefault:kAMDeviceBufferDurationWithDefault];
    
    // Configure the stream format with the hardware sample rate
    [self _configureStreamFormatWithSampleRate:_deviceSampleRate];
    
    // Notify delegate the audio stream basic description was successfully created
    //[self _notifyDelegateOfStreamFormat];
    
    // Get buffer frame size
    _deviceBufferFrameSize = [self getBufferFrameSizeForUnit:microphoneInput];
    
    // Create the audio buffer list and pre-malloc the buffers in the list
    [self _configureAudioBufferListWithFrameSize:_deviceBufferFrameSize];
    
    // Setup input callback
    [self _configureInputCallback];
    
    // Disable buffer allocation (optional - do this if we want to pass in our own)
    [self _disableCallbackBufferAllocation];
    
    // Set the sample converter's stream format
    [self _configureFloatConverterWithFrameSize:_deviceBufferFrameSize];
    
    // Initialize the audio unit
    [self checkError:AudioUnitInitialize( microphoneInput )
           operation:@"Couldn't initialize the input unit"];
    
}

- (void) destroyUnitInput{
    
    AudioUnitUninitialize(microphoneInput);
    AudioComponentInstanceDispose(microphoneInput);
    
    [self deallocFft];
    [self deallocFloatBuffers];
    [self deallocStreamBuffer];
}

- (AudioStreamBasicDescription)canonicalFormatWithSampleRate:(float)sampleRate
{
    AudioStreamBasicDescription asbd;
    UInt32 byteSize = sizeof(AudioUnitSampleType);
    asbd.mBitsPerChannel   = 8 * byteSize;
    asbd.mBytesPerFrame    = byteSize;
    asbd.mBytesPerPacket   = byteSize;
    asbd.mChannelsPerFrame = 2;
    asbd.mFormatFlags      = kAudioFormatFlagsCanonical | kAudioFormatFlagIsNonInterleaved;
    asbd.mFormatID         = kAudioFormatLinearPCM;
    asbd.mFramesPerPacket  = 1;
    asbd.mSampleRate       = sampleRate;
    return asbd;
}

#pragma mark - Stream Format Initialization
-(void)_configureStreamFormatWithSampleRate:(Float64)sampleRate {
    // Set the stream format
    
    streamFormat = [self canonicalFormatWithSampleRate:sampleRate];
    
    UInt32 propSize = sizeof(streamFormat);
    // Set the stream format for output on the microphone's input scope
    [self checkError:AudioUnitSetProperty(microphoneInput,
                                          kAudioUnitProperty_StreamFormat,
                                          kAudioUnitScope_Input,
                                          kAMAudioJackOutputBus,
                                          &streamFormat,
                                          propSize)
           operation:@"Could not set microphone's stream format bus 0"];
    
    // Set the stream format for the input on the microphone's output scope
    [self checkError:AudioUnitSetProperty(microphoneInput,
                                          kAudioUnitProperty_StreamFormat,
                                          kAudioUnitScope_Output,
                                          kAMAudioJackInputBus,
                                          &streamFormat,
                                          propSize)
           operation:@"Could not set microphone's stream format bus 1"];
}

-(void)_configureAudioBufferListWithFrameSize:(UInt32)bufferFrameSize {
    UInt32 bufferSizeBytes = bufferFrameSize * streamFormat.mBytesPerFrame;
    UInt32 propSize = offsetof( AudioBufferList, mBuffers[0] ) + ( sizeof( AudioBuffer ) *streamFormat.mChannelsPerFrame );
    microphoneInputBuffer                 = (AudioBufferList*)malloc(propSize);
    microphoneInputBuffer->mNumberBuffers = streamFormat.mChannelsPerFrame;
    for( UInt32 i = 0; i < microphoneInputBuffer->mNumberBuffers; i++ ){
        microphoneInputBuffer->mBuffers[i].mNumberChannels = streamFormat.mChannelsPerFrame;
        microphoneInputBuffer->mBuffers[i].mDataByteSize   = bufferSizeBytes;
        microphoneInputBuffer->mBuffers[i].mData           = malloc(bufferSizeBytes);
        memset(microphoneInputBuffer->mBuffers[i].mData, 0, bufferFrameSize);
    }
}

#pragma mark - Input Callback Initialization
-(void)_configureInputCallback {
    AURenderCallbackStruct microphoneCallbackStruct;
    microphoneCallbackStruct.inputProc       = renderCallback;
    microphoneCallbackStruct.inputProcRefCon = (__bridge void *)self;
    [self checkError:AudioUnitSetProperty(microphoneInput,
                                          kAudioOutputUnitProperty_SetInputCallback,
                                          kAudioUnitScope_Global,
                                          // output bus for mac
#if TARGET_OS_IPHONE
                                          kAMAudioJackInputBus,
#elif TARGET_OS_MAC
                                          kAMAudioJackOutputBus,
#endif
                                          &microphoneCallbackStruct,
                                          sizeof(microphoneCallbackStruct))
           operation:@"Couldn't set input callback"];
}


-(void)_disableCallbackBufferAllocation {
    [self checkError:AudioUnitSetProperty(microphoneInput,
                                          kAudioUnitProperty_ShouldAllocateBuffer,
                                          kAudioUnitScope_Output,
                                          kAMAudioJackInputBus,
                                          &kAMAudioJackDisableFlag,
                                          sizeof(kAMAudioJackDisableFlag))
           operation:@"Could not disable audio unit allocating its own buffers"];
}


#pragma mark - Samples Converter Initialization
-(void)_configureFloatConverterWithFrameSize:(UInt32)bufferFrameSize {
    UInt32 bufferSizeBytes = bufferFrameSize * streamFormat.mBytesPerFrame;
    samplesCoverter              = [[AEFloatConverter alloc] initWithSourceFormat:streamFormat];
    floatBuffers           = (float**)malloc(sizeof(float*)*streamFormat.mChannelsPerFrame);
    NSAssert(floatBuffers!=nil, @"Could not allocate converter memory.");
    for ( int i=0; i<streamFormat.mChannelsPerFrame; i++ ) {
        floatBuffers[i] = (float*)malloc(bufferSizeBytes);
        memset(floatBuffers[i], 0, bufferSizeBytes);
        NSAssert(floatBuffers[i]!=nil, @"Could not allocate converter cell memory.");
    }
}

- (void) deallocStreamBuffer{
    if (microphoneInputBuffer!=nil) {
        for( UInt32 i = 0; i < microphoneInputBuffer->mNumberBuffers; i++ ){
            free(microphoneInputBuffer->mBuffers[i].mData);
            microphoneInputBuffer->mBuffers[i].mData = NULL;
        }
        free(microphoneInputBuffer);
        microphoneInputBuffer = NULL;
    }
}


- (void) deallocFloatBuffers{
    if (floatBuffers!=nil) {
        for ( int i=0; i<streamFormat.mChannelsPerFrame; i++ ) {
            free(floatBuffers[i]);
        }
        free(floatBuffers);
        floatBuffers = NULL;
    }
}

- (void) dealloc{
    [self destroyUnitInput];
}

@end
