//
//  AMTagNotification.h
//  Atom
//
//  Created by denis svinarchuk on 23.08.14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMBLEProtocol.h"

@class AMTagDeviceMeasurement;

@interface AMTagDeviceCalibrationConstants : NSObject<AMBLEProtocol>

/**
 *  Geiger counter sensetivity is measured in impulses/µR.
 */
@property (readonly,nonatomic) float sensetivity;  // impulses/µR
/**
 *  Geiger counter own background is measured in impulses/sec..
 */
@property (readonly,nonatomic) float ownBackground; // impulses/sec.
/**
 *  Geiger counter dead time is measured in m.seconds
 */
@property (readonly,nonatomic) float deadTime; // ms;

/**
 *  The time interval dose rate recomputing.
 */
@property (readonly,nonatomic) uint16_t measurementDoseRateTime; // must be even, default = 34

@end


@interface AMDeviceInitialMeasurement : NSObject <AMBLEProtocol>
/**
 * Total measured detector impulses has been detected for the all time while device is switched on.
 *
 */
@property (readonly,nonatomic) uint64_t impulsesCount;
@property (readonly,nonatomic) uint32_t impulsesCountSpeed;
@property (readonly,nonatomic) uint32_t impulsesCountLastNSeconds;
@property (readonly,nonatomic) uint32_t doseMeasuringTime;
@property (readonly,nonatomic) uint32_t doseMeasuringStartTime;

- (void) updateMeasurement: (AMTagDeviceMeasurement*)measurement;

@end

/**
 * Additional measurement is the structure which is required to initiate measurement and computing. 
 */
@interface AMTagDeviceInitialMeasurement : AMDeviceInitialMeasurement

@end


/**
 * Additional measurement is the structure which is required to initiate measurement and computing Swift/Fast devices.
 */
@interface AMSwiftDeviceInitialMeasurement : AMDeviceInitialMeasurement

@property (readonly,nonatomic) uint16_t fastImpulsesCount;
@property (readonly,nonatomic) uint16_t mediumImpulsesCount;
@property (readonly,nonatomic) uint16_t slowImpulsesCount;
@property (readonly,nonatomic) uint8_t  firmWareDay;
@property (readonly,nonatomic) uint8_t  firmWareMonth;
@property (readonly,nonatomic) uint32_t firmWareYear;
@property (readonly,nonatomic) uint8_t  firmWareHour;

- (void) updateMeasurement: (AMTagDeviceMeasurement*)measurement;

@end


/**
 *  Atom Tag Notification service characteristic.
 */
@interface AMTagDeviceMeasurement : NSObject <AMBLEProtocol>

/**
 *  Create Notification object from raw data.
 *
 *  @param data raw data represents the notification.
 *
 *  @return instance.
 *
 */
+ (id) notificationWithData:(NSData *)data withCalibration:(AMTagDeviceCalibrationConstants*)calibrations withInitial:(AMDeviceInitialMeasurement*)inital;

/**
 *  Create notification object from raw data.
 *
 *  @param data raw data represents characteristic prifile.
 *
 *  @return object instance.
 *
 */
- (id) initWithData:(NSData*) data withCalibration:(AMTagDeviceCalibrationConstants*)calibrations withInitial:(AMDeviceInitialMeasurement*)inital;

- (void) updateInitialMeasurement:(AMDeviceInitialMeasurement*)initial;

/**
 *  Threshold dose or dose rate alert flag. YES means threshold is reached at the moment.
 *
 */
@property (readonly,nonatomic) BOOL doseRateAlert;

/**
 * Resettable. YES means that dose rate threshold was detected by atom device but it has not be reset by central device, value keeps as long as confirmation before accepting by central comes.
 * Flag must be reset by settings charcteristic.
 *
 * @see AmTagSettings.
 *
 */
@property (readonly,nonatomic) BOOL doseRateDidAlert;

/**
 * Indicate if the detector has dead time duration longer then 90%. App should show allert that values cannot be valid.
 */
@property (readonly,nonatomic) BOOL detectorOverload;

/**
 * Indicate if the charger is connected with Atom device.
 */
@property (readonly,nonatomic) BOOL chargerPlugged;

/*
 * Indicate if the Atom device was switched off according to one of the follow event: 
 *  - lower battery volume
 *  - battery overload volume
 *  - launch error (device hardware problem)
 */
@property (readonly,nonatomic) BOOL safetyShutdown;

/**
 * Cumulative dose. nSv.
 */
@property (readonly,nonatomic) Float32 cumulativeDose;

/**
 *  Dose rate. nSv/h
 */
@property (readonly,nonatomic) Float32 doseRate;

/**
 *  Particles which have been detected for the last seconds, by default == 2.
 */
@property (readonly,nonatomic) uint16_t particlesLastSeconds;

/**
 *  Duration particles detection.
 */
@property (readonly,nonatomic) NSTimeInterval particlesLastDuration;

/**
 *  Particles which have been detected for the time when from device switched on or reset the total particles counter.
 */
@property (readonly,nonatomic) uint64_t particlesTotal;

/**
 *  Battery volume equivalents 0-100%.
 */
@property (readonly,nonatomic) float    batteryVolume;

/**
 *  Temperature absolute value.
 */
@property (readonly,nonatomic) float    temperature;

/**
 *  Dose measuring time.
 *  Unit: seconds.
 */
@property (readonly,nonatomic) NSTimeInterval measuringTime;   // seconds

@property (nonatomic, assign) int searchingMode;

@property (nonatomic, readonly) BOOL isPreferencesChanged;

@end
