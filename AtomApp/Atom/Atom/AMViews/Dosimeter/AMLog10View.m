//
//  AMLog10View.m
//  Atom
//
//  Created by denis svinarchuk on 21/07/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMLog10View.h"
#import "AMUIPreferences.h"

@implementation AMLog10View

- (float) minLow{
    return AM_MINIMUM_THERMOMETER_DOSERATE; 
}

- (float) maxHigh{
    return AM_MAXIMUM_THERMOMETER_DOSERATE;
}

- (float) low{
    if (_low<=0.) {
        _low = self.minLow;
    }
    return _low;
}

- (float) high{
    if (_high<=0.) {
        _high = self.maxHigh; 
    }
    
    return _high;
}

- (float) log10f:(float)value{
    return ( (log10f(value) - log10f(self.low)) / (log10f(self.high) - log10f(self.low)));
}

- (float) valueLog10:(float)value10{
    return value10*(log10f(self.high) - log10f(self.low))+log10f(self.low);
}

- (float) valueFromLog10:(float)value10{
    return powf(10,[self valueLog10:value10]);
}

- (float) log10Value{
    return [self log10f:_value];
}

@end
