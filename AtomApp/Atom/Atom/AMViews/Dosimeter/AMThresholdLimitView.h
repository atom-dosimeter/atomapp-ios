//
//  AMViewThresholdView.h
//  Atom
//
//  Created by denis svinarchuk on 20.07.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMThresholdLimitView : UIView
@property (nonatomic,assign) float limit;
@property (nonatomic,assign) float current;
@end
