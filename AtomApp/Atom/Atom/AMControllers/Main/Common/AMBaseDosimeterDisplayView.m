//
//  AMBaseDosimeterDisplayView.m
//  Atom
//
//  Created by denis svinarchuk on 16/10/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMBaseDosimeterDisplayView.h"
#import "AMUIPreferences.h"
#import "NSAttributedString+AMUnits.h"
#import "NSString+AMUtils.h"
#import "AMSettings.h"

#import <AtomMeasurerSDK/AMDoseUnit.h>

@interface AMBaseDosimeterDisplayView()
@property (strong, nonatomic) AMDigitDisplayView      *displayMountView;
@property (strong, nonatomic) AMView                  *particlesMountView;
@property (strong, nonatomic) UIView                  *progressMountView;
@property (strong, nonatomic) UIProgressView          *progressView;
@property (strong, nonatomic) UIActivityIndicatorView *activityView;

@property (strong,nonatomic) UILabel *displayTitleLable;
@property (strong,nonatomic) UILabel *displayUintLable;
@property (strong,nonatomic) UILabel *displaySigmaLable;
@property (strong,nonatomic) UILabel *displayValueLable;

@property (strong,nonatomic) UILabel *particlesTitleLabel;
@property (strong,nonatomic) UILabel *particlesValueLabel;


@property (nonatomic,readonly) AMDoseUnit          *unit;
@property (nonatomic,readonly) AMSettings          *settings;

@property (nonatomic,strong) NSAttributedString *dash;
@property (nonatomic,strong) NSAttributedString *empty;

@property (nonatomic,strong) NSAttributedString *noConnectionString;

@end

@implementation AMBaseDosimeterDisplayView
{
    NSArray *displayMountViewConstraints;
}

@synthesize chartView=_chartView, unit=_unit, settings=_settings;

- (id) initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    [self __init__];
    return self;
}

- (id) init{
    self = [super init];
    [self __init__];
    return self;
}

- (AMDoseUnit*) unit{
    if (!_unit) {
        _unit = [AMDoseUnit sharedInstance];
    }
    
    return _unit;
}

- (AMSettings*) settings{
    if (!_settings) {
        _settings = [AMSettings sharedInstance];
    }
    return _settings;
}

- (void) __init__{
    
    //
    // create views
    //
    [self progressMountView];
    [self progressView];
    [self displayMountView];
    [self chartView];
    
    //
    // localize
    //
    self.displayTitleLable.text = NSLocalizedString(@"Dose rate display", @"Dose rate display title label");
    
    //
    // initial values
    //
    self.currentUnit = [self.unit current];
    self.sigma = -1.0;
    self.doseRate = -1.0;
    
    //
    // reset
    //
    self.progress = 0.0;
}

- (void) setProgress:(float)progress{
    [self.progressView setProgress:progress animated:YES];
}

- (float) progress{
    return self.progressView.progress;
}

- (NSAttributedString*) dash{
    return [NSAttributedString dash];
}

- (NSAttributedString*) empty{
    return [NSAttributedString empty];
}

- (UILabel*) newLabel{
    UILabel *label = [[UILabel alloc] init];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.adjustsFontSizeToFitWidth = YES;
    label.minimumScaleFactor = 0.5;
    label.font = [UIFont fontWithName:AM_UI_DISPLAY_FONT size:17];
    label.textColor = AM_UI_DISPLAY_TEXT_COLOR;
    return label;
}

- (NSAttributedString*) noConnectionString{
    if (!_noConnectionString) {
        
        NSString *localized = NSLocalizedString(@"Check connection with your ATOM dosimeter", @"");
        NSMutableAttributedString *str=[[NSMutableAttributedString alloc] initWithString: localized];
        [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:AM_UI_FONT size:16.] range:NSMakeRange(0, str.length)];
        [str addAttribute:NSForegroundColorAttributeName  value:AM_UI_DER_LOWCOLOR range:NSMakeRange(0, str.length)];
        NSRange atomrange =[localized rangeOfString:@"ATOM"];
        [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:AM_UI_BOLD_FONT size:20.] range:atomrange];
        
        _noConnectionString = str;
    }
    return _noConnectionString;
}

- (void) setDoseRate:(float)doseRate{
    _doseRate = doseRate;
    self.displayValueLable.attributedText = [self valueString];
}

- (void) setSigma:(float)sigma{
    _sigma = sigma;
    if (!self.sigmaIsHidden) {
        self.displaySigmaLable.text =  [self sigmaValueLabel];        
    }
}

- (void) setCurrentUnit:(AMDoseUnitType)currentUnit{
    _currentUnit=currentUnit;
    self.displayUintLable.text = [NSString stringWithFormat:@"%@%@",[self.unit currentSymbol], AM_UNIT_PERHOUR];
}

- (NSAttributedString*) valueString{
    
    if (!self.hasConnection) {
        self.displayValueLable.numberOfLines=4;
        [self stopProgress];
        return self.noConnectionString;
    }
    
    float doseRate=self.doseRate;
    
    if (doseRate<=0.) {
        return self.empty;
    }
    
    self.displayValueLable.numberOfLines=1;
    
    if (doseRate<=0.) {
        doseRate = 0.0;
    }
    
    int fontSize = self.displayValueLable.bounds.size.height*0.6f; fontSize = MIN(AM_UI_DER_FONT_SIZE, fontSize);
    
    return [NSAttributedString coloredStringWithDose:doseRate withBaseFontSize:fontSize];
}

- (UILabel*) displayValueLable{
    if (!_displayValueLable) {
        
        _displayValueLable = [self newLabel];
        _displayValueLable.textAlignment = NSTextAlignmentCenter;
        _displayValueLable.numberOfLines=1;
        _displayValueLable.backgroundColor = [UIColor clearColor];
        
        [self.displayMountView addSubview:_displayValueLable];
        
        [_displayValueLable.superview addConstraints:[NSLayoutConstraint
                                                      constraintsWithVisualFormat:@"V:|-5-[view]-5-|"
                                                      //                                                      constraintsWithVisualFormat:@"V:[title]-2-[view]-2-[sigma]"
                                                      options:0
                                                      metrics:nil
                                                      views:@{@"view":_displayValueLable, @"title": self.displayTitleLable, @"sigma":self.displaySigmaLable}]];
        [_displayValueLable.superview addConstraints:[NSLayoutConstraint
                                                      constraintsWithVisualFormat:@"H:|-5-[view]-5-|"
                                                      options:0
                                                      metrics:nil
                                                      views:@{@"view":_displayValueLable}]];
    }
    return _displayValueLable;
}


- (UILabel*) displayTitleLable{
    if (!_displayTitleLable) {
        _displayTitleLable = [self newLabel];
        
        [self.displayMountView addSubview:_displayTitleLable];
        
        [_displayTitleLable.superview addConstraints:[NSLayoutConstraint
                                                      constraintsWithVisualFormat:@"V:|-2-[view(fontsize)]"
                                                      options:0
                                                      metrics:@{@"fontsize": [NSNumber numberWithFloat:_displayTitleLable.font.pointSize+1.]}
                                                      views:@{@"view":_displayTitleLable}]];
        [_displayTitleLable.superview addConstraints:[NSLayoutConstraint
                                                      constraintsWithVisualFormat:@"H:|-5-[view]-5-[unit]"
                                                      options:0
                                                      metrics:nil
                                                      views:@{@"view":_displayTitleLable, @"unit": self.displayUintLable}]];
    }
    return _displayTitleLable;
}

- (UILabel*) displayUintLable{
    if (!_displayUintLable) {
        _displayUintLable = [self newLabel];
        _displayUintLable.font = [UIFont fontWithName:AM_UI_FONT size:17];
        _displayUintLable.textAlignment = NSTextAlignmentRight;
        
        [self.displayMountView addSubview:_displayUintLable];
        
        [_displayUintLable.superview addConstraints:[NSLayoutConstraint
                                                     constraintsWithVisualFormat:@"V:|-2-[view(fontsize)]"
                                                     options:0
                                                     metrics:@{@"fontsize": [NSNumber numberWithFloat:_displayUintLable.font.pointSize+1.]}
                                                     views:@{@"view":_displayUintLable}]];
        [_displayUintLable.superview addConstraints:[NSLayoutConstraint
                                                     constraintsWithVisualFormat:@"H:[view]-5-|"
                                                     options:0
                                                     metrics:nil
                                                     views:@{@"view":_displayUintLable}]];
    }
    return _displayUintLable;
}

- (UILabel*) displaySigmaLable{
    if (!_displaySigmaLable) {
        _displaySigmaLable = [self newLabel];
        _displaySigmaLable.font = [UIFont fontWithName:AM_UI_FONT size:17];
        _displaySigmaLable.textAlignment = NSTextAlignmentRight;
        
        [self.displayMountView addSubview:_displaySigmaLable];
        
        [_displaySigmaLable.superview addConstraints:[NSLayoutConstraint
                                                      constraintsWithVisualFormat:@"V:[view(fontsize)]-2-|"
                                                      options:0
                                                      metrics:@{@"fontsize": [NSNumber numberWithFloat:_displaySigmaLable.font.pointSize+1.]}
                                                      views:@{@"view":_displaySigmaLable}]];
        [_displaySigmaLable.superview addConstraints:[NSLayoutConstraint
                                                      constraintsWithVisualFormat:@"H:[view]-5-|"
                                                      options:0
                                                      metrics:nil
                                                      views:@{@"view":_displaySigmaLable}]];
        
        _displaySigmaLable.hidden = self.sigmaIsHidden;
    }
    return _displaySigmaLable;
}

- (void) setSigmaIsHidden:(BOOL)sigmaIsHidden{
    _sigmaIsHidden = sigmaIsHidden;
    self.displaySigmaLable.hidden = _sigmaIsHidden;
}

- (NSString*) sigmaValueLabel{
    
    float sigma = self.sigma;
    
    if (sigma<0.)
        return self.dash.string;
    
    //
    // real sigma
    //
    
    sigma=sigma*self.settings.trustFactor.floatValue;
    
    if (sigma>=100.0) {
        return self.dash.string;
    }
    
    static NSNumberFormatter *percentLessThen0 = nil ;
    if (percentLessThen0 == nil) {
        percentLessThen0 = [self.unit formatterWithPrecision:2];
        percentLessThen0.numberStyle = NSNumberFormatterPercentStyle;
    }
    
    NSNumberFormatter *formatter = percentLessThen0;
    if (sigma>5) {
        formatter = self.unit.percentFormatter;
    }
    
    return [NSString stringWithFormat:@"±%@", [formatter stringFromNumber: [NSNumber numberWithFloat:sigma/100.]]];
}

- (AMChartView*) chartView{
    if (!_chartView) {
        _chartView = [[AMChartView alloc] initWithFrame:self.bounds];
        _chartView.translatesAutoresizingMaskIntoConstraints = NO;
        _chartView.backgroundColor = [UIColor clearColor];
        
        [self.displayMountView insertSubview:_chartView atIndex:0];
        
        [_chartView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view]-2-|" options:0 metrics:nil views:@{@"view":_chartView}]];
        [_chartView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":_chartView}]];
    }
    
    return _chartView;
}

- (UIProgressView*) progressView{
    if (!_progressView) {
        _progressView = [[UIProgressView alloc] init];
        _progressView.translatesAutoresizingMaskIntoConstraints = NO;
        _progressView.progressTintColor = AM_UI_DER_LOWCOLOR;
        _progressView.trackTintColor = [UIColor clearColor];
        
        [self.progressMountView addSubview:_progressView];
        
        [_progressView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[view]-0-|" options:0 metrics:nil views:@{@"view":_progressView}]];
        [_progressView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":_progressView}]];
        
    }
    return _progressView;
}

- (UIView*) progressMountView{
    if (!_progressMountView) {
        _progressMountView = [[UIView alloc] init];
        _progressMountView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_progressMountView];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":_progressMountView}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":_progressMountView}]];
    }
    [_progressMountView.superview bringSubviewToFront:_progressMountView];
    return _progressMountView;
}

- (AMDigitDisplayView*) displayMountView{
    if (!_displayMountView) {
        _displayMountView = [[AMDigitDisplayView alloc] init];
        _displayMountView.backgroundColor = AM_UI_DISPLAY_BG_COLOR;
        _displayMountView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self insertSubview:_displayMountView atIndex:0];
        
        [self addConstraints:displayMountViewConstraints=[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":_displayMountView}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":_displayMountView}]];
    }
    return _displayMountView;
}


- (void) setHasParticlesDisplay:(BOOL)hasParticlesDisplay{
    _hasParticlesDisplay = hasParticlesDisplay;
    if (_hasParticlesDisplay) {
        [self particlesMountView];
    }
    else{
        [self removeConstraints:displayMountViewConstraints];
        [self.particlesMountView removeFromSuperview];
        self.particlesMountView = nil;
        [self addConstraints:displayMountViewConstraints=[NSLayoutConstraint
                                                          constraintsWithVisualFormat:@"V:|-0-[view]-0-|"
                                                          options:0
                                                          metrics:nil
                                                          views:@{@"view":_displayMountView}]];
    }
}

- (UIView*) particlesMountView{
    if (!self.hasParticlesDisplay) {
        return nil;
    }
    if (!_particlesMountView) {
        
        if (self.displayMountView) {
            [self removeConstraints:displayMountViewConstraints];
        }
        
        _particlesMountView = [[AMView alloc] init];
        _particlesMountView.translatesAutoresizingMaskIntoConstraints = NO;
        _particlesMountView.backgroundColor = AM_UI_DISPLAY_BG_COLOR;
        
        [self insertSubview:_particlesMountView belowSubview:self.progressMountView];
        
        NSNumber *height = [NSNumber numberWithInt:((UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)?88:50)];
        
        [self addConstraints:displayMountViewConstraints=[NSLayoutConstraint
                                                          constraintsWithVisualFormat:@"V:|-0-[view]-5-[particles(height)]-0-|"
                                                          options:0
                                                          metrics:@{@"height":height}
                                                          views:@{@"view":self.displayMountView, @"particles":self.particlesMountView}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":_particlesMountView}]];
        
        
        self.particlesTitleLabel.text = NSLocalizedString(@"Particles display", @"Particle numebr display title label");
        self.particlesValueLabel.text = @"-";
    }
    return _particlesMountView;
}


- (void) setParticles:(uint64_t)particles{
    _particles = particles;
    self.particlesValueLabel.text = [self.unit localeStringFromInt:_particles];
}

- (UILabel*) particlesValueLabel{
    if (!_particlesValueLabel) {
        
        _particlesValueLabel = [self newLabel];
        _particlesValueLabel.textAlignment = NSTextAlignmentCenter;
        _particlesValueLabel.font = [UIFont fontWithName:AM_UI_BOLD_FONT size:AM_UI_INFO_FONT_VALUE_SIZE*1.3];
        _particlesValueLabel.adjustsFontSizeToFitWidth = YES;
        _particlesValueLabel.minimumScaleFactor = 0.5;
        
        [self.particlesMountView addSubview:_particlesValueLabel];
        
        [_particlesValueLabel.superview addConstraints:[NSLayoutConstraint
                                                        constraintsWithVisualFormat:@"V:[title]-(-4)-[view]-5-|"
                                                        options:0
                                                        metrics:nil
                                                        views:@{@"view":_particlesValueLabel, @"title": self.particlesTitleLabel}]];
        [_particlesValueLabel.superview addConstraints:[NSLayoutConstraint
                                                        constraintsWithVisualFormat:@"H:|-5-[view]-5-|"
                                                        options:0
                                                        metrics:nil
                                                        views:@{@"view":_particlesValueLabel}]];
    }
    return _particlesValueLabel;
}


- (UILabel*) particlesTitleLabel{
    if (!_particlesTitleLabel) {
        _particlesTitleLabel = [self newLabel];
        
        [self.particlesMountView addSubview:_particlesTitleLabel];
        _particlesValueLabel.font = [UIFont fontWithName:AM_UI_FONT size:AM_UI_INFO_FONT_TITLE_SIZE];
        
        [_particlesTitleLabel.superview addConstraints:[NSLayoutConstraint
                                                        constraintsWithVisualFormat:@"V:|-2-[view(fontsize)]"
                                                        options:0
                                                        metrics:@{@"fontsize": [NSNumber numberWithFloat:_particlesTitleLabel.font.pointSize]}
                                                        views:@{@"view":_particlesTitleLabel}]];
        [_particlesTitleLabel.superview addConstraints:[NSLayoutConstraint
                                                        constraintsWithVisualFormat:@"H:|-5-[view]-5-|"
                                                        options:0
                                                        metrics:nil
                                                        views:@{@"view":_particlesTitleLabel}]];
    }
    return _particlesTitleLabel;
}


- (UIActivityIndicatorView*) activityView{
    if (!_activityView) {
        _activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _activityView.color = AM_UI_DER_LOWCOLOR;
        _activityView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.progressMountView addSubview:_activityView];
        
        [_activityView.superview addConstraint:[NSLayoutConstraint constraintWithItem:self.progressMountView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:_activityView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
        float c=0;
        if (self.hasParticlesDisplay) {
            c=self.particlesMountView.bounds.size.height/2.;
        }
        [_activityView.superview addConstraint:[NSLayoutConstraint constraintWithItem:self.progressMountView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:_activityView attribute:NSLayoutAttributeCenterY multiplier:1 constant:c]];
    }
    return _activityView;
}

- (void) startProgress{    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!self.inProgress && self.hasConnection) {
            self.inProgress = YES;
            self.progressMountView.alpha = 0.0;
            [self.activityView startAnimating];
            [UIView animateWithDuration:AM_UI_ANIMATE_DURATION
                             animations:^{
                                 self.progressMountView.alpha = 1.0;
                                 self.displayMountView.alpha = 0.5;
                             }];
        }
    });
}

- (void) stopProgress{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.inProgress) {
            self.inProgress = NO;
            [UIView animateWithDuration:AM_UI_ANIMATE_DURATION
                             animations:^{
                                 self.progressMountView.alpha = 0.0;
                                 self.displayMountView.alpha = 1.0;
                             }
                             completion:^(BOOL finished) {
                                 [self.activityView stopAnimating];
                             }];
        }
    });
}

@end
