//
//  AMAudioManager.h
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 09/09/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//
//
// Thanks a lot for code examples and ideas: https://github.com/alexbw/novocaine
//

#import "AMErrorHandlerProtocol.h"

#import <CoreFoundation/CoreFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <Accelerate/Accelerate.h>
#import <Block.h>

#if TARGET_OS_IPHONE
#import <AVFoundation/AVFoundation.h>
#elif TARGET_OS_MAC
#import <CoreAudio/CoreAudio.h>
#endif

static const int            kAMAudioBufferSize    = 8192;
static const AudioUnitScope kAMAudioJackInputBus  = 1;
static const AudioUnitScope kAMAudioJackOutputBus = 0;
static const int            kAMAudioDefaultDevice = 999999;

typedef struct {
    float   **buffer;
    UInt32  numberOfChannels;
    UInt32  numberOfFramesPerChannel;
    UInt32  bufferSizePerChannel;
}AMAudioManagerBuffer;

@class AMAudioManager;

/**
 *  Audio manager delegate DSP to the block.
 *
 *  @param data        samples buffer
 *  @param numFrames   number of sample frames
 *  @param numChannels number of channels
 *
 */
typedef void (^AMAudioDSPBlock)(AMAudioManager *manager, AMAudioManagerBuffer *ioData);

/**
 *  The object of the class manage audio input/output samples stream. It saves me to write a lot of Core Audio code lines.
 *
 */
@interface AMAudioManager : NSObject

/**
 *  Audio error handler.
 *
 */
@property(strong,nonatomic) id<AMErrorHandlerProtocol> errorHandler;

/**
 *  Input block executes when a samples buffer come to the device input
 *
 */
@property(copy,nonatomic) AMAudioDSPBlock inputBlock;

/**
 *  Output DSP block
 *
 */
@property(copy,nonatomic) AMAudioDSPBlock outputBlock;


/**
 *  The propertie tells that samples is inteleaved by channels.
 *
 */
@property (nonatomic, assign, readonly) BOOL isInterleaved;

/**
 *  Number of input channels
 */
@property (nonatomic, assign, readonly) UInt32 numInputChannels;

/**
 *  Number of output cjannels
 *
 */
@property (nonatomic, assign, readonly) UInt32 numOutputChannels;

/**
 *  Rate of samples.
 */
@property (nonatomic, assign, readonly) Float64 samplingRate;

/**
 *  Number of samples in defaul buffer
 */
@property (nonatomic, assign, readonly) UInt32 inputBufferSize;
@property (nonatomic, assign, readonly) UInt32 outputBufferSize;

@property (atomic, readonly) BOOL isRunningInput;
@property (atomic, readonly) BOOL isRunningOutput;


#if TARGET_OS_IPHONE
#elif TARGET_OS_MAC
@property (nonatomic, readonly)     AudioDeviceID defaultInputDeviceID;
#endif


/**
 *  Only one audio manager can be in progress simultaneously
 *
 *  @return audio manager
 *
 */
+ (id) sharedInstance;

- (void) startOutput;
- (void) stopOutput;

- (void) startInput;
- (void) stopInput;

/**
 *  Declare to create valid autocomplete signature.
 */
- (void)setInputBlock:(AMAudioDSPBlock)block;
- (void)setOutputBlock:(AMAudioDSPBlock)block;

//
//
//
- (void) checkError: (OSStatus) error operation:(NSString*)operation;
@end
