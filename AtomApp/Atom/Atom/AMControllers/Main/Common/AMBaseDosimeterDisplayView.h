//
//  AMBaseDosimeterDisplayView.h
//  Atom
//
//  Created by denis svinarchuk on 16/10/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AMDigitDisplayView.h"
#import "AMChartView.h"

#import <AtomMeasurerSDK/AMDoseUnit.h>

@interface AMBaseDosimeterDisplayView : UIView

@property (nonatomic,assign  ) float              doseRate;
@property (nonatomic,assign  ) float              sigma;
@property (nonatomic,assign  ) BOOL               sigmaIsHidden;
@property (nonatomic,assign  ) uint64_t           particles;
@property (nonatomic,assign  ) BOOL               hasParticlesDisplay;

@property (atomic,assign     ) BOOL               hasConnection;
@property (nonatomic,assign  ) BOOL               inProgress;
@property (nonatomic,assign  ) float              progress;

@property (nonatomic,assign  ) AMDoseUnitType     currentUnit;
@property (nonatomic,readonly) AMChartView        *chartView;

- (void) startProgress;
- (void) stopProgress;

@end
