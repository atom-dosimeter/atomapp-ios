//
//  AMBatteryView.h
//  Atom
//
//  Created by denis svinarchuk on 04/09/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMBatteryView : UIView
@property (nonatomic,strong) UIColor *textColor;
@property (nonatomic,strong) UIColor *borderColor;
@property (nonatomic,assign) CGSize  size;

@property (nonatomic,assign) BOOL    chargerPlugged;
@property (nonatomic,assign) float   powerVolume; // %
@end
