//
//  NSString+AMUtils.h
//  Atom
//
//  Created by denis svinarchuk on 29/07/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (AMUtils)
- (NSString*) clearedString;
- (NSNumber*) floatNumber;
+ (NSString*) durationStringFromStartTime:(NSTimeInterval)starttime;
@end
