package com.troitsk.dosimeter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Arrays;

//import org.acra.ACRA;








import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder.AudioSource;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class Dosimeter {
	
	//DDS:
	/*
	 * f = (deltai / N) * Fs
	 * delta i  = f*N / fs
	 * f = 16000, di = 1486
	 * 
	 * 
	 * delta i = 1, fs = 44100, N = 4096, f = 10.76 Hz
	 * delta i = 2						  f = 21.5 Hz
	 * delta i = 1024					  f = 11025 Hz
	 * delta i = 1025					  f = 11035 Hz
	 * 
	 * 
	 */

    /* Preferences & constants  */
   
	private final float peak_thresh = 1800.0f/32768f; //Пики, которые ниже, чем это отбрасываются
	private final float peak_kf = 0.7f, ratio_thresh = 0.07f; //Если перепад не меньше, чем max_value*peak_kf, то мы нашли пик
	public boolean autotune = true;
	public float K, Nf, tau = 0; //K - CPM на 1 микрорентген/ч; Nf - собственный фон счетчика в CPM; мертвое время в микросекундах
    private final float usvK = 100; //Коэфф пересчета микрорентген в usvh
	private final int read_samplerate = 44100;
	private final int write_samplerate = 44100;
    
    private int low_freq = 11000; //При подборе частоты пляшем от нижней до верхней
    private int up_freq = 16000;
    private int meandr_low_freq = 45, //Нижняя и верхняя частоты меандра, который появляется при малом напряжении питания
                meandr_up_freq = 130,
                fine_callib_step = 100;

    /* DDS */
    private int table_size = 4096;
    private short[] sin_table = new short[table_size];
    private short[] output_s;

    /* misc */
	private boolean callib_on = false;
	private int total_particles = 0;
	private boolean gen_on = false, recorder_on = false;
	private float sigma = 0,  total_time = 0, CPM = 0, urh = 0;
	private int min_output_buff_size, freq = 0, sm_thresh = 0, min_input_buff_size = 0;
	private Results r = new Results();
	private AudioManager mAudioManager;
    private AudioRecord recorder;
    private Thread gen_thread, record_thread, callib_thread;
	private Handler h;

	public static int MEASURE=0, SEARCH_SLOW=1, SEARCH_FAST=2, SEARCH_MEDIUM=3;
	public int mode = MEASURE, new_mode = MEASURE;
	
	public static final int MODE_CALLIB = -1;
	public final static int MSG_MEASURE_COMPLETE = 0;
	public final static int MSG_CALLIB_STARTED = 1;
	public final static int MSG_CALLIB_COMPLETE = 2;
	public final static int MSG_CALLIB_PROGRESS = 3;
	public final static int MSG_CALLIB_FAILED = 4;
	public final static int MSG_FINE_CALLIB_FAIL = 5;
	public final static int MSG_FINE_CALLIB_COMPLETE = 6;
	public final static int MSG_SMODE_MEASURE_COMPLETE = 7;
	public final static int MSG_FINE_CALLIB_STARTED = 8;
    private boolean freq_update = false;
    private boolean change_mode_request = false, reset_request = false;
    private int lpr_delay = 0;
    
	
	public Dosimeter(Handler h, Context appContext){
		this.h = h;
		mAudioManager = (AudioManager) appContext.getSystemService(Context.AUDIO_SERVICE);
		audio_init();
	}
	
	public int getFreq(){
		return freq;
	}
	
	public void audio_init(){
		min_output_buff_size = AudioTrack.getMinBufferSize(write_samplerate, 
															AudioFormat.CHANNEL_OUT_STEREO, 
															AudioFormat.ENCODING_PCM_16BIT);
		min_input_buff_size = AudioRecord.getMinBufferSize(read_samplerate,
				AudioFormat.CHANNEL_IN_MONO,
				AudioFormat.ENCODING_PCM_16BIT);
		//ACRA.getErrorReporter().putCustomData("min_output_buff_size", Integer.toString(min_output_buff_size));
		output_s = new short[min_output_buff_size / 2];
		for(int i=0;i!=table_size;i++){
			sin_table[i] = (short) (32767*Math.sin( i*2f*Math.PI / (float)table_size ));
		}
	}
	
	public void setMode(int  new_mode){
		this.new_mode = new_mode;
		change_mode_request = true;
	}
	
	public void setMaxVolume(){
		mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
	}

	public void setFreq(int freq){
		this.freq = freq;
		freq_update = true;
	}
    
	public String getSamples(short[] buff){
		StringBuilder sb = new StringBuilder();
			sb.append("filtered = [");
			for(int i=0; i!= buff.length; i++){
				if( i < (buff.length-1))
					sb.append(Short.toString(buff[i])+" ");
				else
					sb.append(Short.toString(buff[i]));
		}
        sb.append("];");
        return sb.toString();
	}

	//Методы для работы с циклическим буфером для режима поиска
	private int[] pbuff = new int[400];
	private int idxin = 0, idxout = 0;
	public void pbuff_put(int t){
        pbuff[idxin] = t;
        idxin++;
        if(idxin == pbuff.length){
        	idxin = 0;
        	idxout = pbuff.length-1;
        } else {
        	idxout = idxin-1;
        }
    }

    public int pbuff_get(){
        int y = pbuff[idxout--];
        if(idxout == -1) {
        	idxout = pbuff.length-1;
        }
        return y;
    }

    public void pbuff_reset(){
        idxin = 0;
        idxout = pbuff.length-1;
        for(int i=0;i!=pbuff.length;i++){
            pbuff[i] = 0;
        }
    }

    
    private boolean measure_complete = false, fine_callib_started = false;
    
	Runnable read_thread = new Runnable() {
		
		private int ffcnt = 1, sign = 1, rfreq = 0;
		private int delay = 0; //задержка после подстройки частоты, чтобы мусор не ловить
		private boolean uplimit_exceed = false, downlimit_exceed = false; //достигли конца частотного диапазона - шагаем в одну сторону
		private DSP dsp;
		float[] fir_output;
		
		public int calc_pulses(float[] samples){
				////////////////////////////////////////////////////////////////////////
				///////////////ФИЛЬТРАЦИЯ///////////////////////////////////////////////
				////////////////////////////////////////////////////////////////////////
				//для айфона можно оставить только поиск пиков
				int window_width = 5;
                dsp.moving_avg_s(samples, 10);
                
                int particles = 0;
                float max_value = 0, min_value = 32768;

                float tmp = 1000f;
                for(int i = 0; i!=samples.length;i++){
                    tmp = 1000f;
                    for(int j=0; j!= window_width; j++){
                        if( (i+j) < samples.length) {
                            if(samples[i+j] < tmp){
                                tmp = samples[i+j];
                            }
                        } else {
                            if( 0 < tmp){
                                tmp = 0;
                            }
                        }
                    }
                    samples[i] = tmp;
                    if(tmp > max_value){
                        max_value = tmp;
                    }
                    if(tmp < min_value){
                        min_value = tmp;
                    }
                }

				float temp = 0, mx = -32769, mn = 32768, delta = max_value*peak_kf;
				boolean search_max = true;
				if(min_value < 0){
					if(Math.abs(min_value) > max_value){
						search_max = false;
						delta = Math.abs(min_value)*peak_kf;
					}
				}

				//подсчет импульсов
				boolean lookformax = true;
				
				int mxpos = 0, mnpos = 0;
				for (int i = 0; i != samples.length; i++)
				{
					temp = samples[i];
					if (temp > mx) {
						mx = temp; 
						mxpos = i;
					}
					if (temp < mn) {
						mn = temp; 
						mnpos = i;
					}
					  
					if(lookformax){
					    if(temp < (mx-delta)){
					    	if(search_max == true && mx > peak_thresh){
					    		particles++;
					    	}
					        mn = temp; 
					        mnpos = i;
					        lookformax = false;
					    }
					}
					  else{
					    if(temp > (mn+delta)){
					    	if(search_max == false && Math.abs(mn) > peak_thresh){
					    		particles++;
					    	}
					        mx = temp; 
					        mxpos = i;
					        lookformax = true;
					    }
					 }
				}
				return particles;
		}
		
		
		
		public boolean lpr_detection(float[] samples){
			int i;
	    	float energy_total = 0, energy_m = 0, ratio;
	    	//dsp.firfs(samples, DSP.lpass256, fir_output);
	    	fir_output = dsp.firf(samples, samples.length, DSP.lpass256);
	    	for(i=0;i!=samples.length;i++){
	    		energy_total += samples[i]*samples[i];
	    		energy_m += fir_output[i]*fir_output[i];
	    	}
             
	    	ratio = energy_m / energy_total;
	    	Log.i("ratio", Float.toString(ratio));
	    	
			if( ratio > ratio_thresh) {
				return true;
			} else {
				return false;
			}
		}
		
		//Вариант с децимацией
		public boolean lpr_detection_decim(float[] samples){
			int i;
	    	float energy_total = 0, energy_m = 0, ratio;
	    	float[] filtered = dsp.firf(samples, samples.length, DSP.lpass256);
	    	for(i=0;i!=samples.length;i++){
	    		energy_total += samples[i]*samples[i];
	    		energy_m += fir_output[i]*fir_output[i];
	    	}
             
	    	ratio = energy_m / energy_total;
	    	Log.i("ratio", Float.toString(ratio));
	    	
			if( ratio > ratio_thresh) {
				return true;
			} else {
				return false;
			}
		}
		
		
		private int lpr_cnt = 0;
		@Override
		public void run() {
			android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_MORE_FAVORABLE);
			
			int record_buff_size_in_shorts = 22050;
			short[] data = new short[record_buff_size_in_shorts];
			float[] samples = new float[record_buff_size_in_shorts];
			float[] corr = new float[2048];
			//fir_output = new float[record_buff_size_in_shorts];
			
			dsp = new DSP();
			//dsp.static_firf_init(record_buff_size_in_shorts, DSP.lpass256.length);
            
            boolean low_power = false;
            float measure_interval_s = (float)record_buff_size_in_shorts / read_samplerate;
            boolean last_flag = false;
            
            //Ждем, пока зарядятся конденсаторы в умножителе и только потом начинаем читать данные с АЦП.
            //Это нужно для того, чтобы не возникло ложного запуска подстройки частоты
            try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
            
			recorder = new AudioRecord(AudioSource.MIC, 
					read_samplerate, 
					AudioFormat.CHANNEL_IN_MONO,
					AudioFormat.ENCODING_PCM_16BIT, 
					record_buff_size_in_shorts*2);
			
			recorder.startRecording();
			fine_callib_started = false;
			dsp.freq_measure_init(2205);
			while(recorder_on){
				int particles = 0;
				if(change_mode_request){
					changeMode(new_mode);
					change_mode_request = false;
				}
				
				if(reset_request){
					reset();
					reset_request = false;
				}
				
				recorder.read(data, 0, record_buff_size_in_shorts);
				dsp.short_to_float_s(data, samples, 0, data.length);
				
				//long time = System.nanoTime();

                /////////////////LOW POWER DETECTION//////////////////////
                
				
				//Задержка, чтобы ложных подстроек не было
				if(lpr_delay == 0){
					//low_power = lpr_detection(samples);
					//corr
					//float freq = dsp.measure_freq(samples, 2048, 0.17f);
					//System.arraycopy(samples, 0, corr, 0, corr.length);
					//float freq = dsp.measure_freq_akf(corr, 0.1f);
					if(autotune){ //Если в настройках разрешена автоподстройка
						float freq = dsp.measure_freq_akf_decim(samples, 0.05f);
						Log.i("LPR", Float.toString(freq));
						if(freq > meandr_low_freq && freq <= meandr_up_freq){
							low_power = true;
						} else {
							low_power = false;
						}
					}
					////corr
					
				} else { 
					lpr_delay--; 
				}
				r.low_power = low_power;
				//Log.i("time", Long.toString((System.nanoTime()-time)/1000000) );
				///////////////АВТОПОДСТРОЙКА ЧАСТОТЫ///////////////////////////////////
				
                if( low_power == false ){
                	if(delay-- > 0) continue; 
                	
                	particles = calc_pulses(samples); //для айфона можно удалить всё, кроме рассчета МЭД и этой строки
                    total_time += measure_interval_s;
                	total_particles += particles;
                	
                	if(last_flag == true){ //Предыдущий флаг указывал на нехватку питания. Сейчас питания хватает, калибровка успешна.
                		last_flag = false;
                		reset();
                		h.sendMessage(h.obtainMessage(MSG_FINE_CALLIB_COMPLETE, r));
                		fine_callib_started = false;
                		Log.i("lowpower", "fine_callib_started = false");
                	}
                	
                	ffcnt = 1;
                	uplimit_exceed = false;
                	downlimit_exceed = false;
                	lpr_cnt = 0;
                	fine_callib_step=50;
                } 
                else if(lpr_cnt > 5){
                	last_flag = true;
                	if(fine_callib_started == false){
                		//Оповещаем UI поток о том, что Подстройка запущена
                		h.sendMessage(h.obtainMessage(MSG_FINE_CALLIB_STARTED, r));
                		fine_callib_started = true;
                		Log.i("lowpower", "fine_callib_started = true");
                	}
                	Log.i("freqs", "Подстройка частоты:" + Integer.toString(rfreq));
                	//Достигли конца диапазона частот - идем в противоположенную сторону
                	if(rfreq == low_freq && !downlimit_exceed) {
                		sign = 1;
                		rfreq = freq + fine_callib_step*ffcnt*sign;
                		setFreq(rfreq);
                		downlimit_exceed = true;
                		continue;
                	} 
                	if(rfreq == up_freq && !uplimit_exceed) {
                		sign = -1;
                		rfreq = freq + fine_callib_step*ffcnt*sign;
                		setFreq(rfreq);
                		uplimit_exceed = true;
                		continue;
                	}
                	//Если не достигли, то перебираем частоты по такому алгоритму: 12500,12400,12600,12300,12700...
                	if(!uplimit_exceed && !downlimit_exceed){
	                		rfreq = freq + fine_callib_step*ffcnt*sign;
	                		if(sign > 0){
	                    		sign = -1;
	                    	} else {
	                    		sign = 1;
                    	     }
                	} else {
                		if(uplimit_exceed && downlimit_exceed){
                			h.sendMessage(h.obtainMessage(MSG_FINE_CALLIB_FAIL));
                			recorder_on = false;
                			gen_on = false;
                			recorder.stop();
                			return;
                		}
                		//Достигли предела - идем в одну сторону до упора
                		rfreq = freq + fine_callib_step*sign;
                	}
                	
                	setFreq(rfreq);
                	
                	ffcnt++;
                	delay = 1;
                	continue;
                } else {
                	lpr_cnt++;
                }
				
 
                
               
 
				/////////////////РАССЧЕТ МЭД////////////////////////////////////////////
                
                
                int res_particles = 0;
                float CPS = 0;

		        if(mode == MEASURE){
                    res_particles = total_particles;
		        }

                r.timeh = (int) (total_time / 3600f) % 24;
                r.timem = (int) (total_time / 60f) % 60;
                r.times = (int) (total_time % 60);
                
		        float sm_time = 0;
		        //Включен режим поиска
		        if(mode != MEASURE){
                    total_time = measure_interval_s;
                    res_particles = 0;
                    pbuff_put(particles);
                    sm_time = 0;
                    int sum = 0;
                    if(particles <= sm_thresh) {
                        //Мало частиц, суммируем предыдущие
                        for (int i = 0; i != pbuff.length-1; i++) {
                        	int tmp1 = pbuff_get();
                            sum += tmp1;
                            sm_time +=  measure_interval_s;
                            if(sum > sm_thresh){
                                total_time = sm_time;
                                res_particles = sum;
                                break;
                            }
                        }
                    } else {
                        //Прилетело сколько надо частиц, рассчитываем за эту секунду
                        total_time = measure_interval_s;
                        res_particles = particles;
                    }
                    if(res_particles <= sm_thresh){
                        res_particles = 0;
                    } 
		        }

                if(res_particles == 0){
                    sigma = -1; //sigma равна бесконечности, рисуем вместо неё пропуск
                } else {
                    sigma = (float) (100f / Math.sqrt((float)res_particles));
                }

                CPS =  ((float)res_particles) / total_time;
                if(CPS  > 1000){
                    CPS = CPS / (1.0f - CPS*tau);
                }

                CPM = CPS * 60f - Nf;
                urh = CPM * 60f / K;

		        if(urh < 0) urh = 0;
		        if(CPM < 0) CPM = 0;

		        r.usvh = urh / usvK;
		        r.sigma = sigma;
		        r.urh =  urh;
		        r.CPM =  CPM;
		        r.sum = res_particles;
		       
		        if(recorder_on){
		        	h.sendMessage(h.obtainMessage(MSG_MEASURE_COMPLETE, r));
		        }
		        if(!measure_complete && (mode == SEARCH_FAST 
		        						|| mode == SEARCH_SLOW
		        						|| mode == SEARCH_MEDIUM)  && r.urh > 0){
		        	h.sendMessage(h.obtainMessage(MSG_SMODE_MEASURE_COMPLETE, r));
		        	measure_complete = true;
		        }
			}	
			recorder.stop();
			recorder.release();
		}
	};	
	
	private Runnable play_thread  = new Runnable() {
        private int phase_acc = 0, ftw = 0, phase_top = table_size-1;
        @Override
		public void run() {
			AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
					write_samplerate,
					AudioFormat.CHANNEL_OUT_STEREO,
					AudioFormat.ENCODING_PCM_16BIT,
					min_output_buff_size,
					AudioTrack.MODE_STREAM);
			phase_acc = 0;
			audioTrack.play();
			while(gen_on){
				if(freq_update){
					ftw = Math.round((float)freq*table_size/(float)write_samplerate);
					freq_update = false;
				}
				for(int i=0;i!=output_s.length;i+=2){
					if(phase_acc > phase_top){
						phase_acc -= table_size;
					}
					output_s[i] = (short) (sin_table[phase_acc]);
					output_s[i+1] = (short) -output_s[i];
					phase_acc += ftw;
				}
				
				audioTrack.write(output_s, 0, output_s.length);
			}
			audioTrack.stop();
			audioTrack.release();
		}
	};

	private Runnable callib_t  = new Runnable() {
		
		private float freq_change_delay_s = 0.6f;
        private DSP dsp = new DSP();
        //private AudioRecord _recorder;
        private int bsize_in_shorts = min_input_buff_size/2; //в short
        private int bsize_in_bytes = min_input_buff_size; //в байтах
        	
        public boolean lpr_detection(float[] samples){
			int i;
	    	float energy_total = 0, energy_m = 0, ratio;
	    	//dsp.firfs(samples, DSP.lpass256, fir_output);
	    	float[] fir_output = dsp.firf(samples, samples.length, DSP.lpass256);
	    	for(i=0;i!=samples.length;i++){
	    		energy_total += samples[i]*samples[i];
	    		energy_m += fir_output[i]*fir_output[i];
	    	}
             
	    	ratio = energy_m / energy_total;
	    	
			if( ratio > ratio_thresh) {
				return true;
			} else {
				return false;
			}
		}
        
		public int scan_freq(int start_freq, int stop_freq, int step) {
					
			int count = 1 + Math.abs( (start_freq - stop_freq) / step);
			int[] freq_arr = new int[count];
			
			for(int i=0;i!=count; i++){
				freq_arr[i] = start_freq + i*step;
			}
				
			/*
			float frame_time = (float)bsize_in_shorts/(float)read_samplerate;
			int itcnt = Math.round(freq_change_delay_s / frame_time);
			if(itcnt == 0) {
				itcnt = 1;
			}
			*/
			
			int[] frequences = new int[count];
			short[] temp = new short[bsize_in_shorts];	
			float[] frame = new float[bsize_in_shorts];
			//Измеряем частоту сигнала на входе
			setFreq(start_freq);
			for(int i=0; i!=count;i++){
				if(!callib_on) return 0;
				//Задержка между чтениями
				try {
					Thread.sleep((long) (freq_change_delay_s*1000));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				recorder.read(temp, 0, temp.length);
				recorder.read(temp, 0, temp.length);
				
				
				//for(int k=0;k!=itcnt;k++){
				//	recorder.read(temp, 0, temp.length);
				//}
				
				dsp.short_to_float_s(temp, frame, 0, frame.length);
				//CORR detection
				//float _freq = dsp.measure_freq(frame, 2048, 0.15f);
				//float _freq = dsp.measure_freq_akf(frame, 0.0f);
				float _freq = dsp.measure_freq(frame, 0.05f);
				Log.i("freq", Float.toString(_freq) + "  " + Float.toString(freq) );
				if(_freq > meandr_low_freq && _freq <= meandr_up_freq){
					frequences[i] = 60;
				} else {
					frequences[i] = 1000;
				}
				
				/*
				if(lpr_detection(frame)){
					frequences[i] = 60;
				} else {
					frequences[i] = 1000;
				}
				*/
				//Log.i( "freqs", Integer.toString(freq_arr[i]) + "   " + Integer.toString(frequences[i]) );
				setFreq(freq+step);
			}
		
			//for(int i=0;i!=frequences.length;i++){
			//	Log.i("# # #", Integer.toString(frequences[i]));
			//}
			
			//Ищем середину интервала, в котором питание в норме
			int pcnt = 0, maxlen = -100, sindex = 0;
			for(int i = 0; i != frequences.length; i++){
                //good freq
				if( frequences[i] != 60 ){
					pcnt++;
				} 
				//bad freq
				if( frequences[i] == 60 ){
					if(pcnt > maxlen){
						maxlen = pcnt;
						sindex = i-pcnt;
					}
					pcnt = 0;
				}
				
				if( i == (frequences.length-1) ){
					if(pcnt > maxlen){
						maxlen = pcnt;
						sindex = (i+1)-pcnt;
					}
					pcnt = 0;
				}
			}
			
			int freq = 13800;
			if(maxlen > 1){
				freq = freq_arr[sindex] + (freq_arr[sindex+maxlen-1] - freq_arr[sindex]) / 2;
			} 
			
			if(maxlen == 1){
				freq = freq_arr[sindex];
			}

			if(maxlen == 0){
				return -1;
			} else { 
				return freq;
			}
		}
		
		@Override
		public void run() {
			Message msg = h.obtainMessage(MSG_CALLIB_STARTED, null);
			h.sendMessage(msg);
			recorder_stop();
			
			int min_input_buff_size = AudioRecord.getMinBufferSize(read_samplerate,
					AudioFormat.CHANNEL_IN_MONO,
					AudioFormat.ENCODING_PCM_16BIT);
			
			bsize_in_shorts = min_input_buff_size/2;
			
			int[] steps =      {1000,   500,   100,   50};
			/*
			freq_start - с этой частоты начинаем шагать с шагом step
			freq_stop - заканчиваем тут
			Каждый столбец - частоты для соответствующего шага из steps
			 */
			int[] freq_start = {low_freq, 12500, 12500, 12500,
								0, 14500, 14500, 14500, 
								0,       low_freq, low_freq, low_freq};
			
			int[] freq_stop =  {up_freq, 14000, 14000, 14000,
                                0,      up_freq, up_freq, up_freq,
								0,       12000, 12000, 12000};
			
			int size = up_freq - low_freq;
			
			if(recorder != null){
				recorder.release();
			}
			recorder = new AudioRecord(AudioSource.MIC, 
					read_samplerate, 
					AudioFormat.CHANNEL_IN_MONO,
					AudioFormat.ENCODING_PCM_16BIT, 
					min_input_buff_size);
	
            setMaxVolume();
            setFreq(low_freq);
			gen_start();
            try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
            
            recorder.startRecording();

			int f = -1,i;
			for(i=0; i!= steps.length;i++){
				if(!callib_on) break;
				h.sendMessage(h.obtainMessage(MSG_CALLIB_PROGRESS, (int) ((size/steps[i])*freq_change_delay_s)));
				if(i < 1)
				{
					//Для одного интервала
					f = scan_freq(freq_start[i], freq_stop[i], steps[i]);
					if(f != -1){
						break;
					}
					//Для двух интервалов
					/*
					f = scan_freq(freq_start[i], freq_stop[i], steps[i]);
					if(f  == -1){
						f = scan_freq(freq_start[i+steps.length], freq_stop[i+steps.length], steps[i]);
						if(f != -1){
							break;
						}
					} else { 
						break; 
					}
					*/
				} 
				else 
				{
					f = scan_freq(freq_start[i], freq_stop[i], steps[i]);
					if(f  == -1){
						f = scan_freq(freq_start[i+steps.length], freq_stop[i+steps.length], steps[i]);
						if(f == -1){
							f = scan_freq(freq_start[i+steps.length*2], freq_stop[i+steps.length*2], steps[i]);
						} else { break; }
					} else { 
						break; 
					}
					
				}
			}
			
			recorder.stop();
			recorder.release();
			
			if(f != -1){
				setFreq(f);
				mode = MEASURE;
				if(i == 3){ //Если калибровка произведена с шагом в 50 Гц
					fine_callib_step = 50;
				} else {
					fine_callib_step = 100;
				}
				reset();
				gen_stop();
				h.sendMessage(h.obtainMessage(MSG_CALLIB_COMPLETE, null));
			} else {
				gen_stop();
				h.sendMessage(h.obtainMessage(MSG_CALLIB_FAILED, null));
			}
		}
	};
	
	public void callib_start(){
		 callib_on = true;
		 callib_thread = new Thread(callib_t);
		 callib_thread.start();
	}
	
	public void callib_stop(){
		callib_on = false;
		try {
			 if(callib_thread != null) {
				 callib_thread.join();
			 }
	      } catch (InterruptedException e) {
	             e.printStackTrace();
	     }
		callib_thread = null;
	}
	
	private void changeMode(int mode){
		if(mode == SEARCH_SLOW){
			sm_thresh = 30;
		}
		if(mode == SEARCH_MEDIUM){
			sm_thresh = 20;
		}
		if(mode == SEARCH_FAST){
			sm_thresh = 12;
		}
		
		this.mode = mode;
		measure_complete = false;
		reset();
	}
	
	public void recorder_start(){
		recorder_stop();
		if(!recorder_on || record_thread == null){
			recorder_on = true;
			record_thread = new Thread(read_thread);
			record_thread.start();  
		}
	}
	
	public void recorder_stop(){
		recorder_on = false;
		try {
			 if(record_thread != null) {
				 record_thread.join();
			 }
	      } catch (InterruptedException e) {
	             e.printStackTrace();
	     }
		record_thread = null;
	}
	
	public void gen_start(){
		gen_stop();
		if(!gen_on || gen_thread == null){
			gen_on = true;
			gen_thread = new Thread(play_thread);
			gen_thread.start();
		}
	}
	
	public void gen_stop(){
		gen_on = false;
		try {
			 if(gen_thread != null) {
				 gen_thread.join();
			 }
	      } catch (InterruptedException e) {
	             e.printStackTrace();
	     }
		gen_thread = null;
	}
	
	public void On(){
		   lpr_delay = 6;
		   setMaxVolume();
		   reset();
		   gen_start();
		   recorder_start();
		   reset();
	}
	
	public void Off(){
		gen_stop();
		recorder_stop();
		callib_stop();
	}
	
	public void reset(){
		reset_request = true;
        pbuff_reset();
        total_particles = 0;
		total_time = 0;
		sigma = 0;
        CPM = 0;
		urh = 0;
	}
	
	
	public int getMode(){
		return mode;
	}
}