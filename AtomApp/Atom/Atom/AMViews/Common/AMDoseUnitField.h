//
//  AMTextUnitField.h
//  Atom
//
//  Created by denis svinarchuk on 02/09/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AtomMeasurerSDK/AMDoseUnit.h>

@class AMDoseUnitField;

@protocol AMDoseUnitFieldDelegate <NSObject>

- (void) amDoseUnitFieldDidEndEditing:(AMDoseUnitField*)field;

@optional
- (void) amDoseUnitFieldWillEdit:(AMDoseUnitField*)field;

@end

@interface AMDoseUnitField :  UIView
@property (readonly,nonatomic) UILabel  *textLabel;
@property (strong,atomic) NSNumber *value;
@property (assign,nonatomic) BOOL isDoseRate;
@property (assign,nonatomic) BOOL isUpper; // to Sv or R
@property (strong,nonatomic) id<AMDoseUnitFieldDelegate> delegate;
@property (nonatomic,assign) NSTextAlignment textAlignment;
@property (nonatomic,assign) float fontPointSize;
@end
