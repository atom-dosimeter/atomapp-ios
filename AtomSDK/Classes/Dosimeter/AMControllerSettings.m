//
//  AMControllerSettings.m
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 29.08.14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMControllerSettings.h"
#import "AMConstants.h"

@implementation AMBoolean
- (id) booleanWithBOOL:(BOOL)value{
    return (AMBoolean*) [AMBoolean numberWithBool:value];
}
@end


@interface AMControllerSettingsMeta()
- (void) __init__:(Class)class;
@end

@implementation AMControllerSettingsMeta

- (id) init{
    self = [super init];
    if (self) {
        _properties = [[NSMutableArray alloc] initWithCapacity:1];
        [self __init__:[self superclass]];
    }
    return self;
}

- (void) __init__:(Class)class{
    unsigned int propertiesCount = 0;
    objc_property_t *properties = class_copyPropertyList(class, &propertiesCount);
    for (int i=0; i<propertiesCount; i++) {
        objc_property_t property = properties[i];
        const char *propName = property_getName(property);        
        NSString *property_name_string = [NSString stringWithCString:propName encoding:NSUTF8StringEncoding];
        [self.properties addObject:property_name_string];
    }
}

@end

@interface AMControllerSettings()
@end

@implementation AMControllerSettings


- (id) initWithUUIDString:(NSString *)uuidstring{
    self = [super init];
    if (self) {
        _UUIDString = uuidstring;
        [self __init__:[self class]];
    }
    return self;
}

- (id) init{
    self = [super init];
    if (self) {
        [self __init__:[self class]];
    }
    return self;
}

- (void) synchronize{}


- (NSInteger) count{
    return self.properties.count;
}

- (id) propertyAtIndex:(NSInteger)index{
    
    //
    // Set defaults
    //
    if (self.properties.count<=index || index<0) {
        return nil;
    }

    const char *property = [self.properties[index] UTF8String];
    
    typedef id (*send_type)(id, SEL); 
    
    send_type func = (send_type)objc_msgSend; 
    return func(self, sel_registerName(property)); 
}

- (NSString*) propertyNameAtIndex:(NSInteger)index{
    if (self.properties.count<=index || index<0) {
        return nil;
    }
    
    return self.properties[index];
}

- (NSString*) propertyLocalizedTitleAtIndex:(NSInteger)index{
    
    //
    // Set defaults
    //
    if (self.properties.count<=index || index<0) {
        return nil;
    }

    NSString *property_name =self.properties[index];
    if ([property_name isEqualToString:@"name"]) {
        return NSLocalizedString(@"Device Name", @"");
    }
    else if ([property_name isEqualToString:@"UUIDString"]) {
        return NSLocalizedString(@"Device UUID", @"");
    }
    else if ([property_name isEqualToString:@"thresholdDose"]) {
        return NSLocalizedString(@"Threshold dose device setting", @"");
    }
    else if ([property_name isEqualToString:@"thresholdDoseRate"]) {
        return NSLocalizedString(@"Threshold dose rate setting", @"");
    }
    
    return nil;
}

- (NSString*) setterNameForProperty:(NSString*)property_name_string{
    return [NSString stringWithFormat:@"set%@%@:",[[property_name_string substringToIndex:1] capitalizedString],[property_name_string substringFromIndex:1]];
}

- (void) setValue:(id)value atPropertyIndex:(NSInteger)index{
    @try {
        if (self.properties.count<=index || index<0) {
            return;
        }
        NSString *property_Name =  [self setterNameForProperty: self.properties[index]];
        
        
        typedef void (*send_type)(id, SEL, id); 
        send_type func = (send_type)objc_msgSend; 
        func(self, sel_registerName([property_Name UTF8String]), value); 
    }
    @catch (NSException *exception) {
        NSLog(@"AMControllerSettings[%@]: %@ %s:%i", self, exception, __FILE__, __LINE__);
    }
} 

@end
