//
//  AMStatusBarView.h
//  Atom
//
//  Created by denis svinarchuk on 12.07.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AMStatusBarView : UIView

@property (nonatomic,assign) BOOL  isConnected;
@property (nonatomic,assign) BOOL  chargerPlugged;
@property (nonatomic,assign) BOOL  hasPowerEnough;
@property (nonatomic,assign) float powerVolume;

@end
