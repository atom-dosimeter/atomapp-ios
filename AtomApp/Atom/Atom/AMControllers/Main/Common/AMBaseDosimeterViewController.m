//
//  AMCommonDosimeterViewController.m
//  Atom
//
//  Created by denis svinarchuk on 16/10/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMBaseDosimeterViewController.h"
#import "AMUIPreferences.h"

@interface AMBaseDosimeterViewController ()
@end

@implementation AMBaseDosimeterViewController
{
    NSLayoutConstraint *topConstraint, *bottomConstraint;
    float topSpaceConstant,bottomSpaceConstant;
}


@synthesize settings = _settings, unit = _unit, screenView=_screenView;


- (void) awakeWithMotion{
    
    topConstraint.constant = -self.view.bounds.size.height;
    bottomConstraint.constant = -self.view.bounds.size.height;
    
    [self.view needsUpdateConstraints];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(AM_UI_AWAKE_DURATION * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:AM_UI_AWAKE_DURATION
                         animations:^{
                             topConstraint.constant = topSpaceConstant;
                             bottomConstraint.constant = bottomSpaceConstant;
                             [self.view layoutIfNeeded];
                         }
                         completion:^(BOOL finished) {
                             
                         }];
    });
}

- (AMTitledView*) titledView{
    return (AMTitledView*)self.view;
}


- (AMController*) defaultController{
    return self.controllerManager.defaultController;
}


- (AMControllerManager*) controllerManager{
    return [AMControllerManager sharedInstance];
}

- (AMSettings*) settings{
    if (!_settings) {
        _settings = [AMSettings sharedInstance];
    }
    return _settings;
}

- (AMDoseUnit*) unit{
    if (!_unit) {
        _unit = [AMDoseUnit sharedInstance];
    }
    
    return _unit;
}

- (AMDosimeter *) currentDosimeter{
    return self.defaultController.dosimeter;
}


- (AMBaseDosimeterScreenView*) screenView{
    if (!_screenView) {
        _screenView = [[AMBaseDosimeterScreenView alloc] initWithFrame:self.view.bounds];
        _screenView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.titledView addSubview:_screenView];        
        [self.titledView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":_screenView}]];
        
        [self.titledView addConstraint:topConstraint=[NSLayoutConstraint constraintWithItem:self.titledView.deviceNameLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_screenView attribute:NSLayoutAttributeTop multiplier:1 constant:0]];
        
        [self.titledView addConstraint:bottomConstraint=[NSLayoutConstraint constraintWithItem:self.titledView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_screenView attribute:NSLayoutAttributeBottom multiplier:1 constant:31]];
        
        topSpaceConstant=topConstraint.constant;
        bottomSpaceConstant=bottomConstraint.constant;
    }
    
    return _screenView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self screenView];
    // Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.titledView.deviceName = self.defaultController.settings.name;
}

@end
