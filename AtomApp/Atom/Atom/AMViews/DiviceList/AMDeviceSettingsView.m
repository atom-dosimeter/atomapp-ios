//
//  AMDeviceSettingsView.m
//  Atom
//
//  Created by denis svinarchuk on 29.08.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMDeviceSettingsView.h"

@implementation AMDeviceSettingsView

- (void) awakeFromNib{
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
    self.title = NSLocalizedString(@"Settings", @"Title of Atom device settings list localized string");
}


@end
