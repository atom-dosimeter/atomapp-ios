//
//  AppDelegate.h
//  AMSignalProcessor-iOS
//
//  Created by denis svinarchuk on 25/06/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
