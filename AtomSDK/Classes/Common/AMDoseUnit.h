//
//  AMDoseUnit.h
//  Atom
//
//  Created by denis svinarchuk on 18.07.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <Foundation/Foundation.h>

#define AM_UNIT_PRECISION 2

static const int kAMDoseUnitNumber = 4;
static const int kAMDoseUnitNumberBegining = 0;

typedef enum {
    AM_DOSE_UNIT_NNSV = kAMDoseUnitNumberBegining,
    AM_DOSE_UNIT_MKSV = 1, 
    AM_DOSE_UNIT_MLSV = 2, 
    AM_DOSE_UNIT_MKRN = 3,
    AM_DOSE_UNIT_SV = 100,
    AM_DOSE_UNIT_RN = 101
} AMDoseUnitType;


/**
 *  Dose unit convertor. 
 *  NOTE: All of internal values use nSvt in base.
 */
@interface AMDoseUnit : NSObject

+ (instancetype) sharedInstance;
+ (float) convertValue:(float)value to:(AMDoseUnitType)unit;
+ (NSString*) unitSymbol:(AMDoseUnitType)unit;
+ (NSString*) unitSymbolForValue:(float*)value withInputValue:(float)input andUnit:(AMDoseUnitType)unit;
- (NSString*) unitSymbolForValue:(float*)value withInputValue:(float)input;

- (AMDoseUnitType) number2Unit:(NSNumber*)number;

@property (nonatomic,assign) AMDoseUnitType current;
@property (nonatomic,readonly) NSString    *currentSymbol;
@property (nonatomic,readonly) NSArray     *availableUnits;
@property (nonatomic,readonly) NSArray     *availableSymbols;
@property (nonatomic,readonly) NSNumberFormatter *percentFormatter;
@property (nonatomic,readonly) NSNumberFormatter *floatFormatter;
@property (nonatomic,readonly) NSNumberFormatter *intFormatter;

- (NSNumberFormatter*) formatterWithPrecision:(NSUInteger)precicion;


- (float) fromValueToBase:(float)value;
- (float) convertFloatToCurrent:(float)value; 
- (NSNumber*) fromNumberToBase:(NSNumber*)value;
- (NSNumber*) convertNumberToCurrent:(NSNumber*)value; 

- (NSString*) localeStringFromFloat:(float)value;
- (NSString*) localeStringFromInt:(UInt64)value;


/**
 *  Convert to upper value if current unit measuraing in Sv convert to Sv, else convert to R.
 *
 *  @param value value in current unit
 *  @param type  return type of unit
 *
 *  @return value in Sv or R.
 *
 */
- (float) fromValueToUpper:(float)value  unit:(AMDoseUnitType*)type;

/**
 *  Convert from upper to base.
 */
- (float) fromUpperValueToBase:(float)value;
- (NSNumber*) fromUpperNumberToBase:(NSNumber*)value;

@end
