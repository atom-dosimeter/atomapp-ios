//
//  AMPickerView.h
//  Atom
//
//  Created by denis svinarchuk on 28/07/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMPickerView : UIPickerView
@property (nonatomic,assign) CGFloat heightRow;
@property (nonatomic,strong) NSArray *selections;
@property (nonatomic,assign) NSInteger selected;
@end
