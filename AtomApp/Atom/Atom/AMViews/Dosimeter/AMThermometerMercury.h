//
//  AMThermometerMercury.h
//  Atom
//
//  Created by denis svinarchuk on 21/07/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMLog10View.h"

@interface AMThermometerMercury : AMLog10View
@property (nonatomic,assign) float sigma;
@property (nonatomic,assign) float trustFactor;
@property (nonatomic,assign) CGRect baseFrame;
@end
