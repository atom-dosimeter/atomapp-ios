//
//  AMControllerManager.m
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 27/08/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMControllerManager.h"
#import "AMSimpleController.h"
#import "AMTagController.h"
#import "AMTagDeviceManager.h"

@interface AMControllerManager() <AMControllerProtocol, AMTagDeviceManagerProtocol, AMDosimeterProtocol, AMErrorHandlerProtocol>
@property (strong,nonatomic) AMList *controllerList;
@property (strong,nonatomic) AMTagDeviceManager *tagDeviceManager;
@property (strong,nonatomic) AMSimpleController *simpleController;
@property (atomic,assign) BOOL isRuning;
@end

static AMControllerManager *__shared_instance = nil;

@implementation AMControllerManager
@synthesize controllerList=_controllerList, defaultController=_defaultController;

+ (id) sharedInstance{
    return [self sharedInstanceWithDelegate:nil];
}


+ (id) sharedInstanceWithDelegate:(id<AMControllerManagerDelegate>)delegate{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __shared_instance = [[AMControllerManager alloc] init];
        if (delegate) {
            __shared_instance.delegate = delegate;
        }
    });
    return __shared_instance;
}


- (id) init{
    
    if (__shared_instance) {
        self=__shared_instance;
        return self;
    }
    
    self = [super init];
    
    
    if (self) {
        __shared_instance = self;
    }
    
    return self;
}

- (void) setDefaultController:(AMController *)defaultController{
    
    if (defaultController==nil) {
        NSLog(@"AMControllerManager: error setting default controller: it cannot be nill");
        return;
    }
    
    _defaultController=defaultController;
    
    [self.delegate amControllerManager:self amController:_defaultController didUpdateConnectionState:_defaultController.connectionState];
    
    if (_defaultController.connectionState) {
        
        [self.delegate amControllerManager:self amController:_defaultController didChangePowerState:_defaultController.powerState];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(amControllerManager:amController:didChangePowerVolume:)]) {
            [self.delegate amControllerManager:self amController:_defaultController didChangePowerVolume:_defaultController.powerVolume];
        }
    }
    
    [self.delegate amControllerManager:self amController:_defaultController didUpdateAmDosimeter:_defaultController.dosimeter];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(amControllerManager:amController:didUpdateReadyState:)]) {
        [self.delegate amControllerManager:self amController:_defaultController didUpdateReadyState:_defaultController.isReady];
    }
}

- (void) setDelegate:(id<AMControllerManagerDelegate>)delegate {
    _delegate = delegate;
    NSAssert([_delegate respondsToSelector:@selector(amControllerManagerDidUpdateControllerList:)], @"AMControllerManager: delegate must implement - amControllerManagerDidUpdateControllerList:");
    NSAssert([_delegate respondsToSelector:@selector(amControllerManager:amController:didUpdateConnectionState:)], @"AMControllerManager: delegate must implement - amControllerManager:amController:didUpdateConnectionState:");
    NSAssert([_delegate respondsToSelector:@selector(amControllerManager:amController:didChangePowerState:)], @"AMControllerManager: delegate must implement - amControllerManager:amController:didChangePowerState:");
    NSAssert([_delegate respondsToSelector:@selector(amControllerManager:amController:didUpdateAmDosimeter:)], @"AMControllerManager: delegate must implement - amControllerManager:amController:didUpdateAmDosimeter:");
}

- (AMList*) controllerList{
    if (!_controllerList) {
        _controllerList = [[AMList alloc] initWithCapacity:1];
    }
    return _controllerList;
}

- (AMTagDeviceManager*) tagDeviceManager{
    if (!_tagDeviceManager) {
        _tagDeviceManager = [AMTagDeviceManager sharedInstance];
        _tagDeviceManager.delegate = self;
    }
    
    return _tagDeviceManager;
}

- (void) start{
    if (self.isRuning) {
        return;
    }
    
    if (!_simpleController) {
        /*
         *  Always we have one atom simple controller in list.
         *
         */
        
        _defaultController = _simpleController = [[AMSimpleController alloc] initWithDelegate:self];
        _simpleController.errorHandler = self;
        _simpleController.dosimeter.delegate = self;
        [self.controllerList setObject:_simpleController forKey:_simpleController.settings.UUIDString];
    }
    
    if (!_defaultController) {
    }
    
    self.isRuning = YES;
    [self.simpleController start];
    [self.tagDeviceManager start];
}

- (void) stop{
    if (self.isRuning == NO) {
        return;
    }
    [self.simpleController stop];
    [self.tagDeviceManager stop];
    self.isRuning = NO;
}

#pragma mark - AMTag Device Manager protocol

- (void) tagDeviceManager:(AMTagDeviceManager *)manager willConnectWithDevice:(AMTagDevice *)device{
    
    if (
        [device.className isEqualToString:AM_DEVICE_SWIFT]
        ||
        [device.className isEqualToString:AM_DEVICE_FAST]
        ||
        [device.className isEqualToString:AM_DEVICE_TAG]
        )
    {
        AMTagController *tagController = [self.controllerList objectForKey:device.UUIDString];
        
        if (!tagController) {
            tagController= [[AMTagController alloc] initWithDelegate:self withTagDevice:device];
            tagController.dosimeter.delegate = self;
            
            [self.controllerList setObject:tagController forKey:tagController.settings.UUIDString];
            [self.delegate amControllerManagerDidUpdateControllerList:self];
        }
    }
}

#pragma mark - AMController Protocol
//
// Connection
//
- (void) tagDeviceManager:(AMTagDeviceManager *)manager didActiveFail:(CBCentralManagerState)cbstate{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(amControllerManager:amController:didFail:)]) {
        
        NSString *message;
        
        switch (cbstate) {
            case CBCentralManagerStatePoweredOff:
                message = NSLocalizedString(@"BLE hardware is powered off", nil);
                break;
            case CBCentralManagerStateResetting:
                message = NSLocalizedString(@"BLE hardware is resetting", nil);
                break;
            case CBCentralManagerStateUnauthorized:
                message = NSLocalizedString(@"BLE state is unauthorized", nil);
                break;
            case CBCentralManagerStateUnknown:
                message = NSLocalizedString(@"BLE state is unknown", nil);
                break;
            case CBCentralManagerStateUnsupported:
            default:
                message = NSLocalizedString(@"BLE hardware is unsupported on this platform", nil);
                break;
        }
        
        NSError *error = [NSError errorWithDomain:@"com.atom.controller.manager"
                                             code:cbstate
                                         userInfo:@{
                                                    NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Bluetooth settings error", nil),
                                                    NSLocalizedDescriptionKey: message
                                                    }];
        
        //[self.tagDeviceManager stop];
        [self.delegate amControllerManager:self amController:nil didFail:error];
    }
}

- (void) tagDeviceManagerDidActive:(AMTagDeviceManager *)manager{
    [self.delegate amControllerManager:self amController:nil didFail:nil];
}

- (void) tagDeviceManagerDidUpdateDeviceList:(AMTagDeviceManager *)manager{
    [self.delegate amControllerManagerDidUpdateControllerList:self];
}

- (void) amController:(AMController *)controller didUpdateConnectionState:(BOOL)available{
    if (available) {
        if ([controller isKindOfClass:[AMSimpleController class]]) {
            [controller start];
        }
    }
    else{
        if ([controller isKindOfClass:[AMSimpleController class]]) {
            [controller stop];
        }
        [controller.dosimeter hardReset];
    }
    [self.delegate amControllerManager:self amController:controller didUpdateConnectionState:available];
}

//
// Ready
//
- (void) amController:(AMController *)controller willReadyAfterTime:(NSTimeInterval)approxTime{
    AMController *simple = [self.controllerList objectForKey:controller.settings.UUIDString];
    if (!simple) {
        [self.controllerList setObject:controller forKey:controller.settings.UUIDString];
        [self.delegate amControllerManagerDidUpdateControllerList:self];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(amControllerManager:amController:willReadyAfterTime:)]) {
        [self.delegate amControllerManager:self amController:controller willReadyAfterTime:approxTime];
    }
}

- (void) amController:(AMController *)controller didReady:(BOOL)ready{
    if (self.delegate && [self.delegate respondsToSelector:@selector(amControllerManager:amController:didUpdateReadyState:)]) {
        [self.delegate amControllerManager:self amController:controller didUpdateReadyState:ready];
    }
}

//
// Power Control
//
- (void) amController:(AMController *)controller didChangePowerState:(AMControllerPowerState)state{
    [self.delegate amControllerManager:self amController:controller didChangePowerState:state];
}

- (void) amController:(AMController *)controller didChangePowerVolume:(float)volume{
    if (self.delegate && [self.delegate respondsToSelector:@selector(amControllerManager:amController:didChangePowerVolume:)]) {
        [self.delegate amControllerManager:self amController:controller didChangePowerVolume:volume];
    }
}


#pragma mark - AMControllerSettings updated

- (void) amController:(AMController *)controller didUpdateSettings:(AMControllerSettings *)settings{
    if (self.delegate && [self.delegate respondsToSelector:@selector(amControllerManager:amController:didUpdateSettings:)]) {
        [self.delegate amControllerManager:self amController:controller didUpdateSettings:settings];
    }
}

- (void) amControllerWillCommitChanges:(AMController *)controller{
    if (self.delegate && [self.delegate respondsToSelector:@selector(amControllerManager:amControllerWillCommitChanges:)]) {
        [self.delegate amControllerManager:self amControllerWillCommitChanges:controller];
    }
}

- (void) amControllerDidCommitChanges:(AMController *)controller{
    if (self.delegate && [self.delegate respondsToSelector:@selector(amControllerManager:amControllerDidCommitChanges:)]) {
        [self.delegate amControllerManager:self amControllerDidCommitChanges:controller];
    }
}

- (void) amController:(AMController *)controller didChangeFromName:(NSString *)fromName toName:(NSString *)toName{
    if (self.delegate && [self.delegate respondsToSelector:@selector(amControllerManager:amController:didChangeFromName:toName:)]) {
        [self.delegate amControllerManager:self amController:controller didChangeFromName:fromName toName:toName];
    }
}

#pragma mark - AMDosimeter protocol
- (void) amDosimeterValuesChanged:(AMDosimeter *)dosimeter{
    AMController *controller = [self.controllerList objectForKey:dosimeter.UUIDString];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(amControllerManager:amController:didUpdateAmDosimeter:)]) {
        [self.delegate amControllerManager:self amController:controller didUpdateAmDosimeter:dosimeter];
    }
}

- (void) amController:(AMController *)controller didDetectParticles:(NSInteger)count{
    if (self.delegate && [self.delegate respondsToSelector:@selector(amControllerManager:amController:didDetectParticles:)]) {
        [self.delegate amControllerManager:self amController:controller didDetectParticles:count];
    }
}

#pragma mark - AMController Warnigs
- (void) amController:(AMController *)controller didWarn:(NSString *)warnString{
    if (self.delegate && [self.delegate respondsToSelector:@selector(amControllerManager:amController:didWarn:)]) {
        [self.delegate amControllerManager:self amController:controller didWarn:warnString];
    }
}

#pragma mark - AMController error handler
- (void) amObject:(id)amobject withError:(NSError *)error{
    AMController *controller = amobject;
    if (self.delegate && [self.delegate respondsToSelector:@selector(amControllerManager:amController:didFail:)]) {
        [self.delegate amControllerManager:self amController:controller didFail:error];
    }
}

@end
