//
//  AMBLEProtocol.h
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 29/09/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CBUUID+UUIDString.h"

#if TARGET_OS_IPHONE
#import <CoreBluetooth/CoreBluetooth.h>
#elif TARGET_OS_MAC
#import <IOBluetooth/IOBluetooth.h>
#endif

#define AM_LITTLE_ENDIAN_FLOAT_TO_HOST(v) ((CFByteOrderGetCurrent()==CFByteOrderLittleEndian?(float)(v):((float)CFSwapInt32BigToHost((v)))))

@protocol AMBLEProtocol <NSObject>
@required

/**
 *  Update object from raw data.
 *
 *  @param data raw data represents characteristic profile.
 *
 *  @return object instance.
 *
 */
- (void) readData: (NSData*)data;

/**
 *  Default uniq characteristic ID of the class object.
 *
 *  @return the uniq identifier.
 *
 */
+ (CBUUID*) identifier;

@optional
/**
 *  Create object from raw data.
 *
 *  @param data raw data represents characteristic profile.
 *
 *  @return object instance.
 *
 */
- (id) initWithData: (NSData*)data;
- (id) initWithCharacteristic:(CBCharacteristic *)characteristic;

/**
 *  Uniq characteristic ID of the object instance.
 *
 *  @return the uniq identifier.
 *
 */
- (CBUUID*) identifier;
@end
