//
//  AMObject.h
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 25/06/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "AMErrorHandlerProtocol.h"

/// Buses
static const AudioUnitScope kAMAudioJackInputBus  = 1;
static const AudioUnitScope kAMAudioJackOutputBus = 0;

/// Flags
#if TARGET_OS_IPHONE
static const UInt32 kAMAudioJackDisableFlag = 1;
#elif TARGET_OS_MAC
static const UInt32 kAMAudioJackDisableFlag = 0;
#endif
static const UInt32 kAMAudioJackEnableFlag  = 1;


@interface AMObject : NSObject

@property(strong,nonatomic) id<AMErrorHandlerProtocol> errorHandler;
- (void) checkError: (OSStatus) error operation:(NSString*)operation;

-(AudioComponentDescription)getInputAudioComponentDescription;
-(AudioComponent)getInputComponentWithAudioComponentDescription:(AudioComponentDescription)audioComponentDescription;
-(void)createNewInstanceForInputComponent:(AudioComponent)audioComponent withUnit:(AudioUnit*) unit;
-(void)disableOutputScopeForinput:(AudioUnit)unit;
-(void)enableInputScopeForInput:(AudioUnit)unit;
-(void)configureDefaultDeviceForInput:(AudioUnit)unit;
-(Float64)configureDeviceSampleRateWithDefault:(float)defaultSampleRate;
-(Float32)configureDeviceBufferDurationWithDefault:(float)defaultBufferDuration;
-(UInt32)getBufferFrameSizeForUnit:(AudioUnit)unit;
-(void*) configureDefaultDeviceID;

/**
 *  HAL volume. By default is 1.0.
 *  @note iOS does not allow to change volume programmatically. Use MPVolumeView instead of AMToneGenerator system volume properties.
 *
 */
#if TARGET_OS_IPHONE
@property(readonly,atomic) Float32 volume;
#else
@property(assign,atomic) Float32 volume;
#endif

@end
