//
//  AMDeviceListView.h
//  Atom
//
//  Created by denis svinarchuk on 29.08.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMDeviceListView : UITableView

@end
