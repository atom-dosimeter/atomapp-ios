//
//  AMDigitDisplayView.m
//  Atom
//
//  Created by denis svinarchuk on 23/07/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMDigitDisplayView.h"

@interface AMDigitDisplayView()

@property (nonatomic) CAGradientLayer *gradient;

@end

@implementation AMDigitDisplayView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) awakeFromNib{
    [super awakeFromNib];
}

- (void) layoutSubviews{
    [super layoutSubviews];
    [self applyShinyBackground];
}

- (CAGradientLayer*) gradient{
    if (!_gradient) {
        CAGradientLayer *layer = [CAGradientLayer layer];
        
        _gradient = layer;
        
        const UIColor *color = self.backgroundColor;
        const CGFloat *cs = CGColorGetComponents(color.CGColor);
        
        layer.colors = [NSArray arrayWithObjects:
                        (id)[color CGColor],
                        (id)[[UIColor colorWithRed:0.90f*cs[0]
                                             green:0.90f*cs[1]
                                              blue:0.90f*cs[2]
                                             alpha:1] CGColor],
                        (id)[[UIColor colorWithRed:0.95f*cs[0]
                                             green:0.95f*cs[1]
                                              blue:0.95f*cs[2]
                                             alpha:1] CGColor],
                        (id)[[UIColor colorWithRed:0.98f*cs[0]
                                             green:0.98f*cs[1]
                                              blue:0.98f*cs[2]
                                             alpha:1] CGColor],
                        nil];
        
        layer.locations = [NSArray arrayWithObjects:
                           [NSNumber numberWithFloat:0.0f],
                           [NSNumber numberWithFloat:0.49f],
                           [NSNumber numberWithFloat:0.51f],
                           [NSNumber numberWithFloat:1.0f],
                           nil];
        
        [self.layer insertSublayer:layer atIndex:0];

    }
    return _gradient;
}

- (void)applyShinyBackground{
    self.gradient.frame = self.bounds;
}

@end
