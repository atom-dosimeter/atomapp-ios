//
//  AMSimpleGenerator.m
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 10/09/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMSimpleGenerator.h"
#import "AMConstants.h"

static AMSimpleGenerator *__shared_tone_generator = nil;

@implementation AMSimpleGenerator
{
    Float32 *sineLeftBuffer, *sineRightBuffer;
    int      sineBufferSize;
}

+ (id) sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __shared_tone_generator = [[AMSimpleGenerator alloc] init];
    });
    return __shared_tone_generator;
}

- (void) dealloc{
    if (sineLeftBuffer) {
        free(sineLeftBuffer);
    }
    
    if (sineRightBuffer) {
        free(sineRightBuffer);
    }
}

- (BOOL) isRuning{
    return self.audioManager.isRunningOutput;
}

- (id) init{
    
    if (__shared_tone_generator) {
        self = __shared_tone_generator;
        
        return self;
    }
    
    self = [super init];
    if (self) {
    
        sineLeftBuffer=sineRightBuffer=NULL;
        sineBufferSize=0;

        _frequency = kAMSineToneGenratorFreq;
        __block float phase = 0.0;
        __block float zero=0.0;
        
        [self.audioManager setOutputBlock:^(AMAudioManager *manager, AMAudioManagerBuffer *ioBuffer) {
            
            int   inNumberFrames=ioBuffer->numberOfFramesPerChannel;
            float *ioDataLeft  = ioBuffer->buffer[0];
            float *ioDataRight = ioBuffer->buffer[1];
            
            if (sineBufferSize==0) {
                
                sineBufferSize = inNumberFrames;
                sineLeftBuffer = sineLeftBuffer = malloc(sizeof(Float32)*sineBufferSize);
                sineRightBuffer = sineRightBuffer = malloc(sizeof(Float32)*sineBufferSize);

                float samplingRate = manager.samplingRate;
                int   sz = 1;
                for (int i=0; i < inNumberFrames; ++i)
                {
                    float phase_rad = phase * M_PI * 2;
                    float sample = 0;
                    vvsinf(&sample, &phase_rad, &sz);
                    
                    sineLeftBuffer[i] = ioDataLeft[i]  = sample;
                    sineRightBuffer[i] = ioDataRight[i] = - sample;
                    
                    phase += 1.0 / (samplingRate / _frequency);
                    
                    if (phase > 1.0) phase = -1;
                }
            }
            else{
                vDSP_vsadd(sineLeftBuffer, 1,  &zero, ioDataLeft, 1,   sineBufferSize);
                vDSP_vsadd(sineRightBuffer, 1, &zero, ioDataRight, 1, sineBufferSize);
            }
        }];
        
    }
    return self;
}

- (void) start{
    
    if (self.audioManager.isRunningOutput) 
        return;
    
    [self.audioManager startOutput];
#if DEBUG
    NSLog(@" ##### AMSimpleGenerator started.");
#endif
}

- (void) stop{
    if (self.audioManager.isRunningOutput==NO) 
        return;
    
    [self.audioManager stopOutput];
#if DEBUG
    NSLog(@" ##### AMSimpleGenerator stopped.");
#endif
}

@end
