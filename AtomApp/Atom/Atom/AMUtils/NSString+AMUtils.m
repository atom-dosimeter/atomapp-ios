//
//  NSString+AMUtils.m
//  Atom
//
//  Created by denis svinarchuk on 29/07/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "NSString+AMUtils.h"

static NSNumberFormatter *__static_number_formatter = nil;

@implementation NSString (AMUtils)

- (NSNumberFormatter*) formatter{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
            if (!__static_number_formatter) {
                
                __static_number_formatter = [[NSNumberFormatter alloc] init];
                
                [__static_number_formatter allowsFloats];
                [__static_number_formatter setLocale:[NSLocale currentLocale]];
                [__static_number_formatter setNumberStyle:NSNumberFormatterDecimalStyle];
                [__static_number_formatter setAlwaysShowsDecimalSeparator:NO];
            }
            
    });
    
    return __static_number_formatter;
}

- (NSString*) clearedString{
    return [[self componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789,."] invertedSet]] componentsJoinedByString:@""];
}

- (NSNumber*) floatNumber{
    return [[self formatter] numberFromString:[self clearedString]];
}


+ (NSString*) durationStringFromStartTime:(NSTimeInterval)starttime{
        
    static NSCalendar *calendar = nil; if(!calendar) calendar =[[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:starttime];
    NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:[NSDate timeIntervalSinceReferenceDate]];
    static unsigned int unitFlags = NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    NSDateComponents *components = [calendar components:unitFlags fromDate:startDate  toDate:endDate  options:0];
    
    if (components.day>0) {
        NSString *days=components.day>=2?NSLocalizedString(@"days", @""):NSLocalizedString(@"day", @"");
        if (components.hour>0) {
            return [NSString stringWithFormat:@"%li %@, %li%@%li%@", (long)components.day, days ,(long)components.hour,NSLocalizedString(@"h", @"") ,(long)components.minute, NSLocalizedString(@"m", @"")];
        }
        return [NSString stringWithFormat:@"%li %@", (long)components.day, days];
    }
    
    return [NSString stringWithFormat:@"%02li:%02li:%02li",(long)components.hour,(long)components.minute,(long)components.second];
}

@end
