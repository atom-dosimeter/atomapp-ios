//
//  AMConnectionView.m
//  Atom
//
//  Created by denis svinarchuk on 05.09.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMConnectionView.h"
#import "AMUIPreferences.h"


@interface AMConnectionView()

@property (nonatomic,strong) UIImage *connectionIcon;
@property (nonatomic,strong) UIImage *disconnectionIcon;
@property (nonatomic,strong) UIImageView *imageView;

@end

@implementation AMConnectionView

@synthesize connectionViewStyle=_connectionViewStyle;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.imageView.image = self.disconnectionIcon;
        self.imageView.alpha = 0.6;
    }
    return self;
}

- (AMConnectionViewStyle) connectionViewStyle{
    if (_connectionViewStyle==0) {
        _connectionViewStyle = AMConnectionViewRadar;
    }
    return _connectionViewStyle;
}

- (void) setConnectionViewStyle:(AMConnectionViewStyle)connectionViewStyle{
    if (_connectionViewStyle!=connectionViewStyle) {
        _disconnectionIcon=nil;
        _connectionIcon = nil;        
    }
    _connectionViewStyle = connectionViewStyle;
}

- (void) setIsConnected:(BOOL)isConnected{
    
    
    if (isConnected==_isConnected) {
        return;
    }
    
    _isConnected = isConnected;
    
    UIImage *image = _isConnected?self.connectionIcon:self.disconnectionIcon;
    
    [UIView animateWithDuration:AM_UI_ANIMATE_DURATION/2.
                     animations:^{
                         self.imageView.alpha = 0.0; 
                     } 
                     completion:^(BOOL finished) {
                         self.imageView.image = image;
                         [UIView animateWithDuration:AM_UI_ANIMATE_DURATION/2. 
                                          animations:^{
                                              self.imageView.alpha = _isConnected?1.:0.6;
                                          }];
                     }];
}

- (CGSize)size{
    if (_size.height==0 || _size.width==0) {
        _size = CGSizeMake(20, 20);
    }
    return _size;
}

- (UIImage*) connectionIcon{
    if (!_connectionIcon) {
        if (self.connectionViewStyle==AMConnectionViewCircle) {
            _connectionIcon = [UIImage imageNamed:@"Icon-connection-small"];
        }
        else
            _connectionIcon = [UIImage imageNamed:@"Icon-connection"];
    }
    return _connectionIcon;
}

- (UIImage*) disconnectionIcon{
    if (!_disconnectionIcon) {
        if (self.connectionViewStyle==AMConnectionViewCircle) {
            _disconnectionIcon = [UIImage imageNamed:@"Icon-disconnection-small.png"];
        }
        else
            _disconnectionIcon = [UIImage imageNamed:@"Icon-disconnection"];
    }
    
    return _disconnectionIcon;
}

- (UIImageView*) imageView{
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithImage:self.disconnectionIcon];
        [self addSubview:_imageView];
    }
    return _imageView;
}


- (void) layoutSubviews{
    [super layoutSubviews];
    
    CGRect frame = self.bounds;
    frame.size = self.size;
    frame.origin.x = self.bounds.size.width/2.-self.size.width/2.;
    frame.origin.y = self.bounds.size.height/2.-self.size.height/2.;
    self.imageView.frame = frame;
    
}

@end
