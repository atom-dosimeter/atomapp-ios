//
//  AMTagSettings.h
//  Atom
//
//  Created by denis svinarchuk on 23.08.14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "AMDevicePreferences.h"

/**
 *  Atom Tag Settings service characteristic. Characteristic should be write to peripheral to change its settings.
 */
@interface AMTagDevicePreferences : AMDevicePreferences

/**
 *  Sound duration.
 */
@property (readonly,nonatomic) uint16_t soundImpulseDuration;  // msec
@property (readonly,nonatomic) uint16_t soundOnImpulseFrequency; // Hz
@property (readonly,nonatomic) uint16_t vibrateOnImpulseDuration;  // msec

@property (readonly,nonatomic) BOOL hasAlarmOnThresholdDosePriority1;
@property (readonly,nonatomic) BOOL hasAlarmOnThresholdDosePriority2;
@property (readonly,nonatomic) BOOL hasAlarmOnThresholdDosePriority3;
@property (readonly,nonatomic) BOOL hasAlarmOnThresholdAll;
@property (readonly,nonatomic) BOOL hasAlarmOnImpulse;
@property (readonly,nonatomic) BOOL hasAlarmsOnAll;

@end
