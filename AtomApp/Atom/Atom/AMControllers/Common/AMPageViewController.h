//
//  AMPageViewController.h
//  Atom
//
//  Created by denis svinarchuk on 17/07/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AMPageControl.h"

@interface AMScrollView : UIScrollView
@end

@interface AMPageViewController : UIViewController
@property (readonly, nonatomic) AMScrollView *pageScrollView;
@property (readonly, nonatomic) AMPageControl *pageControl;
@end
