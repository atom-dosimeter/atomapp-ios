//
//  AMCommonDosimeterViewController.h
//  Atom
//
//  Created by denis svinarchuk on 16/10/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMTitledView.h"
#import "AMSettings.h"
#import "AMBaseDosimeterScreenView.h"

#import <UIKit/UIKit.h>
#import <AtomMeasurerSDK/AMDoseUnit.h>
#import <AtomMeasurerSDK/AMControllerManager.h>

@interface AMBaseDosimeterViewController : UIViewController
@property (nonatomic,readonly) AMTitledView        *titledView;
@property (nonatomic,weak)     AMDosimeter         *currentDosimeter;
@property (nonatomic,readonly) AMControllerManager *controllerManager;
@property (nonatomic,readonly) AMController        *defaultController;
@property (nonatomic,readonly) AMSettings          *settings;
@property (nonatomic,readonly) AMDoseUnit          *unit;
@property (nonatomic,readonly) AMBaseDosimeterScreenView *screenView;

- (void) awakeWithMotion;

@end
