//
//  AMDosimeter.h
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 02/07/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMController.h"

@interface AMSimpleControllerSettings : AMControllerSettings
@end


/**
 * The AMController class creates a controller object that manages a Atom dosimeter device connection. 
 */
@interface AMSimpleController : AMController
@end
