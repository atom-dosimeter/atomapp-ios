//
//  AMBatteryView.m
//  Atom
//
//  Created by denis svinarchuk on 04/09/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMBatteryView.h"
#import "AMUIPreferences.h"

@interface AMBatteryView()
@property (nonatomic,strong) CALayer *batteryLayer;
@property (nonatomic,strong) CALayer *batteryBoxLayer;
@property (nonatomic,strong) CALayer *batteryPlusSideLayer;
@property (nonatomic,strong) CALayer *batteryChargeLayer;
@property (nonatomic,strong) UILabel *percentLabel;
@property (nonatomic,strong) UIColor *powerVolumeColor;
@property (nonatomic,strong) UIImageView *powerChargePluggedView;
@end

@implementation AMBatteryView
{
    UIColor *originalBorderColor;
}

@synthesize textColor=_textColor, powerVolumeColor=_powerVolumeColor, borderColor=_borderColor;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.batteryLayer.frame = [self boxFrame];
        self.layer.frame = self.bounds;  
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (CGRect) boxFrame {
    CGRect  box = self.bounds;
    box.origin.y = self.bounds.size.height/2. - self.size.height/2;
    box.origin.x = self.bounds.size.width/2. - self.size.width/2.;
    box.size.width = self.size.width;
    box.size.height = self.size.height;
    return box;
}

- (CGSize) size{
    if (_size.height==0 || _size.width==0) {
        _size = CGSizeMake(34, 15);
    }
    return _size;
}

- (UIColor*) borderColor{
    if (!_borderColor) {
         originalBorderColor = _borderColor = [UIColor whiteColor];
    }
    return _borderColor;
}

- (void) setBorderColor:(UIColor *)borderColor{
    if (!_borderColor) {
        originalBorderColor = [UIColor whiteColor];
    }
    _borderColor = borderColor;
    _batteryBoxLayer.borderColor = self.borderColor.CGColor;
    _batteryPlusSideLayer.backgroundColor = self.borderColor.CGColor;
    _batteryPlusSideLayer.borderColor = self.batteryBoxLayer.backgroundColor;
    _batteryChargeLayer.borderColor = [UIColor clearColor].CGColor;
}

- (UIColor*) powerVolumeColor{
    if (!_powerVolumeColor) {
        _powerVolumeColor = AM_UI_BATTERY_HIGHCOLOR;
    }
    return _powerVolumeColor;
}

- (void) setPowerVolumeColor:(UIColor *)powerVolumeColor{
    _powerVolumeColor = powerVolumeColor;
    _batteryChargeLayer.backgroundColor = _powerVolumeColor.CGColor;
}

- (CALayer*) batteryLayer{
    if (!_batteryLayer) {
        _batteryLayer = [CALayer layer];
        _batteryLayer.frame = [self boxFrame];
        _batteryLayer.masksToBounds = YES;
        _batteryLayer.backgroundColor = [UIColor clearColor].CGColor;
        [self.layer addSublayer:_batteryLayer];
    }
    return _batteryLayer;
}


- (CALayer*) batteryBoxLayer{
    if (!_batteryBoxLayer) {
        _batteryBoxLayer = [CALayer layer];
        
        _batteryBoxLayer.backgroundColor = [UIColor clearColor].CGColor;
        _batteryBoxLayer.borderWidth = 0.5;
        _batteryBoxLayer.borderColor = self.borderColor.CGColor;
        _batteryBoxLayer.cornerRadius = 1.5;
        
        [self.batteryLayer addSublayer:_batteryBoxLayer];

    }
    return _batteryBoxLayer;
}

- (CALayer*) batteryPlusSideLayer{
    if (!_batteryPlusSideLayer) {
        _batteryPlusSideLayer = [CALayer layer];
        
        _batteryPlusSideLayer.backgroundColor = self.borderColor.CGColor;
        _batteryPlusSideLayer.borderWidth = self.batteryBoxLayer.borderWidth;
        _batteryPlusSideLayer.borderColor = self.batteryBoxLayer.backgroundColor;
        _batteryPlusSideLayer.cornerRadius = self.batteryBoxLayer.cornerRadius/1.5;
        
        [self.batteryLayer addSublayer:_batteryPlusSideLayer];
        
    }
    return _batteryPlusSideLayer;
}

- (CALayer*) batteryChargeLayer{
    if (!_batteryChargeLayer) {
        _batteryChargeLayer = [CALayer layer];
        
        _batteryChargeLayer.backgroundColor = self.powerVolumeColor.CGColor;
        _batteryChargeLayer.borderWidth = self.batteryBoxLayer.borderWidth;
        _batteryChargeLayer.borderColor = [UIColor clearColor].CGColor;
        _batteryChargeLayer.cornerRadius = self.batteryBoxLayer.cornerRadius/1.5;
        
        
        [self.batteryBoxLayer addSublayer:_batteryChargeLayer];
    }
    return _batteryChargeLayer;
}

- (UIColor*) textColor{
    if (!_textColor) {
        _textColor = [UIColor whiteColor];
    }
    return _textColor;
}

- (void) setTextColor:(UIColor *)textColor{
    self.percentLabel.textColor=_textColor=textColor;
}

- (UIColor*)reversedColorFromOriginal:(UIColor*)originalColor {
    return [UIColor colorWithRed:(1-CGColorGetComponents(originalColor.CGColor)[0]) green:(1-CGColorGetComponents(originalColor.CGColor)[1]) blue:(1-CGColorGetComponents(originalColor.CGColor)[2]) alpha:CGColorGetAlpha(originalColor.CGColor)];
}

- (UIImageView*)powerChargePluggedView{
    if (!_powerChargePluggedView) {
        _powerChargePluggedView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Icon-charger-plugged.png"]];
        _powerChargePluggedView.contentMode = UIViewContentModeScaleAspectFit;
        [self.percentLabel addSubview:_powerChargePluggedView];
    }
    return _powerChargePluggedView;
}

- (UILabel*) percentLabel{
    if (!_percentLabel) {
        _percentLabel = [[UILabel alloc] initWithFrame:self.batteryBoxLayer.frame];
        _percentLabel.adjustsFontSizeToFitWidth = YES;
        _percentLabel.minimumScaleFactor = 0.5;
        _percentLabel.textAlignment = NSTextAlignmentCenter;
        _percentLabel.textColor = self.textColor;
        _percentLabel.layer.shadowColor = [self reversedColorFromOriginal:self.textColor].CGColor;
        _percentLabel.layer.shadowOffset = CGSizeMake(1., 1.);
        _percentLabel.layer.shadowRadius = 0.5;
        [self addSubview:_percentLabel];
    }
    return _percentLabel;
}

- (void) layoutSubviews{
    float plusSizeWidth  = 0.05;
    float plusSizeHeight = 0.5;
    
    [super layoutSubviews];
    
    self.batteryLayer.frame = [self boxFrame];
    
    //
    // battery box
    //
    CGRect box = self.batteryLayer.bounds;
    box.size.width -= box.size.width * plusSizeWidth + self.batteryPlusSideLayer.borderWidth*2;
    box.origin.x    = self.batteryBoxLayer.borderWidth;
    box.origin.y   += self.batteryBoxLayer.borderWidth;
    box.size.height-=self.batteryBoxLayer.borderWidth*2.;
    
    self.batteryBoxLayer.frame = box;
    
    //
    // battery plus contact
    //
    box.origin.x = self.batteryBoxLayer.bounds.size.width+self.batteryBoxLayer.borderWidth ;
    box.size.width = self.batteryLayer.bounds.size.width * plusSizeWidth ;
    box.size.height = box.size.height * plusSizeHeight;
    box.origin.y = self.batteryLayer.bounds.size.height/2. - box.size.height/2.;
    
    self.batteryPlusSideLayer.frame = box;
    
    //
    // bettary charge fill
    //
    self.batteryChargeLayer.frame = [self powerVolumeBox];
    
    CGRect labelFrame = self.batteryLayer.frame;
    labelFrame.origin.x -= self.batteryPlusSideLayer.bounds.size.width-self.batteryBoxLayer.borderWidth*2;
    self.percentLabel.frame = labelFrame;
    self.percentLabel.font = [UIFont fontWithName:AM_UI_FONT size:self.batteryBoxLayer.bounds.size.height*0.6];
    
    self.powerChargePluggedView.frame = CGRectInset(self.percentLabel.bounds, self.percentLabel.bounds.size.width/4, self.percentLabel.bounds.size.height/4);
}

- (CGRect) powerVolumeBox{
    CGRect powerBox = CGRectInset(self.batteryBoxLayer.bounds, self.batteryBoxLayer.borderWidth*2.+0.5, self.batteryBoxLayer.borderWidth*2.+0.5);
    powerBox.size.width *=self.powerVolume;
    return powerBox;
}

- (void) setPowerVolume:(float)powerVolume{
    
    _powerVolume = powerVolume;
    
    [CATransaction begin]; 
    [CATransaction setAnimationDuration:AM_UI_ANIMATE_DURATION];
    self.batteryChargeLayer.frame = [self powerVolumeBox];
    
    if (self.chargerPlugged) {
        self.borderColor = self.powerVolumeColor = [UIColor grayColor];
        self.powerChargePluggedView.alpha = 1.0;
    }
    else{
        self.powerChargePluggedView.alpha = 0.0;
        if (powerVolume>0.5) {
            self.powerVolumeColor = AM_UI_BATTERY_HIGHCOLOR;
            self.borderColor = originalBorderColor;
        }
        else if (powerVolume<=0.5 && powerVolume>0.20){
            self.borderColor = self.powerVolumeColor = AM_UI_BATTERY_WARNCOLOR;
        }
        else if (powerVolume>=0.001){
            self.borderColor = self.powerVolumeColor = AM_UI_BATTERY_LOWCOLOR;
        }
        else{
            self.borderColor = self.powerVolumeColor = [UIColor grayColor];
        }
    }
    
    [CATransaction commit];
    
    if (!self.chargerPlugged) {
        self.percentLabel.text = [NSString stringWithFormat:@"%.0f%%",self.powerVolume*100.0];
    }
    else
        self.percentLabel.text = [NSString stringWithFormat:@""];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}


- (void) didMoveToSuperview{
    [super didMoveToSuperview];
}

@end
