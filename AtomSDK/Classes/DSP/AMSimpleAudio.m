//
//  AMSimpleAudio.m
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 10/09/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMSimpleAudio.h"


@implementation AMSimpleAudio

@synthesize audioManager=_audioManager;

- (AMAudioManager*) audioManager{
    if (!_audioManager) {
        _audioManager=[AMAudioManager sharedInstance];
    }
    return _audioManager;
}

- (void) setErrorHandler:(id<AMErrorHandlerProtocol>)errorHandler{
    _audioManager.errorHandler = errorHandler;
}

- (id<AMErrorHandlerProtocol>) errorHandler{
    return _audioManager.errorHandler;
}

- (void) start{}

- (void) stop{}

@end
