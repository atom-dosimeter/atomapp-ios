//
//  AMDosimeterView.m
//  Atom
//
//  Created by denis svinarchuk on 12.07.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMTitledView.h"
#import "AMPageControl.h"
#import "AMUIPreferences.h"

@interface AMTitledView()
@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UILabel *deviceNameLabel;
@end

@implementation AMTitledView
{
    float topSpaceConstant, bottomSpaceConstant;
}


@synthesize title=_title, titleLabel=_titleLabel ;

- (void) awakeFromNib{
    [super awakeFromNib];
    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background-568h.png"]];
    self.title = @"";
}

- (void) awakeWithMotion{
    
    if (self.topSpace && self.bottomSpace) {
        topSpaceConstant = self.topSpace.constant;
        bottomSpaceConstant = self.bottomSpace.constant;
        
        self.topSpace.constant = self.bounds.size.height;
        self.bottomSpace.constant = -self.bounds.size.height;
        
        [self needsUpdateConstraints];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(AM_UI_AWAKE_DURATION * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:AM_UI_AWAKE_DURATION
                             animations:^{
                                 self.topSpace.constant = topSpaceConstant;
                                 self.bottomSpace.constant = bottomSpaceConstant;
                                 [self layoutIfNeeded];
                             }
                             completion:^(BOOL finished) {
                                 
                             }];
        });
    }
}


- (void) setTitle:(NSString *)title{
    _title = title;
    self.titleLabel.text = _title;
}

- (NSString*) title{
    if (_title) {
        return _title;
    }
    _title = @"";
    return _title;
}

- (void) setDeviceName:(NSString *)deviceName{
    _deviceName = deviceName;
    self.deviceNameLabel.text = _deviceName;
}

- (UILabel*) titleLabel{
    if (_titleLabel) {
        return _titleLabel;
    }
    
    _titleLabel = [[UILabel alloc] init];
    _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.textColor = [UIColor lightGrayColor];
    _titleLabel.font = [UIFont fontWithName:AM_UI_BOLD_FONT size:AM_UI_TITLE_FONT_SIZE];

    [self addSubview:_titleLabel];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[view(fontsize)]" options:0 metrics:@{@"fontsize": [NSNumber numberWithInt:AM_UI_TITLE_FONT_SIZE+6.0f]}  views:@{@"view": _titleLabel}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view": _titleLabel}]];

    return _titleLabel;
}

- (UILabel*) deviceNameLabel{
    if (_deviceNameLabel) {
        return _deviceNameLabel;
    }
    
    _deviceNameLabel = [[UILabel alloc] init];
    _deviceNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    _deviceNameLabel.backgroundColor = [UIColor clearColor];
    _deviceNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    _deviceNameLabel.textAlignment = NSTextAlignmentCenter;
    _deviceNameLabel.textColor = [UIColor whiteColor];
    _deviceNameLabel.font = [UIFont fontWithName:AM_UI_BOLD_FONT size:AM_UI_TITLE_DEVICE_NAME_FONT_SIZE];
    
    [self addSubview:_deviceNameLabel];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[titleLabel]-0-[view(fontsize)]" options:0 metrics:@{@"fontsize": [NSNumber numberWithInt:AM_UI_TITLE_DEVICE_NAME_FONT_SIZE+8.0f]} views:@{@"view": _deviceNameLabel, @"titleLabel":self.titleLabel}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view": _deviceNameLabel}]];
    
    return _deviceNameLabel;
}

@end
