//
//  AMDevicePreferences.h
//  Pods
//
//  Created by denis svinarchuk on 11.11.16.
//
//

#import <Foundation/Foundation.h>
#import "AMBLEProtocol.h"

typedef enum {
    AM_PREFERENCES_COMMAND_SOUNDVIBRO_SETTO  = 0x01, // should use with parameters 1: tone duration in ms, 2: tone freq in Hz, 3: vibro duration in ms
    AM_PREFERENCES_COMMAND_PRESET_DEFAULT_ON = 0x04, // 1ms/4KHz without vibro by DEFAULT
    AM_PREFERENCES_COMMAND_PRESET1_ON        = 0x02, // 2ms/1KHz without vibro
    AM_PREFERENCES_COMMAND_PRESET2_ON        = 0x03, // 20ms/1KHz without vibro
    AM_PREFERENCES_COMMAND_PRESET3_ON        = 0x05, // 20ms/4KHz without vibro
    AM_PREFERENCES_COMMAND_PRESET4_ON        = 0x06, // 1ms/4KHz with 10ms vibro
    AM_PREFERENCES_COMMAND_PRESET5_ON        = 0x07, // 20ms/4KHz with 100ms vibro
    AM_PREFERENCES_COMMAND_SOUNDVIBRO_OFF    = 0x08, // off impulses sound-vibro alert only
    
    AM_PREFERENCES_COMMAND_TOTALMUTE_ON    = 0x0A, // off all sounds and vibro
    AM_PREFERENCES_COMMAND_TOTALMUTE_OFF   = 0x10, // deactivate quiet mode
    
    AM_PREFERENCES_COMMAND_ALERT_FLAGS_RESET   = 0x11, //
    
    AM_PREFERENCES_COMMAND_THRESHOLDS_RESET    = 0x12, // reset thresholds to DEFAULT state, needs password in the parameter1: 0xACCE
    AM_PREFERENCES_COMMAND_THRESHOLD_OFF       = 0x13, // off threshold sound-vibro alert, in the parameter1: 0x0000 - all, 0x0001 threshold1, 0x0002 threshold2, 0x0003 threshold3
    AM_PREFERENCES_COMMAND_THRESHOLD_ON        = 0x14, // returns threshold sound-vibro alert, in the parameter1: 0x0000 - all, 0x0001 threshold1, 0x0002 threshold2, 0x0003 threshold3
    
    AM_PREFERENCES_COMMAND_FLASH_SAVE          = 0xCA, // save the preferences to the device volatile memory, needs password in the parameter1: 0xACCE
    AM_PREFERENCES_COMMAND_SEARCHING_PLAY      = 0xAA, // play a signal to search device, parameter1 can be the follow: 0x00, 0x01, 0x02 (difference sounds)
    
    AM_PREFERENCES_COMMAND_DOSE_RESET    = 0xDE, // reset cumulative impulses and dose to 0, needs password in the parameter1: 0xACCE
    
    AM_PREFERENCES_COMMAND_POWER_OFF     = 0xFF,  // off the device power hardware supplyer, needs password in the parameter1: 0xACCE
    
    //
    // CWIFT/FAST
    //
    AM_PREFERENCES_COMMAND_SEARCH_MODE         = 0xE0, // Atom Swift searching rate mode: 0,1,2 - how many particles is being searhed (searching window)
    AM_PREFERENCES_COMMAND_CLICKS_MODE         = 0xE1, // Atom Swift clicks mode (registration rate): 1:1 1:10
    AM_PREFERENCES_COMMAND_ACCELEROMETER_MODE  = 0xE2  // Atom Swift accelerometer mode: 0 - off, 1 - on
    
} AMDevicePreferencesCommand;

typedef struct {
    char command;
    char parameter1[2];
    char parameter2[2];
    char parameter3[2];
    char flags;
} AMDeviceSettingsData;


@interface AMDevicePreferences : NSObject <AMBLEProtocol, NSCopying>
/**
 *  Initialize settings with device UUID to keep settings local in device.
 *
 *  @param data device settings represented as raw data
 *  @param uuid peripheral identifier
 *
 *  @return settings instance
 *
 */
- (id) initWithData:(NSData*)data withDeviceUUIDString:(NSString*)uuidString;

/**
 *  The device UUID.
 *
 */
@property (readonly,nonatomic) NSString *deviceUUIDString;

/**
 *  Get raw data of the settings to send to a peripheral device.
 *
 */
@property (readonly,nonatomic) NSData *data;


/**
 * The command that a device can accept to cnange self preferences.
 */
@property (assign,nonatomic) AMDevicePreferencesCommand command;

/**
 * The first parameter of a command.
 */
@property (assign,nonatomic) uint16_t parameter1;

/**
 * The second parameter of a command.
 */
@property (assign,nonatomic) uint16_t parameter2;

/**
 * The 3d parameter of a command.
 */
@property (assign,nonatomic) uint16_t parameter3;

@property (assign,assign) uint8_t flags;

- (void) readData:(NSData *)data;

@end
