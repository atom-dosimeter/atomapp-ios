//
//  AMTagDeviceThreshold.h
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 29/09/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMBLEProtocol.h"

typedef enum {
    AM_THRESHOLD_PRIORITY_LOW = 1,
    AM_THRESHOLD_PRIORITY_HIGH = 2,
    AM_THRESHOLD_PRIORITY_HIGHER = 3
}AMTagDeviceThresholdPriority;


typedef enum {
    /**
     * it forces to make a sound on exceeding dose rate threshold: 2 short tone signals every 2 seconds and Morse code "D2" on exceeding cumulative dose every 1 minutes
     */
    AM_THRESHOLD_COMMAND_SOUND_ON = 0x11,

    /**
     * It makes the same that makes AM_THRESHOLD_COMMAND_SOUND_ON but adds VIBRO alert with 1 second duration synchronously to every tone signals.
     */
    AM_THRESHOLD_COMMAND_SOUNDVIBRO_ON = 0x22
    
}AMTagDeviceThresholdCommand;

/**
 *  Atom Tag Threshold service characteristic. Characteristic should be write to peripheral to change its thresholds.
 */
@interface AMTagDeviceThreshold : NSObject <AMBLEProtocol>

@property (assign,nonatomic) AMTagDeviceThresholdPriority priority;

/**
 *  Threshold of the comulative dose. Threshold sets in case you want to get alert exceeding the dose. It sets in impulses.
 *  Max impulses in year is about seconds per year / 4 = 7 884 000 impulses per year.
 *  Set to 0 to avoid alert signal. Signal can be sound and vibration. If threshold is seted, you can't off sound alert in this version of device software.
 *  Unit: particles/impulses
 */
@property (assign,nonatomic) float thresholdDose;          // mSv

/**
 *  Threshold of the dose rate. It sets in case you want to get alert exceeding the dose rate. It sets in impulses per value defined by thresholdDetectionTime property.
 *  thresholdDetectionTime always multiple of 2 seconds. 60 impulses per 16 thresholdDetectionTime is equal 60 impulses per 32 seconds, and equal 60µR/h.
 */
@property (assign,nonatomic) float thresholdDoseRate;      // µSv/h

/**
 *  Period which impulses are detecting to compute threshold rate. It must be even. Value is set to 34 secconds by default.
 *
 */
@property (assign,nonatomic) float thresholdDetectionTime; // sec.

/**
 * The command sending to Atom Tag changes sound-vibrations alarm behavior.
 */
@property (assign,nonatomic) AMTagDeviceThresholdCommand  command;

/**
 * Raw data to send to Atom Tag.
 */
@property (readonly,nonatomic) NSData *data;

/**
 * Create threshold object from raw data with concrete threshold priority.
 */
- (id) initWithData:(NSData *)data withPriority:(AMTagDeviceThresholdPriority)priority;

@end
