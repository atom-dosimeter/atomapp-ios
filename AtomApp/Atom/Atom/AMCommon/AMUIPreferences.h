//
//  AMUIPreferences.h
//  Atom
//
//  Created by denis svinarchuk on 16/07/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AtomMeasurerSDK/AMConstants.h>


#define AM_APP_VERSION [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"]
#define AM_APP_RELEASE_DATE @"2014-11-29 00:00:00"
#define AM_APP_BUILD @"r" // == (b-beta|rc-release-candidate|r-release))

#define ME_APP_VESRION_INFO [NSString stringWithFormat:@"%@ (%@%@)", [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"], AM_APP_BUILD, [[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"]]

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define AM_SETTIN_KEY @"settingPreferenceKEY"

#define AM_UI_ANIMATE_DURATION 0.3
#define AM_UI_AWAKE_DURATION 0.5
#define AM_UI_BLINK_DURATION 1.0

#define AM_THRESHOLD_MIN kAMThresholdDoseRateMinimum

#define AM_UI_MINIMUM_PARTICLES_TO_VIEW_COMPUTE 5
#define AM_UI_WAITTIME_PARTICLES_TO_ALERT 32 // sec.

#define AM_UI_FONT  @"HelveticaNeue-Light"
#define AM_UI_BOLD_FONT @"HelveticaNeue-Bold"
#define AM_UI_TITLE_FONT_SIZE 18
#define AM_UI_TITLE_DEVICE_NAME_FONT_SIZE ((UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)?16:12)

#define AM_UI_FONT_SIZE 14
#define AM_UI_BUTTON_FONT_SIZE 14

#define AM_UI_COLOR(index) (((float)(index))/255.0f)
#define AM_UI_DISABLED_ALPHA 0.6

#define AM_UI_DER_LOWCOLOR  [UIColor colorWithRed:AM_UI_COLOR(151) green:AM_UI_COLOR(202) blue:AM_UI_COLOR(40) alpha:1.0]
#define AM_UI_DER_HIGHCOLOR [UIColor colorWithRed:AM_UI_COLOR(250) green:AM_UI_COLOR(42) blue:AM_UI_COLOR(8) alpha:1.0]
#define AM_UI_DER_FONT_SIZE ((UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)?160:96)


#define AM_UI_LOWERCOLOR  [UIColor colorWithRed:AM_UI_COLOR(161) green:AM_UI_COLOR(222) blue:AM_UI_COLOR(40) alpha:1.0]
#define AM_UI_LOWCOLOR  [UIColor colorWithRed:AM_UI_COLOR(151) green:AM_UI_COLOR(180) blue:AM_UI_COLOR(30) alpha:0.8]
#define AM_UI_HIGHCOLOR [UIColor colorWithRed:AM_UI_COLOR(180) green:AM_UI_COLOR(42) blue:AM_UI_COLOR(8) alpha:1.0]
#define AM_UI_HIGHERCOLOR [UIColor colorWithRed:AM_UI_COLOR(150) green:AM_UI_COLOR(10) blue:AM_UI_COLOR(0) alpha:.8]
#define AM_UI_THRESHOLD_BLINK [UIColor colorWithRed:AM_UI_COLOR(180) green:AM_UI_COLOR(42) blue:AM_UI_COLOR(8) alpha:0.2]

#define AM_UI_BATTERY_HIGHCOLOR AM_UI_LOWCOLOR
#define AM_UI_BATTERY_WARNCOLOR [UIColor orangeColor]
#define AM_UI_BATTERY_LOWCOLOR  AM_UI_DER_HIGHCOLOR

#define AM_UI_DISPLAY_BG_COLOR [UIColor colorWithRed:AM_UI_COLOR(60) green:AM_UI_COLOR(60) blue:AM_UI_COLOR(60) alpha:1.0]
#define AM_UI_DISPLAY_TEXT_COLOR [UIColor colorWithRed:AM_UI_COLOR(230) green:AM_UI_COLOR(230) blue:AM_UI_COLOR(230) alpha:1.0]
#define AM_UI_DISPLAY_FONT  @"HelveticaNeue"

#define AM_UI_INFO_TEXT_COLOR [UIColor colorWithRed:AM_UI_COLOR(230) green:AM_UI_COLOR(230) blue:AM_UI_COLOR(230) alpha:1.0]
#define AM_UI_INFO_FONT_TITLE  @"HelveticaNeue-Light"
#define AM_UI_INFO_FONT_VALUE  @"HelveticaNeue"
#define AM_UI_INFO_FONT_TITLE_SIZE  ((UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)?24:12)
#define AM_UI_INFO_FONT_VALUE_SIZE  ((UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)?34:27)
#define AM_UI_INFO_VALUE_HIEGHT  ((UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)?60:40)
#define AM_UI_INFO_FONT_VALUE_COLOR [UIColor colorWithRed:AM_UI_COLOR(255) green:AM_UI_COLOR(255) blue:AM_UI_COLOR(255) alpha:1.0]
#define AM_UI_INFO_TITLE_HIEGHT (AM_UI_INFO_VALUE_HIEGHT*0.6)

#define AM_UI_BG_COLOR [UIColor colorWithRed:AM_UI_COLOR(48) green:AM_UI_COLOR(48) blue:AM_UI_COLOR(48) alpha:1.0]
#define AM_UI_TABBAR_TINTCOLOR [UIColor colorWithRed:AM_UI_COLOR(151) green:AM_UI_COLOR(202) blue:AM_UI_COLOR(40) alpha:1.0]

#define AM_SCALE_OFSET_WIDTH 5.0
#define AM_MERCURY_OFSET_WIDTH 10.0
#define AM_THERMOMETER_MIN_THRESHOLD AM_THRESHOLD_MIN 
#define AM_THERMOMETER_OFSET_WIDTH 4.0
#define AM_THERMOMETER_LABEL_HEIGHT 14.0
#define AM_THERMOMETER_LABEL_COLOR [UIColor colorWithRed:AM_UI_COLOR(220) green:AM_UI_COLOR(220) blue:AM_UI_COLOR(220) alpha:0.9]
#define AM_THERMOMETER_TRUST_MARKER_COLOR [UIColor colorWithRed:AM_UI_COLOR(250) green:AM_UI_COLOR(250) blue:AM_UI_COLOR(250) alpha:0.2]
#define AM_THERMOMETER_THRESHOLD_MARKER_PAN_AREA 44.0 // pt

#define AM_MINIMUM_THERMOMETER_DOSERATE 10.0f // nSv/h
#define AM_MAXIMUM_THERMOMETER_DOSERATE kAMThresholdDoseRateMaximum; // 1 mSv/h
