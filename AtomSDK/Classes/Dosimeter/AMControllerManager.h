//
//  AMControllerManager.h
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 27/08/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMController.h"
#import "AMList.h"

@class AMControllerManager;

/**
 *  Atom device controllers manager ptotocol. It delegate each device controller from list which the manager is controlled.
 *  Also the manager adds delegates to control event list of this controller list.
 */
@protocol AMControllerManagerDelegate <NSObject>

/**
 *  It handles when controller list changed. Controller list can be extended from one to many device controllers by one time.
 *  Controller manager does not delete closed devices and keep its till the end application will finished. You should update states of devices.
 *
 *  @param manager this manager instance.
 *
 */
- (void) amControllerManagerDidUpdateControllerList: (AMControllerManager *)manager;

/**
 *  Handle when host can't, lost or has connection to dosimeter
 *
 *  @param dosimeter the instance of dosimeter object
 *  @param available YES/NO
 *
 */
- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController*)controller didUpdateConnectionState:(BOOL)available;

/**
 *  Indicate that "AM" has power to supply dosimeter or not. Phisicaly "AM" produces meander with frequency has value about 40-150Hz with an amplitude of 5-25% of the maximum (gain?).
 *
 *  @param processor the processor object
 *  @param powerState
 *
 */
- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController*)controller didChangePowerState:(AMControllerPowerState)powerState;

/**
 *  It handles when a next measurment done.
 *
 *  @param manager    manager object
 *  @param controller the dosimeter controller instance
 *  @param dosimeter the dosimeter instance.
 */
- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController*)controller didUpdateAmDosimeter:(AMDosimeter*)dosimeter;

@optional

/**
 *  Tells the delegate object that the dosimeter connected to the i-device is about to be ready during the next approxTime.
 *
 *  @param manager    manager object
 *  @param controller the dosimeter controller instance.
 *  @param approxTime time to be ready
 *
 */
- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController *)controller willReadyAfterTime:(NSTimeInterval)approxTime;

/**
 *  Tells the delegate object that the dosimeter connected to the i-device was ready or not ready to detect radiation.
 *
 *  @param manager    manager object
 *  @param controller the dosimeter controller instance
 *  @param ready ready-state YES/NO
 *
 */
- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController *)controller didUpdateReadyState:(BOOL)ready;

/**
 *  Tells the delegate that i-devices has changed absolute sound volume, which uses to power jack-connected dozimeter (Atom Simple). In case Atom BT connection controller should not care about it.
 *
 *  @param manager    manager object
 *  @param controller the dosimeter controller instance
 *  @param volume     volume value is between 0.0-1.0
 
 */
- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController *)controller didChangePowerVolume:(float)volume;

/**
 *  Indicate that "AM" received local peaks - number of triggering duirng measurement.
 *
 *  @param manager    manager object
 *  @param controller the dosimeter controller instance
 *  @param count     local peaks recived from device during one measure. One measure performs on the interval which the device provides.
 *
 */
- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController*)controller didDetectParticles:(NSInteger)count;

/**
 *  Some error occured.
 *
 *  @param manager    manager object
 *  @param controller the dosimeter controller instance, can be nill if error has occured in manager instance.
 *  @param error      error
 *
 */
- (void) amControllerManager: (AMControllerManager *)manager amController:(AMController*)controller didFail:(NSError*)error;

- (void) amControllerManager: (AMControllerManager *)manager amController:(AMController*)controller didUpdateSettings:(AMControllerSettings*)settings;

@optional
- (void) amControllerManager:(AMControllerManager *)manager amControllerWillCommitChanges:(AMController*)controller;
- (void) amControllerManager:(AMControllerManager *)manager amControllerDidCommitChanges:(AMController*)controller;
- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController *)controller didChangeFromName:(NSString*)fromName toName:(NSString*)toName;

- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController *)controller didWarn:(NSString*)warnString;
@end

/**
 *  The class manages device controllers.
 *
 */
@interface AMControllerManager : NSObject

/**
 *  List of controllers which connect with all type devices
 *
 */
@property (readonly,nonatomic) AMList *controllerList;

/**
 *  Get/Set default current controller.
 *
 */
@property (strong,nonatomic) AMController *defaultController;

/**
 *  Controller manager delegate.
 *
 */
@property (strong,nonatomic) id<AMControllerManagerDelegate> delegate;


/**
 *  Create singletone manager instance.
 *
 *  @return manager object.
 *
 */
+ (id) sharedInstance;
+ (id) sharedInstanceWithDelegate:(id<AMControllerManagerDelegate>)delegate;

/**
 *  Start dispatch devices.
 *
 */
- (void) start;

/**
 *  Stop dispatch divices.
 *
 */
- (void) stop;

@end
