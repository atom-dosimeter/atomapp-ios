//
//  AMDurationLabel.h
//  Atom
//
//  Created by denis svinarchuk on 14.09.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMDurationLabel : UILabel
@property (nonatomic,assign) BOOL isActive;
@property (nonatomic,assign) NSTimeInterval startTime;
@end
