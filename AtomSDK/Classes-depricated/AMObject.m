//
//  AMObject.m
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 25/06/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMObject.h"
#import <AVFoundation/AVFoundation.h>

@implementation AMObject
{
#if TARGET_OS_IPHONE
#elif TARGET_OS_MAC
    Float64 inputScopeSampleRate;
    AudioDeviceID defaultOutputDeviceID;
    BOOL isDefaultOutPutDeviceConfigured;
#endif
    
}

@synthesize volume=_volume;

- (id) init{
    self = [super init];
    if (self) {
#if TARGET_OS_IPHONE
#elif TARGET_OS_MAC
        isDefaultOutPutDeviceConfigured = NO;
#endif
    }
    return self;
}

- (void) checkError: (OSStatus) error operation:(NSString*)operation{

    if (error == noErr) return;

    if (self.errorHandler && [self.errorHandler respondsToSelector:@selector(amObject:withError:)]) {
        
        char str[20];
        // see if it appears to be a 4-char-code
        *(UInt32 *)(str + 1) = CFSwapInt32HostToBig(error);
        if (isprint(str[1]) && isprint(str[2]) && isprint(str[3]) && isprint(str[4])) {
            str[0] = str[5] = '\'';
            str[6] = '\0';
        } else
            // no, format it as an integer
            sprintf(str, "%d", (int)error);
        
        
        NSError *am_error =  [NSError errorWithDomain:NSOSStatusErrorDomain
                                   code:error
                               userInfo:@{
                                          NSLocalizedDescriptionKey: operation,
                                          NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"System error", @"")
                                          }];
        
        [self.errorHandler amObject:self withError:am_error];

#if DEBUG
        NSLog(@"Error: %@ (%s)\n", operation, str);
#endif
        
#if DO_ABBORT_ON_ERROR
        abort();
#endif
    }
}

- (void*) configureDefaultDeviceID{
#if TARGET_OS_IPHONE
    return NULL;
#else
    
    if (isDefaultOutPutDeviceConfigured) {
        return &defaultOutputDeviceID;
    }
    
    AudioObjectPropertyAddress getDefaultOutputDevicePropertyAddress = {
        kAudioHardwarePropertyDefaultOutputDevice,
        kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster
    };
    
    UInt32 defSize = sizeof(defaultOutputDeviceID);
    
    
    [self checkError:AudioObjectGetPropertyData(kAudioObjectSystemObject,
                                                &getDefaultOutputDevicePropertyAddress,
                                                0, NULL,
                                                &defSize, &defaultOutputDeviceID)
           operation:NSLocalizedString(@"Get default device ID", @"")];
    
    isDefaultOutPutDeviceConfigured = YES;
    return &defaultOutputDeviceID;
#endif
}

#if TARGET_OS_IPHONE
- (Float32) volume{
    static AVAudioSession *audiosession = nil; if (audiosession==nil) audiosession =  [AVAudioSession sharedInstance];
    return audiosession.outputVolume;
}
#else
- (Float32) volume{
    
    UInt32 channel = 1; // Channel 0  is master, if available
    AudioObjectPropertyAddress prop = {
        kAudioDevicePropertyVolumeScalar,
        kAudioDevicePropertyScopeOutput,
        channel
    };
    
    UInt32 dataSize = sizeof(_volume);
    
    [self checkError:AudioObjectGetPropertyData(defaultOutputDeviceID, &prop, 0, NULL, &dataSize, &_volume)
           operation:NSLocalizedString(@"Could not get system volume", @"")];
    
    return _volume;
}

- (void) setVolume:(Float32)volume{
    _volume=volume;
    
    UInt32 channel = 0; // Channel 0  is master, if available
    AudioObjectPropertyAddress prop = {
        kAudioDevicePropertyVolumeScalar,
        kAudioDevicePropertyScopeOutput,
        channel
    };
    
    [self checkError:AudioObjectSetPropertyData(defaultOutputDeviceID, &prop, 0, NULL, sizeof(_volume), &_volume)
           operation:NSLocalizedString(@"Could not set system volume", @"")];
    
    prop.mElement = 1;
    [self checkError:AudioObjectSetPropertyData(defaultOutputDeviceID, &prop, 0, NULL, sizeof(_volume), &_volume)
           operation:NSLocalizedString(@"Could not set system volume", @"")];
    
    prop.mElement = 2;
    [self checkError:AudioObjectSetPropertyData(defaultOutputDeviceID, &prop, 0, NULL, sizeof(_volume), &_volume)
           operation:NSLocalizedString(@"Could not set system volume", @"")];
    
}
#endif


#pragma mark - Audio Component Initialization
-(AudioComponentDescription)getInputAudioComponentDescription {
    
    // Create an input component description for mic input
    AudioComponentDescription inputComponentDescription;
    inputComponentDescription.componentType             = kAudioUnitType_Output;
    inputComponentDescription.componentManufacturer     = kAudioUnitManufacturer_Apple;
    inputComponentDescription.componentFlags            = 0;
    inputComponentDescription.componentFlagsMask        = 0;
#if TARGET_OS_IPHONE
    inputComponentDescription.componentSubType          = kAudioUnitSubType_RemoteIO;
#elif TARGET_OS_MAC
    inputComponentDescription.componentSubType          = kAudioUnitSubType_HALOutput;
#endif
    
    // Return the successfully created input component description
    return inputComponentDescription;
    
}

-(AudioComponent)getInputComponentWithAudioComponentDescription:(AudioComponentDescription)audioComponentDescription {
    
    // Try and find the component
    AudioComponent inputComponent = AudioComponentFindNext( NULL , &audioComponentDescription );
    NSAssert(inputComponent,@"Couldn't get input component unit!");
    return inputComponent;
    
}

-(void)createNewInstanceForInputComponent:(AudioComponent)audioComponent withUnit:(AudioUnit*) unit{
    
    [self checkError:AudioComponentInstanceNew(audioComponent, unit)
               operation:NSLocalizedString(@"Couldn't open component for microphone input unit.", @"") ];
    
}


#pragma mark - Input/Output Scope Initialization
-(void)disableOutputScopeForinput:(AudioUnit)unit {
    [self checkError:AudioUnitSetProperty(unit,
                                              kAudioOutputUnitProperty_EnableIO,
                                              kAudioUnitScope_Output,
                                              kAMAudioJackOutputBus,
                                              &kAMAudioJackDisableFlag,
                                              sizeof(kAMAudioJackDisableFlag))
               operation:NSLocalizedString(@"Couldn't disable output on I/O unit.", @"")];
}

-(void)enableInputScopeForInput:(AudioUnit)unit {
    [self checkError:AudioUnitSetProperty(unit,
                                              kAudioOutputUnitProperty_EnableIO,
                                              kAudioUnitScope_Input,
                                              kAMAudioJackInputBus,
                                              &kAMAudioJackEnableFlag,
                                              sizeof(kAMAudioJackEnableFlag))
               operation:NSLocalizedString(@"Couldn't enable output on I/O unit.", @"")];
}

#pragma mark - Pull Default Device (OSX)
#if TARGET_OS_IPHONE
// Not needed, using RemoteIO
-(void)configureDefaultDeviceForInput:(AudioUnit)unit {
}
#elif TARGET_OS_MAC
-(void)configureDefaultDeviceForInput:(AudioUnit)unit {
    // Get the default audio input device (pulls an abstract type from system preferences)
    AudioDeviceID defaultDevice = kAudioObjectUnknown;
    UInt32 propSize = sizeof(defaultDevice);
    AudioObjectPropertyAddress defaultDeviceProperty;
    defaultDeviceProperty.mSelector                  = kAudioHardwarePropertyDefaultInputDevice;
    defaultDeviceProperty.mScope                     = kAudioObjectPropertyScopeGlobal;
    defaultDeviceProperty.mElement                   = kAudioObjectPropertyElementMaster;
    [self checkError:AudioObjectGetPropertyData(kAudioObjectSystemObject,
                                                    &defaultDeviceProperty,
                                                    0,
                                                    NULL,
                                                    &propSize,
                                                    &defaultDevice)
               operation:@"Couldn't get default input device"];
    
    // Set the default device on the microphone input unit
    propSize = sizeof(defaultDevice);
    [self checkError:AudioUnitSetProperty(unit,
                                              kAudioOutputUnitProperty_CurrentDevice,
                                              kAudioUnitScope_Global,
                                              kAMAudioJackOutputBus,
                                              &defaultDevice,
                                              propSize)
               operation:@"Couldn't set default device on I/O unit"];
    
    // Get the stream format description from the newly created input unit and assign it to the output of the input unit
    AudioStreamBasicDescription inputScopeFormat;
    propSize = sizeof(AudioStreamBasicDescription);
    [self checkError:AudioUnitGetProperty(unit,
                                              kAudioUnitProperty_StreamFormat,
                                              kAudioUnitScope_Output,
                                              kAMAudioJackInputBus,
                                              &inputScopeFormat,
                                              &propSize)
               operation:@"Couldn't get ASBD from input unit (1)"];
    
    // Assign the same stream format description from the output of the input unit and pull the sample rate
    AudioStreamBasicDescription outputScopeFormat;
    propSize = sizeof(AudioStreamBasicDescription);
    [self checkError:AudioUnitGetProperty(unit,
                                              kAudioUnitProperty_StreamFormat,
                                              kAudioUnitScope_Input,
                                              kAMAudioJackInputBus,
                                              &outputScopeFormat,
                                              &propSize)
               operation:@"Couldn't get ASBD from input unit (2)"];
    
    // Store the input scope's sample rate
    inputScopeSampleRate = inputScopeFormat.mSampleRate;
}
#endif

#pragma mark - Pull Sample Rate
-(Float64)configureDeviceSampleRateWithDefault:(float)defaultSampleRate {
    Float64 hardwareSampleRate = defaultSampleRate;
#if TARGET_OS_IPHONE
    // Use approximations for simulator and pull from real device if connected
#if !(TARGET_IPHONE_SIMULATOR)
    // Sample Rate
    hardwareSampleRate = [[AVAudioSession sharedInstance] sampleRate];
#endif
#elif TARGET_OS_MAC
    hardwareSampleRate = inputScopeSampleRate;
#endif
    return hardwareSampleRate;
}

#pragma mark - Pull Buffer Duration
-(Float32)configureDeviceBufferDurationWithDefault:(float)defaultBufferDuration {
    
    Float32 bufferDuration = defaultBufferDuration; // Type 1/43 by default
#if TARGET_OS_IPHONE
    // Use approximations for simulator and pull from real device if connected
#if !(TARGET_IPHONE_SIMULATOR)
    
    AVAudioSession *avsession=[AVAudioSession sharedInstance];
    NSError *error;
    
    [avsession setPreferredIOBufferDuration:defaultBufferDuration error:&error];
    if (error) {
#if DEBUG
        NSLog(@"AMObject error: %@", error);
#endif
    }
    bufferDuration = [avsession IOBufferDuration];
#endif
#elif TARGET_OS_MAC
    
#endif
    return bufferDuration;
}

#pragma mark - Pull Buffer Frame Size
-(UInt32)getBufferFrameSizeForUnit:(AudioUnit)unit{
    UInt32 bufferFrameSize;
    UInt32 propSize = sizeof(bufferFrameSize);
    [self checkError:AudioUnitGetProperty(unit,
#if TARGET_OS_IPHONE
                                              kAudioUnitProperty_MaximumFramesPerSlice,
#elif TARGET_OS_MAC
                                              kAudioDevicePropertyBufferFrameSize,
#endif
                                              kAudioUnitScope_Global,
                                              kAMAudioJackOutputBus,
                                              &bufferFrameSize,
                                              &propSize)
               operation:@"Failed to get buffer frame size"];
    return bufferFrameSize;
}


@end
