//
//  AMTagControllerSettings.h
//  Pods
//
//  Created by denis svinarchuk on 20.11.16.
//
//

#import "AMController.h"

@class AMTagDevice;

@interface AMTagControllerSettings : AMControllerSettings
@property (strong,nonatomic) AMBoolean *isTotalMuteOn;
@property (strong,nonatomic) AMBoolean *hasSoundOnImpulse;
@property (strong,nonatomic) AMBoolean *hasVibrationOnImpulse;
@property (strong,nonatomic) AMBoolean *hasVibrationOnThreshold;
@property (strong,nonatomic) AMBoolean *isEnabled;

- (id) initWithAMTagDevice:(AMTagDevice *)tagDevice;

@end
