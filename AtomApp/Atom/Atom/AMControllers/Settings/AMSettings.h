//
//  MESettings.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 11/13/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>

//
// To add new setting, just add a few code lines:
// 1. A new propety
// 2. A new row in ME_DEFAULT_SETTINGS and localize it (optional). The ME_DEFAULT_SETTINGS key must by the same name of property.
//

#define AM_SETTINGS_CHANGED_NOTIFICATION @"ME_SETTINGS_CHANGED_NOTIFICATION"
#define AM_SETTINGS_FLUSH_CACHE_NOTIFICATION @"ME_SETTINGS_FLUSH_CACHE_NOTIFICATION"

#ifdef DEBUG
#define __DEBUG_YES_TO_NO @YES
#else
#define __DEBUG_YES_TO_NO @NO
#endif

@class AMDoseUnit;

@interface MEFlushCache : NSObject
@property(nonatomic, assign) NSInteger size;
- (void) execute;
@end

extern NSDictionary *__settins__();
#define ME_DEFAULT_SETTINGS __settins__()

/**
 *  Application settings.
 */
@interface AMSettings : NSObject

/**
 *  Uints
 */
@property(nonatomic, strong) NSNumber *doseUnit;

/**
 *  Cumulative dose rate threshold.
 */
// @property(nonatomic, strong) NSNumber *thresholdDose;


/**
 *  Searching mode.
 */
//@property(nonatomic, strong) NSNumber *searchingMode;

/**
 *  Confidence Coefficient
 */
@property(nonatomic, strong) NSNumber *trustFactor;

/**
 *  Dose rate duration for avarage measurement.
 */
@property(nonatomic,strong) NSNumber *historyInterval;

/**
 *  Thermometer scale flag
 */
@property(nonatomic, strong) NSNumber *isThermometerScaleDynamic;

/**
 *  Vibrate on threshold rate
 */
@property(nonatomic, strong) NSNumber *isVibrateOn; 

/**
 *  Number of available settings.
 */
- (NSUInteger) count;

/**
 *  Set new preperty value.
 *
 *  @param value new value.
 *  @param number number by order.
 */
- (void) setValue:(id)value forSettingNo:(NSInteger)number;
- (void) setValue:(id)value forSettingNo:(NSInteger)number inCurrentUnit:(BOOL)unit;
- (void) setValue:(id)value forSettingName:(NSString*)name;

/**
 *  Creat one shared instance.
 *
 *  @return shared application settings.
 */
+ (id) sharedInstance;

//
// Helper methods
//

/**
 *  Get boolean value of property.
 *
 *  @param number number by order.
 *
 *  @return YES/NO.
 */
- (BOOL) boolForSettingNo:(NSInteger)number;

/**
 *  Get boolean value.
 *
 *  @param name property name.
 *
 *  @return YES/NO.
 */
- (BOOL) boolForSettingName:(NSString*)name;

/**
 *  Get property object.
 *
 *  @param number number by order.
 *
 *  @return property.
 */
- (id) valueForSettingNo:(NSInteger)number;
- (id) valueForSettingNo:(NSInteger)number inCurrentUnit:(BOOL)current;
- (id) sourceForSettingNo:(NSInteger)number;
- (NSString*) proertyNameForSettingNo:(NSInteger)number;

/**
 *  Get property object.
 *
 *  @param name property name.
 *
 *  @return property.
 */
- (id) valueForSettingName:(NSString*)name;


/**
 *  Get property localized caption.
 *
 *  @param number number by order.
 *
 *  @return localized string.
 */
- (NSString*) captionForSettingNo:(NSUInteger)number;
- (NSString*) typeForSettingNo:(NSUInteger)number;
- (id) selectedSettingNo:(NSUInteger)number;
- (int) selectedIndexForSettingNo:(NSUInteger)number;
- (int) selectedIndexForSettingName:(NSString*)name;

/**
 *  Get property localized caption by name.
 *
 *  @param name name.
 *
 *  @return localized string.
 */
- (NSString*) captionForSettingName:(NSString*)name;
- (id) selectedSettingForSettingName:(NSString*)name;
- (id) sourceForSettingName:(NSString*)name;

@end
