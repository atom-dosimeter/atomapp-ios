//
//  AMInfoViewController.m
//  Atom
//
//  Created by denis svinarchuk on 18.07.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMInfoViewController.h"
#import "AMUIPreferences.h"

@interface AMInfoViewController()<UIWebViewDelegate>

@end

@implementation AMInfoViewController

- (void) viewDidLoad{
    [super viewDidLoad];
    
    self.activityView.alpha = 0.0;
    self.webView.delegate = self;
    
    NSString *languageCode = [[NSLocale preferredLanguages] objectAtIndex:0];
    if (languageCode==nil) {
        languageCode=@"en";
    }
    else if ([languageCode hasPrefix:@"ru"]) {
        languageCode = @"ru";
    }
    
    NSString *docstring = [NSString stringWithFormat:@"Effective-dose-radiation-%@", languageCode];
    
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:docstring ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];

    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"@ATOM_VERSION@" withString:ME_APP_VESRION_INFO];
    
    [self.webView loadHTMLString:htmlString baseURL:nil];
    
    [self.tabBarController setPreferredContentSize:CGSizeZero];
}

- (void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    NSLog(@"AMInfoViewController: %@", error);
}


- (void) webViewDidStartLoad:(UIWebView *)webView{
    [UIView animateWithDuration:AM_UI_ANIMATE_DURATION animations:^{
        self.activityView.alpha = 1.0;
    }];
    [self.activityView startAnimating];
}

- (void) webViewDidFinishLoad:(UIWebView *)webView{
    [UIView animateWithDuration:AM_UI_ANIMATE_DURATION animations:^{
        self.activityView.alpha = 0.0;
    }
     completion:^(BOOL finished) {
         [self.activityView stopAnimating];
     }];
}


- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    return YES;
}



@end
