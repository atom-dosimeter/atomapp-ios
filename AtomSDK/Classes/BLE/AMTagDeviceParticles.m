//
//  AMTagDeviceCalibration.m
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 10.10.14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMTagDeviceParticles.h"

#define AM_CHARACTERISTIC_CALIBRATION_UUID    @"E2423A67-7541-4080-8B5A-59449454A873"

typedef struct {
    char command;
    char particleCounts[16];
} AMTagDeviceCalibrationData;


@interface AMTagDeviceParticleCount()
@property (assign,nonatomic) NSTimeInterval timeStamp;
@property (assign,nonatomic) uint8_t        particles;
@end

@implementation AMTagDeviceParticleCount
@end

@interface AMTagDeviceParticles ()
@property (assign,nonatomic) char command;
@property (nonatomic,strong) NSMutableArray *particleCountsMutable;
@end

@implementation AMTagDeviceParticles

+ (CBUUID*) identifier{
    return [CBUUID UUIDWithString:AM_CHARACTERISTIC_CALIBRATION_UUID];
}


- (id) initWithData:(NSData *)data{
    
    self = [super init];
    
    if (self) {
        [self readData:data];
    }
    
    return self;    
}

- (void) readData:(NSData *)data{
    AMTagDeviceCalibrationData *input_data = (AMTagDeviceCalibrationData *)[data bytes];
    memcpy(&_command, &(input_data->command), sizeof(input_data->command));
    _particleCountsMutable = [NSMutableArray arrayWithCapacity:8];
    
    NSTimeInterval tm= [NSDate timeIntervalSinceReferenceDate]-2.;
    for (int i=0; i<sizeof(input_data->particleCounts); i+=2,tm-=2.0) {
        uint8_t particles;
        char    *p = &(input_data->particleCounts[i]);
        memcpy(&particles , p, sizeof(particles));
        particles = CFSwapInt16LittleToHost(particles);
        
        AMTagDeviceParticleCount *pc = [[AMTagDeviceParticleCount alloc] init];
        pc.timeStamp = tm;
        pc.particles = particles;
        
        [_particleCountsMutable addObject:pc];
    }
}

- (NSArray*) particleCounts{
    return _particleCountsMutable;
}

- (NSData*) data{
    AMTagDeviceCalibrationData output_data; 
    memset(&output_data, 0, sizeof(output_data));
    return [[NSData alloc] initWithBytes:&output_data length:sizeof(output_data)];
} 

- (NSString*) description{
    NSMutableString *s = [[NSMutableString alloc] init];
    
    for (AMTagDeviceParticleCount *p in self.particleCounts) {
        [s appendFormat:@"[%i : %f], ",p.particles,p.timeStamp];
    }
    
    return [NSString stringWithFormat: @"\n\
            command                = %x\n\
            particles              = %@\n",
            self.command,
            s
            ];
}

@end
