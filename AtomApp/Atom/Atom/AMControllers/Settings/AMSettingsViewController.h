//
//  MESettingsViewController.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 11/13/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMSettingsViewController : UITableViewController
- (IBAction)setSwitchFlag:(UISwitch*)sender;
- (void)tapEvent:(id)sender;
@end
