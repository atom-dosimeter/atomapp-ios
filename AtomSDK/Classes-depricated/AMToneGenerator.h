//
//  AMToneGenerator.h
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 24/06/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "AMObject.h"

typedef enum {
    AM_SIGNAL_TYPE_SQUARE,
    AM_SIGNAL_TYPE_SINE
}AMSignalType;

/**
 * AMToneGenerator is a part of Atom Measurer project. This class needs to control hardware power supply for the "Atom Measurer" device via headphones output.
 * "Atom Measurer" is a project to create a set of several devices which meter different types of radiation.
 * Tone generator processes sine/square wave to headphones output for Atome Simple device which uses the signal as a source for electircal supply. 
 * After that device should response sreies of meandrs to detect device state: lack of power or ready(?). In case of responsed state app should correct tone frequency and volume.
 *
 *  @since version number 0.1
 */
@interface AMToneGenerator : AMObject

/**
 *  Tone frequency, should be between 12000-15000Hz. By default is 14000.
 *
 */
@property(assign,atomic) Float32 frequency;
/**
 *  Sample rate. By default is 44100.
 *
 */
@property(assign,atomic) Float32 sampleRate;
/**
 *  Signal form. By default is sine.
 *
 */
@property(assign,atomic) AMSignalType type;
/**
 *  Tone generator sate.
 *
 */
@property(assign,readonly,atomic) BOOL isRunning;

- (void) start;
- (void) stop;

@end
