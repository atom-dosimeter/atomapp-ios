//
//  AMErrorHandlerProtocol.h
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 26/08/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AMErrorHandlerProtocol <NSObject>
@optional
- (void) amObject: (id)amobject withError:(NSError*) error;
@end
