//
//  AMMeasuringViewController.h
//  Atom
//
//  Created by denis svinarchuk on 20/10/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMBaseDosimeterMainViewController.h"

@interface AMMeasuringViewController : AMBaseDosimeterMainViewController

@end
