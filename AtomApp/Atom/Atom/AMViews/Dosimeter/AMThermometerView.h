//
//  AMThermometerView.h
//  Atom
//
//  Created by denis svinarchuk on 21/07/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AMLog10View.h"

@class AMThermometerView;

@protocol AMThermometerViewDelegate <NSObject>

- (void) amThermometerView:(AMThermometerView*)thermometer didEndUpdatingThresholdDoseRate:(NSNumber*)thresholdDoseRate;
- (void) amThermometerView:(AMThermometerView*)thermometer didContinueUpdatingThresholdDoseRate:(NSNumber*)thresholdDoseRate;

@end

@interface AMThermometerView : AMLog10View

@property (nonatomic,assign) float doseRate;
@property (nonatomic,assign) float sigma;
@property (nonatomic,assign) float trustFactor;
@property (nonatomic,assign) BOOL hasDynamicScale;
@property (nonatomic,strong) NSNumber* thresholdDoseRate;

@property (strong,nonatomic) id<AMThermometerViewDelegate> delegate;

- (void) reload;

@end
