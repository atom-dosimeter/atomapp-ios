//
//  AMTagDeviceInfo.h
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 18/09/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//
//
//  https://developer.bluetooth.org/gatt/services/Pages/ServicesHome.aspx
//
//

#import "AMBLEProtocol.h"

#define AM_SERVICE_DEVICE_INFO_UUID @"0x180A"

@interface AMTagDeviceInfoCommon : NSObject
@property (readonly,nonatomic) NSString *value;
@property (readonly,nonatomic) NSNumber *numberValue;

- (id) initWithData:(NSData*)data;
- (void) readData:(NSData*)data;

@end

@interface AMTagDeviceModelString : AMTagDeviceInfoCommon <AMBLEProtocol>
@end

@interface AMTagDeviceFirmwareVersion : AMTagDeviceInfoCommon <AMBLEProtocol>
@end

@interface AMTagDeviceSoftwareVersion : AMTagDeviceInfoCommon <AMBLEProtocol>
@end

@interface AMTagDeviceSerailString : AMTagDeviceInfoCommon <AMBLEProtocol>
@end

@interface AMTagDeviceInfo : NSObject

@property (readonly,nonatomic) NSNumber *firmwareNumber;
@property (readonly,nonatomic) NSNumber *softwareNumber;
@property (readonly,nonatomic) NSString *modelString;
@property (readonly,nonatomic) NSString *serialString;

- (void) readFirmwareNumberFromData:(NSData*)data;
- (void) readSoftwareNumberFromData:(NSData*)data;

- (void) readSerialStringFromData:(NSData*)data;
- (void) readModelStringFromData:(NSData*)data;
@end
