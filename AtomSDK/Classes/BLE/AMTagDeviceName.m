//
//  AMTagDeviceName.m
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 29/09/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMTagDeviceName.h"

@implementation AMTagDeviceName
    
- (id) initWithData:(NSData *)data{
    self = [super init];
    
    if (self) {
        _isReadOnly = NO;
        [self readData:data];
    }
    
    return self;
}
    
    
- (id) initWithCharacteristic:(CBCharacteristic *)characteristic{
    self = [super init];
    
    if (self) {
        _isReadOnly = NO;

        [self readData:characteristic.value];
        if ([_name isEqualToString:@""]) {
            _name = characteristic.service.peripheral.name;
        }
        if (
            [_name hasSuffix:@"Swift"]
            ||
            [_name hasSuffix:@"Fast"]
            ){
            _isReadOnly = YES;
        }
    }
    
    return self;
}
    
    
- (void) readData:(NSData *)data{
    _name = [NSString stringWithCString:[data bytes] encoding:NSASCIIStringEncoding];
}
    
- (NSData*) data{
    char name[20]; memset(name, 0, sizeof(name));
    [_name getCString:name maxLength:sizeof(name) encoding:NSASCIIStringEncoding];
    return [NSData dataWithBytes:name length:sizeof(name)];
}
    
+ (CBUUID*) identifier{
    return [CBUUID UUIDWithString:@"BB6C9C06-C37D-49B0-94CA-83623622573B"];
}
    
- (void) setName:(NSString *)name{
    _name = [name substringWithRange:NSMakeRange(0, MIN(19, name.length))];
}
    
    @end
