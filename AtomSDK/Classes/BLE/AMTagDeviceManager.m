//
//  AMTagDeviceManager.m
//  Atom
//
//  Created by denis svinarchuk on 22.08.14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMTagDeviceManager.h"
#import "AMTagDeviceMeasurement.h"
#import "AMConstants.h"

#import <CoreFoundation/CoreFoundation.h>
#if TARGET_OS_IPHONE
#import <CoreBluetooth/CoreBluetooth.h>
#elif TARGET_OS_MAC
#import <IOBluetooth/IOBluetooth.h>
#endif

@interface AMTagDeviceDisconnectionInfo : NSObject
@property (nonatomic,weak) AMTagDevice *device;
@property (nonatomic,strong) NSError   *error;
@end


@implementation AMTagDeviceDisconnectionInfo
@synthesize device=_device, error=_error;
@end

@interface AMTagDeviceManager() <CBCentralManagerDelegate, CBPeripheralDelegate>
/**
 *
 */
@property (retain,   nonatomic)   dispatch_queue_t      centralQueue;
@property (retain,   nonatomic)   dispatch_queue_t      delegateQueue;
@property (strong,   nonatomic)   CBCentralManager      *centralManager;
@property (strong,   nonatomic)   AMList                *devicesPivate;
@property (readonly, nonatomic)   NSArray               *serviceUUIDs;

@end

static AMTagDeviceManager *__static_instance = nil;

@implementation AMTagDeviceManager
{
    NSInteger lastDevicesCount;
    NSTimer   *processPeripheralsTimer;
}

+ (id) sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __static_instance = [[AMTagDeviceManager alloc] init];
    });
    
    return __static_instance;
}

- (id) init{
    if (__static_instance) {
        self = __static_instance;
        return self;
    }
    
    self = [super init];
    return self;
}


- (void) start{
    
    NSAssert([self.delegate respondsToSelector:@selector(tagDeviceManager:willConnectWithDevice:)], @"AMTagDeviceManager: delegate must implement - tagDeviceManager:willConnectWithDevice");
    
    if (!_centralManager) {
        lastDevicesCount = 0;
        _centralManager = [[CBCentralManager alloc] initWithDelegate:self
                                                               queue:self.centralQueue
                                                             options:@{
                                                                       CBCentralManagerOptionRestoreIdentifierKey: AM_SERVICE_UUID,
                                                                       CBCentralManagerOptionShowPowerAlertKey: @(NO)
                                                                       }];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(tagDeviceManagerWillActive:)]) {
            [self performDelegate:@selector(tagDeviceManagerWillActive:) withBlock:^{
                [self.delegate tagDeviceManagerWillActive:self];
            }];
        }
    }
}

- (void) stop{
    if (_centralManager) {
        [self cleanup];
        _centralManager = nil;
    }
}

- (NSMutableDictionary*) devicesPivate{
    if (!_devicesPivate) {
        _devicesPivate = [[AMList alloc] init];
    }
    return _devicesPivate;
}

- (NSDictionary*) devices{
    return self.devicesPivate;
}

- (NSArray*) serviceUUIDs{
    return [AMTagDevice servicesUUIDs];
}

- (dispatch_queue_t) centralQueue   {
    if (!_centralQueue) {
        _centralQueue = dispatch_queue_create("com.atom.ble.central.manager", DISPATCH_QUEUE_SERIAL);
    }
    return _centralQueue;
}

- (dispatch_queue_t) delegateQueue{
    if (!_delegateQueue) {
        _delegateQueue = dispatch_queue_create("com.atom.ble.central.delegate", DISPATCH_QUEUE_SERIAL);
    }
    return _delegateQueue;
}

-(void) performDelegate:(SEL)delegate withBlock:(void(^)(void))block{
    if (self.delegate && [self.delegate respondsToSelector:delegate]) {
        dispatch_async(self.delegateQueue, ^{
            block();
        });
    }
}


- (void) stopScanDevices{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [NSObject cancelPreviousPerformRequestsWithTarget:self
                                                 selector:@selector(stopScanDevices)
                                                   object:nil];
        dispatch_async(self.centralQueue, ^{
            [self.centralManager stopScan];
        });
    });
}

- (void) startScanDevices{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if ([self.serviceUUIDs count]>0) {
            dispatch_async(self.centralQueue, ^{
                [self.centralManager scanForPeripheralsWithServices:self.serviceUUIDs
                                                            options:@{
                                                                      CBCentralManagerScanOptionAllowDuplicatesKey : @YES
                                                                      }];
            });
        }
        
        processPeripheralsTimer = [NSTimer scheduledTimerWithTimeInterval:kAMBLERescaninginterval target:self selector:@selector(processPeripherals:) userInfo:nil repeats:NO];

    });
}

- (void) processPeripherals:(NSTimer*)timer{

    if (processPeripheralsTimer) {
        [processPeripheralsTimer invalidate];
    }

    dispatch_async(self.centralQueue, ^{
        
        BOOL isScanninhStopped = NO;
        
        
        for (AMTagDevice *device in [self.devices allValues]) {
            if (device.peripheral.state != CBPeripheralStateConnected) {
                
                if (!isScanninhStopped){
                    [self stopScanDevices];
                    isScanninhStopped = YES;
                }
                
                [self.centralManager connectPeripheral:device.peripheral options:nil];
            }
        }
        
        
        if (isScanninhStopped)
            [self startScanDevices];
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                processPeripheralsTimer = [NSTimer scheduledTimerWithTimeInterval:kAMBLERescaninginterval target:self selector:@selector(processPeripherals:) userInfo:nil repeats:NO];
            });
        }
    });
}

- (void)scanForPeripheralsByInterval:(NSUInteger)aScanInterval
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if ([self.serviceUUIDs count]>0) {
            dispatch_async(self.centralQueue, ^{
                [self.centralManager scanForPeripheralsWithServices:self.serviceUUIDs
                                                            options:@{
                                                                      CBCentralManagerScanOptionAllowDuplicatesKey : @YES
                                                                      }];
            });
        }
        
        [NSObject cancelPreviousPerformRequestsWithTarget:self
                                                 selector:@selector(stopScanDevices)
                                                   object:nil];
        
        [self performSelector:@selector(stopScanDevices)
                   withObject:nil
                   afterDelay:aScanInterval];
    });
}

- (void)centralManager:(CBCentralManager *)central willRestoreState:(NSDictionary *)state {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSArray *peripherals = state[CBCentralManagerRestoredStatePeripheralsKey];
        dispatch_async(self.centralQueue, ^{

            if (central.state == CBCentralManagerStatePoweredOn) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(tagDeviceManagerDidActive:)]) {
                    [self performDelegate:@selector(tagDeviceManagerDidActive:) withBlock:^{
                        [self.delegate tagDeviceManagerDidActive:self];
                    }];
                }
                
                for (CBPeripheral *peripheral in peripherals) {
                    
                    [self addDevice:peripheral];
                    
                    AMTagDevice *device = [self peripheralToDevice:peripheral];
                    
                    [self.delegate tagDeviceManager:self willConnectWithDevice:device];
                    
                    if (peripheral.state == CBPeripheralStateConnected) {
                        [device cancelConnection];
                    }
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), self.centralQueue, ^{
                        [self.centralManager connectPeripheral:peripheral options:nil];
                    });
                    
                }
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), self.centralQueue, ^{
                    [self startScanDevices];
                });
            }
        });
    });
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    
    if (central.state == CBCentralManagerStatePoweredOn) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(tagDeviceManagerDidActive:)]) {
            [self performDelegate:@selector(tagDeviceManagerDidActive:) withBlock:^{
                [self.delegate tagDeviceManagerDidActive:self];
            }];
        }
        [self startScanDevices];
    }
    else if (central.state == CBCentralManagerStatePoweredOff) {
        [self stopScanDevices];

        for (AMTagDevice *device in [self.devices allValues]) {
            if (device) {
                if (device.delegate && [device.delegate respondsToSelector:@selector(tagDevice:didReady:error:)]) {
                    dispatch_async(self.delegateQueue, ^{
                        [device.delegate tagDevice:device didReady:NO error:nil];
                    });
                }
            }
            
            [device cleanup];
            [device cancelConnection];
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(tagDeviceManager:didActiveFail:)]) {
            [self performDelegate:@selector(tagDeviceManager:didActiveFail:) withBlock:^{
                [self.delegate tagDeviceManager:self didActiveFail:central.state];
            }];
        }
    }
}

- (AMTagDevice*) peripheralToDevice:(CBPeripheral*)peripheral{
    NSString     *UUIDString = [peripheral.identifier UUIDString];
    return [self.devicesPivate objectForKey:UUIDString];
}

- (BOOL) cleanupDevice:(CBPeripheral*)peripheral{
    AMTagDevice *currentDevice = [self peripheralToDevice:peripheral];
    if (currentDevice) {
        [currentDevice cleanup];
        return YES;
    }
    return NO;
}

- (BOOL) forgetDevice:(NSString *)UUIDString{
    AMTagDevice *device = [self.devicesPivate objectForKey:UUIDString];
    if (device) {
        [self.devicesPivate removeObjectForKey:UUIDString];
        
        lastDevicesCount = self.devices.count;
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(tagDeviceManagerDidUpdateDeviceList:)]) {
            [self performDelegate:@selector(tagDeviceManagerDidUpdateDeviceList:) withBlock:^{
                [self.delegate tagDeviceManagerDidUpdateDeviceList:self];
            }];
        }
        
        [device cleanup];
        [device cancelConnection];
        return YES;
    }
    return NO;
};

- (BOOL) addDevice:(CBPeripheral *)peripheral{
    
    //NSLog(@"add device = %@", peripheral.name);
    
    NSString     *UUIDString = [peripheral.identifier UUIDString];
    AMTagDevice *device = [self peripheralToDevice:peripheral];
    
    if (!device){
        
        device = [[AMTagDevice alloc] initWithPeripheral:peripheral withCentralManager:_centralManager];
        
        [self.devicesPivate setObject:device forKey:UUIDString];
        
        lastDevicesCount = self.devices.count;

        if (self.delegate && [self.delegate respondsToSelector:@selector(tagDeviceManagerDidUpdateDeviceList:)]) {
            [self performDelegate:@selector(tagDeviceManagerDidUpdateDeviceList:) withBlock:^{
                [self.delegate tagDeviceManagerDidUpdateDeviceList:self];
            }];
        }

        return NO;
    }
    
    return YES;
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    
    ///NSLog(@"advertisementData = %@",advertisementData);
    
    BOOL startConnect = [self addDevice:peripheral];
    AMTagDevice *device = [self peripheralToDevice:peripheral];
    
    
    if (device && device.delegate==nil) {
        //
        // Must be defined to set device delegate !!!
        //
        [self.delegate tagDeviceManager:self willConnectWithDevice:device];

        NSAssert([device.delegate respondsToSelector:@selector(tagDevice:didMeasurementUpdate:)], @"AMTagDevice: delegate must implement - tagDevice:didMeasurementUpdate:");
        NSAssert([device.delegate respondsToSelector:@selector(tagDevice:didPreferencesUpdate:)], @"AMTagDevice: delegate must implement - tagDevice:didPreferencesUpdate:");

        NSAssert([device.delegate respondsToSelector:@selector(tagDeviceDidSynchronize:)], @"AMTagDevice: delegate must implement - tagDeviceDidSynchronize:");
        NSAssert([device.delegate respondsToSelector:@selector(tagDeviceWillSynchronize:)], @"AMTagDevice: delegate must implement - tagDeviceWillSynchronize:");
    }
    
    if (startConnect) {
        //
        // Start connection for new peropheral device
        
        if (device.delegate && [device.delegate respondsToSelector:@selector(tagDeviceShouldBeDiscovered:)]) {
            if ([device.delegate tagDeviceShouldBeDiscovered:device]==NO) {
                //
                // do not connect to device
                //                
                return;
            }
        }
        
        [_centralManager connectPeripheral:peripheral options:nil];        
    }
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {

    AMTagDevice *device = [self peripheralToDevice:peripheral];
    
    NSLog(@" #### didFailToConnectPeripheral[%@] = %@", peripheral, error);
    
    if (device) {
        if (device.delegate && [device.delegate respondsToSelector:@selector(tagDevice:didReady:error:)]) {
            dispatch_async(self.delegateQueue, ^{
                [device.delegate tagDevice:device didReady:NO error:error];
            });
        }
        [self cleanupDevice:peripheral];
    }
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    
    AMTagDevice *device = [self peripheralToDevice:peripheral];
    
    if (!device)
        [self addDevice:peripheral];
    
    //
    // Discover new peripheral
    //
    [device discoverServices];

}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    AMTagDevice *device = [self peripheralToDevice:peripheral];
    
    if (device) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (device.disconnectionTimer) {
                [device.disconnectionTimer invalidate];
            }
        });
                       
        if (device.delegate && [device.delegate respondsToSelector:@selector(tagDevice:didReady:error:)]) {
            
            if (device.delegate
                &&
                [device.delegate respondsToSelector:@selector(tagDeviceShouldBeDiscovered:)]
                &&
                [device.delegate tagDeviceShouldBeDiscovered:device] == YES
                ) {
                //
                // if device is enabled
                //
                
                dispatch_async(self.delegateQueue, ^{
                    [device.delegate tagDevice:device didReady:NO error:error];
                });
                
                [device cleanup];
                [device cancelConnection];

            }
            else{
                dispatch_async(self.delegateQueue, ^{
                    [device.delegate tagDevice:device didReady:NO error:error];
                });
            }
        }
    }
}

- (void)cleanup {
    
    dispatch_async(self.centralQueue, ^{
        
        // See if we are subscribed to a characteristic on the peripheral
        
        for (AMTagDevice *device in [self.devicesPivate allValues]) {
            [device cleanup];
            [device cancelConnection];
        }
        
        [self.devicesPivate removeAllObjects];
        
    });
}

@end
