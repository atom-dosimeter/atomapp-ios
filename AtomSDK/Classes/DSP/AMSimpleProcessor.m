//
//  AMSimpleProcessor.m
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 10/09/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMSimpleProcessor.h"
#import "AMConstants.h"

static AMSimpleProcessor *__shared_processor = nil;

@interface AMSimpleProcessor()
@property (nonatomic, assign, readwrite) float *signalsBuffer;

@property (nonatomic, assign, readwrite) float *lowPowerData;
@property (nonatomic, assign, readwrite) int    lowPowerCount;

@property (nonatomic, assign, readwrite) float *lowPowerDetectionLevelData;

@end

@implementation AMSimpleProcessor
{
    BOOL          isFftSetup;
    FFTSetup      fftSetup;
    float         *fftWindow;
    vDSP_Length   fftLog2N;
    vDSP_Length   fftSamplesCount;
    COMPLEX_SPLIT fftA;
    
    dispatch_queue_t queueDelegate;
}

+ (id) sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __shared_processor = [[AMSimpleProcessor alloc] init];
    });
    return __shared_processor;
}


- (void) setuptFFT:(UInt32)numberOfSamples{
    
    if (isFftSetup) {
        return;
    }
    
    // Setup the length
    fftLog2N = log2f(numberOfSamples);
    
    // Calculate the weights array. This is a one-off operation.
    fftSetup = vDSP_create_fftsetup(fftLog2N, FFT_RADIX2);
    
    // Populate *window with the values for a hamming window function
    fftWindow = (float *)malloc(sizeof(float) * numberOfSamples);
    vDSP_hamm_window(fftWindow, numberOfSamples, 0);
    
    // Define complex buffer
    fftA.realp = (float *) malloc(fftSamplesCount*sizeof(float));
    fftA.imagp = (float *) malloc(fftSamplesCount*sizeof(float));
    
    isFftSetup = YES;
}

- (void) deallocFft{
    if (isFftSetup) {
        vDSP_destroy_fftsetup(fftSetup);
        fftSetup = NULL;
        free(fftWindow);
        fftWindow = NULL;
        
        free(fftA.imagp);
        free(fftA.realp);
    }
    isFftSetup = NO;
}

- (void) dealloc{
    if (_signalsBuffer) {
        free(_signalsBuffer); _signalsBuffer = nil;
    }
    
    if (_lowPowerData){
        free(_lowPowerData); _lowPowerData = nil;
        _lowPowerCount = 0;
    }
    
    [self deallocFft];
}

- (void) setFftNumberOfInputSamples:(uint32_t)fftNumberOfInputSamples
{
    _fftNumberOfInputSamples = fftNumberOfInputSamples;
    
    // For an FFT, numSamples must be a power of 2, i.e. is always even
    fftSamplesCount = _fftNumberOfInputSamples/2;
    
    if (isFftSetup) {
        [self deallocFft];
    }
    [self setuptFFT:_fftNumberOfInputSamples];
}

- (BOOL) isRuning{
    return self.audioManager.isRunningInput;
}

- (id) init{
    if (__shared_processor) {
        self=__shared_processor;
        return self;
    }
    
    self = [super init];
    
    if (self) {
        
        queueDelegate = dispatch_queue_create("com.atom.simple.processor", DISPATCH_QUEUE_SERIAL);
        
        _powerStartUpTime = kAMDosimeterStartUpTime;

        self.fftNumberOfInputSamples = kAMAudioBufferSize;
        
        _lowPowerData = (float *)calloc(kAMAudioBufferSize, sizeof(float));
        _lowPowerCount = 0;
        
        _signalsBuffer  = (float *)calloc(self.audioManager.inputBufferSize, sizeof(float));
        _lowPowerDetectionLevelData = (float *)calloc(self.audioManager.inputBufferSize, sizeof(float));
        
        __block BOOL lastImpulseWasAtTheEnd=NO;
        __block vDSP_Length lastImpulseCount = 0;
        __block float zero = 0;
        __block int lowPowerDetection = 0;
        __block  BOOL isLowPowerAlreadyDetected = NO;
        
        [self.audioManager setInputBlock:^(AMAudioManager *manager, AMAudioManagerBuffer *ioBuffer) {
            
            UInt32  numberOfFrames = ioBuffer->numberOfFramesPerChannel;
            float  *ioData = ioBuffer->buffer[0];

            //
            // allign floats to absolute value
            //
            vDSP_vabs(ioData, 1, ioData, 1, numberOfFrames);
            
#if TARGET_OS_IPHONE
#elif TARGET_OS_MAC
            //
            // under OSX find the maximum amplitude
            //
            
            float max=0;

            vDSP_maxv(ioData, 1, &max, numberOfFrames);
            
            //
            // OSX float value may be great then 1.
            // scale vector to 1.
            //
            float scale = 1.0 / kAMHighInputAmplitude;
            if (scale<1.){
                vDSP_vsmul(ioData, 1, &scale, ioData, 1, numberOfFrames);
            }
#endif
            //
            // cut all impulses lower then amplitude with particle detected level
            //
            float one=1.0; // normalize to 1.
            vDSP_vlim(ioData, 1, &kAMPeakSignalAmplitude, &one, _signalsBuffer, 1, numberOfFrames);
            
            //
            // count zerro crossing levels
            //
            vDSP_Length Cl=0,Dl=0,Bl=numberOfFrames;
            
            // count crossing zerro
            vDSP_nzcros(_signalsBuffer, 1, Bl, &Cl, &Dl, numberOfFrames);
            
            if (Cl>0 || Dl>0){
                //
                //  number of impulses == zero crosses/2
                //
                if (Dl%2==1) {
                    if (!lastImpulseWasAtTheEnd) {
                        //
                        // impulse has been deided between 2 imput buffers
                        //
                        lastImpulseCount = Dl;
                        lastImpulseWasAtTheEnd=YES;
                    }
                    else {
                        //
                        // we have the end of impulse detected in the previuos buffer
                        //
                        lastImpulseCount+=Dl;
                        lastImpulseCount=lastImpulseCount/2;
                        lastImpulseWasAtTheEnd=NO;
                    }
                }
                else {
                    if (lastImpulseWasAtTheEnd) {
                        //
                        // previuos impulse has fall at the and of the buffer
                        // but the current buffer has new impulse
                        //
                        lastImpulseCount=lastImpulseCount/2+1+Dl/2;
                    }
                    else{
                        lastImpulseCount = Dl/2;
                    }
                    lastImpulseWasAtTheEnd = NO;
                }
                
                if (lastImpulseCount>0) {
                    dispatch_async(queueDelegate, ^{
                        
                        int peaks = 0;
                        
                        //
                        // Normalize frame
                        //
                        
                        int   current_samples_count = 0;
                        
                        for (int frame = 0, prev_frame=-1; frame < numberOfFrames; ++frame){
                            Float32 sample = ioData[frame];
                                                        
                            if (sample>=kAMPeakSignalAmplitude) {
                                current_samples_count++;
                                if (prev_frame+1==frame) {
                                    // find one impulse
                                    if (peaks==0) {
                                        peaks = 1;
                                    }
                                }
                                else{
                                    // find next impulse
                                    peaks++;
                                    current_samples_count = 0;
                                }
                                prev_frame = frame;
                            }
                        }

                        if (self->_delegate) {                            
                            [self->_delegate amSimpleProcessor:self hasLocalPeaks:lastImpulseCount];                                                    
                        }
                    });
                }
            }
            
            //
            // before starting the FFT detection low level analysis we can clip input samples vector 
            // between kAMLowPowerAmplitudeMinLevel and kAMLowPowerAmplitudeMaxLevel
            //
            vDSP_Length lows=0, highs=0;
            vDSP_vclipc(ioData, 1, &kAMLowPowerAmplitudeMinLevel, &kAMLowPowerAmplitudeMaxLevel, _lowPowerDetectionLevelData, 1, numberOfFrames, &lows, &highs);
            BOOL do_analysis=NO;
            if (lows<numberOfFrames){
                //
                // a noise or low level periodical meander has detected
                // meander can be distorted by hardware frequances filter, 
                // so we should analyse input spectrum instead of input level or its form
                //
                do_analysis = YES;
            }
            
            //
            // FFT low level analysis
            //
            vDSP_Length fftNumberSamples=self->_fftNumberOfInputSamples;

            if ((_lowPowerCount+numberOfFrames)>fftNumberSamples) {
                
                if (do_analysis) {
                    //
                    // To detect low power signal we should pick out ~150Hz fft-bid only
                    //
                    
                    // Window the samples
                    vDSP_vmul(_lowPowerData, 1, fftWindow, 1, _lowPowerData, 1, fftNumberSamples);
                    
                    // Pack samples:
                    // C(re) -> A[n], C(im) -> A[n+1]
                    vDSP_ctoz((COMPLEX*)_lowPowerData, 2, &fftA, 1, fftSamplesCount);
                    
                    //
                    // Run FFT
                    //
                    
                    //
                    //Perform a forward FFT using fftSetup and A
                    //Results are returned in A
                    //
                    vDSP_fft_zrip(fftSetup, &fftA, 1, fftLog2N, FFT_FORWARD);
                    
                    //
                    //Convert COMPLEX_SPLIT A result to magnitudes
                    //
                    
                    float freq_step = manager.samplingRate/fftSamplesCount;
                    float freq = freq_step;
                    
                    int   lowPowerFreqIndex = kAMLowPowerFrequencyMax/freq_step;
                    int   lowPowerFreqIndex_first_analize_index = lowPowerFreqIndex-2;
                    lowPowerFreqIndex_first_analize_index = lowPowerFreqIndex_first_analize_index<0?0:lowPowerFreqIndex_first_analize_index;
                    int   lowPowerFreqIndex_last_analize_index = lowPowerFreqIndex+2;
                    lowPowerFreqIndex_last_analize_index = lowPowerFreqIndex_last_analize_index>=fftSamplesCount?(int)(fftSamplesCount-1):lowPowerFreqIndex_last_analize_index;
                    
                    float power = 0;
                    for(int i=lowPowerFreqIndex_first_analize_index; i<lowPowerFreqIndex_last_analize_index; i++) {
                        float m = fftA.realp[i]*fftA.realp[i]+fftA.imagp[i]*fftA.imagp[i];
                        power = power + m;
                        freq=freq_step*i;
                    }
                    
                    //
                    // This rule is based on assumption the follow fact: SUMM(fft amplitudes on area of analysis frequency ±2Fs/n)/Ninput_samples == input signals amplitude
                    // but we should remember that wa has already applied hamming window to input samples and decline whole power amplitute  
                    // so, this assumption follows from the experimental measurement in the concrete case, and it is very approximate, but it works.
                    //
                    BOOL lowdetected = (power/fftNumberSamples>=kAMLowPowerAmplitudeMinLevel);
                    
                    if (lowdetected) {
                        if (kAMLowPowerFalseAlarmCount<=lowPowerDetection) {
                            if (!isLowPowerAlreadyDetected) {
                                dispatch_async(queueDelegate, ^{
                                    [self.delegate amSimpleProcessor:self hasLowPower:YES];
                                });
                            }
                            isLowPowerAlreadyDetected = YES;
                        }
                        lowPowerDetection++;
                    }
                    else{
                        lowPowerDetection=0;
                        if (isLowPowerAlreadyDetected) {
                            dispatch_async(queueDelegate, ^{
                                [self.delegate amSimpleProcessor:self hasLowPower:NO];
                            });
                        }
                        isLowPowerAlreadyDetected = NO;
                    }
                }
                else {
                    if (isLowPowerAlreadyDetected) {
                        dispatch_async(queueDelegate, ^{
                            [self.delegate amSimpleProcessor:self hasLowPower:NO];
                        });
                    }
                    isLowPowerAlreadyDetected = NO;
                }
                //
                // clear
                //
                vDSP_vclr(_lowPowerData, 1, _lowPowerCount);
                _lowPowerCount = 0;
            }

            //
            // Accamulate low power data while _lowPowerCount != self.fftNumberOfInputSamples
            //
            vDSP_vsadd(ioData, 1, &zero, _lowPowerData+_lowPowerCount, 1, numberOfFrames);
            _lowPowerCount+=numberOfFrames;
            
        }];
    }
    
    return self;
}


- (void) start{
    if (self.audioManager.isRunningInput) 
        return;
    
    NSAssert(self.delegate, @"AMSignalProcessor: AMSignalProcessor has not defined delegate object...");
    NSAssert([self.delegate respondsToSelector:@selector(amSimpleProcessor:hasLocalPeaks:)], @"AMSignalProcessor: AMSimpleProcessor has not defined - processor:(AMSimpleProcessor*)processor hasLocalPeaks:(NSInteger)count delegate");
    NSAssert([self.delegate respondsToSelector:@selector(amSimpleProcessor:hasLowPower:)], @"AMSignalProcessor: AMSimpleProcessor has not defined - processor:(AMSimpleProcessor*)processor hasLowPower:(BOOL)lowePower delegate");

    [self.audioManager startInput];
#if DEBUG
    NSLog(@" ##### AMSimpleProcessor started.");
#endif
    
}

- (void) stop{
    if (self.audioManager.isRunningInput==NO) 
        return;

    [self.audioManager stopInput];
#if DEBUG
    NSLog(@" ##### AMSimpleProcessor stopped.");
#endif
}

#if TARGET_OS_IPHONE
- (Float32) volume{
    static AVAudioSession *audiosession = nil; if (audiosession==nil) audiosession =  [AVAudioSession sharedInstance];
    return audiosession.outputVolume;
}
#else
- (Float32) volume{
    
    UInt32 channel = 1; // Channel 0  is master, if available
    AudioObjectPropertyAddress prop = {
        kAudioDevicePropertyVolumeScalar,
        kAudioDevicePropertyScopeOutput,
        channel
    };
    
    float volume;
    UInt32 dataSize = sizeof(volume);
    
    [self.audioManager checkError:AudioObjectGetPropertyData(self.audioManager.defaultInputDeviceID, &prop, 0, NULL, &dataSize, &volume)
           operation:NSLocalizedString(@"Could not get system volume", @"")];
    
    return volume;
}
#endif

@end
