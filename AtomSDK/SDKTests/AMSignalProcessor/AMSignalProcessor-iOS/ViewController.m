//
//  ViewController.m
//  AMSignalProcessor-iOS
//
//  Created by denis svinarchuk on 25/06/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "ViewController.h"
#import "AMSimpleController.h"
#import "AMSimpleGenerator.h"
#import "AMSimpleProcessor.h"

@interface ViewController () <AMControllerProtocol, AMDosimeterProtocol, AMErrorHandlerProtocol>

@end

@implementation ViewController
{
    AMSimpleController *amcontroller;
    NSTimeInterval firstMeasurmetTime;
    float thresholdDose;
}

- (void) amController:(AMController *)controller didUpdateConnectionState:(BOOL)available{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (available) {
            self.hasConnectionLabel.text = @"YES"   ;
            self.hasConnectionLabel.backgroundColor = [UIColor colorWithRed:0.0 green:1.0 blue:0.0 alpha:0.3];
        }
        else{
            self.hasConnectionLabel.text = @"NO";
            self.hasConnectionLabel.backgroundColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.3];
        }
        
    });
}

- (void) amController:(AMController *)controller didDetectParticles:(NSInteger)count{
    NSLog(@"Has peaks %li",(long)count);
}

- (void) amController:(AMController *)controller didChangePowerState:(AMControllerPowerState)powerState{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (powerState==AM_CONTROLLER_POWER_NOT_ENOUGH) {
            self.powerEnoughLabel.text = @"NO";
            self.powerEnoughLabel.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:1.0 alpha:0.6];
        }
        else{
            self.powerEnoughLabel.text = @"YES"   ;
            self.powerEnoughLabel.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:0.0 alpha:0.6];
        }
        
    });
}

- (void) amController:(AMController *)controller willReadyAfterTime:(NSTimeInterval)approxTime{
    NSLog(@"AMDosimeterController willReadyAfter to measure %f", approxTime);
}

- (void) amController:(AMController *)controller didReady:(BOOL)ready{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (ready) {
            self.isReadyLabel.text = @"YES";
            self.isReadyLabel.backgroundColor = [UIColor colorWithRed:0.0 green:1.0 blue:0.0 alpha:0.6];
        }
        else{
            self.isReadyLabel.text = @"NO"   ;
            self.isReadyLabel.backgroundColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.6];
        }
        
    });
}

- (void) amController:(AMController *)controller didChangePowerVolume:(float)volume{
    self.powerVolumeLabel.text = [NSString stringWithFormat:@"%f",volume];
}

- (void) amObject:(id)amobject withError:(NSError *)error{
    NSLog(@"ViewController: %@", error);
}

- (void) amDosimeterValuesChanged:(AMDosimeter *)dosimeter{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        float unitm = 1000.0;
        
        
        if (firstMeasurmetTime<=0.0) {
            firstMeasurmetTime = [NSDate timeIntervalSinceReferenceDate];
            self.timeMeasurmetLabel.text = @"-";
        }
        else{
            self.timeMeasurmetLabel.text  = [NSString stringWithFormat:@"%d", (int)floor([NSDate timeIntervalSinceReferenceDate] - firstMeasurmetTime)];
        }
        
        self.particlesLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)dosimeter.particlesEquivalentCount];

        self.MADERLabel.text = [NSString stringWithFormat:@"%4.3f",dosimeter.doseEquivalentRate/unitm];
        self.searcheDERLabel.text = [NSString stringWithFormat:@"%4.3f",dosimeter.doseSearchedRate/unitm];
        self.searchedTimeLabel.text = [NSString stringWithFormat:@"%6.3f",dosimeter.searchingStartTime];
        self.MADERParticlesLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)dosimeter.particlesEquivalentCount];
        self.searchedParticlesLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)dosimeter.particlesSearchedCount];

        if (dosimeter.statErrorEquivalentRate<0) 
            self.derSigmaLabel.text = [NSString stringWithFormat:@"-"];                    
        else
            self.derSigmaLabel.text = [NSString stringWithFormat:@"%2.2f%%",dosimeter.statErrorEquivalentRate];
        
        if (dosimeter.statErrorSearchedRate<0)
            self.searchedSigmalLabel.text = [NSString stringWithFormat:@"-"];            
        else
            self.searchedSigmalLabel.text = [NSString stringWithFormat:@"%2.2f%%",dosimeter.statErrorSearchedRate];
        
        if (dosimeter.doseSearchedRate/unitm>=thresholdDose) {
            [UIView animateWithDuration:0.5 animations:^{
                self.thresholdDoseLabel.backgroundColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.6];
            }];
        }
        else if( dosimeter.doseSearchedRate/unitm>=thresholdDose/2.0){
            [UIView animateWithDuration:0.5 animations:^{
                self.thresholdDoseLabel.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:0.0 alpha:0.6];
            }];
        }
        else{
            [UIView animateWithDuration:0.5 animations:^{
                self.thresholdDoseLabel.backgroundColor = [UIColor colorWithRed:0.0 green:1.0 blue:0.0 alpha:0.6];
            }];
        }
        
    });
    
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [amcontroller start];
}

- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear: animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    firstMeasurmetTime = -1.0;
    
    amcontroller = [[AMSimpleController alloc] initWithDelegate:self];
    amcontroller.dosimeter.delegate = self;
    amcontroller.errorHandler = self;
    
	// Do any additional setup after loading the view, typically from a nib.
    self.isReadyLabel.text = @"NO"   ;
    self.isReadyLabel.backgroundColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.6];
    self.powerEnoughLabel.text = @"NO";
    self.powerEnoughLabel.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:1.0 alpha:0.6];
    self.hasConnectionLabel.text = @"NO";
    self.hasConnectionLabel.backgroundColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.3];
    self.powerVolumeLabel.text = [NSString stringWithFormat:@"%f",amcontroller.powerVolume];
    
    thresholdDose = [self.thresholdDoseLabel.text floatValue];    
}

- (void) amSimpleProcessor:(AMSimpleProcessor *)processor hasLocalPeaks:(NSInteger)count{
    NSLog(@"hasLocalPeaks: %li", count);
}

- (void) amSimpleProcessor:(AMSimpleProcessor *)processor hasLowPower:(BOOL)lowPower{
    NSLog(@"HAS LOW: %i", lowPower);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)reset:(id)sender {
    [amcontroller.dosimeter reset];
}
@end
