//
//  AMSearchingViewController.m
//  Atom
//
//  Created by denis svinarchuk on 16/10/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMBaseDosimeterMainViewController.h"
#import "AMBaseDosimeterInfoView.h"
#import "AMUIPreferences.h"

@interface AMBaseDosimeterMainViewController () <AMThermometerViewDelegate, UIGestureRecognizerDelegate>
@property (atomic,assign)     BOOL isActive;
@property (nonatomic,strong) AMBaseDosimeterInfoView *infoView;
@end

@implementation AMBaseDosimeterMainViewController
@synthesize isActive = _isActive;

- (void) setIsActive:(BOOL)isActive{
    _isActive = isActive;
    if (!self.screenView.displayView.inProgress) {
        self.screenView.displayView.chartView.isActive = isActive;
    }
    else if (!isActive) {
        self.screenView.displayView.chartView.isActive = isActive;
    }
}

-(BOOL) isActive{
    return _isActive;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.isActive = YES;
    
    self.infoView.defaultController = self.defaultController;
    
    [self didSettingsUpdate:self.settings];
    
    if (self.currentDosimeter) {
        [self didDosimeterUpdate:self.currentDosimeter];
    }
}

- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.isActive = NO;
}

- (void) didTouchResetButton{
    [self.defaultController resetDosimeter];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.screenView.displayView.hasConnection = YES;
    self.screenView.displayView.chartView.alpha = 0.0;

    self.screenView.thermometerView.delegate = self;    
    self.infoView.thresholdDoseRate = self.defaultController.settings.thresholdDoseRate.floatValue;
    self.infoView.delegate = self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapEvent:)];
    [tap setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tap];
    tap.delegate = self;
    
    // Do any additional setup after loading the view.
}


#pragma mark - Tap event

- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    //
    // Avoid text input closing
    //
    if([touch.view class] == [UITextField class]){
        return NO;
    }
    return YES;
}

- (void)tapEvent:(id)sender {
    [self.infoView hideIntervalPicker];
}

//
// Properties
//

#pragma mark - Properties

- (AMBaseDosimeterInfoView*) infoView{
    if (!_infoView) {
        _infoView = [[AMBaseDosimeterInfoView alloc] initWithFrame:self.screenView.infoBaseView.bounds withIntervalPicker:self.hasIntervalPicker withResetButton:self.hasResetButton];
        _infoView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.screenView.infoBaseView addSubview:_infoView];
        
        [_infoView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":_infoView}]];
        [_infoView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":_infoView}]];
    }
    return _infoView;
}

//
// Delegates
//
- (void) didControllerStateUpdate:(AMController *)controller{
    if (!controller.connectionState) {
        self.screenView.thermometerView.doseRate = 0.0;
    }
    self.screenView.displayView.hasConnection = controller.connectionState;
    self.titledView.deviceName = controller.settings.name;
}


- (void) updateWithDoseRate:(float) doseRate withSigma:(float) sigma withStartTime:(NSTimeInterval)startTime withProgress:(float) progress{
    
    if (doseRate<=0.) {
        self.screenView.displayView.progress=progress;
        [self.screenView.displayView startProgress];
        if (self.screenView.displayView.chartView.currentDosimeter.doseRateHistoryCount>0.0) {
            self.screenView.displayView.chartView.alpha = 0.5;
        }
        else
            self.screenView.displayView.chartView.alpha = 0.0;
    }
    else{
        [self.screenView.displayView stopProgress];
        if (self.isActive) {
            self.screenView.displayView.chartView.isActive =  YES;
            [UIView animateWithDuration:AM_UI_ANIMATE_DURATION animations:^{
                self.screenView.displayView.chartView.alpha = 1.0;
            }];
        }
    }
    
    //
    // display
    //
    self.screenView.displayView.doseRate = doseRate;
    self.screenView.displayView.sigma = sigma;
    
    //
    // thermometer
    //
    self.screenView.thermometerView.doseRate = doseRate;
    self.screenView.thermometerView.sigma = sigma;
    
    //
    // info
    //
    self.infoView.doseRate = doseRate;
    self.infoView.dose = self.currentDosimeter.cumulativeDose;
    self.infoView.startTime = startTime;
}

- (void) didSettingsUpdate:(AMSettings *)settings{

    
    if (!self.isActive) {
        return;
    }
    
    // update controller device name
    self.titledView.deviceName = [[AMControllerManager sharedInstance] defaultController].settings.name;
    
    // update main display
    self.screenView.displayView.currentUnit = [self.unit current];
    
    //
    // update info display
    //
    self.infoView.intervalString = [self.settings selectedSettingForSettingName:@"historyInterval"];
        
    //
    // just reset
    //
    self.infoView.thresholdDoseRate = self.defaultController.settings.thresholdDoseRate.floatValue;
    
    //
    // update dose/dose rate alert limit view
    //
    self.infoView.thresholdDoseRateLimit = self.defaultController.settings.thresholdDoseRate.floatValue;
    self.infoView.doseLimit = self.defaultController.settings.thresholdDose.floatValue;
    
    //
    // set up thermometer
    //
    self.screenView.thermometerView.hasDynamicScale = settings.isThermometerScaleDynamic.boolValue;
    self.screenView.thermometerView.trustFactor = settings.trustFactor.floatValue;
    self.screenView.thermometerView.thresholdDoseRate = self.defaultController.settings.thresholdDoseRate;
    [self.screenView.thermometerView reload];
    
    //
    // set up chart
    //
    self.screenView.displayView.chartView.thresholdDoseRate = self.defaultController.settings.thresholdDoseRate;
}

- (void) amThermometerView:(AMThermometerView *)thermometer didEndUpdatingThresholdDoseRate:(NSNumber *)thresholdDoseRate{
    self.defaultController.settings.thresholdDoseRate = thresholdDoseRate;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.infoView.thresholdDoseRate = thresholdDoseRate.floatValue;
    });
}

- (void) amThermometerView:(AMThermometerView *)thermometer didContinueUpdatingThresholdDoseRate:(NSNumber *)thresholdDoseRate{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.infoView.thresholdDoseRate = thresholdDoseRate.floatValue;
    });
}


@end
