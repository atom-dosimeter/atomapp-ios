//
//  AMPickerView.m
//  Atom
//
//  Created by denis svinarchuk on 28/07/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMPickerView.h"

@implementation AMPickerView

- (CGFloat) heightRow{
    if (_heightRow<=0.0) {
        _heightRow = 44.0f;
    }
    
    return _heightRow;
}



- (void) __init__{
    self.backgroundColor = [UIColor colorWithWhite:0.19 alpha:0.8];
    self.layer.shadowOffset = CGSizeMake(2.0f, .0f);
    self.layer.shadowOpacity = 0.2;
    self.layer.shadowRadius = 4.;
}

- (id) initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self __init__];
    }
    return self;
}

- (id) init{
    self = [super init];
    if (self) {
        [self __init__];
    }
    return self;
}

@end
