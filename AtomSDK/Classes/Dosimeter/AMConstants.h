//
//  AMConstants.h
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 03/07/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - DSP Hardware constants


static const NSUInteger kAMChartHistorySize = 82;
static const float      kAMBLERescaninginterval = 2.0f;

/**
 *  Resonance frequency power signal needs to supply dosimeter.
 *
 */
static const float kAMSineToneGenratorFreq = 14000.0; // AM supplier sine tone generator frequency

/**
 *  The sample rate.
 *
 */
static const float kAMSampleRate = 44100.0; // AM supplier sine tone generator frequency


// maximum float for input amplitude
#if TARGET_OS_IPHONE
static const float kAMHighInputAmplitude = 1.0;
#else
static const float kAMHighInputAmplitude = 2.7;
#endif

static float kAMRentgenSivertConvertion = 114.0f;

static int  kAMDetectorImpulsDuration = 6; //~150µs  if we havw more then 6 impulses that means it trash signal catched

/**
 *  The length of samples the can be grouped to processing signal. This version has 1 always.
 *
 */
static const int   kAMsamplesGroupLength = 1;

/**
 *  The value of amplitudes which will be cut off from signal
 *
 */
static const float kAMLowPassAmplitude  = 0.04;

/**
 *  The volume of low power signal from Atom Simple dosimeter that indicates lack of power from i-divices.
 *
 */
static const float kAMLowPowerSignalAmplitude = 0.10;
static const float kAMLowPowerSignalAmplitudeTopValue = 0.25;

/**
 *  The minimum of a peak value that indicate the dosimpeter dectected one particle.
 *
 */
static const float kAMPeakSignalAmplitude = 0.7;

/**
 *  The avarage window length to smooth input signal to cut off high-frequency signals.
 *
 */
static const int kAMMovingAvgFilterWindow = 10;

/**
 *  Th one HAL divice buffer duration in second.
 *
 */
static const float kAMDeviceBufferDurationWithDefault = 0.0232;

/**
 *  Low frequency which can be detected by DSP-filters.
 */
static const float kAMFFTLowFrequency = 0.1;

/**
 *  High frequency which can be detected by DSP-filters.
 *
 */
static const float kAMFFTHighFrequency = 13000.0;

/**
 *  Low power frequency impulse that can be detected as low power signal.
 *
 */
static const float kAMLowPowerFrequencyMin = 40.0;
/**
 *  High power frequency impulse that can be detected as low power signal.
 *
 */
static const float kAMLowPowerFrequencyMax = 150.0;
static const float kAMLowPowerAmplitudeMinLevel   = 0.05;
static const float kAMLowPowerAmplitudeMaxLevel   = 0.25;

/**
 *  The number of impulses rejected as low power signal.
 *
 */
static const int   kAMLowPowerFalseAlarmCount = 2;

/**
 *  The HAL volume value which can't be usesed to supply Atome Simple.
 *
 */
static const float kAMLowVolumeLevelToReady = 0.7;

/**
 *  The time needs to start up Atome Simple from tone generator launch point.
 *
 */
static const float kAMDosimeterStartUpTime = 1.0; // sec.


#pragma mark - Dosimeter and Geiger Counter constants

static const float kAMThresholdDoseRateMinimum=100.0; //nSv/h
static const float kAMThresholdDoseRateMaximum=1000000.0; // 1 mSv/h
static const float kAMThresholdDoseMinimum=100000.0; // 0.1 mSv
static const float kAMThresholdDoseMaximum=10000000000.0; // 10 Sv

/**
 *  The duration between two measurmet needs to compute dead time correction numbers of particles that can be detected by Geiger counter.
 *
 */
static const NSTimeInterval kAMDosimeterMeasurementDuration = 1.0; // sec. measurement for dead time kAMDosimeterParticlesCorrectionCount

/**
 *  Numbers of detected particles per second which indicate we must recompute to real numbers of detected particles
 *
 */
static const int kAMDosimeterParticlesCorrectionCount = 100;      // number per kAMDosimeterMeasurementDuration
/**
 *  Dead time in seconds.
 */
static const NSTimeInterval kAMDosimeterDeadTime = 0.0003;        // sec.

/**
 *  Dose compute by the follow scheme:  DER = ((((sum (N1:Nn))*K1)/n)*3600)-K2
 *  K1 is a constant sensitivity meter nSv/impulse
 *
 */
static const float kAMDosimeterSensetivityConstant_K1 = 0.128;      // constant sensitivity is measured in nSv/pulse
static const float kAMDosimeterSensetivityConstant_K1PULSE = 78.0;  // constant sensitivity is measured in pulse/µR
/**
 *  K2 is an own background counter has initial value of 40 nSv/hour
 */
static const float kAMDosimeterOwnBackgound_K2 = 40.0;              // own background is measured in nSv/hour
static const float kAMDosimeterOwnBackgound_K2PULSE = 4.0;          // own background is measured in impulses/min
/**
 *  Confidence coefficients.
 *
 */
static const float kAMDosimeterTrustFactor_Kpmin  = 1.0;  // confidence coefficient
static const float kAMDosimeterTrustFactor_Kpmid  = 2.0;  // confidence coefficient
static const float kAMDosimeterTrustFactor_Kphigh = 3.0;  // confidence coefficient

/**
 *  Maximum duration of searching mode time.
 *
 */
static const NSTimeInterval kAMDosimeterSearchModeMeasurementTime = 100.0;      // sec.

/**
 *  Particles that should be detected to start measurement in searching mode.
 *
 */
static const int kAMDosimeterSearchModeFast = 10;      // fast searching
static const int kAMDosimeterSearchModeNormal = 20;    // normal searching
static const int kAMDosimeterSearchModePrecisely = 30; // with good precision searching

static const int kAMSwiftDosimeterSearchModeFast = 20;      // fast searching
static const int kAMSwiftDosimeterSearchModeNormal = 35;    // normal searching
static const int kAMSwiftDosimeterSearchModePrecisely = 75; // with good precision searching

