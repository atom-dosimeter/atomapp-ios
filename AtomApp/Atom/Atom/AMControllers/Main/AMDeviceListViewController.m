//
//  AMDeviceListViewController.m
//  Atom
//
//  Created by denis svinarchuk on 27/08/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMDeviceListViewController.h"
#import "AMControllerUpdateProtocol.h"
#import "NSAttributedString+AMUnits.h"
#import "AMDeviceListView.h"
#import "AMUIPreferences.h"
#import "AMConnectionView.h"

#import <AtomMeasurerSDK/AMSimpleController.h>
#import <AtomMeasurerSDK/AMTagController.h>
#import <AtomMeasurerSDK/AMDoseUnit.h>

@interface AMDeviceListViewController () <AMControllerManagerUpdateProtocol, UITableViewDataSource, UITableViewDelegate>
@property (nonatomic,readonly) AMControllerManager *amControllerManager;
@property (nonatomic,readonly) AMDoseUnit *unit;
@property (nonatomic,strong) NSIndexPath *lastSelectedIndexPath; 
@property (readonly,nonatomic) AMDeviceListView *viewPort;
@property (atomic,assign) BOOL isActive;
@end

@implementation AMDeviceListViewController

@synthesize amControllerManager=_amControllerManager, unit=_unit, lastSelectedIndexPath=_lastSelectedIndexPath;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (AMDeviceListView*)viewPort{
    return (AMDeviceListView*)self.view;
}

- (NSIndexPath*) lastSelectedIndexPath{
    if (!_lastSelectedIndexPath) {
         _lastSelectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    }
    
    return _lastSelectedIndexPath;
}

- (void) setLastSelectedIndexPath:(NSIndexPath *)lastSelectedIndexPath{
    _lastSelectedIndexPath = lastSelectedIndexPath;
}

- (AMControllerManager*) amControllerManager{
    if (!_amControllerManager) {
        _amControllerManager = [AMControllerManager sharedInstance];
    }
    return _amControllerManager;
}

- (AMDoseUnit*) unit{
    return [AMDoseUnit sharedInstance];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    // Do any additional setup after loading the view.
}

- (void) reloadData{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!self.isActive) {
            return;
        }
                
        [self.tableView reloadData];
        [self.tableView selectRowAtIndexPath:self.lastSelectedIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        self.viewPort.deviceName = self.amControllerManager.defaultController.settings.name;
    });
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.isActive = YES;
    [self reloadData];
}

- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.isActive = NO;
}

#pragma mark - Update manager list protocol

- (void) didControllerManagerUpdateList:(AMControllerManager *)manager{
    [self reloadData];
}

- (void) didControllerManagerUpdateDosimeter:(AMControllerManager *)manager amController:(AMController *)controller{
    [self reloadData];
}


#pragma mark - UITableVew data source

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.amControllerManager.controllerList.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell;
    
    AMController *controller = [self.amControllerManager.controllerList objectAtIndex:indexPath.row];
    AMDosimeter  *dosimeter = controller.dosimeter;
    
    if ([controller isKindOfClass:[AMSimpleController class]]) {
        static NSString *cellID = @"CellSimple";
        cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    }
    else if ([controller isKindOfClass:[AMTagController class]]){
        static NSString *cellID = @"CellTag";
        cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    }
        
    cell.accessoryView.backgroundColor = cell.backgroundColor = [UIColor clearColor];
    
    UILabel *nameLabel = (UILabel*) [cell viewWithTag:1];
    nameLabel.text = controller.settings.name;
    
    if ([controller isKindOfClass:[AMTagController class]]){
        UILabel *uuidLabel = (UILabel*) [cell viewWithTag:3];
        uuidLabel.text = controller.settings.UUIDString;
    }
    
    UILabel *doseRateLabel = (UILabel*) [cell viewWithTag:2];
    
    UIActivityIndicatorView *ai =  (UIActivityIndicatorView*)[cell.contentView viewWithTag:100500];
    
    if (!ai) {
        ai = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        ai.translatesAutoresizingMaskIntoConstraints = NO;
        ai.color = AM_UI_LOWERCOLOR;
        ai.tag = 100500;
        ai.hidesWhenStopped = YES;
        ai.alpha = 0.0;
        
        [cell.contentView addSubview:ai];
        
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:ai attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:doseRateLabel attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:ai attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:doseRateLabel attribute:NSLayoutAttributeRight multiplier:1 constant:0]];
    }

    if (controller.dosimeter.doseSearchedRate>0.0) {
        doseRateLabel.attributedText =  [NSAttributedString coloredStringWithDoseInCurrentUnit:controller.dosimeter.doseSearchedRate withThresholdDoseRate:controller.settings.thresholdDoseRate withBaseFontSize:doseRateLabel.font.pointSize];
        if (doseRateLabel.alpha<1.) {
            [UIView animateWithDuration:AM_UI_ANIMATE_DURATION
                             animations:^{
                                 ai.alpha = 0.0;
                                 doseRateLabel.alpha = 1.0;
                             }
                             completion:^(BOOL finished) {
                                 [ai stopAnimating];
                             }];
        }
    }
    else{
        if (controller.connectionState) {
            doseRateLabel.text = @"";
            if (!ai.isAnimating) {
                [ai startAnimating];
                [UIView animateWithDuration:AM_UI_ANIMATE_DURATION
                                 animations:^{
                                     ai.alpha = 1.0;
                                     doseRateLabel.alpha = 0.0;
                                 }
                 ];
            }
        }
        else{
            doseRateLabel.text = @"- - -";
            if (doseRateLabel.alpha<1.) {
                [UIView animateWithDuration:AM_UI_ANIMATE_DURATION
                                 animations:^{
                                     doseRateLabel.alpha = 1.0;
                                 }
                 ];
            }
        }
    }
    
    if (!controller.isReady && [controller isKindOfClass:[AMTagController class]]) {
            cell.contentView.alpha = AM_UI_DISABLED_ALPHA;
    }
    else {
        cell.contentView.alpha = 1.0;
    }
    
    if (controller==self.amControllerManager.defaultController) {
        self.lastSelectedIndexPath = indexPath;
    }
    
    AMConnectionView *cview = (AMConnectionView*) [cell viewWithTag:4];
    cview.connectionViewStyle = AMConnectionViewCircle;
    cview.isConnected = controller.isReady;
    
    UIProgressView *progress = (UIProgressView*) [cell viewWithTag:5];
    float p = (float)dosimeter.particlesSearchedCount / (float)dosimeter.particlesSearchedMinimumCount;
    
    //if (p!=progress.progress) {
        [UIView animateWithDuration:AM_UI_ANIMATE_DURATION animations:^{
            if (p>=0.999) {
                progress.alpha = 0.0;
            }
            else{
                if (dosimeter.particlesSearchedCount>0)
                    progress.alpha = 1.0;
                else
                    progress.alpha = 0.0;
            }
            
            [progress setProgress: p animated:YES];
        }];
    //}
    
    UILabel *particlesLabel = (UILabel*) [cell viewWithTag:6];
    UILabel *cpsLabel = (UILabel*) [cell viewWithTag:7];
    
    if (cpsLabel) {
        if (dosimeter.cpsSearching){
            cpsLabel.text = [NSString stringWithFormat:@"%.2f cps",dosimeter.cpsSearching];
        }
        else {
             cpsLabel.text = @"";
        }
    }
    
    if (dosimeter.particlesSearchedCount>0) {
        particlesLabel.text = [NSString stringWithFormat:@"%llu",dosimeter.particlesEquivalentCount];
    }
    else
        particlesLabel.text = @"";
    
    
    return cell;
    
};

#pragma mark - UITableView delegate


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{    
    self.lastSelectedIndexPath=indexPath;    
    AMController *controller = [self.amControllerManager.controllerList objectAtIndex:indexPath.row];
    self.amControllerManager.defaultController = controller;
    [self reloadData];
}

@end
