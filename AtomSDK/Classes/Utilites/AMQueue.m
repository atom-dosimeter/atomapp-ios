//
//  GIQueue.m
//  MEinfoCXConnector
//
//  Created by denis svinarchuk on 21.03.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "AMQueue.h"

@implementation AMQueue
{
    NSMutableArray *queue;
    //NSUInteger      depth;
}

@synthesize maxSize=_maxSize;

- (id) init{
    self = [super init];
    if(self){
        queue = [NSMutableArray arrayWithCapacity:0];
        _maxSize= 0;
    }
    return self;
}

- (NSUInteger) maxSize { return _maxSize;};

- (void) setMaxSize:(NSUInteger)size{
    @synchronized (self){
        _maxSize=size;
        if ([queue count]>=_maxSize) {
            [queue removeObjectsInRange:NSMakeRange(_maxSize, [queue count]-_maxSize)];
        }
    }
}

- (NSUInteger) size{
    return [queue count];
}

- (void) flush{
    [queue removeAllObjects];
}

- (id) objectAtIndex:(NSUInteger)index{
    return [queue objectAtIndex:index];
}

- (id) lastObject{
    return [queue lastObject];
}

- (id) pop{
    @synchronized (self){
        if ([queue count]==0){
            return nil;
        }
        @try {
            id r_ = [queue objectAtIndex:0];                        
            [queue removeObjectAtIndex:0];
            return r_;
        }
        @catch (NSException *exception) {
            NSLog(@"%@ at %s:%i", exception, __FILE__, __LINE__);
            return nil;
        }
        return nil;
    }
}

- (void) push:(id )frame{
    @synchronized (self){
        if (!frame)
            return;
        if (_maxSize>1 && queue.count>=_maxSize) {
            [queue removeObjectsInRange:NSMakeRange(0, queue.count-_maxSize)];
        }
        [queue addObject:frame];
    }
}

@end
