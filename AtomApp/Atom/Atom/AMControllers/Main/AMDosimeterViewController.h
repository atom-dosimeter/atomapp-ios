//
//  AMFirstViewController.h
//  Atom
//
//  Created by denis svinarchuk on 11.07.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AMTitledView.h"
#import "AMPageViewController.h"
#import "AMStatusBarView.h"

@interface AMDosimeterViewController : AMPageViewController
@property (nonatomic,readonly) AMStatusBarView *statusBarView;
@end
