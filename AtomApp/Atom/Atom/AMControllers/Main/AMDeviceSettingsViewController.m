//
//  AMDeviceSettingsViewController.m
//  Atom
//
//  Created by denis svinarchuk on 29.08.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMDeviceSettingsViewController.h"
#import "AMDeviceSettingsView.h"
#import "AMKeyboardBar.h"
#import "NSString+AMUtils.h"
#import "AMSettings.h"
#import "AMDoseUnitField.h"
#import "UIView+UserDefinedPropertyBinding.h"
#import "AMUIPreferences.h"
#import "NSAttributedString+AMUnits.h"

#import <AtomMeasurerSDK/AMTagController.h>
#import <AtomMeasurerSDK/AMControllerManager.h>
#import <AtomMeasurerSDK/AMTagControllerSettings.h>


@interface AMDeviceSettingsViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, AMDoseUnitFieldDelegate, UIGestureRecognizerDelegate>
@property (readonly,nonatomic) AMControllerManager *amControllerManager;
@property (readonly,nonatomic) AMController *defaultController;
@property (nonatomic,readonly) AMDeviceSettingsView *viewPort;
@property (nonatomic,assign) BOOL isEditingSettings;
@property (atomic,assign) BOOL isActive;
@property (nonatomic,strong) UIActivityIndicatorView *activityView;
@property (nonatomic,strong) AMController *lastController;
@end

@implementation AMDeviceSettingsViewController

@synthesize amControllerManager=_amControllerManager, isEditingSettings=_isEditingSettings;

- (AMControllerManager*) amControllerManager{
    if (!_amControllerManager) {
        _amControllerManager = [AMControllerManager sharedInstance];
    }
    return _amControllerManager;
}

- (AMController*) defaultController{
    return self.amControllerManager.defaultController;
}

- (AMDeviceSettingsView*) viewPort{
    return (AMDeviceSettingsView*)self.view;
}

- (UIActivityIndicatorView*) activityView{
    if (!_activityView) {
        _activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _activityView.color = AM_UI_LOWCOLOR;
        _activityView.hidesWhenStopped = YES;
        [self.view addSubview:_activityView];
        [_activityView bringSubviewToFront:_activityView.superview];
    }
    _activityView.center = CGPointMake(self.view.bounds.size.width/2., self.view.bounds.size.height/2.);
    [_activityView startAnimating];
    
    return _activityView;
}

//
// Update protocol
//
- (void) didControllerChangesCommit:(AMController *)controller{
        
    if (self.defaultController == controller) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:AM_UI_ANIMATE_DURATION
                             animations:^{
                                 self.tableView.userInteractionEnabled = YES;
                                 self.tableView.alpha = 1.0;
                                 self.activityView.alpha = 0.0;
                             }
                             completion:^(BOOL finished) {
                                 self.isEditingSettings = NO;
                                 [self.activityView stopAnimating];
                                 [self reloadWithAnimation:YES];
                             }
             ];        
        });        
    }
}

- (void) willControllerChangesCommit:(AMController *)controller{
    
    //NSLog(@" #### willControllerChangesCommit....");

    if (self.defaultController==controller) {
        self.isEditingSettings = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            self.activityView.alpha = 0.0;
            [self.activityView startAnimating];
            [UIView animateWithDuration:AM_UI_ANIMATE_DURATION animations:^{
                self.activityView.alpha = 1.0;
                self.tableView.userInteractionEnabled = NO;
                self.tableView.alpha = AM_UI_DISABLED_ALPHA;
            }];        
        });        
    }
}

//
// UIController
//
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapEvent:)];
    [tap setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tap];
    tap.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(settingsChangedHandler:) name:AM_SETTINGS_CHANGED_NOTIFICATION object:nil];    
}

- (void) setIsEditingSettings:(BOOL)isEditingSettings{
    _isEditingSettings = isEditingSettings;
}

- (BOOL) isEditingSettings{
    return _isEditingSettings;
}

- (void) settingsChangedHandler:(NSNotification*)event{
    self.isEditingSettings=NO;
}

- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

- (void)tapEvent:(id)sender {
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    //
    // Avoid text input closing
    //
    if([touch.view class] == [UITextField class]){
        return NO;
    }
    return YES;
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.isActive = YES;
    [self reloadWithAnimation:YES];
    
    if (self.lastController) {
        if (self.lastController!=self.defaultController) {
            if ([self.defaultController isKindOfClass:[AMTagController class]]) {
                AMTagController *am=(AMTagController*)self.defaultController;
                if (am.isBusy) {
                    [self willControllerChangesCommit:self.defaultController];
                }
                else{
                    [self didControllerChangesCommit:self.defaultController];
                }
            }
            else{
                [self didControllerChangesCommit:self.defaultController];
            }
        }
    }
    
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.lastController = self.defaultController;
}

- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.isActive = NO;
}

- (void) reloadWithAnimation:(BOOL)animated{
    
    if (!self.isActive) {
        return;
    }
    
    if (self.isEditingSettings) {
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{            
        self.viewPort.deviceName = self.amControllerManager.defaultController.settings.name;
        if (animated) {
            [UIView transitionWithView:self.tableView 
                              duration:AM_UI_ANIMATE_DURATION
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^(void) {
                                [self.tableView reloadData];
                            } completion:NULL];                    
        }
        else{
            [self.tableView reloadData];
        }
    });
}

- (IBAction)playSearch:(id)sender {
    if ([self.defaultController isKindOfClass:[AMTagController class]]) {
        AMTagController *tc = (AMTagController*)self.defaultController;
        [tc playSearchSignal];        
    }
}

- (void) didControllerSettingsUpdate:(AMController *)controller{
    [self reloadWithAnimation:NO];
}



//
// UITableView datasource
//

#pragma mark - UITableView datasource

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {
        //
        // INFO
        //
        if ([self.defaultController isKindOfClass:[AMTagController class]]) {
            return 5;
        }
        return 2;
    }
    return self.defaultController.settings.count-2;
};

- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return NSLocalizedString(@"Atom info", @"Atom dosimeter info localized string");
    }
    return NSLocalizedString(@"Atom device settings", @"Atom dosimeter settings");
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell;
    NSString *vname = nil;
    id value = nil;
    if (indexPath.section==0) {
        if (indexPath.row>=2) {
            //
            // specific for tag device
            //
            if (indexPath.row == 4) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"CellSearchButton" forIndexPath:indexPath];
                UIButton *button = (UIButton*) [cell viewWithTag:1];                
                [button setTitle:NSLocalizedString(@"Play sound to search Device", @"") forState:UIControlStateNormal];                
            }
            else{
                cell = [tableView dequeueReusableCellWithIdentifier:@"CellInfo" forIndexPath:indexPath];
                
                UILabel *title = (UILabel*) [cell viewWithTag:1];
                
                if (indexPath.row==2) {
                    title.text = NSLocalizedString(@"Battery charge", @"");
                }
                else if (indexPath.row==3)
                    title.text = NSLocalizedString(@"BLE signal level", @"");
                
                UILabel *label = (UILabel*) [cell viewWithTag:2];
                label.textAlignment = NSTextAlignmentLeft;
                
                AMTagController *tagc = (AMTagController*)self.defaultController;
                if (indexPath.row==2) {
                    label.attributedText = [NSAttributedString attributedString:[NSString stringWithFormat:@"%.0f", self.defaultController.powerVolume*100.]
                                                                  withExtention:@"%"
                                                               withBaseFontSize:label.font.pointSize] ;
                }
                else if (indexPath.row==3){
#if DEBUG
                    NSString *ext = [NSString stringWithFormat: @"[%.0f] dBm",tagc.avgRSSI.floatValue];
#else
                    NSString *ext = @" dBm";
#endif
                    label.attributedText = [NSAttributedString attributedString:[NSString stringWithFormat:@"%.0f", tagc.RSSI.floatValue]
                                                                  withExtention:ext
                                                               withBaseFontSize:label.font.pointSize];
                }
//                else if (indexPath.row==4){
//                    label.attributedText = [NSAttributedString attributedString:[NSString stringWithFormat:@"%.1f", tagc.measurement.temerature]
//                                                                  withExtention:@" ℃"
//                                                               withBaseFontSize:label.font.pointSize];
//                }
            }
        }
        else{
            if (indexPath.row == 0) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"CellName" forIndexPath:indexPath];
                UITextField *text = (UITextField*) [cell viewWithTag:2];
                text.delegate = self;
                [text setValue:[NSNumber numberWithInteger:indexPath.row] forUndefinedKey:AM_SETTIN_KEY];
                text.text = [self.defaultController.settings propertyAtIndex:indexPath.row];
            }
            else {
                cell = [tableView dequeueReusableCellWithIdentifier:@"CellLabel" forIndexPath:indexPath];
                
                UILabel *label = (UILabel*) [cell viewWithTag:2];
                label.text = [self.defaultController.settings propertyAtIndex:indexPath.row];
            }
            
            UILabel *title = (UILabel*) [cell viewWithTag:1];
            title.text = [self.defaultController.settings propertyLocalizedTitleAtIndex:indexPath.row];
        }
    }
    else{
        NSInteger row = indexPath.row+2;
        
        value = [self.defaultController.settings propertyAtIndex:row];
        vname = [self.defaultController.settings propertyNameAtIndex:row];
        
        if ([value isKindOfClass:[@YES class]]){
            cell = [tableView dequeueReusableCellWithIdentifier:@"CellSwitch" forIndexPath:indexPath];
            
            UISwitch *switchView = (UISwitch*) [cell viewWithTag:2];
            switchView.on = [value boolValue];
            [switchView setValue:[NSNumber numberWithInteger:row] forUndefinedKey:AM_SETTIN_KEY];
        }
        else if ([value isKindOfClass:[NSNumber class]]) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"CellField" forIndexPath:indexPath];
            
            AMDoseUnitField *field = (AMDoseUnitField*) [cell viewWithTag:2];
            
            if (indexPath.row==0) {
                field.isUpper = YES;
            }
            
            field.textAlignment = NSTextAlignmentLeft;
            field.delegate = self;
            field.value = value;
            
            if ([[self.defaultController.settings propertyNameAtIndex:row] hasSuffix:@"Rate"]) {
                field.isDoseRate = YES;
            }
            
            [field setValue:[NSNumber numberWithInteger:row] forUndefinedKey:AM_SETTIN_KEY];
        }
        else
            cell = [tableView dequeueReusableCellWithIdentifier:@"CellField" forIndexPath:indexPath];
        
        UILabel *title = (UILabel*) [cell viewWithTag:1];
        title.text = [self.defaultController.settings propertyLocalizedTitleAtIndex:row];        
    }
    
    if ([self.defaultController isKindOfClass:[AMTagController class]]) {
        
        if (vname && ([vname isEqualToString: @"isEnabled"])) {
            cell.userInteractionEnabled = YES;
            cell.contentView.alpha = 1.0;
        }
        else{
            
            if ([self.defaultController.settings isKindOfClass:[AMTagControllerSettings class]]){
                AMTagControllerSettings *ts = (AMTagControllerSettings*)self.defaultController.settings;
                
                if (self.defaultController.isReady) {
                    if (ts.isTotalMuteOn.boolValue && [value isKindOfClass:[@YES class]]) {
                        if ([vname isEqualToString:@"isTotalMuteOn"]) {
                            cell.userInteractionEnabled = YES;
                            cell.contentView.alpha = 1.0;
                        }
                        else{
                            cell.userInteractionEnabled = NO;
                            cell.contentView.alpha = AM_UI_DISABLED_ALPHA;
                        }
                    }
                    else{
                        cell.userInteractionEnabled = YES;
                        cell.contentView.alpha = 1.0;
                    }
                }
                else{
                    cell.userInteractionEnabled = NO;
                    cell.contentView.alpha = AM_UI_DISABLED_ALPHA;
                }
            }
        }
    }
    else{
        cell.userInteractionEnabled = YES;
        cell.contentView.alpha = 1.0;
    }
    
    cell.backgroundColor = cell.contentView.backgroundColor = [UIColor clearColor];
    
    return cell;
}

//
// AmDoseUnit field
//

- (void) amDoseUnitFieldDidEndEditing:(AMDoseUnitField *)field{
    NSNumber *row = [field valueForKeyPath:AM_SETTIN_KEY];
    [self.defaultController.settings setValue:field.value atPropertyIndex:row.intValue];
    self.isEditingSettings=NO   ;
}

- (void) amDoseUnitFieldWillEdit:(AMDoseUnitField *)field{
    self.isEditingSettings=YES;
}

//
// Text field done
//
- (void) textFieldDidEndEditing:(UITextField *)textField{
    
    NSNumber *row = [textField valueForKeyPath:AM_SETTIN_KEY];
    if (row.intValue==0) {
        [self.defaultController.settings setValue:textField.text atPropertyIndex:row.intValue];
    }
    else{
        NSNumber *value = [textField.text floatNumber];
        [self.defaultController.settings setValue:value atPropertyIndex:row.intValue];
    }
    
    self.isEditingSettings=NO;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    self.isEditingSettings=YES;
    
    NSNumber *row = [textField valueForKeyPath:AM_SETTIN_KEY];
    if (row.intValue==0) {
        return;
    }
    if (textField.inputAccessoryView==nil && UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        textField.inputAccessoryView = [[AMKeyboardBar alloc] init];
    }
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    return YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)switchPropery:(UISwitch *)sender {
    NSNumber *row = [sender valueForKeyPath:AM_SETTIN_KEY];
    AMBoolean *ws = (AMBoolean*)[AMBoolean numberWithBool:sender.on];
    //[self willControllerChangesCommit:self.defaultController];
    [self.defaultController.settings setValue:ws atPropertyIndex:row.intValue];
}
@end
