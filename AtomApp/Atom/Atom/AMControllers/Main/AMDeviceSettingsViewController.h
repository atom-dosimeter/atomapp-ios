//
//  AMDeviceSettingsViewController.h
//  Atom
//
//  Created by denis svinarchuk on 29.08.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AMControllerUpdateProtocol.h"

@interface AMDeviceSettingsViewController : UIViewController<AMControllerUpdateProtocol>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)switchPropery:(UISwitch *)sender;
- (void) reloadWithAnimation:(BOOL)animated;
- (IBAction)playSearch:(id)sender;

@end
