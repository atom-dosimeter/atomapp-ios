//
//  AMChartView.m
//  Atom
//
//  Created by denis svinarchuk on 31/07/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMChartView.h"
#import "AMUIPreferences.h"
#import <AtomMeasurerSDK/AMDoseUnit.h>
#import <AtomMeasurerSDK/AMControllerManager.h>

@interface AMChartView() <MESChartFormater, MESChartDataSource>
@property (nonatomic,readonly) NSNumberFormatter* yLabelFormater;
@property (nonatomic,readonly) AMDoseUnit *unit;
@property (readonly,nonatomic) float lastDoseRate;
@end

@implementation AMChartView
{
    float prevDoseRate, prevMax;
    NSTimer *chartUpdateTimer;
}

@synthesize yLabelFormater = _yLabelFormater, unit=_unit;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self __init__];
    }
    return self;
}


- (id) init{
    self = [super init];
    if (self) {
        [self __init__];
    }
    return self;
}

- (void) setIsActive:(BOOL)isActive{
    _isActive = isActive;
    prevMax = 0.0;
    prevDoseRate = 0.0;
}


- (void) awakeFromNib{
    
    [super awakeFromNib];
    [self __init__];
    
    NSLog(@"AMChartView: awake...");
    
}

- (void) __init__{
    
    self.zonePadding = 0.0;
    
    CGRect rect = self.headerView.frame;
    rect.size.height=32.;
    rect.origin.y=rect.origin.y+22.;
    self.headerView.frame = rect;
    self.headerView.layer.cornerRadius = .5;
    self.headerView.layer.borderColor = [UIColor clearColor].CGColor;
    
    int fontSize = 10;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        fontSize = 12;
    }
    
    self.gridLayer.yAxis.font = [UIFont fontWithName:AM_UI_FONT size:fontSize];
    self.gridLayer.xAxis.font = [UIFont fontWithName:AM_UI_FONT size:fontSize];
    
    self.gridLayer.yAxis.textColor = [UIColor whiteColor];
    self.gridLayer.yAxis.ticks = 8;
    self.gridLayer.yAxis.enableEdges = NO;
    self.gridLayer.yAxis.enableLabels = YES;
    
    self.gridLayer.xAxis.width = 6.0;
    self.gridLayer.xAxis.textColor = [UIColor whiteColor];
    self.gridLayer.xAxis.ticks = 4;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        self.gridLayer.xAxis.ticks = 6;
    }
    self.gridLayer.xAxis.enableLabels = YES;
    self.gridLayer.xAxis.enableEdges  = YES;
    
    self.gridLayer.lineWidth = 0.5;
    self.gridLayer.strokeColor = [UIColor colorWithRed:1. green:1. blue:1. alpha:0.19].CGColor;
    self.gridLayer.xLayer.strokeColor = self.gridLayer.strokeColor;
    self.gridLayer.xLayer.lineWidth = self.gridLayer.yLayer.lineWidth;
    
    self.graphLayer.strokeColor = [UIColor lightGrayColor].CGColor;
    self.graphLayer.opacity = 0.8;
    self.graphLayer.lineWidth =2.;
    self.graphLayer.lineCap = kCALineCapRound;
    self.graphLayer.gradientColor = [UIColor colorWithRed:1. green:1. blue:1. alpha:0.8].CGColor;
    self.graphLayer.enableGradient = YES;
    
    //self.candlesLayer.widthBetween = 1.;
    
    self.dataSource = self;
    self.formater = self;
}

- (float) lastDoseRate{
    AMDoseRatePoint *point = [self.currentDosimeter.doseRateHistory lastObject];
    return point.doseRate;
}

- (void) chartRedrawTimerHandle:(NSTimer*)timer{
    if (!self.isActive) {
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        
// NSLog(@"AMChart redraw... %@", self.currentDosimeter.UUIDString);
        
        float change = fabs((prevDoseRate-self.lastDoseRate)/prevDoseRate);
        if (change>1.) {
            self.enableRedrawAnimation = YES;
        }
        else
            self.enableRedrawAnimation = NO;
        
        if (self.dataSource && [self.dataSource respondsToSelector:@selector(dataRangeInSChartView:atSeriesIndex:)]) {
            
            MESChartDataRange *range = [self.dataSource dataRangeInSChartView:self atSeriesIndex:0];
            NSNumber *max = range.maxValue;
            if (max.floatValue!=prevMax) {
                self.enableRedrawAnimation = YES;                
                prevMax= max.floatValue;
            }
        }
        
        prevDoseRate = self.lastDoseRate;
        
        [self redraw];
    });
}

- (void) setCurrentDosimeter:(AMDosimeter *)currentDosimeter{
    
    if (_currentDosimeter!=nil && _currentDosimeter==currentDosimeter) {
        return;
    }
    
    _currentDosimeter = currentDosimeter;
    
    dispatch_async(dispatch_get_main_queue(), ^{        
        
        if (chartUpdateTimer) {
            [chartUpdateTimer invalidate];
        }
        
        if (_currentDosimeter) {
            _currentDosimeter.doseRateHistoryCount = (NSInteger)floorf(self.graphLayer.bounds.size.width/2.);
                        
            chartUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(chartRedrawTimerHandle:) userInfo:nil repeats:YES];        
        }
    });    
}

- (AMDoseUnit*) unit{
    if (!_unit) {
        _unit = [[AMDoseUnit alloc] init];
    }
    return _unit;
}

- (NSNumberFormatter*)yLabelFormater{
    if (!_yLabelFormater) {
        _yLabelFormater = [[AMDoseUnit sharedInstance] formatterWithPrecision:2];
    }
    return _yLabelFormater;
}

- (NSInteger) numberOfZonesInSChartView:(MESChartView *)chart{
    return 1;
}


- (MESChartZone*) meSChartView:(MESChartView *)chart zoneAtIndex:(NSInteger)zoneIndex{
    MESChartZone *zone = [[MESChartZone alloc] init];
    zone.height = 0.;
    zone.grid = [MESChartGrid layer];
    
    zone.grid.yAxis.font = self.gridLayer.yAxis.font;
    zone.grid.yAxis.textColor = self.gridLayer.yAxis.textColor;
    
    zone.grid.xAxis.width = 10.0;
    zone.grid.xAxis.font = self.gridLayer.xAxis.font;
    zone.grid.xAxis.textColor = self.gridLayer.xAxis.textColor;
    zone.grid.xAxis.ticks = self.gridLayer.xAxis.ticks;
    
    zone.grid.xLayer.lineWidth = self.gridLayer.xLayer.lineWidth;
    zone.grid.yLayer.lineWidth = 0.0;
    
    return zone;
}


- (void) meSChartView:(MESChartView *)chart yAxisLabel:(UILabel *)label withValue:(id)value atTickIndex:(NSInteger)tickIndex forSeriesAtIndex:(NSInteger)seriesIndex{
    //    if (tickIndex==0) {
    //        label.font = [UIFont fontWithName:AM_UI_BOLD_FONT size:12.];
    //    }
    
    if (tickIndex==chart.gridLayer.yAxis.ticks-1) {
        label.text = @"";
    }
    else
        label.text = [self.yLabelFormater stringFromNumber:value];
}

- (void) meSChartView:(MESChartView *)chart xAxisLabel:(UILabel *)label withValue:(id)value atTickIndex:(NSInteger)tickIndex forSeriesAtIndex:(NSInteger)seriesIndex{
    if ([value isKindOfClass:[NSDate class]]) {
        NSDate *time=value;
        //        if (tickIndex==chart.gridLayer.xAxis.ticks-2) {
        //            label.font = [UIFont fontWithName:AM_UI_BOLD_FONT size:12.];
        //            CGRect rect = label.frame;
        //            rect.origin.y-=1.;
        //            label.frame=rect;
        //        }
        
        NSDateFormatterStyle style = NSDateFormatterMediumStyle;
        if (self.currentDosimeter.doseRateHistoryInterval>=60.) {
            style = NSDateFormatterShortStyle;
        }
        label.text = [NSString stringWithFormat:@"%@", [NSDateFormatter localizedStringFromDate:time dateStyle:NSDateFormatterNoStyle timeStyle:style]];                        
    }
}

- (void) meSChartView:(MESChartView *)chart headerView:(UILabel *)label atLeftFingerData:(MESChartData *)leftData atRightFingerData:(MESChartData *)rightData{
    if (leftData){
        if (self.headerView.frame.origin.x==0) {
            return;
        }
        NSNumber *value = leftData.yValue;
        
        float unitBasedValue = [[AMDoseUnit sharedInstance] fromValueToBase:value.floatValue];
        
        if (unitBasedValue>=self.thresholdDoseRate.floatValue) {
            label.textColor = AM_UI_DER_HIGHCOLOR;
        }
        else
            label.textColor = AM_UI_DER_LOWCOLOR;
        
        CGRect rect = label.bounds;
        rect.size.height = 18.0;
        label.bounds=rect;
        label.alpha = 0.98;
        label.backgroundColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.4];
        label.font = [UIFont fontWithName:AM_UI_BOLD_FONT size:label.bounds.size.height*0.9];
        label.text =  [NSString stringWithFormat:@"%@",[self.yLabelFormater stringFromNumber:value]];
    }
}

- (void) drawRect:(CGRect)rect{
    if (self.dataSource && self.currentDosimeter && self.currentDosimeter.doseRateHistory.size>0) {
        [super drawRect:rect];
    }
}

#pragma mark - DATA Source

- (NSInteger) numberOfSeriesInSChartView:(MESChartView *)chart{
    return 1;
}


- (NSInteger) meSChartView:(MESChartView *)chart numberOfChartDataForSeriesAtIndex:(NSInteger)index{
    return self.currentDosimeter.doseRateHistory.size;
}

- (MESChartSeries*) meSChartView:(MESChartView *)chart seriesAtIndex:(NSInteger)index{
    MESChartSeries *series = [[MESChartSeries alloc] init];
    series.type = MESCHART_LINE;
    return series;
}

- (MESChartData*) meSChartView:(MESChartView*)chart chartDataAtIndex:(NSInteger)dataIndex forSeriesAtIndex:(NSInteger)seriesIndex{
    MESChartData *data =  [[MESChartData alloc] init];
    AMDoseRatePoint *point = [self.currentDosimeter.doseRateHistory objectAtIndex:dataIndex];
    data.index  = point.index;
    data.xValue = point.time;
    data.yValue = [self.unit convertNumberToCurrent:[NSNumber numberWithFloat:point.doseRate]];
    return data;
}

- (MESChartDataRange*) dataRangeInSChartView:(MESChartView*)chart atSeriesIndex:(NSInteger)index{
    
    float min = AM_MINIMUM_THERMOMETER_DOSERATE, max=0.0;
    
    AMQueue *doseRateHist = self.currentDosimeter.doseRateHistory;
    for (int i=0; i<doseRateHist.size; i++) {
        AMDoseRatePoint *point = [doseRateHist objectAtIndex:i];
        if (max<point.doseRate) {
            max=point.doseRate;
        }
    }
    
    MESChartDataRange *range=[[MESChartDataRange alloc] initWithMinimum:[NSNumber numberWithFloat:[self.unit convertFloatToCurrent:min]] withMaximum:[NSNumber numberWithFloat:[self.unit convertFloatToCurrent:max]]];
    return range;
}

@end
