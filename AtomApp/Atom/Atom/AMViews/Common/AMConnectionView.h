//
//  AMConnectionView.h
//  Atom
//
//  Created by denis svinarchuk on 05.09.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    AMConnectionViewRadar = 1,
    AMConnectionViewCircle = 2
}AMConnectionViewStyle;

@interface AMConnectionView : UIView
@property (assign,nonatomic) BOOL   isConnected;
@property (assign,nonatomic) CGSize size;
@property (assign,nonatomic) AMConnectionViewStyle connectionViewStyle;
@end
