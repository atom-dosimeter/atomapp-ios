//
//  AMSearchingInfoView.h
//  Atom
//
//  Created by denis svinarchuk on 16/10/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AMDoseUnitField.h"
#import "AMPickerView.h"
#import <AtomMeasurerSDK/AMController.h>

@protocol AMBaseDosimeterInfoDelegate <NSObject>

@optional
- (void) didTouchResetButton;

@end

@interface AMBaseDosimeterInfoView : UIView

@property (nonatomic,weak)     AMController   *defaultController;

@property (nonatomic,strong)   NSString       *intervalString;
@property (nonatomic,assign)   NSTimeInterval startTime; 
@property (nonatomic,assign)   float          dose;
@property (nonatomic,assign)   float          doseRate;
@property (nonatomic,assign)   float          thresholdDoseRate;
@property (nonatomic,assign)   float          doseLimit;
@property (nonatomic,assign)   float          thresholdDoseRateLimit;

@property (nonatomic,strong)    id<AMBaseDosimeterInfoDelegate> delegate;

- (id) initWithFrame:(CGRect)frame withIntervalPicker:(BOOL)intervalIsHidden withResetButton:(BOOL)hasResetButton;
- (void) hideIntervalPicker;
@end
