//
//  AMVibrateOrSound.m
//  Atom
//
//  Created by denis svinarchuk on 15/09/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMVibrateOrSound.h"
#import "AMSettings.h"
#import "AMUIPreferences.h"
#import <AudioToolbox/AudioServices.h>
#import <AtomMeasurerSDK/AMControllerManager.h>
#import <AtomMeasurerSDK/AMSimpleController.h>

static AMVibrateOrSound* __shared_instance = nil;

@interface AMVibrateOrSound()
@property (readonly)   AMController *defaultController;
@property (readonly)   AMSettings *settings;
@end

@implementation AMVibrateOrSound
{
    NSTimer *vibrateTimer;
}

+ (id) sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __shared_instance = [[AMVibrateOrSound alloc] init];
    });
    return __shared_instance;
}


- (id) init{
    
    if (__shared_instance) {
        self=__shared_instance;
        return self;
    }
    
    self = [super init];
    
    if (self) {
    
    }
    
    return self;
}

- (AMController*) defaultController{
    return [[AMControllerManager sharedInstance] defaultController];
}

- (AMSettings*) settings{
    return [AMSettings sharedInstance];
}

- (void) play{
    
    if (self.isPlaing) {
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //
        // vibrate or sound
        //
        _isPlaing=YES;

        [self vibrateOrSound: nil];
        
        if (vibrateTimer) {
            [vibrateTimer invalidate];
            vibrateTimer = nil;
        }
        vibrateTimer = [NSTimer scheduledTimerWithTimeInterval:1. target:self selector:@selector(vibrateOrSound:) userInfo:nil repeats:YES];
    });
}

- (void) pause{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (vibrateTimer) {
            [vibrateTimer invalidate];
            vibrateTimer = nil;
            _isPlaing = NO;
        }
    });
}


- (void) vibrateOrSound:(NSTimer*)timer{
    
    if ([self.defaultController isKindOfClass:[AMSimpleController class]]) {
        return;
    }
    
    if (!self.settings.isVibrateOn.boolValue) {
        return;
    }
        
    if([[UIDevice currentDevice].model isEqualToString:@"iPhone"])
    {
        AudioServicesPlaySystemSound (kSystemSoundID_Vibrate);
    }
    else
    {
        // Not an iPhone, so doesn't have vibrate
        // play the less annoying tick noise or one of your own
        for (int i=0; i<5; i++) {
            AudioServicesPlayAlertSound (1105);
            usleep(50000);
        }
    }
}

@end
