//
//  AMSwiftDevicePreferences.h
//  Pods
//
//  Created by denis svinarchuk on 11.11.16.
//
//

#import <Foundation/Foundation.h>
#import "AMBLEProtocol.h"
#import "AMDevicePreferences.h"

@interface AMSwiftDevicePreferences : AMDevicePreferences

@property (readonly,nonatomic) uint16_t soundImpulseDuration;      // msec
@property (readonly,nonatomic) uint16_t vibrateOnImpulseDuration;  // msec
@property (readonly,nonatomic) BOOL     clicksMode;       //  1 = false 1/10  = true
@property (readonly,nonatomic) BOOL     accelerometerMode;  //  false = off, true = on
@property (readonly,nonatomic) uint8_t  searchingMode;    //  0,1,2

@property (readonly,nonatomic) BOOL hasAlarmOnThresholdDosePriority1;
@property (readonly,nonatomic) BOOL hasAlarmOnThresholdDosePriority2;
@property (readonly,nonatomic) BOOL hasAlarmOnThresholdDosePriority3;
@property (readonly,nonatomic) BOOL hasAlarmOnThresholdAll;
@property (readonly,nonatomic) BOOL hasAlarmOnImpulse;
@property (readonly,nonatomic) BOOL hasAlarmsOnAll;

@end
