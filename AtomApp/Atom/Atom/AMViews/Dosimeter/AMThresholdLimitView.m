//
//  AMViewThresholdView.m
//  Atom
//
//  Created by denis svinarchuk on 20.07.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMThresholdLimitView.h"
#import "AMUIPreferences.h"

@interface AMThresholdLimitView()
@property (strong,nonatomic)  CALayer  *blinkLayer;
@property (assign,atomic)     BOOL     isBlinking;
@end

@implementation AMThresholdLimitView

@synthesize isBlinking=_isBlinking;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

@synthesize current = _current, limit=_limit;

- (CALayer*) blinkLayer{
    if (!_blinkLayer) {
        _blinkLayer = [CALayer layer];
        _blinkLayer.backgroundColor = [UIColor clearColor].CGColor;
        _blinkLayer.frame = self.bounds;
        _blinkLayer.opacity = 0.7;
        [self.layer insertSublayer:_blinkLayer atIndex:0];
    }
    return _blinkLayer;
}

- (void) layoutSubviews{
    [super layoutSubviews];
    self.blinkLayer.frame = self.bounds;
}

- (void) startBlink:(float)repeat{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //Create animation
        CAKeyframeAnimation *colorsAnimation = [CAKeyframeAnimation animationWithKeyPath:@"backgroundColor"];
        
        colorsAnimation.values   = [NSArray arrayWithObjects: (id)AM_UI_HIGHCOLOR.CGColor, (id)[UIColor clearColor].CGColor, nil];
        colorsAnimation.keyTimes = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.5], [NSNumber numberWithFloat:0.5], nil];
        colorsAnimation.calculationMode = kCAAnimationPaced;
        colorsAnimation.removedOnCompletion = NO;
        colorsAnimation.fillMode = kCAFillModeForwards;
        colorsAnimation.duration = 2;
        colorsAnimation.repeatCount = repeat;
        
        //Add animation
        [self.blinkLayer addAnimation:colorsAnimation forKey:nil];
    });
}


- (void) stopBlink{
    [self startBlink:1.];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.blinkLayer removeAllAnimations];   
    });
}

- (void) setIsBlinking:(BOOL)isBlinking{
    if (isBlinking==NO) {
        if (_isBlinking) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self stopBlink];
            });    
        }
    }
    else{
        if (isBlinking) {
            if (!_isBlinking) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self startBlink: MAXFLOAT];
                });                                
            }
        }
    }    
    _isBlinking = isBlinking;
}

- (BOOL) isBlinking{
    return _isBlinking;
}

- (void) awakeFromNib{
    [super awakeFromNib];
    self.isBlinking = NO;
}

-(void) setCurrent:(float)current{
    
    _current = current;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!self.isBlinking) {
            if (_current>_limit) {
                self.isBlinking = YES;
            }       
            else{
                self.isBlinking = NO;
            }
            
        }
        if (self.isBlinking) {
            if (_current<=_limit) {
                self.isBlinking = NO;                
            }
        }
    });
}

- (float) current{
    return _current;
}

- (void) setLimit:(float)limit{
    _limit=limit;
}

- (float) limit{
    return _limit;
}

@end
