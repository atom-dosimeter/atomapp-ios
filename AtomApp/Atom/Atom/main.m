//
//  main.m
//  Atom
//
//  Created by denis svinarchuk on 11.07.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AMAppDelegate class]));
    }
}
