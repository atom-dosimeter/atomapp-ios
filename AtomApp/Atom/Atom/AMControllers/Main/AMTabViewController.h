//
//  AMTabViewController.h
//  Atom
//
//  Created by denis svinarchuk on 18.07.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMTabViewController : UITabBarController

@end
