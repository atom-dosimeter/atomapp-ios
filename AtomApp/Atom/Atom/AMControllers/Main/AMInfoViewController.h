//
//  AMInfoViewController.h
//  Atom
//
//  Created by denis svinarchuk on 18.07.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMInfoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityView;

@end
