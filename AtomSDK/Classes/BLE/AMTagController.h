//
//  AMTagController.h
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 26/08/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMController.h"
#import "AMTagDevice.h"

@class AMTagController;

//
// Controller specific settings
//
@interface AMControllerSettings()

- (id) initWithAMTagDevice:(AMTagDevice*)tagDevice;

//
// reference to an exist tagDevice instance
//
@property (weak,nonatomic)     AMTagDevice          *tagDevice;
@property (weak,nonatomic)     AMTagController      *tagController;

@end

/**
 *  "Atom Tag" device controller.
 *
 */
@interface AMTagController : AMController

- (id) initWithDelegate:(id<AMControllerProtocol>)delegate withTagDevice:(AMTagDevice*) device;

@property (readonly,nonatomic) AMTagDeviceMeasurement *measurement;
@property (readonly,nonatomic) NSNumber               *RSSI;
@property (readonly,nonatomic) NSNumber               *avgRSSI;
@property (readonly,nonatomic) BOOL                   isBusy;

- (void) playSearchSignal;

@end
