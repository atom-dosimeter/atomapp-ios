//
//  AMSimpleAudio.h
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 10/09/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMAudioManager.h"


@interface AMSimpleAudio : NSObject
@property (nonatomic,readonly) AMAudioManager *audioManager;
@property (strong,nonatomic) id<AMErrorHandlerProtocol> errorHandler;
@property (nonatomic,readonly) BOOL isRuning;
- (void) start;
- (void) stop;
@end
