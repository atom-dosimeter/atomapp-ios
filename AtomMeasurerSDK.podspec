Pod::Spec.new do |s|

  s.name         = "AtomMeasurerSDK"
  s.version      = "0.5.0"
  s.summary      = "AtomMeasurerSDK is a iOS/OSX framework provides programm abstract layer supports Atom series dosimeters hardware."

  s.homepage     = "http://google.com"

  s.license      = 'MIT'

  s.author             = { "denis svinarchuk" => "denn.nevera@gmail.com" }
  s.social_media_url = "https://twitter.com/denn_nevera"

  #  When using multiple platforms
  s.ios.deployment_target = '7.0'
  s.osx.deployment_target = '10.9'

  s.source       = { :git => "https://bitbucket.org/denn_nevera/atommeasurer.git", :tag => "0.5.0" }

  s.source_files  = 'AtomSDK/Classes/**/*.{h,m}'
  s.public_header_files = 'AtomSDK/Classes/**/*.h'

  s.frameworks = 'CoreAudio', 'AudioToolbox'

  s.requires_arc = true

  # s.xcconfig = { 'HEADER_SEARCH_PATHS' => '$(SDKROOT)/usr/include/libxml2' }
  # s.dependency 'JSONKit', '~> 1.4'

end
