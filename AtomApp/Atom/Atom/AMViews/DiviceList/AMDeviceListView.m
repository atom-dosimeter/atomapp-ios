//
//  AMDeviceListView.m
//  Atom
//
//  Created by denis svinarchuk on 27/08/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMDeviceListView.h"

@implementation AMDeviceListView

- (void) awakeFromNib{
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
    self.title = NSLocalizedString(@"Atom List", @"Title of Atom device list localized string");
}

@end
