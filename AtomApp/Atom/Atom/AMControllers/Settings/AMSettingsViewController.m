//
//  MESettingsViewController.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 11/13/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "AMSettingsViewController.h"
#import "AMSettings.h"
#import "UIView+UserDefinedPropertyBinding.h"
#import "AMUIPreferences.h"
#import "UIImageView+AMLogo.h"
#import "AMPickerView.h"
#import "AMKeyboardBar.h"
#import "NSString+AMUtils.h"
#import "AMUIPreferences.h"
#import <AtomMeasurerSDK/AMDoseUnit.h>

@interface AMSettingsViewController () <UIPickerViewDataSource, UIPickerViewDelegate, UIAlertViewDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate>
@property (nonatomic,strong) AMPickerView *selectionPicker;
@property (nonatomic,readonly) NSNumberFormatter *numberFormatter;

/**
 *  It should be atomic strongly!
 */
@property (atomic,strong) AMSettings *settings;
@end

@implementation AMSettingsViewController

@synthesize settings=_settings;

- (NSNumberFormatter*) numberFormatter{
    return [AMDoseUnit sharedInstance].floatFormatter;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (id) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        self.settings = [AMSettings sharedInstance];
    }
    
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapEvent:)];
    [tap setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tap];
    
    tap.delegate = self;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self tapEvent:nil];
}

#pragma mark - Tap event

- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{ 
    //
    // Avoid text input closing
    //
    if([touch.view class] == [UITextField class]){
        return NO;
    }
    return YES;    
}

- (void)tapEvent:(id)sender {
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    if (!self.selectionPicker.hidden) {
        [self hidePickerView];        
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.settings count];
}

- (UITableViewCell*) tableCellForRowAtIndexPath:(NSIndexPath*)indexPath{
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell_%@",[self.settings typeForSettingNo:indexPath.row]];
    return [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier ];
}

- (UILabel*) textLabelForCell:(UITableViewCell*)cell atIndexPath:(NSIndexPath*)indexPath{
    UILabel *text   = (UILabel*) [cell viewWithTag:1];
    
    text.text = [self.settings captionForSettingNo:indexPath.row];;
    text.numberOfLines = 10;
    text.lineBreakMode = NSLineBreakByWordWrapping;
    
    [text sizeToFit];
    
    return text;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [self tableCellForRowAtIndexPath:indexPath];
    
    CGFloat height = [self textLabelForCell:cell atIndexPath:indexPath].frame.size.height + 15;
    
    if (height<60.0) {
        height = 60.0;
    }
    
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [self tableCellForRowAtIndexPath:indexPath];
 
    @try {
        [self textLabelForCell:cell atIndexPath:indexPath];
        
        NSString *setting_type = [self.settings typeForSettingNo:indexPath.row];
        
        if ([setting_type isEqualToString:@"switch"]) {
            UISwitch *check_switch = (UISwitch*) [cell viewWithTag:2];
            check_switch.on = [self.settings boolForSettingNo:indexPath.row];
            //check_switch.tag = indexPath.row;
            [check_switch setValue:[NSNumber numberWithInteger:indexPath.row] forUndefinedKey:AM_SETTIN_KEY];
        }
        else if ([setting_type isEqualToString:@"number"]) {
            UITextField *textf = (UITextField*) [cell viewWithTag:2];
            textf.delegate = self;
            NSNumber *value = [self.settings valueForSettingNo:indexPath.row inCurrentUnit:YES];
            textf.text = [NSString stringWithFormat:@"%@",[self.numberFormatter stringFromNumber:value]];
            textf.tag = indexPath.row;
        }
        else if ([setting_type isEqualToString:@"selection"]){
            UILabel *label = (UILabel*) [cell viewWithTag:2];
            NSArray *source =[self.settings sourceForSettingNo:indexPath.row];
            NSInteger index = [[source objectAtIndex:0] indexOfObject:[self.settings valueForSettingNo:indexPath.row]];
            label.text = [[source objectAtIndex:1] objectAtIndex:index];
            self.selectionPicker.tag = indexPath.row;
            self.selectionPicker.selected = [self.settings selectedIndexForSettingNo:indexPath.row];
        }
        else if ([setting_type isEqualToString:@"flush_cache"]){
            UIButton *button = (UIButton*) [cell viewWithTag:2];
            [button setTitle:NSLocalizedString(@"Flush button", @"") forState:UIControlStateNormal];
        }

    }
    @catch (NSException *exception) {
        NSLog(@"%@ %s:%i row:%li", exception, __FILE__, __LINE__, (long)indexPath.row);
    }
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 64;
}

- (UIView*) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    view.backgroundColor = [UIColor clearColor];
    
    UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"LogoWhiteSmall"]];        

    CGPoint center = view.center; center.y+=10.0;
    [logo placeToView:view withCenterPoint:center withScale:1.];

    [view addSubview:logo];
        
    return view;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *setting_type = [self.settings typeForSettingNo:indexPath.row];
    if ([setting_type isEqualToString:@"selection"]) {
        self.selectionPicker.selections = [self.settings sourceForSettingNo:indexPath.row];
        [self bringUpPickerViewWithRow:indexPath];
    }
}

- (IBAction)setSwitchFlag:(UISwitch*)check_switch {
    NSNumber *row = [check_switch valueForKeyPath:AM_SETTIN_KEY];
    [self.settings setValue:[NSNumber numberWithBool:check_switch.on] forSettingNo:row.integerValue];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {
        return;
    }
    [NSUserDefaults resetStandardUserDefaults];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:AM_SETTINGS_FLUSH_CACHE_NOTIFICATION object:self];;
}

#pragma mark - picker

- (UIPickerView*) selectionPicker{
    if (_selectionPicker == nil) {
        _selectionPicker = [[AMPickerView alloc] initWithFrame:self.view.bounds];
        _selectionPicker.delegate = self;
        _selectionPicker.dataSource = self;
        _selectionPicker.center = (CGPoint){160, 640};
        _selectionPicker.hidden = YES;
        [self.tableView addSubview:_selectionPicker];
    }
    return _selectionPicker;
}

- (void) pickerView:(AMPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    pickerView.selected = row;
    [self.settings setValue:[NSNumber numberWithInt:[[[pickerView.selections objectAtIndex:0]objectAtIndex:row] intValue]] forSettingNo:pickerView.tag];
    [self hidePickerView];
    [self.tableView reloadData];
}

- (CGFloat) pickerView:(AMPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return  pickerView.heightRow;
}

- (NSInteger) numberOfComponentsInPickerView:(AMPickerView *)pickerView{
    return 1;
}

- (NSInteger) pickerView:(AMPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [[pickerView.selections objectAtIndex:0] count];
}

- (UIView*) pickerView:(AMPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 44)];
   
    label.backgroundColor = AM_UI_BG_COLOR;
    label.textColor = [UIColor whiteColor];
    
    int fontSize = 18;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        fontSize = 30;
    }
    
    label.font = [UIFont fontWithName:AM_UI_BOLD_FONT size:fontSize];
    label.textAlignment = NSTextAlignmentCenter;
    
    label.text = [NSString stringWithFormat:@"%@", [[pickerView.selections objectAtIndex:1] objectAtIndex:row] ];
    
    return label;
}

- (void)bringUpPickerViewWithRow:(NSIndexPath*)indexPath
{
    UITableViewCell *currentCellSelected = [self.tableView cellForRowAtIndexPath:indexPath];
    self.selectionPicker.alpha = 0.0;
    self.selectionPicker.hidden = NO;
    self.selectionPicker.center = (CGPoint){currentCellSelected.bounds.size.width/2, self.tableView.frame.origin.y + currentCellSelected.frame.origin.y+currentCellSelected.frame.size.height/2.};
    self.selectionPicker.tag = indexPath.row;
    self.selectionPicker.heightRow = currentCellSelected.frame.size.height;
    [self.selectionPicker reloadAllComponents];
    
    [self.selectionPicker selectRow:[self.settings selectedIndexForSettingNo:self.selectionPicker.tag] inComponent:0 animated:YES];
    
    [UIView animateWithDuration:AM_UI_ANIMATE_DURATION
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.selectionPicker.alpha = 1.0;
                     }
                     completion:nil
     ];
}

- (void)hidePickerView
{
    [UIView animateWithDuration:AM_UI_ANIMATE_DURATION
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.selectionPicker.alpha = 0.0;
                     }
                     completion:^(BOOL finished){
                         self.selectionPicker.hidden = YES;
                     }
     ];
}

- (void) textFieldDidEndEditing:(UITextField *)textField{
    NSNumber *value = [textField.text floatNumber];
    [self.settings setValue:value forSettingNo:textField.tag inCurrentUnit:YES];
    [self.tableView reloadData];

}

-(void)textFieldDidBeginEditing:(UITextField *)textField {    
    [self hidePickerView];
    if (textField.inputAccessoryView==nil && UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        textField.inputAccessoryView = [[AMKeyboardBar alloc] init];
    }
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{    
    [self.view endEditing:YES];
    return YES;
}

- (void)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (void) doneButton:(id)sender{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
@end

