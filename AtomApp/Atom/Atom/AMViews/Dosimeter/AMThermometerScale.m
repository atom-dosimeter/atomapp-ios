//
//  AMThermometerScale.m
//  Atom
//
//  Created by denis svinarchuk on 21/07/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMThermometerScale.h"
#import "AMSettings.h"
#import "AMUIPreferences.h"
#import "NSAttributedString+AMUnits.h"
#import "AMThermometerMercury.h"
#import "AMThermometerView.h"
#import <AtomMeasurerSDK/AMDoseUnit.h>
#import <MESChartView/MESChartCanvas.h>

@interface __AMPaths : NSObject

@property (nonatomic,readonly) CGMutablePathRef mainlines;
@property (nonatomic,readonly) CGMutablePathRef sublines;
@property (nonatomic,readonly) CGMutablePathRef threshold;

@end

@implementation __AMPaths

@synthesize mainlines=_mainlines, sublines=_sublines, threshold=_threshold;

- (id) init{
    self = [super init];
    
    if (self) {
    }
    
    return self;
};




- (CGMutablePathRef)mainlines{
    if (!_mainlines) {
        _mainlines = CGPathCreateMutable();
    }
    return _mainlines;
}

- (CGMutablePathRef)sublines{
    if (!_sublines) {
        _sublines = CGPathCreateMutable();
    }
    return _sublines;
}


- (CGMutablePathRef)threshold{
    if (!_threshold) {
        _threshold = CGPathCreateMutable();
    }
    return _threshold;
}

- (void) dealloc{
    if (_mainlines)   CGPathRelease(_mainlines);
    if (_sublines)    CGPathRelease(_sublines);
    if (_threshold)   CGPathRelease(_threshold);
}

@end


@interface AMThermometerScale() <UIGestureRecognizerDelegate>

@property (atomic, assign) BOOL isThresholdChanging;

@property (nonatomic,strong)     CAShapeLayer *scaleMainLayer;
@property (nonatomic,strong)     CAShapeLayer *scaleSubLayer;
@property (nonatomic,strong)     CAShapeLayer *thresholdMarkerLayer;
@property (nonatomic,strong)     NSMutableArray *levelLabels;
@property (nonatomic,strong)     UIPanGestureRecognizer *panThresholMarker;

@property (nonatomic,readonly)   float thresholdY;
@property (nonatomic,readonly)   AMSettings *settings;

@end

@implementation AMThermometerScale
{
    float lastLow, lastHigh, lastDoseRateThreshold;
    float thresholdOpacityBase, thresholdLineWidthBase;
    BOOL  thresholdMarkerSelected;
    AMDoseUnitType lastUnitType;
}

@synthesize settings=_settings, isThresholdChanging=_isThresholdChanging;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self panThresholMarker];
        [CAShapeLayer setAnimationDuration:AM_UI_ANIMATE_DURATION/2.];
        [self resetLasts];
        lastUnitType = [AMDoseUnit sharedInstance].current;
    }
    return self;
}

- (void) resetLasts{
    lastDoseRateThreshold = 0.0;
    lastHigh = 0.0;
    lastLow = 0.0;
}

- (AMSettings*) settings{
    if (!_settings) {
        _settings = [AMSettings sharedInstance];
    }
    return _settings;
}

- (void) setLow:(float)low{
    self.mercury.low = low;
    [super setLow:low];
}

- (void) setHigh:(float)high{
    self.mercury.high = high;
    [super setHigh:high];
}

- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gesture shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    CGPoint panPoint = [gesture locationInView:self];
    float ty=self.thresholdY;
    if (
        (panPoint.y+AM_THERMOMETER_THRESHOLD_MARKER_PAN_AREA)>=ty
        &&
        (panPoint.y-AM_THERMOMETER_THRESHOLD_MARKER_PAN_AREA)<=ty
        ) {
        return NO;
    }
    return YES;
}

- (UIPanGestureRecognizer*) panThresholMarker{
    if (!_panThresholMarker) {
        _panThresholMarker = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panThresholdMarkerHandler:)];
        _panThresholMarker.delegate = self;
        [self addGestureRecognizer:_panThresholMarker];
    }
    return _panThresholMarker;
}

- (void) selectThresholdMarker{
    thresholdMarkerSelected = YES;
    self.thresholdMarkerLayer.opacity = 1.0;
    self.thresholdMarkerLayer.lineWidth *= 1.5;
}

- (void) deselectThresholdMarker{
    thresholdMarkerSelected = NO;
    self.thresholdMarkerLayer.opacity = thresholdOpacityBase;
    self.thresholdMarkerLayer.lineWidth = thresholdLineWidthBase;
    [self resetLasts];
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    if ([event allTouches].count==1) {
        int i=0;
        UITouch *touchFirst = nil;
        for (UITouch *t in touches) {
            if (i==0)
                touchFirst = t;
            i++; if (i>1) break;
        }
        if (touchFirst) {
            CGPoint touchPoint  = [touchFirst locationInView:self];
            float ty=self.thresholdY;
            if ((touchPoint.y+AM_THERMOMETER_THRESHOLD_MARKER_PAN_AREA)>=ty && (touchPoint.y-AM_THERMOMETER_THRESHOLD_MARKER_PAN_AREA)<=ty) {
                [self selectThresholdMarker];
            }
        }
    }
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self deselectThresholdMarker];
}

- (void) panThresholdMarkerHandler:(UIPanGestureRecognizer*)gesture{
    
    CGPoint panPoint = [gesture locationInView:self];
    
    float newDoseThreshold =[self thresholdFromY:panPoint.y];
    
    if (newDoseThreshold>self.maxHigh) {
        newDoseThreshold=self.maxHigh;
    }
    
    if (newDoseThreshold<=AM_THERMOMETER_MIN_THRESHOLD) {
        newDoseThreshold = AM_THERMOMETER_MIN_THRESHOLD;
    }
    
    float y = [self pointYFromTreshold:newDoseThreshold];
    if (y<0. && newDoseThreshold<self.maxHigh) {
        newDoseThreshold = [self thresholdFromY:-.0001];
    }
    
    if (gesture.state == UIGestureRecognizerStateBegan) {
        _isThresholdChanging = YES;
    }
    
    if ( thresholdMarkerSelected ) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{            
            self.thermometer.thresholdDoseRate = [NSNumber numberWithFloat: ceilf(newDoseThreshold)];
            if (self.thermometer.delegate && [self.thermometer.delegate respondsToSelector:@selector(amThermometerView:didContinueUpdatingThresholdDoseRate:)]) {
                [self.thermometer.delegate amThermometerView:self.thermometer didContinueUpdatingThresholdDoseRate:self.thermometer.thresholdDoseRate];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.thermometer reload];                
            });
        });
    }
    
    if (gesture.state == UIGestureRecognizerStateEnded
        ||
        gesture.state == UIGestureRecognizerStateFailed
        ||
        gesture.state == UIGestureRecognizerStateCancelled
        ) {
        [self deselectThresholdMarker];
        _isThresholdChanging = NO;
        if (self.thermometer.delegate && [self.thermometer.delegate respondsToSelector:@selector(amThermometerView:didEndUpdatingThresholdDoseRate:)]) {
            [self.thermometer.delegate amThermometerView:self.thermometer didEndUpdatingThresholdDoseRate:self.thermometer.thresholdDoseRate];
        }
    }
    
}

- (CAShapeLayer*) scaleMainLayer{
    if (!_scaleMainLayer) {
        _scaleMainLayer = [CAShapeLayer layer];
        _scaleMainLayer.strokeColor = [UIColor whiteColor].CGColor;
        _scaleMainLayer.lineWidth = 1;
        _scaleMainLayer.opacity = 0.5;
        _scaleMainLayer.drawsAsynchronously = YES;
        _scaleMainLayer.shouldRasterize = NO;
        _scaleMainLayer.backgroundColor = [UIColor clearColor].CGColor;
        
        _scaleMainLayer.frame = self.bounds;
        
        [self.layer addSublayer:_scaleMainLayer];
    }
    
    
    return _scaleMainLayer;
}

- (CAShapeLayer*) scaleSubLayer{
    if (!_scaleSubLayer) {
        _scaleSubLayer = [CAShapeLayer layer];
        _scaleSubLayer.strokeColor = [UIColor whiteColor].CGColor;
        _scaleSubLayer.lineWidth = .5;
        _scaleSubLayer.opacity = 0.2;
        _scaleSubLayer.drawsAsynchronously = YES;
        _scaleSubLayer.shouldRasterize = NO;
        _scaleSubLayer.backgroundColor = [UIColor clearColor].CGColor;
        _scaleSubLayer.frame = self.bounds;
        
        [self.layer addSublayer:_scaleSubLayer];
    }
    
    
    return _scaleSubLayer;
}

- (CAShapeLayer*) thresholdMarkerLayer{
    if (!_thresholdMarkerLayer) {
        _thresholdMarkerLayer = [CAShapeLayer layer];
        _thresholdMarkerLayer.strokeColor = [UIColor colorWithRed:0.9 green:0. blue:0. alpha:1.0].CGColor;
        
        thresholdLineWidthBase = _thresholdMarkerLayer.lineWidth = 4.;
        thresholdOpacityBase = _thresholdMarkerLayer.opacity = 0.6;
        
        _thresholdMarkerLayer.drawsAsynchronously = YES;
        _thresholdMarkerLayer.shouldRasterize = NO;
        _thresholdMarkerLayer.backgroundColor = [UIColor clearColor].CGColor;
        
        _thresholdMarkerLayer.shadowColor = [UIColor blackColor].CGColor;
        _thresholdMarkerLayer.shadowOffset = CGSizeMake(0.0, 1.0);
        _thresholdMarkerLayer.shadowRadius = 1.0;
        _thresholdMarkerLayer.shadowOpacity = 0.5;
        
        _thresholdMarkerLayer.frame = self.bounds;
        
        [self.layer addSublayer:_thresholdMarkerLayer];
        
    }
    
    return _thresholdMarkerLayer;
}

- (NSMutableArray*) levelLabels{
    if (!_levelLabels) {
        _levelLabels = [[NSMutableArray alloc] init];
    }
    return _levelLabels;
}

- (void) removeLevelLabels{
    if (self.levelLabels.count>0) {
        for (UILabel *l in self.levelLabels) {
            [l removeFromSuperview];
        }
        
        [self.levelLabels removeAllObjects];
    }
}

- (void) setNeedsDisplay{
    if (lastUnitType!=[AMDoseUnit sharedInstance].current) {
        lastUnitType=[AMDoseUnit sharedInstance].current;
        [self resetLasts];
    }

    [super setNeedsDisplay];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    BOOL scaleChanged = NO;
    if (lastLow != self.low || lastHigh != self.high ) {
        
        scaleChanged =YES;
        lastLow = self.low;
        lastHigh = self.high;
        
        AMDoseUnit *unit = [AMDoseUnit sharedInstance];
        float step = self.high  / 10.;
        float width = self.bounds.size.width;
        
        __AMPaths *paths = [[__AMPaths alloc] init];
        
        [self removeLevelLabels];
        
        int i=0;
        NSString *lastsymbol=@"";
        for (float h=self.high; h>self.low;) {
            float y = self.bounds.size.height-[self log10f:h] * self.bounds.size.height;
            
            CGMutablePathRef path;
            float offset = AM_THERMOMETER_OFSET_WIDTH;
            if (i==0) {
                path = paths.mainlines;
                
                UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, y, width-offset, AM_THERMOMETER_LABEL_HEIGHT)];
                UILabel *symbolLabel = [[UILabel alloc] initWithFrame:CGRectMake(offset, y-AM_THERMOMETER_LABEL_HEIGHT, width, AM_THERMOMETER_LABEL_HEIGHT)];
                
                [self.levelLabels addObject:label];
                [self.levelLabels addObject:symbolLabel];
                
                float hv=0.0;
                NSString *symbol = [unit unitSymbolForValue:&hv withInputValue:h];
                
                if (![lastsymbol isEqualToString:symbol]) {
                    symbolLabel.text = [NSString stringWithFormat:@"%@%@", symbol, AM_UNIT_PERHOUR];
                }
                else{
                    symbolLabel.text = @"";
                }
                
                lastsymbol = symbol;
                NSNumberFormatter *formater;
                if (hv<1.) {
                    formater = [unit formatterWithPrecision:2];
                }
                else
                    formater = unit.intFormatter;
                
                label.text = [NSString stringWithFormat:@"%@", [formater stringFromNumber: [NSNumber numberWithFloat:hv]]];;
                
                symbolLabel.textColor = label.textColor = AM_THERMOMETER_LABEL_COLOR;
                symbolLabel.backgroundColor = label.backgroundColor = [UIColor clearColor];
                symbolLabel.font = label.font = [UIFont fontWithName:AM_UI_BOLD_FONT size:0.8*AM_THERMOMETER_LABEL_HEIGHT];
                label.textAlignment = NSTextAlignmentRight;
                symbolLabel.textAlignment = NSTextAlignmentLeft;
                
                symbolLabel.shadowOffset = label.shadowOffset = CGSizeMake(.3, .3);
                symbolLabel.shadowColor = label.shadowColor = [UIColor blackColor];
                
                [self addSubview:label];
                [self addSubview:symbolLabel];
                
            }
            else{
                path = paths.sublines;
            }
            CGPathMoveToPoint(path, nil, offset, y);
            CGPathAddLineToPoint(path, nil, width-offset, y);
            CGPathCloseSubpath(path);
            
            
            h-=step;
            
            i++;
            if (h<=step) {
                step/=10.0;
                i = 0;
            }
        }
        
        [self.scaleMainLayer setPath:paths.mainlines animated:YES];
        [self.scaleSubLayer setPath:paths.sublines animated:YES];
    }
    
    [self drawThresholdMarker:scaleChanged];
}

- (void) drawThresholdMarker:(BOOL)scaleChanged{
    
    NSNumber *thresholdDoseRate=self.thermometer.thresholdDoseRate;

    if (lastDoseRateThreshold!=thresholdDoseRate.floatValue || scaleChanged) {
        
        lastDoseRateThreshold = thresholdDoseRate.floatValue;
        
        float width = self.bounds.size.width;
        __AMPaths *paths = [[__AMPaths alloc] init];
        
        float y_thresh_log = self.thresholdY;
        
        CGPathMoveToPoint(paths.threshold, nil, -AM_THERMOMETER_OFSET_WIDTH, y_thresh_log);
        CGPathAddLineToPoint(paths.threshold, nil, width+AM_THERMOMETER_OFSET_WIDTH, y_thresh_log);
        CGPathCloseSubpath(paths.threshold);
        
        [self.thresholdMarkerLayer setPath:paths.threshold animated:YES];

    }
}

- (float) thresholdY{
    NSNumber *thresholdDoseRate=self.thermometer.thresholdDoseRate;
    if (thresholdDoseRate == nil || thresholdDoseRate.floatValue<=0.0f) {
        return 0.0f;
    }
    return [self pointYFromTreshold:thresholdDoseRate.floatValue];
}

- (float) pointYFromTreshold:(float)threshold{
    return self.bounds.size.height*(1 - [self log10f:threshold]);
}


- (float) thresholdFromY:(float)y{
    return [self valueFromLog10:1-(y)/self.bounds.size.height];
}

@end
