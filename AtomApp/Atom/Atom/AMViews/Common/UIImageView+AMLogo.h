//
//  UIImageView+AMLogo.h
//  Atom
//
//  Created by denis svinarchuk on 18.07.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (AMLogo)
- (void) placeToView:(UIView *)view withCenterPoint:(CGPoint)center withScale:(float)scale;
@end
