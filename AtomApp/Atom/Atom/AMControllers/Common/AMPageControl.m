//
//  AMPageControll.m
//  Atom
//
//  Created by denis svinarchuk on 17/07/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMPageControl.h"

@implementation AMPageControl

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
