//
//  AMDoseUnit.m
//  Atom
//
//  Created by denis svinarchuk on 18.07.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMDoseUnit.h"
#import "AMConstants.h"

static AMDoseUnit *__shared_instance = nil;

@interface AMDoseUnit()
@property (nonatomic,strong) NSString    *currentSymbol;
@property (nonatomic,strong) NSMutableArray     *availableUnits_;
@property (nonatomic,strong) NSMutableArray     *availableSymbols_;
@end

@implementation AMDoseUnit

@synthesize floatFormatter=_floatFormatter, intFormatter=_intFormatter, percentFormatter=_percentFormatter;

+ (instancetype) sharedInstance{
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __shared_instance = [[AMDoseUnit alloc] init];
    });
    
    return __shared_instance;
}

- (instancetype) init{
    
    if (__shared_instance) {
        self = __shared_instance;
        return self;
    }
    
    self = [super init];
    
    if (self) {
        __shared_instance = self;
        
        _current = (AMDoseUnitType)[[NSUserDefaults standardUserDefaults] integerForKey:@"doseUnit"];
        _availableUnits_ = [[NSMutableArray alloc] initWithCapacity:kAMDoseUnitNumber];
        _availableSymbols_ = [[NSMutableArray alloc] initWithCapacity:kAMDoseUnitNumber];
                        
        for (int i=0; i<kAMDoseUnitNumber; i++) {
            [_availableUnits_ addObject:[NSNumber numberWithInt:i]];
            [_availableSymbols_ addObject:[AMDoseUnit unitSymbol:i]];
        }        
    }
    
    return self;
}

- (int) precision{
    
    int prec = AM_UNIT_PRECISION;
    
    switch (self.current) {
        case AM_DOSE_UNIT_NNSV:
            prec = 0;
            break;
        case AM_DOSE_UNIT_MKSV:
            prec = 2;
            break;
        case AM_DOSE_UNIT_MLSV:
            prec = 5;
            break;
        case AM_DOSE_UNIT_MKRN:
            prec = 1;
            break;
        default:
            break;
    }
    
    return prec;
}

- (NSNumberFormatter*) floatFormatter{
    NSInteger precision = [self precision];

    if (!_floatFormatter) {
        _floatFormatter = [self formatterWithPrecision:precision];
    }
    
    _floatFormatter.maximumFractionDigits=precision;
    _floatFormatter.minimumFractionDigits=precision;
    
    return _floatFormatter;
}

- (NSNumberFormatter*) intFormatter{
    if (!_intFormatter) {
        _intFormatter = [self formatterWithPrecision:0];
    }
    
    return _intFormatter;    
}

- (NSNumberFormatter*) percentFormatter{
    if (!_percentFormatter) {
        _percentFormatter = [[NSNumberFormatter alloc] init];
        _percentFormatter.numberStyle=NSNumberFormatterPercentStyle;
    }
    return _percentFormatter;
}

- (NSNumberFormatter*) formatterWithPrecision:(NSUInteger)precision{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    
    formatter.locale=[NSLocale currentLocale];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    formatter.alwaysShowsDecimalSeparator=NO;
    
    formatter.maximumFractionDigits=precision;
    formatter.minimumFractionDigits=precision;
    
    return formatter;
}


- (NSArray*) availableUnits{
    return _availableUnits_;
}

- (NSArray*) availableSymbols{
    return _availableSymbols_;
}

+ (float) convertValue:(float)value to:(AMDoseUnitType)unit{
    
    double rvalue;
    
    switch (unit) {
        case AM_DOSE_UNIT_NNSV:            
            rvalue = value;
            break;
        case AM_DOSE_UNIT_MKSV:
            rvalue = value / 1000.0f;
            break;
        case AM_DOSE_UNIT_MLSV:
            rvalue = value / 1000.0f / 1000.0f;
            break;
        case AM_DOSE_UNIT_MKRN:
            rvalue = value / 1000.0f * kAMRentgenSivertConvertion;
            break;
        default:
            rvalue = value;
            break;
    }
    
    return rvalue;
}

- (float) fromValueToBase:(float)value{
    double rvalue;
    
    switch (self.current) {
        case AM_DOSE_UNIT_NNSV:            
            rvalue = value;
            break;
        case AM_DOSE_UNIT_MKSV:
            rvalue = value * 1000.0f;
            break;
        case AM_DOSE_UNIT_MLSV:
            rvalue = value * 1000.0f * 1000.0f;
            break;
        case AM_DOSE_UNIT_MKRN:
            rvalue = value * 1000.0f / kAMRentgenSivertConvertion;
            break;
        default:
            rvalue = value;
            break;
    }
    return rvalue;
}

- (float) fromValueToUpper:(float)value unit:(AMDoseUnitType*)type{
    double rvalue;
    
    *type = AM_DOSE_UNIT_SV;
    
    switch (self.current) {
        case AM_DOSE_UNIT_NNSV:
        case AM_DOSE_UNIT_MKSV:
        case AM_DOSE_UNIT_MLSV:
            rvalue = value / 1000000000.0f; // sv
            break;
        case AM_DOSE_UNIT_MKRN:
            rvalue = value / 1000000000.0f * kAMRentgenSivertConvertion; // rentgen
            *type = AM_DOSE_UNIT_RN;
            break;
        default:
            rvalue = value;
            break;
    }
    return rvalue;
}

- (float) fromUpperValueToBase:(float)value{
    double rvalue;
    
    switch (self.current) {
        case AM_DOSE_UNIT_NNSV:
        case AM_DOSE_UNIT_MKSV:
        case AM_DOSE_UNIT_MLSV:
            rvalue = value * 1000000000.0f; // sv
            break;
        case AM_DOSE_UNIT_MKRN:
            rvalue = value * 1000000000.0f / kAMRentgenSivertConvertion; // rentgen
            break;
        default:
            rvalue = value;
            break;
    }
    return rvalue;
}

- (NSNumber*) fromUpperNumberToBase:(NSNumber*)value{
    return [NSNumber numberWithFloat:[self fromUpperValueToBase:value.floatValue]];
}


- (NSNumber*) fromNumberToBase:(NSNumber*)value{
    return  [NSNumber numberWithFloat: [self fromValueToBase:value.floatValue]];
}

- (float) convertFloatToCurrent:(float)value{
    return [AMDoseUnit convertValue:value to:self.current];
}

- (NSNumber*) convertNumberToCurrent:(NSNumber *)value{
    return [NSNumber numberWithFloat:[self convertFloatToCurrent:value.floatValue]];
}

- (NSString*) localeStringFromFloat:(float)value{
    return [self.floatFormatter stringFromNumber:[NSNumber numberWithFloat:[AMDoseUnit convertValue:value to:self.current]]];
};

- (NSString*) localeDoseStringFromFloat:(float)value{
    return [NSString stringWithFormat:@"%@%@", [self localeStringFromFloat:value], [AMDoseUnit unitSymbol:self.current]];
};

- (NSString*) localeDoseRateStringFromFloat:(float)value{
    return [NSString stringWithFormat:@"%@%@/%@", [self localeStringFromFloat:value], [AMDoseUnit unitSymbol:self.current], NSLocalizedString(@"/h", @"")];
};


- (NSString*) localeStringFromInt:(UInt64)value{
    return [self.intFormatter stringFromNumber:[NSNumber numberWithUnsignedInteger:(NSUInteger)value]];
}

+ (NSString*) unitSymbol:(AMDoseUnitType)unit{
    
    NSString *symbol = NSLocalizedString(@"nSv", @"");
    switch (unit) {
        case AM_DOSE_UNIT_NNSV:                        
            break;
        case AM_DOSE_UNIT_MKSV:
            symbol = NSLocalizedString(@"µSv", @"");
            break;
        case AM_DOSE_UNIT_MLSV:
            symbol = NSLocalizedString(@"mSv", @"");
            break;
        case AM_DOSE_UNIT_MKRN:
            symbol = NSLocalizedString(@"µR", @"");
            break;
        case AM_DOSE_UNIT_SV:
            symbol = NSLocalizedString(@"Sv", @"");
            break;
        case AM_DOSE_UNIT_RN:
            symbol = NSLocalizedString(@"R", @"");
            break;
        default:
            break;
    }
    
    return symbol;
}

+ (NSString*) unitSymbolForValue:(float*)value withInputValue:(float)input andUnit:(AMDoseUnitType)unit{
    NSString *symbol = NSLocalizedString(@"nSv", @"");
    
    double rvalue = input;
    
    if (unit==AM_DOSE_UNIT_MKRN) {
        if (input>=10000.0f){
            rvalue = input/1000000.0f*kAMRentgenSivertConvertion;
            symbol = NSLocalizedString(@"mR", @"");
        }
        else if (input>=100.0f){
            rvalue = input/1000.0f*kAMRentgenSivertConvertion;
            symbol = NSLocalizedString(@"µR", @"");
        }
        else{
            rvalue = input*kAMRentgenSivertConvertion;
            symbol = NSLocalizedString(@"nR", @"");
        }
    }
    else{
        if (input>=1000000.0f){
            rvalue = input/1000000.0f;
            symbol = NSLocalizedString(@"mSv", @"");
        }
        else if (input>=1000.0f){
            rvalue = input/1000.0f;
            symbol = NSLocalizedString(@"µSv", @"");
        }
    }
    
    if (value)
        *value = rvalue;
    
    return symbol;
}

- (NSString*) unitSymbolForValue:(float*)value withInputValue:(float)input{
    return [AMDoseUnit unitSymbolForValue:value withInputValue:input andUnit:self.current];
}


- (NSString*) currentSymbol{
    return [AMDoseUnit unitSymbol:self.current];
}

- (AMDoseUnitType) number2Unit:(NSNumber *)index{
    int i = [index intValue]; 
    return i>kAMDoseUnitNumber?kAMDoseUnitNumber-1:i<0?kAMDoseUnitNumber:i;
}



@end
