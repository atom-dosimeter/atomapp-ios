//
//  AMFirstViewController.m
//  Atom
//
//  Created by denis svinarchuk on 11.07.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMDosimeterViewController.h"
#import "AMUIPreferences.h"
#import "UIImageView+AMLogo.h"
#import "AMControllerUpdateProtocol.h"
#import "AMSettings.h"
#import "AMVibrateOrSound.h"

#import "AMDeviceSettingsViewController.h"

#import <AtomMeasurerSDK/AMControllerManager.h>
#import <AtomMeasurerSDK/AMSimpleController.h>

#import <MediaPlayer/MediaPlayer.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>


@interface AMNameAlertView : UIAlertView
@property (strong,nonatomic) NSString *nameNew;
@property (strong,nonatomic) NSString *nameOld;
@property (weak,nonatomic)   AMController *amController;
@end

@implementation AMNameAlertView
@synthesize nameNew=_nameNew, nameOld=_nameOld;
@end

@interface AMDosimeterViewController () <AMControllerManagerDelegate, UIAlertViewDelegate>

@property (nonatomic,strong)     UIAlertView *volumeAlertView;
@property (nonatomic,strong)     UIAlertView *alertBLEWarnView;


@property (nonatomic,strong)     AMNameAlertView *nameAlertView;
@property (nonatomic,strong)     UIAlertView *absentAlertView;
@property (nonatomic,assign)     BOOL doNotShowAlertView;
@property (nonatomic,assign)     BOOL isAllertShown;

@property (nonatomic,readonly)   AMControllerManager *amControllerManager;
@property (nonatomic,strong)     AMDeviceSettingsViewController *settingsController;

@property (nonatomic,strong)    AMVibrateOrSound *vibrateOrSound;

@end

@implementation AMDosimeterViewController
{
    UIImageView *logoImage;
    NSTimeInterval timeBetweenAlerts;
    NSTimer *particlesAbsentTimer;
    
    BOOL isActive;
    
}

@synthesize statusBarView=_statusBarView, amControllerManager=_amControllerManager;

#pragma mark - ALERTS
- (UIAlertView*) volumeAlertView{
    if (!_volumeAlertView) {
        _volumeAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Volume", @"Volume alert")
                                                      message:NSLocalizedString(@"Atom Simple does not have enough power. Increase the volume.", @"Volume alert message")
                                                     delegate:self
                                            cancelButtonTitle:NSLocalizedString(@"OK", @"OK button")
                                            otherButtonTitles:NSLocalizedString(@"Don't show again", @"Don't show again button") ,nil];
        _volumeAlertView.cancelButtonIndex = 0;
        _volumeAlertView.delegate = self;
    }
    return _volumeAlertView;
}

- (UIAlertView*) absentAlertView{
    if (!_absentAlertView) {
        _absentAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Check Atom Simple", @"Check Atom Simple")
                                                      message:NSLocalizedString(@"Please, make sure that you connect with Atom Simple. I could not detect any particles for a long time.", @"Check device alert message")
                                                     delegate:self
                                            cancelButtonTitle:NSLocalizedString(@"Close", @"Close button")
                                            otherButtonTitles:nil];
        _absentAlertView.cancelButtonIndex = 0;
        _absentAlertView.delegate = self;
        _absentAlertView.hidden = YES;
    }
    return _absentAlertView;
}

- (AMNameAlertView*) nameAlertView{
    if (!_nameAlertView) {
        _nameAlertView = [[AMNameAlertView alloc] initWithTitle:NSLocalizedString(@"Device name is changed", @"Device name is changed")
                                                        message:NSLocalizedString(@"", @"")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Keep old", @"Keep old name button")
                                              otherButtonTitles:NSLocalizedString(@"Accept new", @"Accept new name button alert"), nil];
        _nameAlertView.cancelButtonIndex = 0;
        _nameAlertView.delegate = self;
    }
    return _nameAlertView;
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView==self.nameAlertView) {
        AMNameAlertView *alert=(AMNameAlertView*)alertView;
        if (buttonIndex==0) {
            //
            // keep old
            //
            alert.amController.settings.name =alert.nameOld;
            
        }
        else{
            //
            // accept new
            //
            alert.amController.settings.name = alert.nameNew;
        }
    }
}

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (alertView==self.volumeAlertView) {
        if (buttonIndex==1) {
            self.doNotShowAlertView = YES;
        }
        self.isAllertShown = NO;
    }
}

- (AMVibrateOrSound*) vibrateOrSound{
    if (!_vibrateOrSound) {
        _vibrateOrSound = [AMVibrateOrSound sharedInstance];
    }
    return _vibrateOrSound;
}

#pragma mark - geters

- (AMControllerManager*) amControllerManager{
    if (!_amControllerManager) {
        _amControllerManager  = [AMControllerManager sharedInstance];
        _amControllerManager.delegate = self;
    }
    return _amControllerManager;
}

#pragma mark - Main dosimeter controller

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"measuringViewController"]];
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"searchingViewController"]];
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"deviceListView"]];
    [self addChildViewController:_settingsController = [self.storyboard instantiateViewControllerWithIdentifier:@"deviceSettingsView"]];
    
    //AMSettings *settings = [AMSettings sharedInstance];
    
    [self.amControllerManager start];
    
    //self.amControllerManager.defaultController.dosimeter.particlesSearchedMinimumCount = settings.searchingMode.intValue;
    self.amControllerManager.defaultController.dosimeter.particlesSearchedMinimumCount = kAMDosimeterSearchModeNormal;
    [self applyConnection:self.amControllerManager.defaultController];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnterBackgroundHandler:) name:UIApplicationDidEnterBackgroundNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didBecomActiveHandler:) name:UIApplicationDidBecomeActiveNotification object:nil];

    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        if (granted) {
            NSLog(@"Microphone granted");
        } else {
            NSLog(@"Microphone denied");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Microphone Access Denied", @"Microphone Access Denied alert title")
                                                            message:NSLocalizedString(@"You must allow microphone access in Settings > Privacy > Microphone",@"Microphone Access Denied alert message")
                                                           delegate:nil
                                                  cancelButtonTitle:@"Close"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }];
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void) didEnterBackgroundHandler:(NSNotification*)event{
    isActive = NO;
    [self stopAbsentTimer];    
}

- (void) didBecomActiveHandler:(NSNotification*)event{
    isActive = YES;
}

//
// Reset settings
//
- (void) updateWhenSettingsIsChanged:(NSNotification*) event{
        
    dispatch_async(dispatch_get_main_queue(), ^{
        AMSettings *settings = [AMSettings sharedInstance];
        
        for (AMController *controller in [self.amControllerManager.controllerList allValues]) {
            //NSLog(@"Controller %@", controller);
            //controller.dosimeter.particlesSearchedMinimumCount = settings.searchingMode.intValue;
            controller.dosimeter.particlesSearchedMinimumCount = kAMDosimeterSearchModeNormal;
            controller.dosimeter.doseRateHistoryInterval = settings.historyInterval.floatValue;
        }
        
        for (id<AMControllerUpdateProtocol> vc in self.childViewControllers) {
            if ([vc respondsToSelector:@selector(didDosimeterUpdate:)]) {
                [vc didSettingsUpdate:[AMSettings sharedInstance]];
            }
        }
    });
    [self amDosimeterValuesChanged:self.amControllerManager.defaultController.dosimeter];
}

- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController *)controller didUpdateSettings:(AMControllerSettings *)settings{
    dispatch_async(dispatch_get_main_queue(), ^{
        for (id<AMControllerUpdateProtocol> vc in self.childViewControllers) {
            if ([vc respondsToSelector:@selector(didControllerSettingsUpdate:)]) {
                [vc didControllerSettingsUpdate:controller];
            }
        }
    });
}

- (void) amControllerManager:(AMControllerManager *)manager amControllerDidCommitChanges:(AMController *)controller{
    //NSLog(@" ### DID commit chamges: %@", controller.settings.UUIDString);
    dispatch_async(dispatch_get_main_queue(), ^{
        for (id<AMControllerUpdateProtocol> vc in self.childViewControllers) {
            if ([vc respondsToSelector:@selector(didControllerChangesCommit:)]) {
                [vc didControllerChangesCommit:controller];
            }
        }
    });
}

- (void) amControllerManager:(AMControllerManager *)manager amControllerWillCommitChanges:(AMController *)controller{
    //NSLog(@" ### WILL commit chamges: %@", controller.settings.UUIDString);
    dispatch_async(dispatch_get_main_queue(), ^{
        for (id<AMControllerUpdateProtocol> vc in self.childViewControllers) {
            if ([vc respondsToSelector:@selector(willControllerChangesCommit:)]) {
                [vc willControllerChangesCommit:controller];
            }
        }
    });
}

- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController *)controller didChangeFromName:(NSString *)fromName toName:(NSString *)toName{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.nameAlertView.nameOld = fromName;
        self.nameAlertView.nameNew = toName;
        self.nameAlertView.amController = controller;
        self.nameAlertView.message = [NSString stringWithFormat:NSLocalizedString(@"Device \"%@\" was changed to \"%@\"", @""), fromName, toName];
        [self.nameAlertView show];
    });
}

#pragma mark - AM dosimeter delegate

//
// Dosimeter
//
- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController *)controller didUpdateAmDosimeter:(AMDosimeter *)dosimeter{
    
    if (controller == self.amControllerManager.defaultController) {
        [self amDosimeterValuesChanged:controller.dosimeter];
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.vibrateOrSound pause];
        });
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        for (id<AMControllerManagerUpdateProtocol> vc in self.childViewControllers) {
            if ([vc respondsToSelector:@selector(didControllerManagerUpdateDosimeter:amController:)]) {
                [vc didControllerManagerUpdateDosimeter:manager amController:controller];
            }
        }
    });
}

- (void) amDosimeterValuesChanged:(AMDosimeter *)dosimeter{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (self.amControllerManager.defaultController.isReady) {
            if (dosimeter.doseSearchedRate>=self.amControllerManager.defaultController.settings.thresholdDoseRate.floatValue
                ||
                dosimeter.cumulativeDose>=self.amControllerManager.defaultController.settings.thresholdDose.floatValue
                ) {
                [self.vibrateOrSound play];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.vibrateOrSound pause];
                });
            }
            else if (self.vibrateOrSound.isPlaing){
                [self.vibrateOrSound pause];
            }
        }
        
        for (id<AMControllerUpdateProtocol> vc in self.childViewControllers) {
            if ([vc respondsToSelector:@selector(didDosimeterUpdate:)]) {
                [vc didDosimeterUpdate:dosimeter];
            }
        }
    });
}


#pragma mark - AMControllerManager delegates
//
// Controller manager list delegate
//
- (void) amControllerManagerDidUpdateControllerList:(AMControllerManager *)manager{
    dispatch_async(dispatch_get_main_queue(), ^{
        for (id<AMControllerManagerUpdateProtocol> vc in self.childViewControllers) {
            if ([vc respondsToSelector:@selector(didControllerManagerUpdateList:)]) {
                [vc didControllerManagerUpdateList:manager];
            }
        }
        
        AMSettings *settings = [AMSettings sharedInstance];
        for (AMController *controller in [self.amControllerManager.controllerList allValues]) {
            //
            // update dosimeter
            //
            NSArray* intervals = [settings sourceForSettingName:@"historyInterval"];
            for (NSNumber *interval in intervals[0]) {
                [controller.dosimeter addDoseRateHistoryInterval:interval.floatValue];
                //    [settings setValue:[NSNumber numberWithInt:[[[pickerView.selections objectAtIndex:0]objectAtIndex:row] intValue]] forSettingNo:pickerView.tag];
            }
            
            controller.dosimeter.doseRateHistoryInterval = settings.historyInterval.floatValue;
        }
    });
}


//
// Power state
//
- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController *)controller didChangePowerVolume:(float)volume{
    if (controller == self.amControllerManager.defaultController) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.statusBarView.powerVolume = volume;
        });
    }
}

- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController *)controller didChangePowerState:(AMControllerPowerState)powerState{
    
    // NSLog(@"  *** didChangePowerState %@", @(powerState));
    
    if (controller == self.amControllerManager.defaultController) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (powerState==AM_CONTROLLER_POWER_UNKNOWN)
                return;
            
            self.statusBarView.chargerPlugged = powerState==AM_CONTROLLER_POWER_CHARGE_PLUGGED;

            if ([controller isKindOfClass:[AMSimpleController class]]) {
                [self.volumeAlertView setMessage:[NSString stringWithFormat:NSLocalizedString(@"%@ does not have enough power (%.0f%%). Increase the volume.", @"Volume message alert"), controller.settings.name, controller.powerVolume*100.]];
            }
            else{
                //
                // Tag
                //
                [self.volumeAlertView setMessage:[NSString stringWithFormat:NSLocalizedString(@"%@ does not have enough power (%.0f%%). Please charge the battery.", @"Volume message alert"), controller.settings.name, controller.powerVolume*100.]];
            }
            
            BOOL lowPower = !(powerState==AM_CONTROLLER_POWER_ENOUGH);
            
            self.statusBarView.hasPowerEnough = powerState == AM_CONTROLLER_POWER_ENOUGH;
            
            if (!self.doNotShowAlertView) {
                
                if (lowPower && !self.isAllertShown) {
                    
                    if (controller.powerVolume>=0.8) {
                        return;
                    }
                    
                    timeBetweenAlerts = [NSDate timeIntervalSinceReferenceDate];
                    [self.volumeAlertView show];
                    self.isAllertShown = YES;
                }
                else{
                    if (self.isAllertShown) {
                        if ([NSDate timeIntervalSinceReferenceDate]-timeBetweenAlerts>.2) {
                            [self.volumeAlertView dismissWithClickedButtonIndex:0 animated:YES];
                            self.isAllertShown = NO;
                        }
                    }
                }
            }
        });
    }
}

//
// Ready state
//

- (void) scheduleAbsentTimer{
    
    if (!isActive) {
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [particlesAbsentTimer invalidate];
        particlesAbsentTimer = [NSTimer scheduledTimerWithTimeInterval:AM_UI_WAITTIME_PARTICLES_TO_ALERT target:self selector:@selector(particlesAbsentHandler:) userInfo:nil repeats:NO];
    });
}

- (void) stopAbsentTimer{
    if (particlesAbsentTimer) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [particlesAbsentTimer invalidate];
            particlesAbsentTimer = nil;
        });
    }
}


- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController *)controller willReadyAfterTime:(NSTimeInterval)approxTime{
    if ([controller isKindOfClass:[AMSimpleController class]]) {
        [self scheduleAbsentTimer];
    }
    else{
        [self stopAbsentTimer];
    }
}

- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController *)controller didUpdateReadyState:(BOOL)ready{
    if (controller == self.amControllerManager.defaultController) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.statusBarView.powerVolume = controller.powerVolume;
            
            if (!ready)                             
                [self.vibrateOrSound pause];
            
        });
    }
    [self.settingsController reloadWithAnimation:NO];
}

//
// Connection sate
//
- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController *)controller didUpdateConnectionState:(BOOL)available{
    
    if ([controller isKindOfClass:[AMSimpleController class]]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            AVAudioSession *avsession=[AVAudioSession sharedInstance];
            NSError *error;
            if (available) {
                [avsession setActive:YES error:&error];
                if (error) {
                    NSLog(@"AVError: %@ %s:%i", error, __FILE__, __LINE__);
                }
                
                error = nil;
                [avsession setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:0 error:&error];
                if (error) {
                    NSLog(@"AVError: %@ %s:%i", error, __FILE__, __LINE__);
                }
                error = nil;
                [avsession setMode:AVAudioSessionModeMeasurement error:&error];
                if (error) {
                    NSLog(@"AVError: %@ %s:%i", error, __FILE__, __LINE__);
                }
            }
            else{
                [avsession setMode:AVAudioSessionModeDefault error:&error];
                if (error) {
                    NSLog(@"AVError: %@ %s:%i", error, __FILE__, __LINE__);
                }
                error = nil;
                [avsession setActive:NO error:&error];
                if (error) {
                    NSLog(@"AVError: %@ %s:%i", error, __FILE__, __LINE__);
                }
            }

            [self stopAbsentTimer];
            
        });
    }
    
    if (controller == self.amControllerManager.defaultController) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.statusBarView.isConnected = available;
            
            [self.vibrateOrSound pause];

            self.statusBarView.chargerPlugged = controller.powerState == AM_CONTROLLER_POWER_CHARGE_PLUGGED;

            if (!available)
                self.statusBarView.powerVolume = 0.0;
            else{
                self.statusBarView.powerVolume = controller.powerVolume;
            }
        });
        
        [self applyConnection:controller];
    }
    
    if (!available) {
        //
        // Commit all changes failed too.
        //
        [self amControllerManager:manager amControllerDidCommitChanges:controller];
    }
    
    [self.settingsController reloadWithAnimation:NO];
    
}

- (void) applyConnection:(AMController*)controller{
    if (controller == self.amControllerManager.defaultController) {
        dispatch_async(dispatch_get_main_queue(), ^{
            for (id<AMControllerUpdateProtocol> vc in self.childViewControllers) {
                if ([vc respondsToSelector:@selector(didDosimeterUpdate:)]) {
                    [vc didControllerStateUpdate:controller];
                }
            }
        });
    }
}

- (void) particlesAbsentHandler:(NSTimer*)timer{
    if (self.absentAlertView.isHidden) {
        [self.absentAlertView show];
    }
}

//
// Detection
//
- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController *)controller didDetectParticles:(NSInteger)count{
    if ([controller isKindOfClass:[AMSimpleController class]]) {
        [self scheduleAbsentTimer];
    }
}

//
// Warnings
//
- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController *)controller didWarn:(NSString *)warnString{    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", @"")
                                                            message:warnString
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"Close", @"Close button")
                                                  otherButtonTitles:nil];
        alertView.cancelButtonIndex = 0;
        [alertView show];            
    });
}


- (UIAlertView*) alertBLEWarnView{
    if (!_alertBLEWarnView) {
        _alertBLEWarnView = [[UIAlertView alloc] initWithTitle:@"Bluetooth settings error"
                                                            message:@"Bluetooth is powered off"
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"Close", @"Close button")
                                                  otherButtonTitles:nil];
        
    }
    
    return _alertBLEWarnView;
}

//
// Error handling
//
- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController *)controller didFail:(NSError *)error{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (error != nil) {
            [self.vibrateOrSound pause];
            
            [self.alertBLEWarnView setTitle:error.localizedFailureReason];
            [self.alertBLEWarnView setMessage:error.localizedDescription];
            
            self.alertBLEWarnView.cancelButtonIndex = 0;
            [self.alertBLEWarnView show];
        }
        else {
            [self.alertBLEWarnView dismissWithClickedButtonIndex:0 animated:YES];
        }
    });
    
    //
    // Commit all changes failed too.
    //
    [self amControllerManager:manager amControllerDidCommitChanges:controller];
}


#pragma mark - Main View

- (void) awakeFromNib{
    
    [super awakeFromNib];
    
    [self.statusBarView awakeFromNib];
    
    logoImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"LogoWhiteBig.png"]];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        logoImage.frame = CGRectMake(244.5, 357.5, logoImage.bounds.size.width, logoImage.bounds.size.height);
    }
    else
        logoImage.frame = CGRectMake(21, 129.5, logoImage.bounds.size.width, logoImage.bounds.size.height);
    
    [self.view addSubview:logoImage];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateWhenSettingsIsChanged:) name:AM_SETTINGS_CHANGED_NOTIFICATION object:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(AM_UI_AWAKE_DURATION * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self minimizeLogo];
    });
    
    self.isAllertShown = NO;
}

- (void) minimizeLogo{
    
    UIImage *logoSmallImage = [UIImage imageNamed:@"LogoWhiteSmall"];
    float scale = logoImage.frame.size.height/logoSmallImage.size.height;
    
    [UIView animateWithDuration:AM_UI_AWAKE_DURATION animations:^{
        [logoImage placeToView:self.view withCenterPoint:_statusBarView.center withScale:scale];
    } completion:^(BOOL finished) {
        [logoImage setImage:logoSmallImage];
    }];
}

- (AMStatusBarView*) statusBarView{
    if (_statusBarView) {
        return _statusBarView;
    }
    
    _statusBarView = [[AMStatusBarView alloc] initWithFrame:CGRectMake(0, 20, self.view.bounds.size.width, 44)];
    _statusBarView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:_statusBarView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[view(44)]" options:0 metrics:nil views:@{@"view": _statusBarView, @"top": self.topLayoutGuide}]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view": _statusBarView}]];
    
    return _statusBarView;
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self amDosimeterValuesChanged:self.amControllerManager.defaultController.dosimeter];
    //NSLog(@" ### viewWillAppear ...");
}


- (void)keyboardDidShow:(NSNotification *)notification
{
    //Assign new frame to your view
    
    [UIView animateWithDuration:AM_UI_ANIMATE_DURATION animations:^{
        [self.pageScrollView setFrame:CGRectMake(0,-88, self.pageScrollView.bounds.size.width, self.pageScrollView.bounds.size.height)]; //here taken -20 for example i.e. your view will be scrolled to -20. change its value according to your requirement.
        
        self.statusBarView.alpha = 0.0;
        logoImage.alpha = 0.0;
        
        
    }];
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    [UIView animateWithDuration:AM_UI_ANIMATE_DURATION animations:^{
        [self.pageScrollView setFrame:CGRectMake(0,0,self.view.bounds.size.width,self.view.bounds.size.height)];
        self.statusBarView.alpha = 1.0;
        logoImage.alpha = 1.0;
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


@end
