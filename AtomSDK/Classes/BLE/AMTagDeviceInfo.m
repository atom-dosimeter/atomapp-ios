//
//  AMTagDeviceInfo.m
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 18/09/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMTagDeviceInfo.h"


@implementation AMTagDeviceInfoCommon

- (id) initWithData:(NSData *)data{
    self = [super init];
    
    if (self) {
        [self readData:data];
    }
    
    return self;
}

- (void) readData:(NSData *)data{
    _value = [NSString stringWithCString:[data bytes] encoding:NSASCIIStringEncoding];
}

- (NSNumber*)numberValue{
    NSNumber *n = nil;
    
    if (self.value!=nil) {
        
        NSArray *v = [self.value componentsSeparatedByString:@"."];
        
        if (v.count==4) {
            // v.1.0.0
            float f = 0.0;
            for (int i=1,m=1; i<v.count; i++,m*=10) {
                NSNumber *n1=[v objectAtIndex:i];
                f+=n1.floatValue/(float)m;
            }        
            n = [NSNumber numberWithFloat:f];
        }
        else{
            n = [NSNumber numberWithFloat:1.];
        }
    }
    
    return n;
}

@end

@implementation AMTagDeviceSoftwareVersion

+ (CBUUID*) identifier{
    return [CBUUID UUIDWithString:@"0x2A28"];
}

@end

@implementation AMTagDeviceFirmwareVersion

+ (CBUUID*) identifier{
    return [CBUUID UUIDWithString:@"0x2A26"];
}
@end


@implementation AMTagDeviceSerailString

+ (CBUUID*) identifier{
    return [CBUUID UUIDWithString:@"0x2A25"];
}
@end


@implementation AMTagDeviceModelString

+ (CBUUID*) identifier{
    return [CBUUID UUIDWithString:@"0x2A24"];
}

@end

@implementation AMTagDeviceInfo

- (void) readFirmwareNumberFromData:(NSData *)data{
    AMTagDeviceFirmwareVersion *fwv = [[AMTagDeviceFirmwareVersion alloc] initWithData:data];
    _firmwareNumber = fwv.numberValue;
}

- (void) readSoftwareNumberFromData:(NSData *)data{
    AMTagDeviceSoftwareVersion *swv = [[AMTagDeviceSoftwareVersion alloc] initWithData:data];
    _softwareNumber = swv.numberValue;
}

- (void) readModelStringFromData:(NSData *)data{
    AMTagDeviceModelString *ms = [[AMTagDeviceModelString alloc] initWithData:data];
    _modelString = ms.value;
}

- (void) readSerialStringFromData:(NSData *)data{
    AMTagDeviceSerailString *sn = [[AMTagDeviceSerailString alloc] initWithData:data];
    _serialString = sn.value;
}

- (NSString*) description{
    return [NSString stringWithFormat: @"\n\
            firmware               = %@\n\
            sowtware               = %@\n\
            model                  = %@\n\
            serail                 = %@\n",
            self.firmwareNumber,
            self.softwareNumber,
            self.modelString,
            self.serialString
            ];
}

@end
