//
//  AMDosimeterView.h
//  Atom
//
//  Created by denis svinarchuk on 12.07.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AMStatusBarView.h"

@interface AMTitledView : UIView
@property (nonatomic,readonly) UILabel *titleLabel;
@property (nonatomic,readonly) UILabel *deviceNameLabel;
@property (nonatomic,strong)   NSString *title;
@property (nonatomic,strong)   NSString *deviceName;
@property (weak, nonatomic) NSLayoutConstraint *bottomSpace;
@property (weak, nonatomic) NSLayoutConstraint *topSpace;

- (void) awakeWithMotion;

@end
