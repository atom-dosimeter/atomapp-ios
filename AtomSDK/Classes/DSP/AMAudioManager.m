//
//  AMAudioManager.m
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 09/09/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMAudioManager.h"
#import "AMConstants.h"
#import "AEFloatConverter.h"

#import <AudioToolbox/AudioToolbox.h>

static AMAudioManager *__shared_instance = nil;

@interface AMAudioManager()

- (void) checkError: (OSStatus) error operation:(NSString*)operation;

@property (nonatomic, assign, readwrite) AMAudioManagerBuffer *inData;
@property (nonatomic, assign, readwrite) AMAudioManagerBuffer *outData;

@property (nonatomic, assign, readwrite) AudioUnit inputUnit;
@property (nonatomic, assign, readwrite) AudioUnit outputUnit;
@property (nonatomic, assign, readwrite) AudioBufferList *inputBuffer;

@property (nonatomic, readonly) AudioComponentDescription inputDescription;
@property (nonatomic, readonly) AudioComponentDescription outputDescription;

@property (nonatomic, assign, readwrite) AudioStreamBasicDescription inputFormat;
@property (nonatomic, assign, readwrite) AudioStreamBasicDescription outputFormat;

@property (atomic, readwrite) BOOL isRunningInput;
@property (atomic, readwrite) BOOL isRunningOutput;

#if TARGET_OS_IPHONE
#elif TARGET_OS_MAC
@property (nonatomic, assign)     AudioDeviceID defaultInputDeviceID;
@property (nonatomic, assign)     AudioDeviceID defaultOutputDeviceID;
#endif
//
// redifine public properties
//
@property (nonatomic, assign, readwrite) UInt32 numInputChannels;
@property (nonatomic, assign, readwrite) UInt32 numOutputChannels;
@property (nonatomic, assign, readwrite) Float64 samplingRate;
@property (nonatomic, assign, readwrite) BOOL isInterleaved;

@end

@implementation AMAudioManager
{
    AEFloatConverter *samplesCoverter;

#if TARGET_OS_IPHONE
#elif TARGET_OS_MAC
    Float64 inputScopeSampleRate;
    BOOL isDefaultOutPutDeviceConfigured;
#endif
}

+ (id) sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __shared_instance = [[AMAudioManager alloc] init];
    });
    return __shared_instance;
};


- (id) init{
    
    if (__shared_instance) {
        self = __shared_instance;
        return self;
    }
    
    self = [super init];
    if (self) {
        [self setup];
        [self setupAudioUnits];
        
    }
    return self;
}

- (void) startOutput{
    // Set the audio session category for simultaneous play and record
    if (!self.isRunningOutput) {
        [self checkError:  AudioOutputUnitStart(_outputUnit)
               operation:@"Couldn't start the output unit"];
        self.isRunningOutput = YES;
        
    }
}

- (void) stopOutput{
	if (self.isRunningOutput) {
		[self checkError: AudioOutputUnitStop(_outputUnit)
               operation: @"Couldn't stop the output unit"];
		self.isRunningOutput = NO;
	}
}

- (void) startInput{
    if (!self.isRunningInput) {
        [self checkError:  AudioOutputUnitStart(_inputUnit)
               operation: @"Couldn't start the input unit"];
        self.isRunningInput=YES;
    }
}

- (void) stopInput{
    if (self.isRunningInput) {
        [self checkError: AudioOutputUnitStop(_inputUnit)
               operation:@"Couldn't stop the inpit unit"];
        self.isRunningInput = NO;
    }
}

//
// Callbacks
//

OSStatus inputCallback   (void						    *inRefCon,
                          AudioUnitRenderActionFlags    *ioActionFlags,
                          const AudioTimeStamp 		    *inTimeStamp,
                          UInt32						 inOutputBusNumber,
                          UInt32						 inNumberFrames,
                          AudioBufferList		     	*ioData)
{
    @autoreleasepool {
        
        AMAudioManager       *amAudioManager = (__bridge AMAudioManager *)inRefCon;
        AMAudioManagerBuffer *inBuffer       = amAudioManager->_inData;
        AudioStreamBasicDescription format   = amAudioManager->_inputFormat;
        AudioBufferList *bufferList = amAudioManager->_inputBuffer;

        inBuffer->numberOfFramesPerChannel = inNumberFrames;
        
        if (amAudioManager->_inputBlock == nil)
            return noErr;
        
        
        // Check the current number of channels
        // Let's actually grab the audio
#if TARGET_IPHONE_SIMULATOR
        // this is a workaround for an issue with core audio on the simulator, //
        //  likely due to 44100 vs 48000 difference in OSX //
        if( inNumberFrames == 471 )
            inNumberFrames = 470;
#endif
        OSStatus result = AudioUnitRender(amAudioManager->_inputUnit, ioActionFlags, inTimeStamp, inOutputBusNumber, inNumberFrames, bufferList);
        //
        // call as rarely as possible
        //
        if (result!=noErr) {
            [amAudioManager checkError: result operation: @"Couldn't render the output unit"];
        }
        
        float **inData=inBuffer->buffer;
        
        // Convert the audio in something manageable
        // For Float32s ...
        
        if ( format.mFormatFlags & kAudioFormatFlagIsFloat  ) // then we've already got floats
        {
            float zero = 0.0f;
            int   channels=amAudioManager->_numInputChannels;
            for (int i=0; i < channels; ++i) {
                int interleaved_stride = amAudioManager->_isInterleaved?(i+1):1;
                vDSP_vsadd((float *)bufferList->mBuffers[i].mData, interleaved_stride, &zero, inData[i], 1, inNumberFrames);
            }
        }
        
        // For SInt16s ...
        else if ( format.mFormatFlags & kAudioFormatFlagIsSignedInteger ) // then we're dealing with SInt16's
        {
            
            //
            // this method executes during within 23-25 µs
            //
            //            AEFloatConverterToFloat(amAudioManager->samplesCoverter,
            //                                    bufferList,
            //                                    inData,
            //                                    inNumberFrames);
            //

            //
            // this block executes during within 9-18 µs
            //
            
            BOOL  is32 = ((format.mBitsPerChannel/8)==4);
            float scale = 1.0 / (float)(is32?INT32_MAX:INT16_MAX);
            
            for (int i=0; i < amAudioManager->_numInputChannels; ++i) {
                int interleaved_stride = amAudioManager->_isInterleaved?(i+1):1;
                int *si = bufferList->mBuffers[i].mData;
                if (is32) {
                    vDSP_vflt32(si, interleaved_stride, inData[i], 1, inNumberFrames);
                }
                else
                    vDSP_vflt16((SInt16*)si, interleaved_stride, inData[i], 1, inNumberFrames);
                                
                vDSP_vsmul(inData[i], 1, &scale, inData[i], 1, inNumberFrames);
            }
        }
        
        // Now do the processing!
        amAudioManager->_inputBlock(amAudioManager, inBuffer);
    }
    
    return noErr;
}


OSStatus renderCallback (void						*inRefCon,
                         AudioUnitRenderActionFlags	* ioActionFlags,
                         const AudioTimeStamp 		* inTimeStamp,
                         UInt32						inOutputBusNumber,
                         UInt32						inNumberFrames,
                         AudioBufferList				* ioData)
{
    // autorelease pool for much faster ARC performance on repeated calls from separate thread
    @autoreleasepool {
        
        AMAudioManager       *amAudioManager = (__bridge AMAudioManager *)inRefCon;
        AMAudioManagerBuffer *outData        = amAudioManager->_outData;
        AudioStreamBasicDescription format   = amAudioManager->_outputFormat;

        float zero = 0.0;
        
        for (int i=0; i < ioData->mNumberBuffers; ++i) {
            //memset(ioData->mBuffers[i].mData, 0, ioData->mBuffers[i].mDataByteSize);
            vDSP_vclr(ioData->mBuffers[i].mData, 1, ioData->mBuffers[i].mDataByteSize);
        }
        
        if (!amAudioManager->_outputBlock)
            return noErr;
        
        
        // Collect data to render from the callbacks
        outData->numberOfFramesPerChannel = inNumberFrames;
        amAudioManager->_outputBlock(amAudioManager, outData);
        
        // Put the rendered data into the output buffer
        if ( format.mFormatFlags & kAudioFormatFlagIsFloat ) // then we've already got floats
        {
            
            for (int iBuffer=0; iBuffer < ioData->mNumberBuffers; ++iBuffer) {
                
                int thisNumChannels = ioData->mBuffers[iBuffer].mNumberChannels;
                
                for (int iChannel = 0; iChannel < thisNumChannels; ++iChannel) {
                    
                    int interleaveOffset = iChannel;
                    if (iBuffer < amAudioManager.numOutputChannels){
                        interleaveOffset += iBuffer;
                    }
                    
                    vDSP_vsadd(outData->buffer[interleaveOffset], 1, &zero, (float *)ioData->mBuffers[iBuffer].mData, thisNumChannels, inNumberFrames);
                    
                }
            }
        }
        else if (  format.mFormatFlags & kAudioFormatFlagIsSignedInteger ) // then we need to convert SInt16 -> Float (and also scale)
        {
            float scale = (float)INT16_MAX;
            vDSP_vsmul((float*)outData->buffer, 1, &scale, (float*)outData->buffer, 1, outData->bufferSizePerChannel*outData->numberOfChannels);
            
            for (int iBuffer=0; iBuffer < ioData->mNumberBuffers; ++iBuffer) {
                
                int thisNumChannels = ioData->mBuffers[iBuffer].mNumberChannels;
                
                for (int iChannel = 0; iChannel < thisNumChannels; ++iChannel) {
                    
                    int interleaveOffset = iChannel;
                    if (iBuffer < amAudioManager.numOutputChannels){
                        interleaveOffset += iBuffer;
                    }
                    
                    vDSP_vfix16(outData->buffer[interleaveOffset], 1, (SInt16 *)ioData->mBuffers[iBuffer].mData+iChannel, thisNumChannels, inNumberFrames);
                }
            }
        }
    }
    return noErr;
}

//
// Setups
//

- (void) setup{
    memset(&_inputDescription, 0, sizeof(_inputDescription));
    memset(&_outputDescription, 0, sizeof(_outputDescription));
    
    
    _inputDescription.componentType = kAudioUnitType_Output;
    _inputDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
    
    _outputDescription.componentType = kAudioUnitType_Output;
    _outputDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
    
#if TARGET_OS_IPHONE
    
    _inputDescription.componentSubType = kAudioUnitSubType_RemoteIO;
    _outputDescription.componentSubType = kAudioUnitSubType_RemoteIO;
    
#elif TARGET_OS_MAC
    _inputDescription.componentSubType = kAudioUnitSubType_HALOutput;
    
    _outputDescription.componentSubType = kAudioUnitSubType_HALOutput;
    
#endif
    
    [self checkError:AudioComponentInstanceNew(AudioComponentFindNext(NULL, &_inputDescription), &_inputUnit)
           operation:NSLocalizedString(@"Couldn't create the input audio unit",@"")];
    
    [self checkError:AudioComponentInstanceNew(AudioComponentFindNext (NULL, &_outputDescription), &_outputUnit)
           operation:NSLocalizedString(@"Couldn't create the output audio", @"")];
    
}

//
// Audio unit setups
//
- (void) setupAudioUnits{
    
    UInt32 one = 1;
    [self checkError: AudioUnitSetProperty(_inputUnit,
                                           kAudioOutputUnitProperty_EnableIO,
                                           kAudioUnitScope_Input,
                                           kAMAudioJackInputBus,
                                           &one,
                                           sizeof(one)) operation: @"Couldn't enable IO on the input scope of output unit"];
    
#if TARGET_OS_IPHONE
    
    //
    // nothing for ios
    //
    
#elif TARGET_OS_MAC
    // Disable output on the input unit
    // (only on Mac, since on the iPhone, the input unit is also the output unit)
    UInt32 zero = 0;
    [self checkError: AudioUnitSetProperty(_inputUnit,
                                           kAudioOutputUnitProperty_EnableIO,
                                           kAudioUnitScope_Output,
                                           kAMAudioJackOutputBus,
                                           &zero,
                                           sizeof(UInt32)) operation: @"Couldn't disable output on the audio unit"];
    
    // Enable output
    [self checkError: AudioUnitSetProperty(_outputUnit,
                                           kAudioOutputUnitProperty_EnableIO,
                                           kAudioUnitScope_Output,
                                           kAMAudioJackOutputBus,
                                           &one,
                                           sizeof(one)) operation: @"Couldn't enable IO on the input scope of output unit"];
    
    // Disable input
    [self checkError: AudioUnitSetProperty(_outputUnit,
                                           kAudioOutputUnitProperty_EnableIO,
                                           kAudioUnitScope_Input,
                                           kAMAudioJackInputBus,
                                           &zero,
                                           sizeof(UInt32)) operation: @"Couldn't disable output on the audio unit"];
    
#endif
    
    // configure device
    [self configureDefaultDevice];
    
    // configure device peoperties
    [self configureProperties];
    
    // alloc buffers
    [self allocBuffers];
    
    // calbacks
    [self configureCallbacks];
    
    
    //
    // Initialize
    //
	[self checkError: AudioUnitInitialize(_inputUnit)
           operation: @"Couldn't initialize the output unit"];
    
    [self checkError: AudioUnitInitialize(_outputUnit)
           operation: @"Couldn't initialize the output unit"];
    
}

- (void) configureDefaultDevice{
#if TARGET_OS_IPHONE
    
    //
    // nothin for ios
    //
    
#elif TARGET_OS_MAC
    
    AudioObjectPropertyAddress propertyAddress;
    propertyAddress.mSelector = kAudioHardwarePropertyDefaultInputDevice;
    propertyAddress.mScope = kAudioObjectPropertyScopeGlobal;
    propertyAddress.mElement = kAudioObjectPropertyElementMaster;
    
    UInt32 propsize = sizeof(_defaultInputDeviceID);
    [self checkError: AudioObjectGetPropertyData(kAudioObjectSystemObject,
                                                 &propertyAddress, 0, NULL,
                                                 &propsize,
                                                 &_defaultInputDeviceID)
           operation:NSLocalizedString(@"Could not get the default device", @"")];
    
    //
    // Output device
    //
    AudioObjectPropertyAddress getDefaultOutputDevicePropertyAddress = {
        kAudioHardwarePropertyDefaultOutputDevice,
        kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster
    };
    
    propsize = sizeof(_defaultOutputDeviceID);
    
    [self checkError:AudioObjectGetPropertyData(kAudioObjectSystemObject,
                                                &getDefaultOutputDevicePropertyAddress,
                                                0, NULL,
                                                &propsize, &_defaultOutputDeviceID)
           operation:NSLocalizedString(@"Could not get the default device", @"")];
#endif
}

-(Float64)configureDeviceSampleRateWithDefault:(float)defaultSampleRate {
    Float64 hardwareSampleRate = defaultSampleRate;
#if TARGET_OS_IPHONE
    // Use approximations for simulator and pull from real device if connected
#if !(TARGET_IPHONE_SIMULATOR)
    // Sample Rate
    hardwareSampleRate = [[AVAudioSession sharedInstance] sampleRate];
#endif
#elif TARGET_OS_MAC
    hardwareSampleRate = inputScopeSampleRate;
#endif
    return hardwareSampleRate;
}

- (void) configureProperties{
#if TARGET_OS_IPHONE
    
    // Set the format to 32 bit, single channel, floating point, linear PCM
    self.samplingRate = [self configureDeviceSampleRateWithDefault:kAMSampleRate];

    //
    // Input format
    //
    
    //
    // Set the stream format as canonical
    //
    
    UInt32 byteSize = sizeof(AudioUnitSampleType);
    _inputFormat.mBitsPerChannel   = 8 * byteSize;
    _inputFormat.mBytesPerFrame    = byteSize;
    _inputFormat.mBytesPerPacket   = byteSize;
    _inputFormat.mChannelsPerFrame = 2;
    _inputFormat.mFormatFlags      = kAudioFormatFlagsCanonical | kAudioFormatFlagIsNonInterleaved;
    _inputFormat.mFormatID         = kAudioFormatLinearPCM;
    _inputFormat.mFramesPerPacket  = 1;
    _inputFormat.mSampleRate       = self.samplingRate;
    
    UInt32 propSize = sizeof(_inputFormat);
    // Set the stream format for the input on the microphone's output scope
    [self checkError:AudioUnitSetProperty(_inputUnit,
                                          kAudioUnitProperty_StreamFormat,
                                          kAudioUnitScope_Output,
                                          kAMAudioJackInputBus,
                                          &_inputFormat,
                                          propSize)
           operation:@"Could not set microphone's stream format bus 1"];

    //
    // Output format
    //
	_outputFormat.mSampleRate = self.samplingRate;
	_outputFormat.mFormatID = kAudioFormatLinearPCM;
	_outputFormat.mFormatFlags = kAudioFormatFlagsNativeFloatPacked | kAudioFormatFlagIsNonInterleaved;
	_outputFormat.mBytesPerPacket = 4;
	_outputFormat.mFramesPerPacket = 1;
	_outputFormat.mBytesPerFrame = 4;
	_outputFormat.mChannelsPerFrame = 2;
	_outputFormat.mBitsPerChannel = _outputFormat.mBytesPerPacket * 8;
    
    [self checkError:AudioUnitSetProperty (_outputUnit,
                                           kAudioUnitProperty_StreamFormat,
                                           kAudioUnitScope_Input,
                                           0,
                                           &_outputFormat,
                                           sizeof(AudioStreamBasicDescription))
           operation:NSLocalizedString(@"Couldn't initialize output stream properties", @"")];
    

    self.numInputChannels  = _inputFormat.mChannelsPerFrame;
    self.numOutputChannels = _outputFormat.mChannelsPerFrame;
    
    samplesCoverter              = [[AEFloatConverter alloc] initWithSourceFormat:_inputFormat];

#elif TARGET_OS_MAC
    
    // Set the current device to the default input unit.
    [self checkError: AudioUnitSetProperty( _inputUnit,
                                           kAudioOutputUnitProperty_CurrentDevice,
                                           kAudioUnitScope_Global,
                                           kAMAudioJackOutputBus,
                                           &_defaultInputDeviceID,
                                           sizeof(AudioDeviceID) )
           operation:NSLocalizedString(@"Couldn't set the current input audio device",@"")];
    
    [self checkError: AudioUnitSetProperty( _outputUnit,
                                           kAudioOutputUnitProperty_CurrentDevice,
                                           kAudioUnitScope_Global,
                                           kAMAudioJackOutputBus,
                                           &_defaultOutputDeviceID,
                                           sizeof(AudioDeviceID) )
           operation:NSLocalizedString(@"Couldn't set the current output audio device", @"")];
    
    
	UInt32 propertySize = sizeof(AudioStreamBasicDescription);
	[self checkError: AudioUnitGetProperty(_inputUnit,
                                           kAudioUnitProperty_StreamFormat,
                                           kAudioUnitScope_Output,
                                           kAMAudioJackInputBus,
                                           &_outputFormat,
                                           &propertySize)
           operation:NSLocalizedString(@"Couldn't get ASBD from input unit",@"")];
    
    
    // 9/6/10 - check the input device's stream format
    [self checkError: AudioUnitGetProperty(_inputUnit,
                                           kAudioUnitProperty_StreamFormat,
                                           kAudioUnitScope_Input,
                                           kAMAudioJackInputBus,
                                           &_inputFormat,
                                           &propertySize)
           operation:NSLocalizedString(@"Couldn't get ASBD from input unit", @"")];
    
    
    _outputFormat.mSampleRate = _inputFormat.mSampleRate;
    
    self.samplingRate = _inputFormat.mSampleRate;        
    self.numInputChannels  = _inputFormat.mChannelsPerFrame;
    self.numOutputChannels = _outputFormat.mChannelsPerFrame;
    
    propertySize = sizeof(AudioStreamBasicDescription);
    [self checkError: AudioUnitSetProperty(_inputUnit,
                                           kAudioUnitProperty_StreamFormat,
                                           kAudioUnitScope_Output,
                                           kAMAudioJackInputBus,
                                           &_outputFormat,
                                           propertySize)
           operation:NSLocalizedString(@"Couldn't set the ASBD on the audio unit (after setting its sampling rate)",@"")];
    
#endif
    
    _inputBufferSize = [self getBufferFrameSizeForUnit:_inputUnit];
    _outputBufferSize = [self getBufferFrameSizeForUnit:_outputUnit];
    
}


-(UInt32)getBufferFrameSizeForUnit:(AudioUnit)unit{
    UInt32 bufferFrameSize;
    UInt32 propSize = sizeof(bufferFrameSize);
    [self checkError:AudioUnitGetProperty(unit,
#if TARGET_OS_IPHONE
                                          kAudioUnitProperty_MaximumFramesPerSlice,
#elif TARGET_OS_MAC
                                          kAudioDevicePropertyBufferFrameSize,
#endif
                                          kAudioUnitScope_Global,
                                          kAMAudioJackOutputBus,
                                          &bufferFrameSize,
                                          &propSize)
           operation:@"Failed to get buffer frame size"];
        
    return bufferFrameSize;
}

-(void) allocAudioManagerBuffer:(AMAudioManagerBuffer*)buffer withUnit:(AudioUnit)unit forStream:(AudioStreamBasicDescription)streamFormat{
    
    
    //
    // channels
    //
    buffer->numberOfChannels = streamFormat.mChannelsPerFrame;
    //
    // buffer size constantely
    //
    buffer->bufferSizePerChannel = [self getBufferFrameSizeForUnit:unit];
    //
    // it will be defined at render time.
    //
    buffer->numberOfFramesPerChannel = 0;
    
    //
    // initialize buffer
    //
    buffer->buffer = (float **)malloc(buffer->numberOfChannels*sizeof(float*));
    for( UInt32 channel_index = 0; channel_index < buffer->numberOfChannels; channel_index++ ){
        buffer->buffer[channel_index] = (float *)malloc(buffer->bufferSizePerChannel*sizeof(float));
        memset(buffer->buffer[channel_index], 0, buffer->bufferSizePerChannel*sizeof(float));
    }
}

- (void) deallocAudioManagerBuffer:(AMAudioManagerBuffer*)buffer{
    if (buffer->buffer) {
        for( UInt32 channel_index = 0; channel_index < buffer->numberOfChannels; channel_index++ ){
            free(buffer->buffer[channel_index]);
        }
        free(buffer->buffer);
        buffer->buffer = NULL;
    }
}

- (void) allocBuffers{
    
    _inData = (AMAudioManagerBuffer*)malloc(sizeof(AMAudioManagerBuffer));
    _outData = (AMAudioManagerBuffer*)malloc(sizeof(AMAudioManagerBuffer));
    
    [self allocAudioManagerBuffer:_inData withUnit:self.inputUnit forStream:self.inputFormat];
    [self allocAudioManagerBuffer:_outData withUnit:self.outputUnit forStream:self.outputFormat];
    
    // Get the size of the IO buffer(s)
	UInt32 bufferSizeBytes = [self getBufferFrameSizeForUnit:_inputUnit] * sizeof(Float32);
    
    if (_outputFormat.mFormatFlags & kAudioFormatFlagIsNonInterleaved) {
        // The audio is non-interleaved
        
#if DEBUG
        NSLog(@"AMAuidoManager: not interleaved!\n");
#endif
        self.isInterleaved = NO;
        
        // allocate an AudioBufferList plus enough space for array of AudioBuffers
		UInt32 propsize = offsetof(AudioBufferList, mBuffers[0]) + (sizeof(AudioBuffer) * _outputFormat.mChannelsPerFrame);
		
		//malloc buffer lists
		self.inputBuffer = (AudioBufferList *)malloc(propsize);
		self.inputBuffer->mNumberBuffers = _outputFormat.mChannelsPerFrame;
		
		//pre-malloc buffers for AudioBufferLists
		for(UInt32 i =0; i< self.inputBuffer->mNumberBuffers ; i++) {
			self.inputBuffer->mBuffers[i].mNumberChannels = 1;
			self.inputBuffer->mBuffers[i].mDataByteSize = bufferSizeBytes;
			self.inputBuffer->mBuffers[i].mData = malloc(bufferSizeBytes);
            memset(self.inputBuffer->mBuffers[i].mData, 0, bufferSizeBytes);
		}
        
	} else {
		
#if DEBUG
        NSLog(@"AMAuidoManager: format is interleaved\n");
#endif
        self.isInterleaved = YES;
        
		// allocate an AudioBufferList plus enough space for array of AudioBuffers
		UInt32 propsize = offsetof(AudioBufferList, mBuffers[0]) + (sizeof(AudioBuffer) * 1);
		
		//malloc buffer lists
		self.inputBuffer = (AudioBufferList *)malloc(propsize);
		self.inputBuffer->mNumberBuffers = 1;
		
		//pre-malloc buffers for AudioBufferLists
		self.inputBuffer->mBuffers[0].mNumberChannels = _outputFormat.mChannelsPerFrame;
		self.inputBuffer->mBuffers[0].mDataByteSize = bufferSizeBytes;
		self.inputBuffer->mBuffers[0].mData = malloc(bufferSizeBytes);
        memset(self.inputBuffer->mBuffers[0].mData, 0, bufferSizeBytes);
        
	}
}


- (void) configureCallbacks{
    
    // Slap a render callback on the unit
    AURenderCallbackStruct callbackStruct;
    callbackStruct.inputProc = inputCallback;
    callbackStruct.inputProcRefCon = (__bridge void *)(self);
    
    [self checkError:  AudioUnitSetProperty(_inputUnit,
                                            kAudioOutputUnitProperty_SetInputCallback,
                                            kAudioUnitScope_Global,
                                            0,
                                            &callbackStruct,
                                            sizeof(callbackStruct))
           operation:NSLocalizedString(@"Couldn't set the callback on the input unit",@"")];
    
    
    callbackStruct.inputProc = renderCallback;
    callbackStruct.inputProcRefCon = (__bridge void *)(self);
    
    [self checkError:  AudioUnitSetProperty(_outputUnit,
                                            kAudioUnitProperty_SetRenderCallback,
                                            kAudioUnitScope_Input,
                                            0,
                                            &callbackStruct,
                                            sizeof(callbackStruct))
           operation:NSLocalizedString(@"Couldn't set the render callback on the input unit",@"")];
}

//
// Memory management
//
- (void)dealloc
{
    if (_inData) {
        [self deallocAudioManagerBuffer:_inData];
        free(_inData);
        _inData = nil;
    }
    if (_outData) {
        [self deallocAudioManagerBuffer:_outData];
        free(_outData);
        _outData=nil;
    }
    [self deallocBuffers];
}

- (void)deallocBuffers
{
    if (self.inputBuffer){
        
		for(UInt32 i =0; i< self.inputBuffer->mNumberBuffers ; i++) {
            
			if(self.inputBuffer->mBuffers[i].mData){
                free(self.inputBuffer->mBuffers[i].mData);
            }
		}
        
        free(self.inputBuffer);
        self.inputBuffer = NULL;
    }
}

//
// Error handling
//
- (void) checkError: (OSStatus) error operation:(NSString*)operation{
    
    if (error == noErr) return;
    
    if (self.errorHandler && [self.errorHandler respondsToSelector:@selector(amObject:withError:)]) {
        
        char str[20];
        // see if it appears to be a 4-char-code
        *(UInt32 *)(str + 1) = CFSwapInt32HostToBig(error);
        if (isprint(str[1]) && isprint(str[2]) && isprint(str[3]) && isprint(str[4])) {
            str[0] = str[5] = '\'';
            str[6] = '\0';
        } else
            // no, format it as an integer
            sprintf(str, "%d", (int)error);
        
        
        NSError *am_error =  [NSError errorWithDomain:NSOSStatusErrorDomain
                                                 code:error
                                             userInfo:@{
                                                        NSLocalizedDescriptionKey: operation,
                                                        NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"System error", @"")
                                                        }];
        
        [self.errorHandler amObject:self withError:am_error];
        
#if DEBUG
        NSLog(@"AMAuidoManager error: %@ (%s)\n", operation, str);
#endif
        
#if DO_ABBORT_ON_ERROR
        abort();
#endif
    }
}

@end
