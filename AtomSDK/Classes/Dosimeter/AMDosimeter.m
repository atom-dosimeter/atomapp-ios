//
//  AMDosimeter.m
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 04.07.14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMDosimeter.h"

//
// nSv/h
//
#define ADER(__particles__, __time_measurement_in_seconds__, __SK1__, __BK2__) ((((float)__particles__) * (__SK1__) / (__time_measurement_in_seconds__)) * 3600.0 - (__BK2__))

//
// without considering self background
//
#define SELF_ADER(__particles__, __time_measurement_in_seconds__, __SK1__) ((((float)__particles__) * __SK1__ / (__time_measurement_in_seconds__)) * 3600.0)

//
// convert ADER to impulse/particles
//
#define PARTICLES_ADER(__ader_in_nSv__,__time_measurement_in_seconds__, __SK1__, __BK2__) ((uint32_t)((__ader_in_nSv__ + __BK2__)/3600.0 * (float)__time_measurement_in_seconds__ /  __SK1__))

//
// without considering self background
//
#define PARTICLES_SELF_ADER(__ader_in_nSv__,__time_measurement_in_seconds__, __SK1__) ((uint32_t)((__ader_in_nSv__ )/3600.0 * (float)__time_measurement_in_seconds__ /  __SK1__))

//
// nSv
//
#define DOSE(__particles__, __time_in_minutes__, __SK1Pulse__)  ((((__particles__)-(kAMDosimeterOwnBackgound_K2PULSE)*(__time_in_minutes__))/(__SK1Pulse__))/10.0)

//
// cnvert dose to particles
//
#define PARTICLES_DOSE(__dose_in_nSv__,__time_in_minutes__, __SK1Pulse__)  ((__dose_in_nSv__)*((kAMDosimeterSensetivityConstant_K1PULSE)*10)+(__SK1Pulse__)*(__time_in_minutes__))

//
// %
//
#define SIGMA(__particles__) ((__particles__)==0.0?-1.0:100.0/sqrt(__particles__))



@implementation AMDosimeterSettings

- (id) init{
    self = [super init];
    
    if (self) {
        _sensetivity     = kAMDosimeterSensetivityConstant_K1PULSE;
        _sensetivitySvP  = kAMDosimeterSensetivityConstant_K1;
        _ownBackgound    = kAMDosimeterOwnBackgound_K2PULSE;
        _ownBackgoundSvH = kAMDosimeterOwnBackgound_K2;
        _deadTime = kAMDosimeterDeadTime;
    }
    
    return self;
}

- (void) setSensetivity:(float)sensetivity{
    _sensetivity = sensetivity; // imp/µR
    _sensetivitySvP = ((1.0/sensetivity)/kAMRentgenSivertConvertion)*1000.; // nSv/imp
}

- (void) setOwnBackgound:(float)ownBackgound{
    _ownBackgound = ownBackgound; // imp/min
    _ownBackgoundSvH = (((_ownBackgound) * (1./(self.sensetivity*kAMRentgenSivertConvertion)*1000.) / (60.)) * 3600.0); // nSv/h
}

@end

@interface AMDoseRatePoint()

@property (strong,nonatomic) NSDate *time;
@property (assign,nonatomic) NSUInteger   index;
@property (assign,nonatomic) NSUInteger   count;
@property (assign,nonatomic) float   comulativeDoseRate;
@property (assign,nonatomic) float   doseRate;
@end


@implementation AMDoseRatePoint

@end

@interface AMSearchBufferPoint : NSObject
@property(nonatomic, assign)    float          particlesN;
@property(nonatomic, assign)    NSTimeInterval timeStamp;

- (id) initWithParticles:(float)count atTime:(NSTimeInterval)time;

@end

@implementation AMSearchBufferPoint

- (id) initWithParticles:(float)count atTime:(NSTimeInterval)time{
    self = [super init];
    if (self) {
        _particlesN = count;
        _timeStamp = time;
    }
    return self;
}

@end

@interface AMDosimeter()

@property (atomic,assign) UInt64  particlesSearchedTotal;
@property (atomic,assign) NSTimeInterval measuringStartTime;

@property (atomic,assign) float doseEquivalentRatePrivate;
@property (atomic,assign) float doseSearchedRate;

@property (atomic,assign) NSTimeInterval searchingModeTime;

@property (atomic,assign) float particlesNAccumulated;
@property (atomic,assign) float particlesSAccumulated;

@property (atomic,assign) float statErrorEquivalentRate;
@property (atomic,assign) float statErrorSearchedRate;

@property (atomic,assign) NSTimeInterval searchingStartTime;

@property (atomic,assign) BOOL isRunning;

// particles by measurment point
@property (nonatomic,strong)     AMQueue      *searchBuffer;
@property (atomic,strong)     NSMutableDictionary      *historyBuffers;

@property (nonatomic,strong) NSThread *historyThread;

@property (nonatomic,assign) float cpsMeasuring;

@property (atomic,assign) NSTimeInterval lastTime;

@end

@implementation AMDosimeter
{
UInt64       prevparticlesCount;
dispatch_queue_t delegateQ;
NSTimeInterval last_interval ;
}

@synthesize doseRateHistory=_doseRateHistory, particlesEquivalentCount=_particlesEquivalentCount, cumulativeDose=_cumulativeDose, historyBuffers=_historyBuffers;

- (id) init{
    
    self = [super init];
    
    if (self) {
        
        _settings = [[AMDosimeterSettings alloc] init];
        
        _measuringTime = -1.0;
        _cumulativeDose = -1.0;
        _particlesSearchedMinimumCount = kAMDosimeterSearchModeNormal;
        _particlesEquivalentCount = INT64_MAX;
        
        _minRefreshTime = .25;
        _doseRateHistoryInterval = 1.0;
        
        [self __hardReset];
        [self addDoseRateHistoryInterval:_doseRateHistoryInterval];
        
        delegateQ = dispatch_queue_create("com.atom.dosimeter.delegate", DISPATCH_QUEUE_SERIAL);
    }
    
    return self;
}

- (NSThread*) historyThread{
    if (!_historyThread) {
        _historyThread = [[NSThread alloc] initWithTarget:self selector:@selector(refreshHistoryThread) object:nil];
    }
    return _historyThread;
}

- (NSMutableDictionary*) historyBuffers{
    if (!_historyBuffers) {
        _historyBuffers = [[NSMutableDictionary alloc] initWithCapacity:1];
    }
    return _historyBuffers;
}

- (void) setHistoryBuffers:(NSMutableDictionary *)historyBuffers{
    _historyBuffers=historyBuffers;
}

- (AMQueue*) searchBuffer{
    
    if (!_searchBuffer) {
        _searchBuffer = [[AMQueue alloc] init];
        _searchBuffer.maxSize = kAMDosimeterSearchModePrecisely+1;
    }
    return _searchBuffer;
}

- (void) setParticlesSearchedMinimumCount:(float)searchingModeThreshold{
    _particlesSearchedMinimumCount = searchingModeThreshold;
}

- (AMQueue*) doseRateHistory{
    
    NSNumber *key = [self keyForInterval:self.doseRateHistoryInterval];
    _doseRateHistory = [self.historyBuffers objectForKey:key];
    
    if (!_doseRateHistory) {
        _doseRateHistory = [[AMQueue alloc] init];
        _doseRateHistory.maxSize = kAMChartHistorySize;
        [self.historyBuffers setObject:_doseRateHistory forKey:key];
        [self initDoseRateHistoryBuffer:_doseRateHistory withKey:key];
    }
    
    return _doseRateHistory;
}

- (NSNumber*) keyForInterval:(NSTimeInterval)interval{
    return [NSNumber numberWithFloat:interval];
}


- (void) initDoseRateHistoryBuffer:(AMQueue*)q withKey:(NSNumber*)key{
    for (int i=0; i<q.maxSize; i++) {
        AMDoseRatePoint *dosePoint = [[AMDoseRatePoint alloc] init];
        if (i>=q.maxSize-2) {
            dosePoint.doseRate = dosePoint.comulativeDoseRate = 100.0;
        }
        else
            dosePoint.doseRate = dosePoint.comulativeDoseRate = 0.0;
        
        dosePoint.index = i;
        dosePoint.count = 0;
        dosePoint.time = [NSDate dateWithTimeIntervalSince1970:time(NULL)-(q.maxSize-1-i)*key.floatValue];
        [q push:dosePoint];
    }
}

-  (void) addDoseRateHistoryInterval:(NSTimeInterval)interval{
    NSNumber *key = [self keyForInterval:interval];
    if ([self.historyBuffers objectForKey:key]==nil) {
        AMQueue *q = [[AMQueue alloc] init];
        q.maxSize = kAMChartHistorySize;
        [self.historyBuffers setObject:q forKey:key];
        [self initDoseRateHistoryBuffer:q withKey:key];
    }
}

- (void) setDoseRateHistoryCount:(NSUInteger)doseRateHistoryCount{
    
    if (_doseRateHistoryCount==doseRateHistoryCount)
        return;
    
    _doseRateHistoryCount = doseRateHistoryCount;
    
    NSArray *keys = [self.historyBuffers allKeys];
    for (NSNumber *key in keys) {
        AMQueue *doseRateBuffer = [self.historyBuffers objectForKey:key];
        if (doseRateBuffer.maxSize!=doseRateHistoryCount) {
            doseRateBuffer.maxSize = doseRateHistoryCount;
            [doseRateBuffer flush];
            [self initDoseRateHistoryBuffer:doseRateBuffer withKey:key];
        }
    }
}

- (void) setParticlesEquivalentCount:(uint64_t)particlesEquivalentCount{
    _particlesEquivalentCount = particlesEquivalentCount;
}

- (float) doseEquivalentRate{
    if (_particlesEquivalentCount==INT64_MAX || _measuringTime<0.) {
        return _doseEquivalentRatePrivate;
    }
    
    return ADER(self.particlesEquivalentCount, self.measuringTime, self.settings.sensetivitySvP, self.settings.ownBackgoundSvH);
}

- (uint64_t) particlesEquivalentCount {
    if (_particlesEquivalentCount==INT64_MAX) {
        return ceil(self.particlesNAccumulated);
    }
    return _particlesEquivalentCount;
}

- (NSUInteger) particlesSearchedCount{
    return ceil(self.particlesSAccumulated);
}

- (float) cumulativeDose{
    if (_cumulativeDose<0) {
        NSTimeInterval period = [NSDate timeIntervalSinceReferenceDate] - self.measuringStartTime;
        float dose = (self.particlesNAccumulated - self.settings.ownBackgound*(period/60.))/self.settings.sensetivity;
        dose = dose * 10.0 ; // to nSv
        return dose;
    }
    return _cumulativeDose;
}

- (void) setCumulativeDose:(float)cumulativeDose{
    _cumulativeDose=cumulativeDose;
}

- (float) statErrorEquivalentRate {
    return SIGMA(self.particlesEquivalentCount);
}

- (void) setMeasuringTime:(NSTimeInterval)measuringTime{
    _measuringTime = measuringTime;
    self.measuringStartTime = [[NSDate dateWithTimeIntervalSinceNow:(self.measuringTime>0.?-self.measuringTime:0.0)] timeIntervalSinceReferenceDate];
}

//
//  recompute dose rate
//
//
- (void) recomputeDoseRateWithTime:(NSTimeInterval)currentTime withCountDuration:(NSTimeInterval)countDuration{
    
    if (self.measuringStartTime<0.) {
        self.measuringStartTime = [NSDate timeIntervalSinceReferenceDate];
    }
    
    {
        
        if (self.searchingStartTime<0.) {
            //
            // new searching
            //
            self.searchingStartTime = [NSDate timeIntervalSinceReferenceDate];
        }
        
        
        NSTimeInterval time_measurement = currentTime-self.measuringStartTime;
        
        float particlesN = (float)(self.particlesSearchedTotal - prevparticlesCount); // observed number of captured meter
        
        //
        //
        //  TODO: I need to test this block!
        //  I should check
        //
        //
        
        // number of particles beetwen measurement leads to recompute the true number of particules
        if (particlesN/time_measurement>kAMDosimeterParticlesCorrectionCount) {
            //
            // recompute to N=M/(1-M*T) the true number of captured meter
            //
            particlesN = particlesN/(1.0-particlesN*self.settings.deadTime);
        }
        
        //
        // Compute integral params for the searching mode
        //
        float particlesSearched = 0.0;
        NSTimeInterval time_searching=0.0;
        BOOL isSearched = NO;
        
        // Keep buffer
        AMSearchBufferPoint *point = [[AMSearchBufferPoint alloc] initWithParticles:particlesN atTime:currentTime];
        [self.searchBuffer push:point];
                
        if (particlesN>=self.particlesSearchedMinimumCount && countDuration>0) {
            //
            // first window detected enough particles
            //
            time_searching = countDuration;
            particlesSearched = particlesN;
            isSearched = YES;
        }
        else{
            if (self.searchBuffer.size>2) {
                
                NSTimeInterval lastParticleDetectionTime = [self.searchBuffer.lastObject timeStamp];
                for (NSInteger i = self.searchBuffer.size-1; i>=0; i--) {
                    AMSearchBufferPoint *spoint = [self.searchBuffer objectAtIndex:i];
                    
                    if (spoint.particlesN>=self.particlesSearchedMinimumCount) {
                        //
                        // first window detected enough particles
                        //
                        if (countDuration>0) {
                            time_searching = countDuration;
                            particlesSearched = spoint.particlesN;
                            isSearched = YES;
                            break;
                        }
                    }
                    
                    //
                    // accummulate
                    //
                    particlesSearched += spoint.particlesN;
                    
                    if ((NSInteger)particlesSearched>=self.particlesSearchedMinimumCount){
                        //
                        // limit achieved
                        //
                        time_searching = lastParticleDetectionTime-spoint.timeStamp;
                        
                        if (time_searching==0) {
                            // detected threshold during the one step
                            time_searching = lastParticleDetectionTime - [[self.searchBuffer objectAtIndex:self.searchBuffer.size-2] timeStamp];
                        }
                        isSearched = YES;
                        break;
                    }
                }
            }
        }
        _particlesSAccumulated = particlesSearched;
        if (isSearched  || time_searching>=kAMDosimeterSearchModeMeasurementTime) {
            float dose = 0.0;
            
            // SEARCHED S
            self.searchingModeTime = time_searching;
            
            // Sigma
            self.statErrorSearchedRate   = SIGMA(self.particlesSAccumulated);
            
            // SEARCHED ADER
            dose =  ADER(self.particlesSAccumulated, self.searchingModeTime, self.settings.sensetivitySvP, self.settings.ownBackgoundSvH);
            self.doseSearchedRate = dose<=0.0?-1.0:dose;
            
        }
        
        // MEASUREMENT S = SUM(N1:Nn)
        _particlesNAccumulated += particlesN;
        
        // Sigma (statistical error)
        //self.statErrorEquivalentRate = SIGMA(particlesM);
        //self.statErrorEquivalentRate = SIGMA(self.particlesEquivalentCount);
        
        // MEASUREMENT ADER = ((((SUMM (N1:Nn))*K1)/n)*3600)-K2 in nSv/hour
        if (time_measurement<=1.) {
            //
            // precision is too low to compute
            //
            self.doseEquivalentRatePrivate = 0.0;
        }
        else{
            float dose = 0.0;
            //dose = ADER(self.particlesNAccumulated, time_measurement);
            dose =  ADER(self.particlesNAccumulated, time_measurement, self.settings.sensetivitySvP, self.settings.ownBackgoundSvH);
            self.doseEquivalentRatePrivate = dose<=0.0?0.0:dose;
        }
        
        prevparticlesCount = self.particlesSearchedTotal;
        
        //
        // update every self.minRefreshTime secs onlye
        //
        if (self.minRefreshTime>0) {
            if (currentTime-last_interval>=self.minRefreshTime) {
                last_interval = currentTime;
            }
            else
                return;
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(amDosimeterValuesChanged:)]) {
            dispatch_async(delegateQ, ^{
                [self.delegate amDosimeterValuesChanged:self];
            });
        }
    }
}

- (void) setParticlesSearchedTotal:(UInt64)count{
    NSTimeInterval  ct = [NSDate timeIntervalSinceReferenceDate];
    _particlesSearchedTotal = count;
    float t = (float)(ct - self.searchingStartTime);
    _cpsMeasuring = t>0 ? (float)(_particlesSearchedTotal)/t : _cpsMeasuring;
}

- (void) updateCPS:(UInt64)count{
    NSTimeInterval  ct = [NSDate timeIntervalSinceReferenceDate];
    if (_lastTime == 0 ){
        _lastTime = ct;
    }
    float lt = (float)(ct - _lastTime);
    _cpsSearching = lt>0 ? (float)(count)/lt : _cpsSearching;
    _lastTime = [NSDate timeIntervalSinceReferenceDate];
}

- (void) addParticles:(NSInteger)count{
    if (count>0) {
        // update particles and send event to recompute dose rates
        [self updateCPS:count];
        self.particlesSearchedTotal += count;
        [self recomputeDoseRateWithTime:[NSDate timeIntervalSinceReferenceDate] withCountDuration:0.0];
    }
}

- (void) addParticles:(NSInteger)count withCountDuration:(NSTimeInterval)countDuration{
    if (count>0) {
        // update particles and send event to recompute dose rates
        [self updateCPS:count];
        self.particlesSearchedTotal += count;
        [self recomputeDoseRateWithTime:[NSDate timeIntervalSinceReferenceDate] withCountDuration:countDuration];
        [self updateHistoyDoseRate:self.doseSearchedRate withTimeStamp:[NSDate date]];
    }
}

- (void) addParticles:(NSInteger)count withCountDuration:(NSTimeInterval)countDuration withSearchedDoseRate:(float)doseRate{
    
    if (count>0) {
        // update particles and send event to recompute dose rates and correct by BLE device value
        [self updateCPS:count];
        self.particlesSearchedTotal += count;
        [self recomputeDoseRateWithTime:[NSDate timeIntervalSinceReferenceDate] withCountDuration:countDuration];
        self.doseSearchedRate = doseRate;
        [self updateHistoyDoseRate:self.doseSearchedRate withTimeStamp:[NSDate date]];
    }
}


- (void) addParticles:(NSInteger)count withTime:(NSTimeInterval)timeStamp{
    if (count>0) {
        // update particles and send event to recompute dose rates
        [self updateCPS:count];
        self.particlesSearchedTotal += count;
        [self recomputeDoseRateWithTime:timeStamp withCountDuration:0.0f];
    }
}

- (void) refreshHistoryThread{
    while ([[NSThread currentThread] isCancelled] == NO) {
        [self refreshHistory:nil];
        [NSThread sleepForTimeInterval:1];
    }
    
}

- (void) updateHistoyDoseRate:(float) dose withTimeStamp:(NSDate*)timeStamp{
    
    NSArray *keys = [self.historyBuffers allKeys];
    
    for (NSNumber *key in keys) {
        
        AMDoseRatePoint *dosePoint = [[AMDoseRatePoint alloc] init];
        
        if (dose<=0.0) {
            return;
        }
        
        AMQueue *doseRateBuffer = [self.historyBuffers objectForKey:key];
        
        if (doseRateBuffer==nil) {
            NSLog(@"AMDosimeter(%@) %@ not found...", self.UUIDString, key);
            continue;
        }
        
        AMDoseRatePoint *lastDosePoint = [doseRateBuffer lastObject];
        
        if (lastDosePoint.time!=nil && ([lastDosePoint.time timeIntervalSinceNow]+key.floatValue)>0.0) {
            //
            // if last computing is within history interval
            // do not push it to history
            //
            lastDosePoint.count++;
            if (lastDosePoint.count>1) {
                lastDosePoint.comulativeDoseRate+=dose;
                lastDosePoint.doseRate = lastDosePoint.comulativeDoseRate/lastDosePoint.count;
            }
        }
        else{
            //
            // first measurement
            //
            dosePoint.count = 0;
            dosePoint.time = timeStamp;
            dosePoint.index = lastDosePoint.index+1;
            dosePoint.doseRate = dosePoint.comulativeDoseRate = dose;
            [doseRateBuffer push:dosePoint];
            
            //NSLog(@"AMDosimeter(%@): doseRate=%f comDoseRate=%f  key=%@ count=%lu time=%@ maxSize=%lu", self.UUIDString, lastDosePoint.doseRate, lastDosePoint.comulativeDoseRate, key, (unsigned long)lastDosePoint.count, lastDosePoint.time, (unsigned long)doseRateBuffer.maxSize);
        }
    }
}

- (void) refreshHistory:(NSTimer*) timer{
    [self updateHistoyDoseRate:self.doseSearchedRate withTimeStamp:[NSDate date]];
}

- (void)start{
    if (self.isRunning) return;
    self.isRunning = YES;
    [self.historyThread start];
}

- (void)stop{
    if (!self.isRunning) return;
    self.isRunning = NO;
    [self.historyThread cancel];
    self.historyThread = nil;
}

- (void) reset{
    //
    // reset only measuring mode
    //
    
    self.particlesEquivalentCount = INT64_MAX;
    self.cumulativeDose = -1.0;
    
    self.particlesNAccumulated = 0.0;
    self.doseEquivalentRatePrivate = 0.0;
    self.measuringStartTime = -1.0;
    self.measuringTime = -1.0;
    self.statErrorEquivalentRate = -1.0;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(amDosimeterValuesChanged:)]) {
        dispatch_async(delegateQ, ^{
            [self.delegate amDosimeterValuesChanged:self];
        });
    }
}


- (void) __hardReset{
    
    self.particlesNAccumulated = 0.0;
    self.particlesSAccumulated = 0.0;
    self.particlesSearchedTotal = 0.0;
    
    prevparticlesCount = 0.0;
    
    self.doseEquivalentRatePrivate = 0.0;
    self.doseSearchedRate   = 0.0;
    
    self.measuringStartTime  = -1.0;
    self.searchingStartTime  = -1.0;
    
    self.statErrorEquivalentRate = -1.0;
    self.statErrorSearchedRate   = -1.0;
    
    [self.searchBuffer flush];
}

- (void) hardReset{
    
    [self __hardReset];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(amDosimeterValuesChanged:)]) {
        dispatch_async(delegateQ, ^{
            [self.delegate amDosimeterValuesChanged:self];
        });
    }
}

@end
