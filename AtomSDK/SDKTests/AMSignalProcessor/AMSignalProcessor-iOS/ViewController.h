//
//  ViewController.h
//  AMSignalProcessor-iOS
//
//  Created by denis svinarchuk on 25/06/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *particlesLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeMeasurmetLabel;
@property (weak, nonatomic) IBOutlet UILabel *hasConnectionLabel;
@property (weak, nonatomic) IBOutlet UILabel *powerEnoughLabel;
@property (weak, nonatomic) IBOutlet UILabel *powerVolumeLabel;
@property (weak, nonatomic) IBOutlet UILabel *isReadyLabel;
@property (weak, nonatomic) IBOutlet UILabel *MADERLabel;
@property (weak, nonatomic) IBOutlet UILabel *MADERParticlesLabel;
@property (weak, nonatomic) IBOutlet UILabel *searcheDERLabel;
@property (weak, nonatomic) IBOutlet UILabel *searchedParticlesLabel;
@property (weak, nonatomic) IBOutlet UILabel *searchedTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *thresholdDoseLabel;
@property (weak, nonatomic) IBOutlet UILabel *derSigmaLabel;
@property (weak, nonatomic) IBOutlet UILabel *searchedSigmalLabel;

@property (weak, nonatomic) IBOutlet UIButton *resetButton;
- (IBAction)reset:(id)sender;

@end
