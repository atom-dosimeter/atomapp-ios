//
//  AMControllerUpdateProtocol.h
//  Atom
//
//  Created by denis svinarchuk on 18.07.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AtomMeasurerSDK/AMDosimeter.h>
#import "AMSettings.h"
#import <AtomMeasurerSDK/AMControllerManager.h>

@protocol AMControllerUpdateProtocol <NSObject>

@optional

- (void) didControllerStateUpdate:(AMController*) controller;
- (void) didDosimeterUpdate:(AMDosimeter*) dosimeter;
- (void) didSettingsUpdate:(AMSettings*) settings;

- (void) didControllerSettingsUpdate:(AMController*) settings;

- (void) willControllerChangesCommit:(AMController*) controller;
- (void) didControllerChangesCommit:(AMController*) controller;


@end

@protocol AMControllerManagerUpdateProtocol <NSObject>

- (void) didControllerManagerUpdateList:(AMControllerManager*)manager;
- (void) didControllerManagerUpdateDosimeter:(AMControllerManager*)manager amController:(AMController*)controller;

@end