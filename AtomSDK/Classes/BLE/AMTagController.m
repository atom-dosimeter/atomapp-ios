//
//  AMTagController.m
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 26/08/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMTagController.h"
#import "AMControllerSettings.h"
#import "AMTagControllerSettings.h"
#import "AMSwiftControllerSettings.h"

//
// Controller
//
@interface AMTagController() <AMTagDeviceProtocol>
@property (readonly,nonatomic) AMTagDevice  *tagDevice;
@property (assign,nonatomic) BOOL            connectionStatePrivate;
@end

@implementation AMTagController
{
    AMControllerPowerState lastLowPowerState;
    BOOL                   lastChargerPlugged;
    BOOL isReady;
    float prevBatteryVolume;
}

@synthesize settings=_settings;

- (id) initWithDelegate:(id<AMControllerProtocol>)delegate withTagDevice:(AMTagDevice *)device{
    self = [super initWithDelegate:delegate];
    if (self) {
        lastChargerPlugged = NO;
        prevBatteryVolume = 1.0;
        _tagDevice = device;
        _tagDevice.delegate = self;
        lastLowPowerState = -1;
                
        if ([device.className isEqualToString:AM_DEVICE_SWIFT]
            ){
            _settings = [[AMSwiftControllerSettings alloc] initWithAMTagDevice:self.tagDevice];
        }
        else if  (
                  [device.className isEqualToString:AM_DEVICE_FAST]
                  ){
            _settings = [[AMFastControllerSettings alloc] initWithAMTagDevice:self.tagDevice];
        }
        else if ([device.className isEqualToString:AM_DEVICE_TAG]){
            _settings = [[AMTagControllerSettings alloc] initWithAMTagDevice:self.tagDevice];
        }
        
        _settings.tagController = self;
    }
    
    return self;
}

- (void) playSearchSignal{
    [self.tagDevice playSearchSignal];
}

- (void) resetDosimeter{
    [self.tagDevice resetDose];
    [self.dosimeter reset];
}

- (BOOL) isReady{
    return isReady;
}

- (BOOL) isBusy{
    return self.tagDevice.isBusy;
}

- (NSString*) UUIDString {
    return self.tagDevice.UUIDString;
}

- (float) powerVolume{
    return self.measurement.batteryVolume;
}

- (AMControllerPowerState) powerState{
    
    if (self.measurement == nil) {
        return AM_CONTROLLER_POWER_UNKNOWN;
    }
    
    if (self.measurement.chargerPlugged) {
        return AM_CONTROLLER_POWER_CHARGE_PLUGGED;
    }

    float bv = self.measurement.batteryVolume;
    
    if (bv>0.20)
        return AM_CONTROLLER_POWER_ENOUGH; 
    
    else if (bv>=0.01 && bv<=0.20)
        return AM_CONTROLLER_POWER_NOT_ENOUGH;
    
    return AM_CONTROLLER_POWER_CUT_DISCHARGE;
}

//- (AMControllerSettings*)settings{
//    if (!_settings) {
//        _settings = [[AMTagControllerSettings alloc] initWithUUIDString:self.tagDevice.UUIDString];
//    }
//    return _settings;
//}

- (BOOL) connectionState{
    return _connectionStatePrivate;
}

#pragma mark - AMTag Device Protocol

//
// Ready 
//

- (BOOL) tagDeviceShouldBeDiscovered:(AMTagDevice *)device{
    if ([device.className isEqualToString:AM_DEVICE_TAG]){
        AMTagControllerSettings *s = (AMTagControllerSettings*)self.settings;
        return s.isEnabled.boolValue;
    }
    else if (
             [device.className isEqualToString:AM_DEVICE_SWIFT]
             ||
             [device.className isEqualToString:AM_DEVICE_FAST]
             ){
        AMSwiftControllerSettings *s = (AMSwiftControllerSettings*)self.settings;
        return s.isEnabled.boolValue;
    }
    
    return NO;
}

- (void) tagDeviceWillReady:(AMTagDevice *)device{
    if (self.delegate && [self.delegate respondsToSelector:@selector(amController:willReadyAfterTime:)]) {
        [self.delegate amController:self willReadyAfterTime:0.1];
    }    
}

- (void) tagDevice:(AMTagDevice *)device didReady:(BOOL)state error:(NSError *)error{

    self.connectionStatePrivate = state;
    
    [self.delegate amController:self didUpdateConnectionState:state];
    
    if (!state) {        
        isReady = NO;
        if (self.delegate && [self.delegate respondsToSelector:@selector(amController:didReady:)]) {
            [self.delegate amController:self didReady:isReady];
            lastLowPowerState = -1;
        }
        if (error &&  self.errorHandler && [self.errorHandler respondsToSelector:@selector(amObject:withError:)]) {
            [self.errorHandler amObject:self withError:error];
        }
        
        [self stop];
        [self.dosimeter stop];
        
        _measurement = nil;
    }
}

//
// Name manages
//

- (void) tagDevice:(AMTagDevice *)device didChangeFromName:(NSString *)fromName toName:(NSString *)toName{
    NSLog(@"AMTagController: name %@ is change to %@", fromName, toName);
    if (self.delegate && [self.delegate respondsToSelector:@selector(amController:didChangeFromName:toName:)]) {
        [self.delegate amController:self didChangeFromName:fromName toName:toName];
    }
}

//
// Notification processing
//
- (void) tagDevice:(AMTagDevice *)device didMeasurementUpdate:(AMTagDeviceMeasurement *)measurement{
        
    //
    // TODO: optimize this! it needs to reset once when calibartion is reread.
    //
    self.dosimeter.settings.sensetivity = device.calibration.sensetivity;
    self.dosimeter.settings.ownBackgound = device.calibration.ownBackground*60.0;
    self.dosimeter.settings.deadTime = device.calibration.deadTime/1000.0;
    
    self.dosimeter.measuringTime = measurement.measuringTime;
    self.dosimeter.cumulativeDose = measurement.cumulativeDose;
    self.dosimeter.particlesEquivalentCount = measurement.particlesTotal;
    
    NSInteger count = measurement.particlesLastSeconds;

    self.dosimeter.particlesSearchedMinimumCount = measurement.searchingMode;
    
    if (_measurement == nil) {
        for (AMTagDeviceParticleCount *p in self.tagDevice.particleCounts) {
            [self.dosimeter addParticles:p.particles withTime:p.timeStamp];
        }
    }
    else{
                
        if ([device.className isEqualToString:@"Tag"]
            ){
            [self.dosimeter addParticles:count withCountDuration:measurement.particlesLastDuration];
        }
        else{
            [self.dosimeter addParticles:count withCountDuration:measurement.particlesLastDuration withSearchedDoseRate:measurement.doseRate];
        }
    }
    
    [self.delegate amController:self didDetectParticles:count];
    
    _measurement = measurement;
    
    //NSLog(@"[%@] = %@", device.name, _measurement);
        
    if (self.measurement.batteryVolume>0.) {
        if (prevBatteryVolume!=self.measurement.batteryVolume ) {
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(amController:didChangePowerVolume:)]) {
                [self.delegate amController:self didChangePowerVolume:self.powerVolume];
            }
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(amController:didChangePowerState:)]) {
                // when changed check low battery status
                AMControllerPowerState lowPowerState = self.powerState;
                if (lastLowPowerState != lowPowerState) {
                    [self.delegate amController:self didChangePowerState:(lowPowerState)];
                }
                lastLowPowerState = lowPowerState;
            }
        }
    }
    
    if (lastChargerPlugged!=self.measurement.chargerPlugged) {
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(amController:didChangePowerState:)]) {
            [self.delegate amController:self didChangePowerState:(self.powerState)];
        }
        
        if (self.powerState == AM_CONTROLLER_POWER_CHARGE_PLUGGED || lastChargerPlugged) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(amController:didChangePowerVolume:)]) {
                [self.delegate amController:self didChangePowerVolume:self.powerVolume];
            }
        }
        
        lastChargerPlugged = self.measurement.chargerPlugged;

    }

    prevBatteryVolume=self.measurement.batteryVolume;
    
    if (!isReady) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(amController:didReady:)]) {
            [self.delegate amController:self didReady:YES];            
        }
    }
    isReady = YES;
}

- (void) tagDevice:(AMTagDevice *)device didPreferencesUpdate:(AMTagDevicePreferences *)settings{                
    if (!self.dosimeter.isRunning) {
        [self start];
        [self.dosimeter start];        
    }else {
        self.dosimeter.UUIDString = self.settings.UUIDString;
        if (self.delegate && [self.delegate respondsToSelector:@selector(amController:didUpdateSettings:)]) {
            [self.delegate amController:self didUpdateSettings:self.settings];
        }
    }
}

- (void) tagDevicedidUpdateRSSI:(AMTagDevice *)device{
    _RSSI = device.RSSI;
    _avgRSSI = device.avgRSSI;
    if (self.delegate && [self.delegate respondsToSelector:@selector(amController:didUpdateSettings:)]) {
        [self.delegate amController:self didUpdateSettings:self.settings];
    }
}

//
// Synchronization
//

- (void) tagDeviceWillSynchronize:(AMTagDevice *)device{
    //NSLog(@" ###  tagDeviceWillSynchronize WILL commit chamges: %@", device.UUIDString);

    if (self.delegate && [self.delegate respondsToSelector:@selector(amControllerWillCommitChanges:)]) {
        [self.delegate amControllerWillCommitChanges:self];
    }
}

- (void) tagDeviceDidSynchronize:(AMTagDevice *)device{
    if (self.delegate && [self.delegate respondsToSelector:@selector(amControllerDidCommitChanges:)]) {
        [self.delegate amControllerDidCommitChanges:self];
    }
}


//
// Warnings
//
- (void) tagDevice:(AMTagDevice *)device didWarn:(NSString *)warnString{
    if (self.delegate && [self.delegate respondsToSelector:@selector(amController:didWarn:)]) {
        [self.delegate amController:self didWarn:warnString];
    }
}

//
// Error
//
- (void) tagDevice:(AMTagDevice *)device didFail:(NSError *)error{
    if (self.errorHandler && [self.errorHandler respondsToSelector:@selector(amObject:withError:)]) {
        [self.errorHandler amObject:self withError:error];
    }
}

@end
