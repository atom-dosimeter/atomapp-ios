//
//  AMDeviceListViewController.h
//  Atom
//
//  Created by denis svinarchuk on 27/08/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMDeviceListViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
