//
//  AMTagDeviceCalibration.h
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 10.10.14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMBLEProtocol.h"

@interface AMTagDeviceParticleCount : NSObject
@property (readonly,nonatomic) NSTimeInterval timeStamp;
@property (readonly,nonatomic) uint8_t        particles;
@end

/**
 *  Calibration characteristic uses last measurements part only.
 */
@interface AMTagDeviceParticles : NSObject<AMBLEProtocol>
/**
 *  Array contains count of particles number sorted from the late to older. The amount of particles measurement every 2 seconds.
 *  This version of firmware keeps 8 measurements.
 */
@property (nonatomic,readonly) NSArray *particleCounts;
@property (nonatomic,readonly) NSData  *data;
@end
