//
//  MESettings.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 11/13/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <objc/runtime.h>
#import <objc/message.h>
#import "AMSettings.h"
#import <AtomMeasurerSDK/AMDoseUnit.h>
#import <AtomMeasurerSDK/AMConstants.h>

static AMSettings *__shared_instance = nil;

@implementation AMSettings
{
    objc_property_t *properties;
    NSDictionary *captionBook;
    NSUInteger _count;
    NSMutableDictionary *getters;
}

#pragma Properties definition

- (NSUInteger) count{
    return _count;
}

#pragma Utils
- (void) setValue:(id)value forSettingNo:(NSInteger)number{
    
    @try {
        if (_count==0 || number>=_count) {
            return;
        }
        NSString *property_Name =  [self setterNameForProperty:[NSString stringWithCString:property_getName(properties[number]) encoding:NSUTF8StringEncoding]];
        
        
        typedef void (*send_type)(id, SEL, id); 
        send_type func = (send_type)objc_msgSend; 
        
        func(self, sel_registerName([property_Name UTF8String]), value); 

        //objc_msgSend(self, sel_registerName([property_Name UTF8String]), value);
    }
    @catch (NSException *exception) {
        NSLog(@"AMSettings: %@ %s:%i", exception, __FILE__, __LINE__);
    }
}

- (void) setValue:(id)value forSettingName:(NSString *)name{
    @try {
        for (int i = 0; i < _count; i++) {
            if ([[NSString stringWithFormat:@"%s",property_getName(properties[i])] isEqualToString:name]) {
                [self setValue:value forSettingNo:i];
                return;
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"AMSettings: %@ %s:%i", exception, __FILE__, __LINE__);
    }
}

- (void) setValue:(id)value forSettingNo:(NSInteger)number inCurrentUnit:(BOOL)unit{    id rvalue = value;
    
    @try {
        if (unit) {
            if (_count==0 || number>=_count) {
                return;
            }
            NSString *nm = [NSString stringWithFormat:@"%s", property_getName(properties[number])];
            if ([nm isEqualToString:@"thresholdDoseRate"]
                ||
                [nm isEqualToString:@"thresholdDose"]
                ){
                rvalue = [[AMDoseUnit sharedInstance] fromNumberToBase:value];
            }
        }
        
        [self setValue:rvalue forSettingNo:number];
    }
    @catch (NSException *exception) {
        NSLog(@"AMSettings: %@ %s:%i", exception, __FILE__, __LINE__);
    }
}

- (NSString*) captionForSettingName:(NSString *)name{
    @try {
        
        for (int i = 0; i < _count; i++) {
            if ([[NSString stringWithFormat:@"%s",property_getName(properties[i])] isEqualToString:name]) {
                return [self captionForSettingNo:i];
            }
        }
        return nil;
    }
    @catch (NSException *exception) {
        return name;
    }
    
}

- (NSString*) captionForSettingNo:(NSUInteger)number{
    if (_count==0 || number>=_count) {
        return nil;
    }
    @try {
        NSString *name= [NSString stringWithFormat:@"%s", property_getName(properties[number])];
        NSString *ret = [[captionBook valueForKey:name] objectAtIndex:0];
        return ret==nil?name:ret;
    }
    @catch (NSException *exception) {
        return nil;
    }
};

- (NSString*) typeForSettingNo:(NSUInteger)number{
    if (_count==0 || number>=_count) {
        return nil;
    }
    @try {
        NSString *name= [NSString stringWithFormat:@"%s", property_getName(properties[number])];
        NSString *ret = [[captionBook valueForKey:name] objectAtIndex:2];
        return ret==nil?name:ret;
    }
    @catch (NSException *exception) {
        return nil;
    }
};

- (id) selectedSettingNo:(NSUInteger)number{
    if (_count==0 || number>=_count) {
        return nil;
    }
    @try {
        NSString *name= [NSString stringWithFormat:@"%s", property_getName(properties[number])];
//        NSLog(@" @@@@@@   selectedSettingNo: name --  %@[%i]", name, number);

        id value = [self valueForSettingNo:number];
//        NSLog(@" @@@@@@   selectedSettingNo: value --  %@", value);
        NSArray *valuesArray = [[captionBook valueForKey:name] objectAtIndex:1][0];
        NSArray *namesArray = [[captionBook valueForKey:name] objectAtIndex:1][1];
//        NSLog(@" @@@@@@   selectedSettingNo: values --  %@", valuesArray);
//        NSLog(@" @@@@@@   selectedSettingNo: names  --  %@", namesArray);
        int i=0;
        for (id v in valuesArray) {
            if ([v isEqual:value]) {
                return [namesArray objectAtIndex:i];
            }
            i++;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"AMSettings: %@: %s:%i",exception, __FILE__, __LINE__);
        return nil;
    }
    NSLog(@"AMSettings: index %lu out of range: %s:%i", (unsigned long)number, __FILE__, __LINE__);
    return nil;
}

- (int) selectedIndexForSettingNo:(NSUInteger)number{
    if (_count==0 || number>=_count) {
        return 0;
    }
    @try {
        NSString *name= [NSString stringWithFormat:@"%s", property_getName(properties[number])];
        id value = [self valueForSettingNo:number];
        NSArray *valuesArray = [[captionBook valueForKey:name] objectAtIndex:1][0];
        int i=0;
        for (id v in valuesArray) {
            if ([v isEqual:value]) {
                return i;
            }
            i++;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"AMSettings: %@: %s:%i",exception, __FILE__, __LINE__);
    }
    return 0;
}

- (int) selectedIndexForSettingName:(NSString*)name{
    @try {
        id value = [self valueForSettingName:name];
        NSArray *valuesArray = [[captionBook valueForKey:name] objectAtIndex:1][0];
        int i=0;
        for (id v in valuesArray) {
            if ([v isEqual:value]) {
                return i;
            }
            i++;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"AMSettings: %@: %s:%i",exception, __FILE__, __LINE__);
    }
    return 0;
}

- (id) selectedSettingForSettingName:(NSString *)name{
    @try {
//        NSLog(@" @@@@@@   selectedSettingForSettingName: name --  %@", name);
        for (int i = 0; i < _count; i++) {
            if ([[NSString stringWithFormat:@"%s",property_getName(properties[i])] isEqualToString:name]) {
//                NSLog(@" @@@@@@   selectedSettingForSettingName (%@: name --  %@ : %s  i = %i  /  %i", self, name, property_getName(properties[i]), i, _count);
                return [self selectedSettingNo:i];
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"AMSettings: %@: %s:%i",exception, __FILE__, __LINE__);
    }
    return nil;
}

- (BOOL) boolForSettingNo:(NSInteger)number{
    return [[self valueForSettingNo:number ] boolValue];
}

- (BOOL) boolForSettingName:(NSString *)name{
    @try {
        for (int i = 0; i < _count; i++) {
            if ([[NSString stringWithFormat:@"%s",property_getName(properties[i])] isEqualToString:name]) {
                return [self boolForSettingNo:i];
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"AMSettings: %@: %s:%i",exception, __FILE__, __LINE__);
    }
    return NO;
}


- (id) valueForSettingNo:(NSInteger)number{
    if (_count==0 || number>=_count) {
        return nil;
    }
    @try {
        NSString *selector = [NSString stringWithCString:property_getName(properties[number]) encoding:NSUTF8StringEncoding];
        
        typedef id (*send_type)(id, SEL); 
        send_type func = (send_type)objc_msgSend; 
        
        id propertyValue =  func(self, sel_registerName([selector UTF8String])); 

        //id propertyValue = objc_msgSend(self, sel_registerName([selector UTF8String]));
        
        return propertyValue;
    }
    @catch (NSException *exception) {
        NSLog(@"AMSettings: %@: %s:%i",exception, __FILE__, __LINE__);
    }
    return nil;
}

- (id) valueForSettingNo:(NSInteger)number inCurrentUnit:(BOOL)current{
    
    @try {
        id value = [self valueForSettingNo:number];
        
        if (current) {
            NSString *nm = [NSString stringWithCString:property_getName(properties[number]) encoding:NSUTF8StringEncoding];
            if (
                [nm isEqualToString:@"thresholdDoseRate"]
                ||
                [nm isEqualToString:@"thresholdDose"]
                ) {
                //
                // recompute to the base unit
                //
                value = [[AMDoseUnit sharedInstance] convertNumberToCurrent:value];
            }
            
        }
        return value;
    }
    @catch (NSException *exception) {
        NSLog(@"AMSettings: %@: %s:%i",exception, __FILE__, __LINE__);
    }
    return nil;
}

- (id) sourceForSettingNo:(NSInteger)number{
    if (_count==0 || number>=_count) {
        return nil;
    }
    @try {
        NSString *selector = [NSString stringWithCString:property_getName(properties[number]) encoding:NSUTF8StringEncoding];
        NSArray *a = [captionBook valueForKey:selector];
        return [a objectAtIndex:1];
    }
    @catch (NSException *exception) {
        return nil;
    }
}

- (id) sourceForSettingName:(NSString*)name{
    @try {
        for (int i = 0; i < _count; i++) {
            if ([[NSString stringWithFormat:@"%s",property_getName(properties[i])] isEqualToString:name]) {
                return [self sourceForSettingNo:i];
            }
        }
        return nil;
    }
    @catch (NSException *exception) {
        return name;
    }
}

- (NSString*) proertyNameForSettingNo:(NSInteger)number{
    if (_count==0 || number>=_count) {
        return nil;
    }
    @try {
        return [NSString stringWithCString:property_getName(properties[number]) encoding:NSUTF8StringEncoding];
    }
    @catch (NSException *exception) {
        NSLog(@"AMSettings: %@: %s:%i",exception, __FILE__, __LINE__);
    }
    return nil;
}

- (id) valueForSettingName:(NSString *)name{
    @try {
        for (int i = 0; i < _count; i++) {
            if ([[NSString stringWithFormat:@"%s",property_getName(properties[i])] isEqualToString:name]) {
                return [self valueForSettingNo:i];
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"AMSettings: %@: %s:%i",exception, __FILE__, __LINE__);
    }
    return nil;
}


#pragma Constructors
+ (id) sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!__shared_instance) {
            __shared_instance = [[AMSettings alloc] init];
        }
    });
    
    return __shared_instance;
}

id getterMethod(id self, SEL _cmd){
    NSString *nm =  NSStringFromSelector(_cmd);
    id value = [[NSUserDefaults standardUserDefaults] valueForKey:nm];
    return value;
}

void setterMethod(id self, SEL _cmd, id value){
    NSString *nm = [self getterNameForSetter: NSStringFromSelector(_cmd)];
    
    if ([nm isEqualToString:@"doseUnit"]) {
        [AMDoseUnit sharedInstance].current = [value intValue];
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:value forKey:nm ];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:AM_SETTINGS_CHANGED_NOTIFICATION object:self];;
}

- (NSString*) setterNameForProperty:(NSString*)property_name_string{
    return [NSString stringWithFormat:@"set%@%@:",[[property_name_string substringToIndex:1] capitalizedString],[property_name_string substringFromIndex:1]];
}

- (NSString*) getterNameForSetter:(NSString*)setter_name_string{
    return [getters valueForKey:setter_name_string];
}

- (id) init{
    
    if (__shared_instance!=nil) {
        self = __shared_instance;
        return self;
    }
    
    self = [super init];
    
    if (self) {
        
        @try {
            captionBook = ME_DEFAULT_SETTINGS;
            
            getters = [[NSMutableDictionary alloc] init];
            
            unsigned int outCount = 0;
            properties = class_copyPropertyList([self class], &outCount);
            _count = (NSUInteger) outCount;
            
            //
            // Set defaults
            //
            for (int i = 0; i < _count; i++) {
                objc_property_t property = properties[i];
                const char *propName = property_getName(property);
                
                NSString *property_name_string = [NSString stringWithCString:propName encoding:NSUTF8StringEncoding];
                NSArray *a = [captionBook valueForKey:property_name_string];
                
                if ([[NSUserDefaults standardUserDefaults] objectForKey:property_name_string] == nil) {
                    
                    id obj = [a objectAtIndex:1];
                    
                    if ([obj isKindOfClass:[NSArray class]]) {
                        id aobj = obj;
                        
                        aobj = [aobj objectAtIndex:0];
                        
                        if ([aobj isKindOfClass:[NSArray class]]) {
                            aobj = [aobj objectAtIndex:0];
                            id default_v = nil;
                            @try {
                                default_v = [a objectAtIndex:3];
                                aobj = default_v;
                            }
                            @catch (NSException *exception) {}
                        }
                        [[NSUserDefaults standardUserDefaults] setValue:aobj forKey:property_name_string];
                        if ([property_name_string isEqualToString:@"doseUnit"]) {
                            NSNumber *u = aobj;
                            [[AMDoseUnit sharedInstance] setCurrent:u.intValue];
                        }
                    }
                    else {
                        [[NSUserDefaults standardUserDefaults] setValue:obj forKey:property_name_string];
                    }
                }
                
                NSString *getter_name = property_name_string;
                NSString *setter_name = [self setterNameForProperty:property_name_string];
                
                [getters setValue:property_name_string forKey:setter_name];
                
                SEL getter_selector = sel_registerName([getter_name UTF8String]);
                SEL setter_selector = sel_registerName([setter_name UTF8String]);
                
                class_replaceMethod([self class], getter_selector, (IMP) (getterMethod), method_getTypeEncoding(class_getInstanceMethod([self class], getter_selector)));
                class_replaceMethod([self class], setter_selector, (IMP) (setterMethod), method_getTypeEncoding(class_getInstanceMethod([self class], setter_selector)));
            }
            
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        @catch (NSException *exception) {
            NSLog(@"AMSettings: %@: %s:%i",exception, __FILE__, __LINE__);
        }
    }
    return self;
}

- (void) dealloc{
    free(properties);
}

@end

extern NSDictionary *__settins__(){
    static NSDictionary *d=nil;
    AMDoseUnit *units = [AMDoseUnit sharedInstance];
    
    NSString *isVibrateOnString ;
    if([[UIDevice currentDevice].model isEqualToString:@"iPhone"]){
        isVibrateOnString = NSLocalizedString(@"Vibrate when dose rate more the threshold",@"");
    }
    else {
        isVibrateOnString = NSLocalizedString(@"Play alert sound when dose rate more the threshold",@"");
    }
    
    if (!d) {
        d=  @{ \
              @"thresholdDoseRate":   @[NSLocalizedString(@"Threshold of Ambient DER", @"Threshold label text"),
                                        @600.00,
                                        @"number"], //nSv/h
              
              @"thresholdDose":   @[NSLocalizedString(@"Cumulative dose rate threshold", @""),
                                              @10.0,
                                              @"number"], //nSv/h
              
//              @"searchingMode": @[NSLocalizedString(@"Searching mode", @"Searching mode title label"),
//                                  @[
//                                      @[
//                                          [NSNumber numberWithFloat:kAMDosimeterSearchModeFast],
//                                          [NSNumber numberWithFloat:kAMDosimeterSearchModeNormal],
//                                          [NSNumber numberWithFloat:kAMDosimeterSearchModePrecisely],
//                                          
//                                          ],
//                                      @[
//                                          NSLocalizedString(@"Fast",@""),
//                                          NSLocalizedString(@"Normal",@""),
//                                          NSLocalizedString(@"Slow",@""),
//                                          ]], @"selection", [NSNumber numberWithFloat:kAMDosimeterSearchModeNormal]],
              
              @"trustFactor": @[NSLocalizedString(@"Confidence level", @""),
                                @[
                                    @[
                                        [NSNumber numberWithFloat:kAMDosimeterTrustFactor_Kpmin],
                                        [NSNumber numberWithFloat:kAMDosimeterTrustFactor_Kpmid],
                                        [NSNumber numberWithFloat:kAMDosimeterTrustFactor_Kphigh]],
                                    @[
                                        NSLocalizedString(@"67% (low)",@""),
                                        NSLocalizedString(@"95% (normal)",@""),
                                        NSLocalizedString(@"99% (high)",@""),
                                        ]], @"selection", [NSNumber numberWithFloat:kAMDosimeterTrustFactor_Kpmid]],

              @"historyInterval": @[NSLocalizedString(@"Chart interval", @""),
                                @[
                                    @[
                                        [NSNumber numberWithFloat:1],
                                        [NSNumber numberWithFloat:10],
                                        [NSNumber numberWithFloat:60]],
                                    @[
                                        NSLocalizedString(@"1 sec",@""),
                                        NSLocalizedString(@"10 sec",@""),
                                        NSLocalizedString(@"1 min",@""),
                                        ]], @"selection", [NSNumber numberWithFloat:1]],
              

              @"doseUnit" : @[NSLocalizedString(@"Base unit", @""),
                              @[
                                  [units availableUnits],
                                  [units availableSymbols]
                                  ], @"selection", [NSNumber numberWithInt:AM_DOSE_UNIT_MKSV]
                              ],

              @"isVibrateOn":   @[isVibrateOnString,
                                                @YES,
                                                @"switch"],

              @"isThermometerScaleDynamic":   @[NSLocalizedString(@"Indicator scale should be dynamic",
                                                                  @"Should thermometer scale be dynamic."),
                                                @YES,
                                                @"switch"],

              };
    }
    return d;
}

