//
//  AMThermometerView.m
//  Atom
//
//  Created by denis svinarchuk on 21/07/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMThermometerView.h"
#import "AMUIPreferences.h"
#import "AMThermometerMercury.h"
#import "AMThermometerScale.h"
#import <QuartzCore/QuartzCore.h>
#import "AMSettings.h"
#import <AtomMeasurerSDK/AMDoseUnit.h>
#import <AtomMeasurerSDK/AMConstants.h>
#import <AtomMeasurerSDK/AMControllerManager.h>

@interface AMThermometerView()
@property (nonatomic,readonly) AMDoseUnit *unit;
@property (nonatomic,readonly) AMThermometerMercury *mercury;
@property (nonatomic,readonly) AMThermometerScale   *scale;
@property (nonatomic,readonly) float startHigh;
@property (nonatomic,readonly) AMSettings *settings;
@property (nonatomic,readonly) AMControllerSettings *defaultControllerSettings;
@property (nonatomic,readonly) CGRect mercuryFrame;
@property (nonatomic,readonly) CGRect scaleFrame;
@end

@implementation AMThermometerView

@synthesize doseRate=_doseRate, unit = _unit, mercury = _mercury, scale=_scale, settings=_settings;

- (AMDoseUnit*) unit{
    if (!_unit) {
        _unit = [AMDoseUnit sharedInstance];
    }
    
    return _unit;
}

- (AMSettings*) settings{
    if (!_settings) {
        _settings = [AMSettings sharedInstance];
    }
    return _settings;
}

- (AMControllerSettings*) defaultControllerSettings{
    return [[AMControllerManager sharedInstance] defaultController].settings;
}

- (float) startHigh{ 
    return self.low*10.;
}

- (void) __init__{
    self.doseRate = 0.0;
    self.sigma = 100.0;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self __init__];
    }
    return self;
}

- (void) awakeFromNib{
    
    
    [super awakeFromNib];
    [self __init__];
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    self.layer.shadowRadius = 1.5;
    self.layer.shadowOpacity = 0.2;

}

- (void) setValue:(float)value{
    [super setValue:value];
    self.scale.value = value;
    self.mercury.value = value;
}

- (void) setLow:(float)low{
    [super setLow:low];
    self.scale.low = low;
    self.mercury.low = low;
}

- (void) setHigh:(float)high{
    [super setHigh:high];
    if (self.scale.isThresholdChanging) {
        return;
    }
    self.scale.high = high;
    self.mercury.high = high;
}


- (void) setDoseRate:(float)doseRate{
    _doseRate = doseRate;
    
    if (self.hasDynamicScale || self.scale.isThresholdChanging == NO) {
        float  maxDoseWithSigma = _doseRate+_doseRate*self.sigma/100.*kAMDosimeterTrustFactor_Kphigh;
        float  high = self.high;
        
        if (high<=maxDoseWithSigma) { // 50% - min sigma
            high = high*10.;
        }
        else if (maxDoseWithSigma<self.high/10. &&
                 maxDoseWithSigma/10. > self.startHigh * 10.
                 ){
            high = high/10.;
        }
        else if (maxDoseWithSigma<=self.startHigh/2.)
            high = self.startHigh * 10.;
        
        //NSNumber *thresholdDoseRate=[[AMControllerManager sharedInstance] defaultController].settings.thresholdDoseRate;
        float doseThreshold = self.thresholdDoseRate.floatValue;
        if (high<doseThreshold) {
            while (high<doseThreshold) {
                high = high * 10.;
            }
        }
        self.high = high;
        [self.scale setNeedsDisplay];
    }

    if (doseRate<=self.low) {
        doseRate = self.low;
    }
    else if (doseRate>=self.high){
        doseRate = self.high;
    }
    
    self.value = _doseRate;
}

- (void) reload{
    
    if (self.hasDynamicScale) {
        [self resetDynamics];
    }
    [self setNeedsDisplay];
    [self.scale setNeedsDisplay];
    [self.mercury setNeedsDisplay];
}

- (NSNumber*) thresholdDoseRate{
    if (!_thresholdDoseRate) {
        _thresholdDoseRate  = [[AMControllerManager sharedInstance] defaultController].settings.thresholdDoseRate;
    }
    return _thresholdDoseRate;
}

- (void) resetDynamics{
    float high = self.startHigh;
    //NSNumber *thresholdDoseRate=[[AMControllerManager sharedInstance] defaultController].settings.thresholdDoseRate;
    while (high<self.thresholdDoseRate.floatValue || high<self.doseRate-self.settings.trustFactor.floatValue*self.sigma/100.) {
        high = high * 10.;
    }
    self.high = high;
}

- (void) setHasDynamicScale:(BOOL)hasDynamicScale{
    _hasDynamicScale=hasDynamicScale;
    if (_hasDynamicScale) {
        [self resetDynamics];
    }
    else
        self.high = self.maxHigh;
}

- (CGRect) mercuryFrame{
    return CGRectInset(self.scale.bounds, AM_MERCURY_OFSET_WIDTH, 0);
}

- (AMThermometerMercury*) mercury{
    if (!_mercury) {
        _mercury = [[AMThermometerMercury alloc] initWithFrame: self.mercuryFrame];
        _mercury.low = self.low;
        _mercury.high = self.high;
        [self.scale addSubview:_mercury];
        self.scale.mercury = _mercury;
    }
    return _mercury;
}

- (CGRect) scaleFrame{
    return CGRectInset(self.bounds, 0, AM_THERMOMETER_LABEL_HEIGHT+1);
}

- (AMThermometerScale*) scale{
    if (!_scale) {
        _scale = [[AMThermometerScale alloc] initWithFrame: self.scaleFrame];
        _scale.thermometer = self;
        _scale.low = self.low;
        _scale.high = self.high;
        [self addSubview:_scale];        
        _scale.backgroundColor = [UIColor colorWithRed:1. green:0.5 blue:0.3 alpha:0.01];
    }
    return _scale;
}

- (void) setSigma:(float)sigma{
    self.mercury.sigma = sigma;
}

- (void) setTrustFactor:(float)trustFactor{
    self.mercury.trustFactor = trustFactor;
}

- (float) sigma{
    return self.mercury.sigma;
}

- (float) trustFactor{
    return self.mercury.trustFactor;
}


- (void) layoutSubviews{
    [super layoutSubviews];
    self.scale.frame = self.scaleFrame;
    self.mercury.baseFrame = self.mercuryFrame;
}

@end
