//
//  AMTagDeviceThreshold.m
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 29/09/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMTagDeviceThreshold.h"
#import <Accelerate/Accelerate.h>

typedef struct {
    char thresholdDose[4]; // µSv
    char thresholdDoseRate[4]; // µSv/h
    char thresholdDetectionTime; // sec.
    char command;
    
} AMTagThresholdData;

@implementation AMTagDeviceThreshold

- (id) init{
    self = [super init];
    if (self) {
        _priority = AM_THRESHOLD_PRIORITY_LOW;
    }
    return self;
}

- (id) initWithData:(NSData *)data{
    self = [super init];
    if (self) {
        _priority = AM_THRESHOLD_PRIORITY_LOW;
        [self readData:data];
    }
    return self;
}

- (id) initWithData:(NSData *)data withPriority:(AMTagDeviceThresholdPriority)priority{
    self = [super init];
    
    if (self) {
        _priority = priority;
        [self readData:data];
    }
    
    return self;
}

- (void) readData:(NSData*)data{
    AMTagThresholdData *input_data = (AMTagThresholdData *)[data bytes];
    
    memcpy(&_thresholdDose, input_data->thresholdDose, sizeof(input_data->thresholdDose));
    _thresholdDose = AM_LITTLE_ENDIAN_FLOAT_TO_HOST(_thresholdDose); // µSv
    
    memcpy(&_thresholdDoseRate, input_data->thresholdDoseRate, sizeof(input_data->thresholdDoseRate));
    _thresholdDoseRate = AM_LITTLE_ENDIAN_FLOAT_TO_HOST(_thresholdDoseRate); // µSv
    
    _thresholdDetectionTime = (float)((int)input_data->thresholdDetectionTime);
    
    _command = (AMTagDeviceThresholdCommand)input_data->command;
}


- (NSData*) data{
    AMTagThresholdData output_data;
    
#warning TODO: we must prepare all float to little endian representation format for Atom Tag device chipset
    //
    // TODO
    //
    
    memcpy(output_data.thresholdDose, &_thresholdDose, sizeof(_thresholdDose));
    memcpy(output_data.thresholdDoseRate, &_thresholdDoseRate, sizeof(_thresholdDoseRate));

    output_data.thresholdDetectionTime = (char)self.thresholdDetectionTime;
    output_data.command = self.command;
    
    return [NSData dataWithBytes:&output_data length:sizeof(output_data)];
}


+ (CBUUID*) identifier{
    return [CBUUID UUIDWithString:@"3F71E820-1D98-46D4-8ED6-324C8428868C"];
}

- (CBUUID*) identifier{
    switch (self.priority) {
        case AM_THRESHOLD_PRIORITY_LOW:
            return [AMTagDeviceThreshold identifier];
        case AM_THRESHOLD_PRIORITY_HIGH:
            return [CBUUID UUIDWithString:@"2E95D467-4DB7-4D7F-9D82-4CD5C102FA05"];
        case AM_THRESHOLD_PRIORITY_HIGHER:
            return [CBUUID UUIDWithString:@"F8DE242F-8D84-4C12-9A2F-9C64A31CA7CA"];
    }
}


- (NSString*) description{
    
    return [NSString stringWithFormat: @"\n\
            thresholdDose          = %f\n\
            thresholdDoseRate      = %f\n\
            thresholdDetectionTime = %f\n\
            command                = %x\n",
            self.thresholdDose,
            self.thresholdDoseRate,
            self.thresholdDetectionTime,
            self.command
            ];
}

@end
