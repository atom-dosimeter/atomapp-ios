//
//  ViewController.m
//  AtomControllerManagerTest
//
//  Created by denis svinarchuk on 27/08/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "ViewController.h"
#import "AMControllerManager.h"
#import "AMSimpleController.h"
#import "AMTagController.h"

@interface ViewController () <AMControllerManagerDelegate>

@property (strong,nonatomic) AMControllerManager *controllerManager;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    _controllerManager = [AMControllerManager sharedInstance];
    _controllerManager.delegate = self;
}


- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [_controllerManager start];

}


- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [_controllerManager stop];
}

#pragma mark - Dosimeter delegate
//
// Dosimeter
//
- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController *)controller didUpdateAmDosimeter:(AMDosimeter *)dosimeter{
    NSLog(@"ViewController: dozimeter - rate : = %f searched = %f <- %@", dosimeter.doseEquivalentRate/10., dosimeter.doseSearchedRate/10., dosimeter.UUIDString);
}

#pragma mark - AMController manager protocol
//
// Controllers list
//
- (void) amControllerManagerDidUpdateControllerList:(AMControllerManager *)manager{
    NSLog(@"ViewController: List update count=%i", manager.controllerList.count);
}

//
// Connction state
//
- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController *)controller didUpdateConnectionState:(BOOL)available{
    NSLog(@"ViewController: controller %@ update connection: %i", controller.settings.UUIDString, available);
}


//
// Power Control
//
- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController *)controller didChangePowerState:(AMControllerPowerState)lowPower{
    NSLog(@"ViewController: controller - low power state : = %i <- %@", lowPower, controller.settings.UUIDString);
}

- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController *)controller didChangePowerVolume:(float)volume{
    if ([controller isKindOfClass:[AMTagController class]]){
        AMTagController *tcontroller = (AMTagController *)controller;
        NSLog(@"ViewController: controller - power volume : = %f/%f <- %@", controller.powerVolume, tcontroller.notification.batteryVolte, controller.settings.UUIDString);
    }
    else
        NSLog(@"ViewController: controller - power volume : = %f <- %@", controller.powerVolume, controller.settings.UUIDString);
}


// Ready Control

- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController *)controller willReadyAfterTime:(NSTimeInterval)approxTime{
    NSLog(@"ViewController: controller - will ready after: %f <- %@", approxTime, controller.settings.UUIDString);
}

- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController *)controller didUpdateReadyState:(BOOL)ready{
    if ([controller isKindOfClass:[AMSimpleController class]]) {
        NSLog(@"ViewController: controller - update ready status: %i <- %@", ready, controller.settings.UUIDString);
    }
    else if ([controller isKindOfClass:[AMTagController class]]){
        AMTagController *tcontroller = (AMTagController *)controller;
        NSLog(@"ViewController: controller - update ready status: %i <- %@ settings = %@", ready, controller.settings.UUIDString, tcontroller.settings);
    }
}


// Error handling

- (void) amControllerManager:(AMControllerManager *)manager amController:(AMController *)controller didFail:(NSError *)error{
    NSLog(@"ViewController: controller: %@ did fail: %@", controller, error);
}

@end
