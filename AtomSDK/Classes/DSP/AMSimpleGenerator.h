//
//  AMSimpleGenerator.h
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 10/09/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMSimpleAudio.h"

@interface AMSimpleGenerator : AMSimpleAudio
/**
 *  Tone frequency, should be between 12000-15000Hz. By default is 14000.
 *
 */
@property(assign,atomic) Float32 frequency;
+ (id) sharedInstance;
@end
