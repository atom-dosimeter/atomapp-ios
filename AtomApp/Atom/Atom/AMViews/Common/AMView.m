//
//  AMView.m
//  Atom
//
//  Created by denis svinarchuk on 23/07/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMView.h"

@implementation AMView

- (void) layoutSubviews{
    [super layoutSubviews];
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    self.layer.shadowRadius = 1.5;
    self.layer.shadowOpacity = .2;
}

//- (void) awakeFromNib{
//    [super awakeFromNib];
//    [self __init__];
//}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}

@end
