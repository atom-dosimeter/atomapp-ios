//
//  AMTagNotification.m
//  Atom
//
//  Created by denis svinarchuk on 23.08.14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMTagDeviceMeasurement.h"

//
// Calibration raw data
//
typedef struct {
    char sensetivity[4];             // impulses/µR
    char ownBackgound[4];            // impulses/min.
    char deadTime[4];                // sec.;
    char measurementDoseRateTime[2]; // default = 34
}AMTagCalibrationData;

@implementation AMTagDeviceCalibrationConstants

- (id) initWithData:(NSData *)data{
    self = [super init];
    if (self) {
        [self readData:data];
    }
    return self;
}

- (void) readData:(NSData*)data{
    AMTagCalibrationData *input_data = (AMTagCalibrationData *)[data bytes];
    
    memcpy(&_sensetivity, input_data->sensetivity, sizeof(input_data->sensetivity));
    _sensetivity = AM_LITTLE_ENDIAN_FLOAT_TO_HOST(_sensetivity);

    memcpy(&_ownBackground, input_data->ownBackgound, sizeof(input_data->ownBackgound));
    _ownBackground = AM_LITTLE_ENDIAN_FLOAT_TO_HOST(_ownBackground);

    memcpy(&_deadTime, input_data->deadTime, sizeof(input_data->deadTime));
    _deadTime = AM_LITTLE_ENDIAN_FLOAT_TO_HOST(_deadTime);

    memcpy(&_measurementDoseRateTime, input_data->measurementDoseRateTime, sizeof(input_data->measurementDoseRateTime));
    _measurementDoseRateTime = (int16_t)CFSwapInt16LittleToHost(_measurementDoseRateTime);        
}

+ (CBUUID*) identifier{
    return [CBUUID UUIDWithString:@"57F7031F-03C1-4016-8749-BAABAA58612D"];
}


- (NSString*) description{
    return [NSString stringWithFormat: @"\n\
            sensetivity             = %f \n\
            ownBackgound            = %f \n\
            deadTime                = %f \n\
            measurementDoseRateTime = %hu\n",
            self.sensetivity,
            self.ownBackground,            
            self.deadTime,
            self.measurementDoseRateTime ];
}

@end

//
// Initial measurements
//

typedef struct {
    char impulsesCount[8];
    char impulsesCountSpeed[4];
    char impulsesCountLastNSeconds[4];
    char doseMeasuringTime[4];
} AMTagInitialData;

@interface AMDeviceInitialMeasurement()
@property (assign,nonatomic) uint64_t impulsesCount;
@property (assign,nonatomic) uint32_t impulsesCountSpeed;
@property (assign,nonatomic) uint32_t impulsesCountLastNSeconds;
@property (assign,nonatomic) uint32_t doseMeasuringTime;
@property (assign,nonatomic) uint32_t doseMeasuringStartTime;
@end

@implementation AMDeviceInitialMeasurement
+ (CBUUID*) identifier{
    return [CBUUID UUIDWithString:@"8E26EDC8-A1E9-4C06-9BD0-97B97E7B3FB9"];
}
@end

@interface AMTagDeviceInitialMeasurement()
@end

@implementation AMTagDeviceInitialMeasurement

- (id) initWithData:(NSData *)data{
    self = [super init];
    if (self) {
        [self readData:data];
    }
    return self;
}

- (void) updateMeasurement:(AMTagDeviceMeasurement *)measurement{
    self.impulsesCount+=measurement.particlesLastSeconds;
    self.doseMeasuringTime+=2.0;
}

- (void) readData: (NSData*)data{
    NSTimeInterval currentTime = [NSDate timeIntervalSinceReferenceDate];

    AMTagInitialData *input_data = (AMTagInitialData *)[data bytes];
    
    uint64_t impulsesCount;
    memcpy(&impulsesCount, input_data->impulsesCount, sizeof(input_data->impulsesCount));
    self.impulsesCount = CFSwapInt64LittleToHost(impulsesCount);
    
    uint32_t impulsesCountSpeed;
    memcpy(&impulsesCountSpeed, input_data->impulsesCountSpeed, sizeof(input_data->impulsesCountSpeed));
    self.impulsesCountSpeed = CFSwapInt32LittleToHost(self.impulsesCountSpeed);
    
    uint32_t impulsesCountLastNSeconds;

    memcpy(&impulsesCountLastNSeconds, input_data->impulsesCountLastNSeconds, sizeof(input_data->impulsesCountLastNSeconds));
    self.impulsesCountLastNSeconds = CFSwapInt32LittleToHost(impulsesCountLastNSeconds);

    uint32_t doseMeasuringTime;
    uint32_t doseMeasuringStartTime;

    memcpy(&doseMeasuringTime, input_data->doseMeasuringTime, sizeof(input_data->doseMeasuringTime));
    
    self.doseMeasuringTime = CFSwapInt32LittleToHost(doseMeasuringTime);
        
    self.doseMeasuringStartTime = currentTime-self.doseMeasuringTime;
}

- (NSString*) description{
    return [NSString stringWithFormat: @"\n\
            impulsesCount              = %llu \n\
            impulsesCountSpeed         = %i \n\
            impulsesCountLastNSeconds  = %i \n\
            doseMeasuringTime          = %i \n",
            self.impulsesCount,
            self.impulsesCountSpeed,
            self.impulsesCountLastNSeconds,
            self.doseMeasuringTime ];
}

@end


//
//
//
typedef struct {
    char fastImpulsesCount[2];
    char mediumImpulsesCount[2];
    char slowImpulsesCount[2];
    char firmWareDay;
    char firmWareMonth;
    char firmWareYear;
    char firmWareHour;
} AMSwiftInitialData;


@interface AMSwiftDeviceInitialMeasurement()
@property (assign,nonatomic) uint64_t impulsesCount;
@end

@implementation AMSwiftDeviceInitialMeasurement

- (id) initWithData:(NSData *)data{
    self = [super init];
    if (self) {
        [self readData:data];
    }
    return self;
}

- (void) updateMeasurement:(AMTagDeviceMeasurement *)measurement{
    self.impulsesCount+=measurement.particlesLastSeconds;
    self.doseMeasuringTime+=2.0;
}

- (void) readData: (NSData*)data{
    NSTimeInterval currentTime = [NSDate timeIntervalSinceReferenceDate];
    
    self.impulsesCount = 0;
    
    AMSwiftInitialData *input_data = (AMSwiftInitialData *)[data bytes];
    
    memcpy(&_fastImpulsesCount, input_data->fastImpulsesCount, sizeof(input_data->fastImpulsesCount));
    _fastImpulsesCount = CFSwapInt16LittleToHost(_fastImpulsesCount);

    memcpy(&_mediumImpulsesCount, input_data->mediumImpulsesCount, sizeof(input_data->mediumImpulsesCount));
    _mediumImpulsesCount = CFSwapInt16LittleToHost(_mediumImpulsesCount);

    memcpy(&_slowImpulsesCount, input_data->slowImpulsesCount, sizeof(input_data->slowImpulsesCount));
    _slowImpulsesCount = CFSwapInt16LittleToHost(_slowImpulsesCount);

    _firmWareDay   = input_data->firmWareDay;
    _firmWareMonth = input_data->firmWareMonth;
    _firmWareYear  = 2000 + (uint32_t)input_data->firmWareYear;
    _firmWareHour  = input_data->firmWareHour;
    
    self.doseMeasuringTime = self.doseMeasuringStartTime = currentTime-self.doseMeasuringTime;
}

- (NSString*) description{
    return [NSString stringWithFormat: @"\n\
            fastImpulsesCount          = %i \n\
            mediumImpulsesCount        = %i \n\
            slowImpulsesCount          = %i \n\
            data                       = %i-%i-%i %i \n\
            doseMeasuringStartTime     = %i \n\
            doseMeasuringTime          = %i \n",
            self.fastImpulsesCount,
            self.mediumImpulsesCount,
            self.slowImpulsesCount,
            self.slowImpulsesCount,
            self.firmWareYear, self.firmWareMonth, self.firmWareDay, self.firmWareHour,
            self.doseMeasuringStartTime,
            self.doseMeasuringTime
            ];
}

@end


//
// Measuremnet raw data structure
//
typedef struct {
    char flags;
    char cumulativeDose[4];
    char doseRate[4];
    char particlesLast2Seconds[2];
    char batteryVolume;
    char temperature;
}AMTagMainMeasurementData;


@interface AMTagDeviceMeasurement()
/**
 *  State flags.
 *  0000 0000
 *       ---- - reserved
 *     ^ ---- (3) ressetable flag. 1 means that dose rate threshold is reached, value keeps as long as confirmation before accepting by central comes.
 *    ^ ----- (2) ressetable flag. 1 means that comulative dose is reached, value keeps as long as confirmation before accepting by central comes.
 *                acceptiong confirmation send by central uses AMTagSettings characteristics.
 *   ^ ------ (1) threshold dose rate alert flag. 1 means threshold is reached.
 *  ^ ------- (0) threshold comulative dose alert flag. 1 means threshold is reached.
 */
@property (readonly,nonatomic) uint8_t  flags;
@property (strong,nonatomic) AMTagDeviceCalibrationConstants *calibrations;
@property (strong,nonatomic) AMDeviceInitialMeasurement      *initialMeasurements;

@property (assign,nonatomic) NSTimeInterval updateTime;

@end

@implementation AMTagDeviceMeasurement
{
    NSUInteger v_particlesTotal;
}

+ (id) notificationWithData:(NSData *)data withCalibration:(AMTagDeviceCalibrationConstants*)calibrations withInitial:(AMDeviceInitialMeasurement*)inital{
    return [[AMTagDeviceMeasurement alloc] initWithData:data withCalibration:calibrations withInitial:inital]; 
}

+ (CBUUID*) identifier{
    return [CBUUID UUIDWithString:@"70BC767E-7A1A-4304-81ED-14B9AF54F7BD"];
}


- (NSTimeInterval) particlesLastDuration{
    return 2.0f;
}

- (NSTimeInterval) measuringTime{
    return (NSTimeInterval)(self.updateTime-self.initialMeasurements.doseMeasuringStartTime);
}

- (void) readData:(NSData*)data{
    
    self.updateTime = [NSDate timeIntervalSinceReferenceDate];
    
    AMTagMainMeasurementData *input_data = (AMTagMainMeasurementData *)[data bytes];
    
    _flags = input_data->flags;
        
    memcpy(&_cumulativeDose, input_data->cumulativeDose, sizeof(input_data->cumulativeDose));
    _cumulativeDose = AM_LITTLE_ENDIAN_FLOAT_TO_HOST(_cumulativeDose); // mSv

    _cumulativeDose *= 1000000.0f; // nSv
    
    memcpy(&_doseRate, input_data->doseRate, sizeof(input_data->doseRate));
    _doseRate = AM_LITTLE_ENDIAN_FLOAT_TO_HOST(_doseRate); // µSv

    _doseRate *= 1000.0f; // nSv
    
    memcpy(&_particlesLastSeconds, input_data->particlesLast2Seconds, sizeof(input_data->particlesLast2Seconds));
    _particlesLastSeconds = (int16_t)CFSwapInt16LittleToHost(_particlesLastSeconds);
    
    v_particlesTotal+=_particlesLastSeconds;
    
    _batteryVolume = (float)((int)input_data->batteryVolume)/100.0f;
    
    _temperature = (float)((int)input_data->temperature);
    
    _isPreferencesChanged = (_flags && (0x01<<3));
}

- (void) updateInitialMeasurement:(AMDeviceInitialMeasurement*)initial{
    self.initialMeasurements = initial;
}

- (uint64_t) particlesTotal {
    return self.initialMeasurements.impulsesCount+v_particlesTotal;
}

- (id) initWithData:(NSData *)data withCalibration:(AMTagDeviceCalibrationConstants*)calibrations withInitial:(AMDeviceInitialMeasurement*)inital{
    self = [super init];
    
    if (self) {
        _calibrations = calibrations;
        _initialMeasurements = inital;
        _batteryVolume = 1.0;
        v_particlesTotal = 0;
        [self readData:data];
    }
    
    return self;
}

- (BOOL) doseRateAlert{
    return _flags&(0x01) && YES;
}

- (BOOL) doseRateDidAlert{
    return _flags&(0x01<<1) && YES;
}

- (BOOL) detectorOverload{
    return _flags&(0x01<<5) && YES;
}

- (BOOL) chargerPlugged{
    return _flags&(0x01<<6) && YES;
}

- (BOOL) safetyShutdown{
    return _flags&(0x01<<7) && YES;
}

- (NSString*) description{
    
    return [NSString stringWithFormat: @"\n\
            endian          = %@ \n\
            flags           = %x \n\
            doseRateAlert   = %i \n\
            doseRateDidAlert= %i \n\
            detectorOverload= %i \n\
            chargerPlugged  = %i \n\
            safetyShutdown  = %i \n\
            cumulativeDose  = %f \n\
            doseRate        = %f \n\
            particles(2sec) = %i \n\
            particles(total)= %llu / %llu / %f \n\
            battery         = %f \n\
            temperature     = %f\n\
            preferences is changed  = %@",
            CFByteOrderGetCurrent()==CFByteOrderLittleEndian?@"little endian":@"big endian",
            self.flags,
            self.doseRateAlert,
            self.doseRateDidAlert,
            self.detectorOverload,
            self.chargerPlugged,
            self.safetyShutdown,
            self.cumulativeDose,
            self.doseRate,
            self.particlesLastSeconds,
            self.particlesTotal,
            self.initialMeasurements.impulsesCount,
            self.calibrations.deadTime*self.initialMeasurements.impulsesCountSpeed,
            self.batteryVolume,
            self.temperature,
            @(self.isPreferencesChanged)
            ];
}

@end
