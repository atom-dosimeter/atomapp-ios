//
//  AMAppDelegate.h
//  Atom
//
//  Created by denis svinarchuk on 11.07.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
