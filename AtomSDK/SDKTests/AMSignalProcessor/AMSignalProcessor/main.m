//
//  main.m
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 24/06/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMSimpleGenerator.h"
#import "AMSimpleProcessor.h"


@interface AMDsp : NSObject <AMSimpleProcessorProtocol>
@end

@implementation AMDsp

- (void) amSimpleProcessor:(AMSimpleProcessor *)processor hasLocalPeaks:(NSInteger)count{
    NSLog(@"AMSimpleProcessor: hasLocalPeaks count=%li", (long)count);
}

- (void) amSimpleProcessor:(AMSimpleProcessor *)processor hasLowPower:(BOOL)lowPower{
    NSLog(@"AMSimpleProcessor: hasLowPower %i", lowPower);
}

@end

int main(int argc, const char * argv[])
{
    
    @autoreleasepool {

        AMSimpleGenerator *generator = [AMSimpleGenerator sharedInstance];

        [generator start];

        AMDsp *dsp = [[AMDsp alloc] init];
        
        AMSimpleProcessor *processor = [AMSimpleProcessor sharedInstance];
        processor.delegate = dsp;
        [processor start];
        
        NSLog(@"Am manager samples rate: %f", generator.audioManager.samplingRate);
        
        sleep(1000);
        
        [processor stop];
        [generator stop];
    }
    
    return 0;
}

