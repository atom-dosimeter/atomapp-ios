//
//  AMLog10View.h
//  Atom
//
//  Created by denis svinarchuk on 21/07/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMLog10View : UIView

@property (nonatomic,readonly) float maxHigh;
@property (nonatomic,readonly) float minLow;

@property (nonatomic,assign) float low;
@property (nonatomic,assign) float high;
@property (nonatomic,assign) float value;
@property (nonatomic,readonly) float log10Value;

- (float) log10f:(float)value;
- (float) valueFromLog10:(float)value10;

@end
