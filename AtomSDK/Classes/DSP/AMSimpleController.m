//
//  AMController.m
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 02/07/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMSimpleController.h"
#import "AMConstants.h"
#import "AMSimpleProcessor.h"
#import "AMSimpleGenerator.h"
#import "AMDosimeter.h"

#import <AVFoundation/AVFoundation.h>
#if TARGET_OS_IPHONE
#import <MediaPlayer/MediaPlayer.h>
#endif


typedef enum {
    kAM_AudioDevicePropertyJackIsConnected = 'hdpn',
    kAM_AudioDevicePropertySpeakerIsConnected = 'ispk'
    
} AMDataSourceExtention;

@interface AMSimpleControllerSettings()
- (id) initWithSimpleController:(AMSimpleController*)simpleController;
@property (weak,nonatomic) AMSimpleController *simpleController;
@end

@implementation AMSimpleControllerSettings

@synthesize name=_name, thresholdDose=_thresholdDose, thresholdDoseRate=_thresholdDoseRate;

- (id) initWithSimpleController:(AMSimpleController *)simpleController{
    self = [super init];
    if (self) {
        self.simpleController = simpleController;                
    }
    return self;
}

- (NSString*)keyForProperty:(NSString*) propertyName{
    return [NSString stringWithFormat:@"%@-%@", self.UUIDString, propertyName];
}

- (void) setName:(NSString *)name{
    _name = name;
    [self setValue:name forName:@"name"];
}

- (NSString*) name{
    if (!_name) {
        _name = [[NSUserDefaults standardUserDefaults] stringForKey:[self keyForProperty:@"name"]];
        if (!_name) {
            [self setName:@"Atom Simple"];
        }
    }
    return _name;
}

- (NSString*) UUIDString{
    return @"AMSIMPLE-0000-0000-0000-000000000000";
}

- (NSString*) description{
    return [NSString stringWithFormat:@"%@(%@): thresholdDose = %@, thresholdDoseRate = %@", 
            self.name, 
            self.UUIDString,
            self.thresholdDose,
            self.thresholdDoseRate];
}

- (NSNumber*) thresholdDose{
    if (!_thresholdDose) {
        _thresholdDose=[[NSUserDefaults standardUserDefaults] objectForKey:[self keyForProperty:@"thresholdDose"]];
        if (!_thresholdDose) {
            [self setThresholdDose: [NSNumber numberWithFloat:1000000000.0f]]; // 1Sv
        }
    }
    return _thresholdDose;
}

- (void) setThresholdDose:(NSNumber *)thresholdDose{
    
    if (thresholdDose.floatValue<kAMThresholdDoseMinimum) {
        _thresholdDoseRate = [NSNumber numberWithFloat:kAMThresholdDoseMinimum];
        if (self.simpleController.delegate && [self.simpleController.delegate respondsToSelector:@selector(amController:didWarn:)]){
            [self.simpleController.delegate amController:self.simpleController
                                                 didWarn: [NSString stringWithFormat:NSLocalizedString(@"You tried to set very low dose threshold. It can't be less then %.3f(mSv)", @""),kAMThresholdDoseMinimum/1000000.0f]];
        }
    }
    else if (thresholdDose.floatValue>kAMThresholdDoseMaximum){
        _thresholdDose = [NSNumber numberWithFloat:kAMThresholdDoseMaximum];
        if (self.simpleController.delegate && [self.simpleController.delegate respondsToSelector:@selector(amController:didWarn:)]){
            [self.simpleController.delegate amController:self.simpleController
                                                 didWarn: [NSString stringWithFormat:NSLocalizedString(@"You tried to set very high dose threshold. It can't be greate then %.2f(Sv)", @""),kAMThresholdDoseMaximum/1000000000.0f]];
        }
    }
    else
        _thresholdDose=thresholdDose;
    
    [self setValue:thresholdDose forName:@"thresholdDose"];
}

- (NSNumber*) thresholdDoseRate{
    if (!_thresholdDoseRate) {
        _thresholdDoseRate=[[NSUserDefaults standardUserDefaults] objectForKey:[self keyForProperty:@"thresholdDoseRate"]];
        if (!_thresholdDoseRate) {
            [self setThresholdDoseRate: [NSNumber numberWithFloat:600.]]; // 60µR
        }
    }
    return _thresholdDoseRate;
}

- (void) setThresholdDoseRate:(NSNumber *)thresholdDoseRate{
    
    if (thresholdDoseRate.floatValue<kAMThresholdDoseRateMinimum) {
        _thresholdDoseRate = [NSNumber numberWithFloat:kAMThresholdDoseRateMinimum];
        if (self.simpleController.delegate && [self.simpleController.delegate respondsToSelector:@selector(amController:didWarn:)]){
            [self.simpleController.delegate amController:self.simpleController
                                                didWarn: [NSString stringWithFormat:NSLocalizedString(@"You tried to set very low dose rate threshold. It can't be less then %.0f(nSv)", @""),kAMThresholdDoseRateMinimum]];
        }
    }
    else if (thresholdDoseRate.floatValue>kAMThresholdDoseRateMaximum){
        _thresholdDoseRate = [NSNumber numberWithFloat:kAMThresholdDoseRateMaximum];
        if (self.simpleController.delegate && [self.simpleController.delegate respondsToSelector:@selector(amController:didWarn:)]){
            [self.simpleController.delegate amController:self.simpleController
                                                 didWarn: [NSString stringWithFormat:NSLocalizedString(@"You tried to set very high dose rate threshold. It can't be greate then %.2f(mSv)", @""),kAMThresholdDoseRateMaximum/1000000.0f]];
        }
    }
    else
        _thresholdDoseRate=thresholdDoseRate;
    
    [self setValue:thresholdDoseRate forName:@"thresholdDoseRate"];
}

- (void) setValue:(id)value forName:(NSString*)name{
    [[NSUserDefaults standardUserDefaults] setValue:value forKey:[self keyForProperty:name]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if (self.simpleController && self.simpleController.delegate && [self.simpleController.delegate respondsToSelector:@selector(amController:didUpdateSettings:)]) {
        [self.simpleController.delegate amController:self.simpleController didUpdateSettings:self];
    }
}

@end

@interface AMSimpleController() <AMSimpleProcessorProtocol>

/**
 *  The signal processor instance. Processor object creates automaticaly. Partially properties of the signal processor can be changed.
 */
@property (nonatomic,readonly) AMSimpleProcessor *processor;

/**
 * The tone genetrator onstance needs to supply Atom Simple.
 *
 */
@property (nonatomic,readonly) AMSimpleGenerator   *generator;

@property (nonatomic,assign) AMControllerPowerState powerStatePrivate;

@end

@implementation AMSimpleController
{
    NSTimeInterval startTime;
    
    BOOL hasDosimeterConnection;
    BOOL isReady;
    BOOL isRunning;
    BOOL lowPowerDidNotDetect;
}

@synthesize settings=_settings;

- (AMControllerSettings*)settings{
    if (!_settings) {
        _settings = [[AMSimpleControllerSettings alloc] initWithSimpleController:self];;        
    }
    return _settings;
}

- (BOOL) isReady{
    return isReady;
}

- (void) __init__{
    
    _processor = [[AMSimpleProcessor alloc] init];
    _processor.delegate = self;
    
    _generator = [[AMSimpleGenerator alloc] init];
    _generator.frequency = kAMSineToneGenratorFreq;
    
    hasDosimeterConnection = NO;
    
    //
    // volume ~= 1.0
    //
    lowPowerDidNotDetect = YES;    
    
#if TARGET_OS_IPHONE
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioSessionInterruptionCalback:)
                                                 name:AVAudioSessionInterruptionNotification
                                               object:nil];
#endif
    
}

- (void) audioSessionInterruptionCalback:(NSNotification*)event{
    [self stop];
}

- (void) setErrorHandler:(id<AMErrorHandlerProtocol>)errorHandler{
    _processor.errorHandler = self.errorHandler;
    _generator.errorHandler = self.errorHandler;
}

- (void) __init_hardware_dectectors__{
    [self detectJackInput];
    [self detectVolumeLevelChanging];
}

- (id) initWithDelegate:(id<AMControllerProtocol>)delegate{
    self = [super initWithDelegate:delegate];
    
    if (self) {
        self.delegate = delegate;
        [self __init__];        
    }
    
    return self;
}

- (id) init{
    self = [super init];
    
    if (self) {
        [self __init__];
    }
    
    return self;
}

- (void) start{
    NSAssert(self.delegate, @"AMController: AMController:delegate does not define.");
    if (!isRunning) {
        
        isRunning = YES;        
        
        [super start];
        
        if ([self.delegate respondsToSelector:@selector(amController:willReadyAfterTime:)]) {
            [self.delegate amController:self willReadyAfterTime:_processor.powerStartUpTime];
        }
        
        [self __init_hardware_dectectors__];        
    }
}

- (void) dsp_start{
    [_generator start];        
    startTime = [NSDate timeIntervalSinceReferenceDate];
    [_processor start];        
}

- (BOOL) connectionState{
    return hasDosimeterConnection;
}

- (AMControllerPowerState) powerState{
    return _powerStatePrivate;
}

- (void) dsp_stop{
    [_generator stop];        
    [_processor stop];
}

- (void) stop{
    [super stop];
    if (isRunning) {
        isRunning = NO;
        [self dsp_stop];
    }
}

- (void) resetDosimeter{
    [self.dosimeter reset];
}

- (BOOL) isStartedUp{
    if (([NSDate timeIntervalSinceReferenceDate]-startTime)>_processor.powerStartUpTime) {
        return YES;
    }
    return NO;
}

- (void) amSimpleProcessor:(AMSimpleProcessor *)processor hasLocalPeaks:(NSInteger)count{
    
    if (! hasDosimeterConnection || ![self isStartedUp] || !isRunning) return;
    
    if (!isReady) {
        //
        // Use the first peak as analogue ready detector
        //
        isReady = YES;
        
        if (lowPowerDidNotDetect) {
            //
            // first detection means volume is good
            //
            _powerStatePrivate = AM_CONTROLLER_POWER_ENOUGH;
            [self.delegate amController:self didChangePowerState:_powerStatePrivate];
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(amController:didReady:)]) {
            [self.delegate amController:self didReady:isReady];
        }
        
        if (isReady) {
            [self.dosimeter start];
        }
    }
    
    [self.dosimeter addParticles:count];
    [self.delegate amController:self didDetectParticles:count];
}

- (void) amSimpleProcessor:(AMSimpleProcessor *)dsp hasLowPower:(BOOL)lowPower{
    
    if (!isRunning) return;
    
    lowPowerDidNotDetect = NO;
    _powerStatePrivate = lowPower?AM_CONTROLLER_POWER_NOT_ENOUGH:AM_CONTROLLER_POWER_ENOUGH;
    [self.delegate amController:self didChangePowerState:_powerStatePrivate];
}


#pragma mark - Hardware event handlers

- (void) volumeHasChangedTo:(Float32)volume{
    if (! hasDosimeterConnection ) return;
    [self.delegate amController:self didChangePowerVolume:volume];
}

- (void) connectionHasChangedTo:(BOOL)available{
    if (!available) {             
        if (isReady) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(amController:didReady:)]) {
                [self.delegate amController:self didReady:NO];
            }            
        }
        isReady = NO;
        [self.dosimeter stop];
    }
    else {
        [self volumeHasChangedTo:self.processor.volume];
    }
    
    if (available) {
        [self dsp_start];
    }
    else{
        [self dsp_stop];
    }
    
    [self.delegate amController:self didUpdateConnectionState:available];
}


#pragma mark - OS specific utilites

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
#if TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR
    if ([keyPath isEqualToString:@"outputVolume"]) {
        NSNumber *volumeN=change[@"new"];
        [self volumeHasChangedTo:[volumeN floatValue]];
    }
#endif
}


#if TARGET_OS_IPHONE
- (void)audioRouteChangeListenerCallback:(NSNotification*)notification
{
    AVAudioSession *avsession = [AVAudioSession sharedInstance];
    AVAudioSessionRouteDescription *currentRoute = [avsession currentRoute];
    
    BOOL prevHasConnection = hasDosimeterConnection;
    
    if (currentRoute && currentRoute.outputs) {
        for (AVAudioSessionPortDescription *port in currentRoute.outputs) {
            if ([port.portType isEqualToString: AVAudioSessionPortHeadphones]) {
                
                //if (hasDosimeterConnection) return;                    
                
                hasDosimeterConnection = YES;
                break;
            }
            else{
                if (!hasDosimeterConnection) return;
                
                hasDosimeterConnection = NO;
                break;
            }
        }
    }
    
    if (hasDosimeterConnection!=prevHasConnection) {        
        [self connectionHasChangedTo:hasDosimeterConnection];
    }
}
#endif

- (void) detectJackInput{
    
#if TARGET_IPHONE_SIMULATOR
    
    hasDosimeterConnection = YES;
    [self connectionHasChangedTo:hasDosimeterConnection];
    
    
#elif TARGET_OS_IPHONE
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioRouteChangeListenerCallback:)
                                                 name:AVAudioSessionRouteChangeNotification
                                               object:nil];
    
    [self audioRouteChangeListenerCallback:nil];
    
#elif TARGET_OS_MAC
    
    // Read its current data source
    if (self.processor.audioManager.defaultInputDeviceID) {
        
        AudioObjectPropertyAddress sourceAddr = {
            kAudioDevicePropertyDataSource,
            kAudioDevicePropertyScopeOutput,
            kAudioObjectPropertyElementMaster
        };
        
        void (^dataSourceBlock)(void) = ^(void){
            UInt32 dataSourceId = 0;
            UInt32 dataSourceIdSize = sizeof(UInt32);
            
            [self.processor.audioManager checkError:AudioObjectGetPropertyData(self.processor.audioManager.defaultInputDeviceID, &sourceAddr, 0, NULL, &dataSourceIdSize, &dataSourceId)
                                          operation:NSLocalizedString(@"Could not get system input source", @"")];
            
            
            if (dataSourceId == kAM_AudioDevicePropertyJackIsConnected){
                hasDosimeterConnection = YES;
            }
            else{
                hasDosimeterConnection = NO;
            }
            [self connectionHasChangedTo:hasDosimeterConnection];
        };
        
        dataSourceBlock();
        
        [self.processor.audioManager checkError: AudioObjectAddPropertyListenerBlock(self.processor.audioManager.defaultInputDeviceID, &sourceAddr, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                                                                                     ^(UInt32 inNumberAddresses, const AudioObjectPropertyAddress *inAddresses) {
                                                                                         dataSourceBlock();
                                                                                     })
                                      operation:NSLocalizedString(@"Could not proccess system input source listener", @"")
         ];
    }
    
#endif
    
}

- (float) powerVolume{
    return self.processor.volume;
}

- (void) detectVolumeLevelChanging{
    
    
#if TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR
    
    AVAudioSession *avsession= [AVAudioSession sharedInstance];
    
    [avsession addObserver:self forKeyPath:@"outputVolume" options:NSKeyValueObservingOptionNew context:nil];
    
    [self volumeHasChangedTo:self.processor.volume];
    
#elif TARGET_OS_MAC
    
    if (self.processor.audioManager.defaultInputDeviceID) {
        
        UInt32 channel = 1; // Channel 0  is master, if available
        AudioObjectPropertyAddress sourceAddr = {
            kAudioDevicePropertyVolumeScalar,
            kAudioDevicePropertyScopeOutput,
            channel
        };
        
        void (^dataSourceBlock)(void) = ^(void){
            
            Float32 volume;
            UInt32 dataSize = sizeof(volume);
            
            [self.processor.audioManager checkError:AudioObjectGetPropertyData(self.processor.audioManager.defaultInputDeviceID, &sourceAddr, 0, NULL, &dataSize, &volume)
                                          operation:NSLocalizedString(@"Could not get system volume", @"")];
                        
            [self volumeHasChangedTo:volume];
        };
        
        dataSourceBlock();
        
        [self.processor.audioManager checkError: AudioObjectAddPropertyListenerBlock(self.processor.audioManager.defaultInputDeviceID, &sourceAddr, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                                                                                     ^(UInt32 inNumberAddresses, const AudioObjectPropertyAddress *inAddresses) {
                                                                                         dataSourceBlock();
                                                                                     })
                                      operation:NSLocalizedString(@"Could not proccess system volume changing listener", @"")
         ];
        
    }
    
#endif
}


#pragma mark - CBCentral



@end
