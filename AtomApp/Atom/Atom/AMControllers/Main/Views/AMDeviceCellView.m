//
//  AMDeviceCellView.m
//  Atom
//
//  Created by denis svinarchuk on 29.08.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMDeviceCellView.h"

@implementation AMDeviceCellView

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = self.backgroundColor = [UIColor redColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
    [self setSelectedBG:selected || self.isHighlighted];
}


- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    
    [super setHighlighted:highlighted animated:animated];    
    // Configure the view for the selected state
    [self setSelectedBG:highlighted || self.isSelected];
}

- (void) setSelectedBG:(BOOL)selected{
    
    if (selected) {
        self.contentView.backgroundColor = self.backgroundColor = [UIColor darkGrayColor];        
    }
    else{
        self.contentView.backgroundColor = self.backgroundColor = [UIColor clearColor];
    }
}

@end
