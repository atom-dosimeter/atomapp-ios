//
//  UIImageView+AMLogo.m
//  Atom
//
//  Created by denis svinarchuk on 18.07.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "UIImageView+AMLogo.h"

@implementation UIImageView (AMLogo)
- (void) placeToView:(UIView *)view withCenterPoint:(CGPoint)center withScale:(float)scale{
    CGRect frame = self.frame;
    frame.origin.y = center.y-frame.size.height/scale/2.;
    frame.origin.x = view.frame.size.width- self.frame.size.width/scale-10.0;        
    frame.size.width=frame.size.width/scale;
    frame.size.height=frame.size.height/scale;
    self.frame = frame;
    self.alpha = 0.7;        
}
@end
