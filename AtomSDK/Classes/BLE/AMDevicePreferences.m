//
//  AMDevicePreferences.m
//  Pods
//
//  Created by denis svinarchuk on 11.11.16.
//
//

#import "AMDevicePreferences.h"

#define AM_CHARACTERISTIC_SETTINGS_UUID    @"EA50CFCD-AC4A-4A48-BF0E-879E548AE157"


@implementation AMDevicePreferences
+ (CBUUID*) identifier{
    return [CBUUID UUIDWithString:AM_CHARACTERISTIC_SETTINGS_UUID];
}


- (instancetype) copyWithZone:(NSZone *)zone{
    AMDevicePreferences *p = [[[self class] allocWithZone:zone] init];
    p->_deviceUUIDString = [self.deviceUUIDString copy];
    p->_command = self.command;
    p->_parameter1 = 0;
    p->_parameter2 = 0;
    p->_parameter3 = 0;
    return p;
}

- (id) initWithData:(NSData *)data withDeviceUUIDString:(NSString *)uuidString{
    
    self = [super init];
    
    if (self) {
        _command = 0x0;
        _deviceUUIDString = uuidString;
        [self readData:data];
    }
    
    return self;
    
}

- (void) readData:(NSData *)data{}

- (NSData*) data{
    
    NSMutableData *data= [[NSMutableData alloc] init];
    
    [data appendBytes:&_command length:1];
    [data appendBytes:&_parameter1 length:2];
    [data appendBytes:&_parameter2 length:2];
    [data appendBytes:&_parameter3 length:2];
    [data appendBytes:&_flags length:1];
    
    return data;
}


- (NSString*) description{
    
    return [NSString stringWithFormat: @"\n\
            flags                  = %x\n\
            priority1 on           = %i\n\
            priority1 on           = %i\n\
            priority1 on           = %i\n\
            command                = %x\n\
            parameter1             = %x\n\
            parameter2             = %x\n\
            parameter3             = %x\n",
            self.flags,
            self.command,
            self.parameter1,
            self.parameter2,
            self.parameter3
            ];
}


@end
