//
//  AMThermometerScale.h
//  Atom
//
//  Created by denis svinarchuk on 21/07/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMLog10View.h"

@class AMThermometerMercury;
@class AMThermometerView;

@interface AMThermometerScale : AMLog10View
@property (nonatomic,weak) AMThermometerView *thermometer;
@property (nonatomic,weak) AMThermometerMercury *mercury;
@property (atomic, readonly) BOOL isThresholdChanging;
@end
