//
//  ViewController.m
//  AMTagTest
//
//  Created by denis svinarchuk on 25/08/14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "ViewController.h"
#import "AMTagDeviceManager.h"
#import "AMTagDeviceMeasurement.h"

@interface ViewController () <AMTagDeviceManagerProtocol, AMTagDeviceProtocol>

@property (strong,nonatomic) AMTagDeviceManager *tagDeviceManager;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tagDeviceManager = [AMTagDeviceManager sharedInstance];
    self.tagDeviceManager.delegate = self;
    [self.tagDeviceManager start];
    
	// Do any additional setup after loading the view, typically from a nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(foreground:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(background:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
}


- (void) foreground:(NSNotification*)event{    
    [self.tagDeviceManager start];
}

- (void) background:(NSNotification*)event{
    [self.tagDeviceManager stop];
}

- (void) viewWillDisappear:(BOOL)animated{
    [self viewWillDisappear:animated];
}

#pragma mark - AMTag Device Manager delegate

- (void) tagDeviceManagerWillActive:(AMTagDeviceManager *)manager{
    NSLog(@"ViewController: tagmanager will active: %@", manager);
}

- (void) tagDeviceManagerDidActive:(AMTagDeviceManager *)manager{
    NSLog(@"ViewController: tagmanager has activated: %@", manager);
}

- (void) tagDeviceManager:(AMTagDeviceManager *)manager didActiveFail:(CBCentralManagerState)error{
    NSLog(@"ViewController: tagmanager has fault activation: %@: %i", manager, error);
}

- (void) tagDeviceManager:(AMTagDeviceManager *)manager willConnectWithDevice:(AMTagDevice *)device{
    NSLog(@"ViewController: tagmanager: %@  will active with device %@", manager, device.UUIDString);
    device.delegate = self;
}

- (void) tagDeviceManagerDidUpdateDeviceList:(AMTagDeviceManager *)manager{
    NSLog(@"ViewController: tagmanager did update list: %@", manager);
    for (AMTagDevice *device in [manager.devices allValues]) {
        NSLog(@"  - device: %@:%@", device.name, device.UUIDString);
    }
}


- (void) tagDeviceManager:(AMTagDeviceManager *)manager withDevice:(AMTagDevice *)device didConnectionUpdateState:(BOOL)state error:(NSError *)error{
    NSLog(@"ViewController: tagmanager: %@  with device %@ and connection state: %i with error=%@", manager, device.UUIDString, state, error);
}

#pragma mark - AMTag Device delegate

- (void) tagDeviceDidSynchronize:(AMTagDevice *)device{
    NSLog(@"ViewController:  with device %@ / %@ - DID synchronize", device.UUIDString, device.name);
}

- (void) tagDeviceWillSynchronize:(AMTagDevice *)device{
    NSLog(@"ViewController:  with device %@ / %@ - WILL synchronize", device.UUIDString, device.name);
}

- (void) tagDevice:(AMTagDevice *)device didMeasurementUpdate:(AMTagDeviceMeasurement *)measurement{
    NSLog(@"ViewController:  with device %@ / %@ - has update notification: %@", device.UUIDString, device.name, measurement);
    static int i = 1;

    //device.name = [NSString stringWithFormat:@"Atom Tag #00%i",i++] ;
    //device.thresholdDefault.thresholdDose = 10.;
    //device.thresholdDefault.thresholdDoseRate = 0.5;
    //[device synchronize];

}

- (void) tagDevice:(AMTagDevice *)device didThresholdUpdate:(AMTagDeviceThreshold *)threshold{
    NSLog(@"ViewController:  with device %@ has update threshold: %@", device.UUIDString, threshold);
}

- (void) tagDevice:(AMTagDevice *)device didPreferencesUpdate:(AMTagDeviceMeasurement *)pref{
    NSLog(@"ViewController:  with device %@\n info: %@ has update preferences: %@", device.UUIDString, device.name, pref);    
}

- (void) tagDevice:(AMTagDevice *)device didNameUpdate:(AMTagDeviceName *)name{
    NSLog(@"ViewController:  with device %@ has update name: %@", device.UUIDString, name.name);
}

@end
