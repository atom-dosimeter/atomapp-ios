//
//  AMTagDeviceManager.h
//  Atom
//
//  Created by denis svinarchuk on 22.08.14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMTagDevice.h"
#import "AMList.h"

@class AMTagDeviceManager;
@class AMTagDevice;

@protocol AMTagDeviceManagerProtocol <NSObject>

- (void) tagDeviceManager:(AMTagDeviceManager *)manager willConnectWithDevice:(AMTagDevice*)device;

@optional

- (void) tagDeviceManagerWillActive:(AMTagDeviceManager*)manager;
- (void) tagDeviceManagerDidActive:(AMTagDeviceManager*)manager;
- (void) tagDeviceManagerDidUpdateDeviceList:(AMTagDeviceManager*)manager;
- (void) tagDeviceManager:(AMTagDeviceManager*)manager didActiveFail:(CBCentralManagerState)error;

@end


/**
 *  Scan, discover, connect... Manage Atom Tag devices.
 *
 */
@interface AMTagDeviceManager : NSObject

/**
 *  Atom Tag devices list.
 *
 */
@property (readonly, nonatomic)   AMList *devices;

/**
 *  Delegate manager protocol.
 */
@property (strong, nonatomic) id<AMTagDeviceManagerProtocol> delegate;

/**
 *  Create manager object.
 *
 *  @return object instance.
 *
 */
+ (id) sharedInstance;

/**
 *  Forget device that has been once connected.
 *
 *  @param UUIDString device UUID string
 *
 *  @return in case device was kept YES, else NO
 *
 */
- (BOOL) forgetDevice:(NSString*)UUIDString;

/**
 *  Start BLE Atom Tag devices discover.
 *
 */
- (void) start;

/**
 *  Stop.
 *
 */
- (void) stop;

@end
