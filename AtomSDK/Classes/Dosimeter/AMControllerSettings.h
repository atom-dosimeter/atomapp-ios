//
//  AMControllerSettings.h
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 29.08.14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>
#import <objc/message.h>

@interface AMBoolean : NSNumber
- (id) booleanWithBOOL:(BOOL)value;
@end


@interface AMControllerSettingsMeta : NSObject
@property (nonatomic,readonly) NSMutableArray *properties;
@end

@interface AMControllerSettings : AMControllerSettingsMeta

/**
 *  The name of the device
 */
@property (strong,nonatomic) NSString *name;

/**
 *  The device uniq identifier
 */
@property (nonatomic,readonly) NSString *UUIDString;

/**
 *  The common threshold dose of the connected device.
 */
@property (strong,nonatomic) NSNumber  *thresholdDose;

/**
 *  The common threshold dose rate of the connected device.
 */
@property (strong,nonatomic) NSNumber  *thresholdDoseRate;

/**
 *  Property iterate accessors.
 */
- (NSInteger) count;

/**
 *  Get the property at the index.
 *
 *  @param index index
 *
 *  @return property object
 *
 */
- (id) propertyAtIndex:(NSInteger)index;

/**
 *  Get the property name ath the index.
 *
 *  @param index index
 *
 *  @return name string.
 *
 */
- (NSString*) propertyNameAtIndex:(NSInteger)index;

/**
 *  Get the property localized title string at the index
 *
 *  @param index index
 *
 *  @return title string
 *
 */
- (NSString*) propertyLocalizedTitleAtIndex:(NSInteger)index;

/**
 *  Set property at the index.
 *
 *  @param value property value.
 *  @param index index
 *
 */
- (void) setValue:(id)value atPropertyIndex:(NSInteger)index;

/**
 *  To keep/restore the device setting object instance in persistant storage init with a uniq device identifier string.
 *
 *  @param uuidstring uniq device identifier string.
 *
 *  @return object instance
 */
- (id) initWithUUIDString:(NSString*)uuidstring;

/**
 *  Syncronize with the device storage if it allow to keep settings self, or keep it in NSUserDefault.
 */
- (void) synchronize;


@end
