//
//  AMSearchingInfoView.m
//  Atom
//
//  Created by denis svinarchuk on 16/10/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMBaseDosimeterInfoView.h"
#import "AMUIPreferences.h"
#import "AMSettings.h"
#import "AMDurationLabel.h"
#import "AMThresholdLimitView.h"
#import "NSAttributedString+AMUnits.h"

#define __AM_SEARCHING_PICKER_PROP @"historyInterval"

@interface AMBaseDosimeterInfoView() <UIPickerViewDataSource, UIPickerViewDelegate, AMDoseUnitFieldDelegate>

@property (nonatomic,strong) UILabel         *intervalTitleLabel;
@property (nonatomic,strong) UILabel         *intervalValueLabel;
@property (nonatomic,strong) UIButton        *intervalPickerButton;

@property (nonatomic,strong) UIButton        *resetButton;

@property (nonatomic,strong) UILabel         *doseThresholdTitleLabel;

@property (nonatomic,strong) UILabel         *doseTitleLabel;
@property (nonatomic,strong) UILabel         *doseValueLabel;

@property (nonatomic,strong) UILabel         *timeTitleLabel;
@property (nonatomic,strong) AMDurationLabel *timeValueLabel;

@property (nonatomic,strong) AMThresholdLimitView *thresholdDoseBlinknigView;
@property (nonatomic,strong) AMThresholdLimitView *thresholdDoseRateBlinknigView;

@property (nonatomic,readonly) AMDoseUnitField *doseThresholdView;
@property (nonatomic,readonly) AMPickerView    *intervalPickerView;

@property (nonatomic,assign)   BOOL hasIntervalPicker;
@property (nonatomic,assign)   BOOL hasResetButton;
@end

@implementation AMBaseDosimeterInfoView

@synthesize doseThresholdView=_doseThresholdView, intervalPickerView=_intervalPickerView, dose=_dose;;

- (void) __init__{
    
    self.doseThresholdView.delegate = self;
    self.doseThresholdView.isDoseRate = YES;

    self.doseThresholdTitleLabel.text = NSLocalizedString(@"Threshold of Ambient DER", @"Threshold label text");
    self.doseTitleLabel.text = NSLocalizedString(@"Cumulative Dose", @"Threshold of cummlative dose label text");
    
    if (self.hasResetButton) {
        self.timeTitleLabel.text = NSLocalizedString(@"Measuring time", @"Time of measuring label text");
    }
    else
        self.timeTitleLabel.text = NSLocalizedString(@"Searching time", @"Time of searching label text");
    
    
    if (self.hasIntervalPicker) {
        self.intervalTitleLabel.text = NSLocalizedString(@"Interval", @"Chart interval title label in the main screen");
        [self intervalPickerButton];
    }
    
    if (self.hasResetButton) {
        [self resetButton];
    }
    
    [self thresholdDoseBlinknigView];
    [self thresholdDoseRateBlinknigView];
}


- (id) init{
    self = [super init];
    if (self) {
        [self __init__];
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self __init__];
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame withIntervalPicker:(BOOL)intervalIsHidden withResetButton:(BOOL)hasResetButton{
    self = [super initWithFrame:frame];
    if (self) {
        _hasIntervalPicker=intervalIsHidden;
        _hasResetButton = hasResetButton;
        [self __init__];
    }
    return self;
}

#pragma mark - threshold blinking view

- (AMThresholdLimitView*) thresholdDoseBlinknigView{
    if (!_thresholdDoseBlinknigView) {
        _thresholdDoseBlinknigView = [[AMThresholdLimitView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, AM_UI_INFO_VALUE_HIEGHT)];
        _thresholdDoseBlinknigView.translatesAutoresizingMaskIntoConstraints = NO;
        [self insertSubview:_thresholdDoseBlinknigView atIndex:0];
        
        _thresholdDoseBlinknigView.backgroundColor = [UIColor clearColor];

        [_thresholdDoseBlinknigView.superview addConstraint:[NSLayoutConstraint constraintWithItem:_thresholdDoseBlinknigView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.doseValueLabel attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        [_thresholdDoseBlinknigView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":_thresholdDoseBlinknigView}]];
        [_thresholdDoseBlinknigView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]" options:0 metrics:@{@"height": [NSNumber numberWithInt:AM_UI_INFO_VALUE_HIEGHT]} views:@{@"view":_thresholdDoseBlinknigView}]];

    }
    return _thresholdDoseBlinknigView;
}

- (AMThresholdLimitView*) thresholdDoseRateBlinknigView{
    if (!_thresholdDoseRateBlinknigView) {
        _thresholdDoseRateBlinknigView = [[AMThresholdLimitView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, AM_UI_INFO_VALUE_HIEGHT)];
        _thresholdDoseRateBlinknigView.translatesAutoresizingMaskIntoConstraints = NO;
        
        _thresholdDoseRateBlinknigView.backgroundColor = [UIColor clearColor];
        
        [self insertSubview:_thresholdDoseRateBlinknigView atIndex:0];
        
        [_thresholdDoseRateBlinknigView.superview addConstraint:[NSLayoutConstraint constraintWithItem:_thresholdDoseRateBlinknigView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.doseThresholdView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        [_thresholdDoseRateBlinknigView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":_thresholdDoseRateBlinknigView}]];
        [_thresholdDoseRateBlinknigView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]" options:0 metrics:@{@"height": [NSNumber numberWithInt:AM_UI_INFO_VALUE_HIEGHT]} views:@{@"view":_thresholdDoseRateBlinknigView}]];
    }
    return _thresholdDoseRateBlinknigView;
}

#pragma mark - interval picker

- (AMPickerView*) intervalPickerView{
    if (!_intervalPickerView) {
        
        UIView *baseView = self.superview.superview;
        
        
        _intervalPickerView = [[AMPickerView alloc] initWithFrame:CGRectMake(0, 0, baseView.bounds.size.width, baseView.bounds.size.height)];
        
        AMSettings *settings = [AMSettings sharedInstance];

        _intervalPickerView.delegate = self;
        _intervalPickerView.dataSource = self;
        _intervalPickerView.hidden = YES;
        _intervalPickerView.backgroundColor = [UIColor colorWithWhite:0.24 alpha:0.5];
        _intervalPickerView.layer.shadowOffset = CGSizeMake(2.0f, .0f);
        _intervalPickerView.layer.shadowOpacity = .5;
        _intervalPickerView.layer.shadowRadius = 4.;
        _intervalPickerView.selections = [settings sourceForSettingName:__AM_SEARCHING_PICKER_PROP];

        _intervalPickerView.translatesAutoresizingMaskIntoConstraints = NO;

        //
        // root of controller?
        //
        [baseView addSubview:_intervalPickerView];
        
        [_intervalPickerView.superview addConstraint:[NSLayoutConstraint constraintWithItem:_intervalPickerView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.intervalValueLabel attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        //[_intervalPickerView.superview addConstraint:[NSLayoutConstraint constraintWithItem:_intervalPickerView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
        [_intervalPickerView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":_intervalPickerView}]];
        
    }
    
    return _intervalPickerView;
}

#pragma mark - picker

- (UIButton*) intervalPickerButton{
    if (!self.hasIntervalPicker) {
        return nil;
    }
    if (!_intervalPickerButton) {
        _intervalPickerButton = [UIButton buttonWithType:UIButtonTypeSystem];
        _intervalPickerButton.frame = CGRectMake(0, 0, self.bounds.size.width, 40);
        _intervalPickerButton.backgroundColor = [UIColor clearColor];
        _intervalPickerButton.titleLabel.textColor = [UIColor clearColor];
        _intervalPickerButton.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self addSubview:_intervalPickerButton];
        
        [_intervalPickerButton.superview addConstraint:[NSLayoutConstraint constraintWithItem:_intervalPickerButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.intervalValueLabel attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        [_intervalPickerButton.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":_intervalPickerButton}]];
        
        [_intervalPickerButton addTarget:self action:@selector(bringUpPickerViewWithRow:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _intervalPickerButton;
}

- (void) hideIntervalPicker{
    if (!self.intervalPickerView.isHidden) {
        [self hidePickerView];
    }
}

- (void)hidePickerView
{
    [UIView animateWithDuration:AM_UI_ANIMATE_DURATION
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.intervalPickerView.alpha = 0.0;
                     }
                     completion:^(BOOL finished){
                         self.intervalPickerView.hidden = YES;
                         self.intervalPickerButton.enabled = YES;
                     }
     ];
}

- (void) pickerView:(AMPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    pickerView.selected = row;
    
    AMSettings *settings = [AMSettings sharedInstance];
    
    [settings setValue:[NSNumber numberWithInt:[[[pickerView.selections objectAtIndex:0]objectAtIndex:row] intValue]] forSettingName:__AM_SEARCHING_PICKER_PROP];
    
    [self hidePickerView];
}

- (CGFloat) pickerView:(AMPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return  pickerView.heightRow;
}

- (NSInteger) numberOfComponentsInPickerView:(AMPickerView *)pickerView{
    return 1;
}

- (NSInteger) pickerView:(AMPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [[pickerView.selections objectAtIndex:0] count];
}

- (UIView*) pickerView:(AMPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 44)];
    
    label.backgroundColor = AM_UI_BG_COLOR;
    label.textColor = [UIColor whiteColor];
        
    label.font = [UIFont fontWithName:AM_UI_BOLD_FONT size:AM_UI_INFO_FONT_VALUE_SIZE];
    label.textAlignment = NSTextAlignmentCenter;
    
    label.text = [NSString stringWithFormat:@"%@", [[pickerView.selections objectAtIndex:1] objectAtIndex:row] ];
    
    return label;
}

- (void)bringUpPickerViewWithRow:(UIButton*)button{
    
    [self.intervalPickerView bringSubviewToFront:self.intervalPickerView.superview];
    
    self.intervalPickerButton.enabled = NO;
    
    self.intervalPickerView.alpha = 0.0;
    self.intervalPickerView.hidden = NO;
    [self.intervalPickerView reloadAllComponents];
    
    AMSettings *settings = [AMSettings sharedInstance];
    
    [self.intervalPickerView selectRow: [settings selectedIndexForSettingName:__AM_SEARCHING_PICKER_PROP] inComponent:0 animated:YES];
    
    [UIView animateWithDuration:AM_UI_ANIMATE_DURATION
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.intervalPickerView.alpha = 1.0;
                     }
                     completion:nil
     ];
}


#pragma mark - reset button

- (UIButton*) resetButton{
    if (!self.hasResetButton) {
        return nil;
    }
    if (!_resetButton) {
        _resetButton = [UIButton buttonWithType:UIButtonTypeSystem];
        _resetButton.frame = CGRectMake(0, 0, self.bounds.size.width, 40);
        _resetButton.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self addSubview:_resetButton];
        
        [_resetButton.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[time]-5-[view]-0-|" options:0 metrics:nil views:@{@"view":_resetButton, @"time": self.timeValueLabel}]];
        [_resetButton.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":_resetButton}]];
        
        [_resetButton addTarget:self action:@selector(reset:) forControlEvents:UIControlEventTouchUpInside];
        
        _resetButton.titleLabel.minimumScaleFactor = 0.5;
        _resetButton.titleLabel.adjustsFontSizeToFitWidth = YES;
        _resetButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        [_resetButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_resetButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        _resetButton.titleLabel.font = [UIFont fontWithName:AM_UI_BOLD_FONT size:27];
        _resetButton.backgroundColor = AM_UI_DISPLAY_BG_COLOR;
        
        [_resetButton setTitle:NSLocalizedString(@"Reset button", @"Reset button") forState:UIControlStateNormal];
        [_resetButton setTitle:NSLocalizedString(@"Reset button", @"Reset button") forState:UIControlStateSelected];
        [_resetButton setTitle:NSLocalizedString(@"Reset button", @"Reset button") forState:UIControlStateHighlighted];

    }
    return _resetButton;
}


- (void) reset:(NSNotification*)event{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTouchResetButton)]) {
        [self.delegate didTouchResetButton];
    }
}


#pragma mark - Labels and inputs

- (UILabel*) newTitleLabel{
    UILabel *label = [[UILabel alloc] init];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.adjustsFontSizeToFitWidth = YES;
    label.minimumScaleFactor = 0.5;
    label.font = [UIFont fontWithName:AM_UI_INFO_FONT_TITLE size:AM_UI_INFO_FONT_TITLE_SIZE];
    label.textColor = AM_UI_INFO_TEXT_COLOR;
    label.numberOfLines = 3;
    label.textAlignment = NSTextAlignmentLeft;//(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)?NSTextAlignmentLeft:NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    return label;
}

- (UILabel*) newValueLabel{
    UILabel *label = [[UILabel alloc] init];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.adjustsFontSizeToFitWidth = YES;
    label.minimumScaleFactor = 0.5;
    label.font = [UIFont fontWithName:AM_UI_INFO_FONT_VALUE size:AM_UI_INFO_FONT_VALUE_SIZE];
    label.textColor = AM_UI_INFO_FONT_VALUE_COLOR;
    label.numberOfLines = 3;
    label.textAlignment = NSTextAlignmentLeft;//(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)?NSTextAlignmentLeft:NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    return label;
}

- (UILabel*) intervalTitleLabel{
    
    if (!_intervalTitleLabel) {
        _intervalTitleLabel = [self newTitleLabel];
        [self addSubview:_intervalTitleLabel];
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {            
            [_intervalTitleLabel.superview addConstraint:[NSLayoutConstraint constraintWithItem:_intervalTitleLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.intervalValueLabel attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
            [_intervalTitleLabel.superview addConstraint:[NSLayoutConstraint
                                                               constraintWithItem:_intervalTitleLabel
                                                               attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                               toItem:self.intervalValueLabel
                                                               attribute:NSLayoutAttributeHeight multiplier:1 constant:0]];

            [_intervalTitleLabel.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view(100)]-4-[value]" options:0 metrics:nil views:@{@"view":_intervalTitleLabel, @"value": self.intervalValueLabel}]];
        }
        else{
            [_intervalTitleLabel.superview addConstraints:[NSLayoutConstraint 
                                                           constraintsWithVisualFormat:@"V:|-0-[view(height)]" options:0
                                                           metrics:@{@"height": [NSNumber numberWithInt:AM_UI_INFO_TITLE_HIEGHT]} 
                                                           views:@{@"view":_intervalTitleLabel}]];
            [_intervalTitleLabel.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":_intervalTitleLabel}]];
        }
        
    }
    return _intervalTitleLabel;
};

- (UILabel*) intervalValueLabel{
    
    if (!_intervalValueLabel) {
        _intervalValueLabel = [self newValueLabel];
        [self addSubview:_intervalValueLabel];
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
            [_intervalValueLabel.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[view(height)]" options:0 metrics:@{@"height": [NSNumber numberWithInt:AM_UI_INFO_VALUE_HIEGHT]} views:@{@"view":_intervalValueLabel}]];
            [_intervalValueLabel.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[view]-0-|" options:0 metrics:nil views:@{@"view":_intervalValueLabel}]];            
        }
        else {
            [_intervalValueLabel.superview addConstraints:[NSLayoutConstraint 
                                                           constraintsWithVisualFormat:@"V:[title]-0-[view(height)]" options:0 
                                                           metrics:@{@"height": [NSNumber numberWithInt:AM_UI_INFO_VALUE_HIEGHT]} 
                                                           views:@{@"view":_intervalValueLabel, @"title":self.intervalTitleLabel}]];
            [_intervalValueLabel.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":_intervalValueLabel}]];
        }
    }
    
    if (!self.hasIntervalPicker) {
        _intervalValueLabel.hidden = YES;
    }
    
    return _intervalValueLabel;
}

- (void) setIntervalString:(NSString *)intervalString{
    self.intervalValueLabel.text = intervalString;
}

- (NSString*) intervalString{
    return self.intervalValueLabel.text;
}

- (UILabel*) doseThresholdTitleLabel{
    if (!_doseThresholdTitleLabel) {
        _doseThresholdTitleLabel = [self newTitleLabel];
        [self addSubview:_doseThresholdTitleLabel];
                
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
            [_doseThresholdTitleLabel.superview addConstraint:[NSLayoutConstraint 
                                                               constraintWithItem:_doseThresholdTitleLabel 
                                                               attribute:NSLayoutAttributeCenterY 
                                                               relatedBy:NSLayoutRelationEqual 
                                                               toItem:self.doseThresholdView 
                                                               attribute:NSLayoutAttributeCenterY 
                                                               multiplier:1 constant:0]];
            
            [_doseThresholdTitleLabel.superview addConstraint:[NSLayoutConstraint
                                                               constraintWithItem:_doseThresholdTitleLabel
                                                               attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                               toItem:self.doseThresholdView
                                                               attribute:NSLayoutAttributeHeight multiplier:1 constant:0]];
            
            [_doseThresholdTitleLabel.superview addConstraints:[NSLayoutConstraint 
                                                                constraintsWithVisualFormat:@"H:|-0-[view(100)]-4-[value]" 
                                                                options:0 
                                                                metrics:nil 
                                                                views:@{@"view":_doseThresholdTitleLabel, @"value": self.doseThresholdView}]];            
        }
        else{
            if (self.hasIntervalPicker) {
                [_doseThresholdTitleLabel.superview addConstraints:[NSLayoutConstraint
                                                                    constraintsWithVisualFormat:@"V:[prev]-4-[view(height)]" options:0
                                                                    metrics:@{@"height": [NSNumber numberWithInt:AM_UI_INFO_TITLE_HIEGHT]}
                                                                    views:@{@"view":_doseThresholdTitleLabel, @"prev":self.intervalValueLabel}]];
            }
            else{
                [_doseThresholdTitleLabel.superview addConstraints:[NSLayoutConstraint
                                                                    constraintsWithVisualFormat:@"V:|-4-[view(height)]" options:0
                                                                    metrics:@{@"height": [NSNumber numberWithInt:AM_UI_INFO_TITLE_HIEGHT]}
                                                                    views:@{@"view":_doseThresholdTitleLabel}]];
            }
            [_doseThresholdTitleLabel.superview addConstraints:[NSLayoutConstraint
                                                                constraintsWithVisualFormat:@"H:|-0-[view]-0-|" 
                                                                options:0 metrics:nil 
                                                                views:@{@"view":_doseThresholdTitleLabel}]];
        }
    }
    return _doseThresholdTitleLabel;
};

- (AMDoseUnitField*) doseThresholdView{
    if (!_doseThresholdView) {
        _doseThresholdView = [[AMDoseUnitField alloc] initWithFrame:CGRectMake(0, 0, 100, 34)];
        _doseThresholdView.backgroundColor = [UIColor clearColor];
        _doseThresholdView.translatesAutoresizingMaskIntoConstraints = NO;
        _doseThresholdView.fontPointSize = AM_UI_INFO_FONT_VALUE_SIZE;
        _doseThresholdView.textLabel.textColor = AM_UI_INFO_FONT_VALUE_COLOR;
        _doseThresholdView.textLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_doseThresholdView];
        

        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
            if (self.hasIntervalPicker) {
                [_doseThresholdView.superview addConstraints:[NSLayoutConstraint
                                                              constraintsWithVisualFormat:@"V:[prev]-4-[view(height)]"
                                                              options:0
                                                              metrics:@{@"height": [NSNumber numberWithInt:AM_UI_INFO_VALUE_HIEGHT]}
                                                              views:@{@"view":_doseThresholdView, @"prev": self.intervalValueLabel}]];
            }
            else
                [_doseThresholdView.superview addConstraints:[NSLayoutConstraint
                                                              constraintsWithVisualFormat:@"V:|-0-[view(height)]"
                                                              options:0
                                                              metrics:@{@"height": [NSNumber numberWithInt:AM_UI_INFO_VALUE_HIEGHT]}
                                                              views:@{@"view":_doseThresholdView}]];
            
            [_doseThresholdView.superview addConstraints:[NSLayoutConstraint
                                                          constraintsWithVisualFormat:@"H:[view]-0-|" 
                                                          options:0 
                                                          metrics:nil 
                                                          views:@{@"view":_doseThresholdView}]];   
        }        
        else{
            [_doseThresholdView.superview addConstraints:[NSLayoutConstraint 
                                                                constraintsWithVisualFormat:@"V:[prev]-4-[view(height)]" options:0
                                                                metrics:@{@"height": [NSNumber numberWithInt:AM_UI_INFO_VALUE_HIEGHT]} 
                                                                views:@{@"view":_doseThresholdView, @"prev":self.doseThresholdTitleLabel}]];
            [_doseThresholdView.superview addConstraints:[NSLayoutConstraint 
                                                                constraintsWithVisualFormat:@"H:|-0-[view]-0-|" 
                                                                options:0 metrics:nil 
                                                                views:@{@"view":_doseThresholdView}]];
        }
    }
    return _doseThresholdView;
}

- (UILabel*) doseTitleLabel{
    if (!_doseTitleLabel) {
        _doseTitleLabel = [self newTitleLabel];
        [self addSubview:_doseTitleLabel];
        
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
            [_doseTitleLabel.superview addConstraint:[NSLayoutConstraint 
                                                      constraintWithItem:_doseTitleLabel 
                                                      attribute:NSLayoutAttributeCenterY 
                                                      relatedBy:NSLayoutRelationEqual 
                                                      toItem:self.doseValueLabel 
                                                      attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
            
            [_doseTitleLabel.superview addConstraint:[NSLayoutConstraint
                                                          constraintWithItem:_doseTitleLabel
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                          toItem:self.doseValueLabel
                                                          attribute:NSLayoutAttributeHeight multiplier:1 constant:0]];

            [_doseTitleLabel.superview addConstraints:[NSLayoutConstraint
                                                       constraintsWithVisualFormat:@"H:|-0-[view(100)]-4-[value]" 
                                                       options:0 
                                                       metrics:nil 
                                                       views:@{@"view":_doseTitleLabel, @"value": self.doseValueLabel}]];            
        }
        else{
            [_doseTitleLabel.superview addConstraints:[NSLayoutConstraint 
                                                                constraintsWithVisualFormat:@"V:[prev]-4-[view(height)]" options:0
                                                                metrics:@{@"height": [NSNumber numberWithInt:AM_UI_INFO_TITLE_HIEGHT]} 
                                                                views:@{@"view":_doseTitleLabel, @"prev":self.doseThresholdView}]];
            [_doseTitleLabel.superview addConstraints:[NSLayoutConstraint 
                                                                constraintsWithVisualFormat:@"H:|-0-[view]-0-|" 
                                                                options:0 metrics:nil 
                                                                views:@{@"view":_doseTitleLabel}]];
        }
        
    }
    return _doseTitleLabel;
};

- (UILabel*) doseValueLabel{
    if (!_doseValueLabel) {
        _doseValueLabel = [self newValueLabel];
        [self addSubview:_doseValueLabel];
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {            
            [_doseValueLabel.superview addConstraints:[NSLayoutConstraint 
                                                       constraintsWithVisualFormat:@"V:[threshold]-4-[view(height)]" 
                                                       options:0 
                                                       metrics:@{@"height": [NSNumber numberWithInt:AM_UI_INFO_VALUE_HIEGHT]} 
                                                       views:@{@"view":_doseValueLabel, @"threshold": 
                                                                   self.doseThresholdView}]];
            [_doseValueLabel.superview addConstraints:[NSLayoutConstraint 
                                                       constraintsWithVisualFormat:@"H:[view]-0-|" 
                                                       options:0 
                                                       metrics:nil 
                                                       views:@{@"view":_doseValueLabel}]];
        }
        else{
            [_doseValueLabel.superview addConstraints:[NSLayoutConstraint 
                                                          constraintsWithVisualFormat:@"V:[prev]-4-[view(height)]" options:0
                                                          metrics:@{@"height": [NSNumber numberWithInt:AM_UI_INFO_VALUE_HIEGHT]} 
                                                          views:@{@"view":_doseValueLabel, @"prev":self.doseTitleLabel}]];
            [_doseValueLabel.superview addConstraints:[NSLayoutConstraint 
                                                          constraintsWithVisualFormat:@"H:|-0-[view]-0-|" 
                                                          options:0 metrics:nil 
                                                          views:@{@"view":_doseValueLabel}]];
        }        
    }
    return _doseValueLabel;
}

- (void) setDoseRate:(float)doseRate{
    self.thresholdDoseRateBlinknigView.current = doseRate;
}

- (float) doseRate{
    return self.thresholdDoseRateBlinknigView.current;
}

- (void) setThresholdDoseRate:(float)thresholdDose{
    
    if (thresholdDose<=0.0f) {
        self.doseThresholdView.textLabel.attributedText=[NSAttributedString dash];
    }
    else 
        self.doseThresholdView.value = [NSNumber numberWithFloat:thresholdDose];
}

- (float) thresholdDoseRate{
    return self.doseThresholdView.value.floatValue;
} 

- (void) setDose:(float)dose{
    _dose = dose;
    self.thresholdDoseBlinknigView.current = dose;
    
    if (dose<0.0f)
        self.doseValueLabel.attributedText=[NSAttributedString dash];
    else
        self.doseValueLabel.attributedText=[NSAttributedString stringWithDoseInCurrentUnit:dose withBaseFontSize:self.doseValueLabel.font.pointSize];
}

- (float) dose{
    return _dose;
}

- (UILabel*) timeTitleLabel{
    
    
    if (!_timeTitleLabel) {
        _timeTitleLabel = [self newTitleLabel];
        [self addSubview:_timeTitleLabel];
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
            [_timeTitleLabel.superview addConstraint:[NSLayoutConstraint 
                                                      constraintWithItem:_timeTitleLabel 
                                                      attribute:NSLayoutAttributeCenterY 
                                                      relatedBy:NSLayoutRelationEqual toItem:self.timeValueLabel 
                                                      attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
            
            [_timeTitleLabel.superview addConstraint:[NSLayoutConstraint
                                                          constraintWithItem:_timeTitleLabel
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                          toItem:self.timeValueLabel
                                                          attribute:NSLayoutAttributeHeight multiplier:1 constant:0]];

            [_timeTitleLabel.superview addConstraints:[NSLayoutConstraint
                                                       constraintsWithVisualFormat:@"H:|-0-[view(100)]-4-[value]" 
                                                       options:0 
                                                       metrics:nil 
                                                       views:@{@"view":_timeTitleLabel, @"value": self.timeValueLabel}]];            
        }
        else{
            [_timeTitleLabel.superview addConstraints:[NSLayoutConstraint 
                                                       constraintsWithVisualFormat:@"V:[prev]-4-[view(height)]" options:0
                                                       metrics:@{@"height": [NSNumber numberWithInt:AM_UI_INFO_TITLE_HIEGHT]} 
                                                       views:@{@"view":_timeTitleLabel, @"prev":self.doseValueLabel}]];
            [_timeTitleLabel.superview addConstraints:[NSLayoutConstraint 
                                                       constraintsWithVisualFormat:@"H:|-0-[view]-0-|" 
                                                       options:0 metrics:nil 
                                                       views:@{@"view":_timeTitleLabel}]];
        }
        
    }
    return _timeTitleLabel;
};

- (UILabel*) timeValueLabel{
    if (!_timeValueLabel) {
        _timeValueLabel = [[AMDurationLabel alloc] init];
        _timeValueLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _timeValueLabel.adjustsFontSizeToFitWidth = YES;
        _timeValueLabel.minimumScaleFactor = 0.5;
        _timeValueLabel.font = [UIFont fontWithName:AM_UI_INFO_FONT_VALUE size:AM_UI_INFO_FONT_TITLE_SIZE*1.1f];
        _timeValueLabel.textColor = AM_UI_INFO_FONT_VALUE_COLOR;
        _timeValueLabel.numberOfLines = 3;
        _timeValueLabel.textAlignment = NSTextAlignmentLeft;
        _timeValueLabel.backgroundColor = [UIColor clearColor];

        [self addSubview:_timeValueLabel];
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
            [_timeValueLabel.superview addConstraints:[NSLayoutConstraint 
                                                       constraintsWithVisualFormat:@"V:[dose]-4-[view(height)]" 
                                                       options:0 
                                                       metrics:@{@"height": [NSNumber numberWithInt:AM_UI_INFO_VALUE_HIEGHT]} 
                                                       views:@{@"view":_timeValueLabel, @"dose": self.doseValueLabel}]];
            [_timeValueLabel.superview addConstraints:[NSLayoutConstraint 
                                                       constraintsWithVisualFormat:@"H:[view]-0-|" 
                                                       options:0 metrics:nil 
                                                       views:@{@"view":_timeValueLabel}]];                    
        }
        else{
            [_timeValueLabel.superview addConstraints:[NSLayoutConstraint 
                                                       constraintsWithVisualFormat:@"V:[prev]-4-[view(height)]" options:0
                                                       metrics:@{@"height": [NSNumber numberWithInt:AM_UI_INFO_VALUE_HIEGHT]} 
                                                       views:@{@"view":_timeValueLabel, @"prev":self.timeTitleLabel}]];
            [_timeValueLabel.superview addConstraints:[NSLayoutConstraint 
                                                       constraintsWithVisualFormat:@"H:|-0-[view]-0-|" 
                                                       options:0 metrics:nil 
                                                       views:@{@"view":_timeValueLabel}]];            
        }
    }
    return _timeValueLabel;
}

- (void) setStartTime:(NSTimeInterval)startTime{
    self.timeValueLabel.startTime=startTime;
}

- (NSTimeInterval) startTime{
    return self.timeValueLabel.startTime;
}

- (void) amDoseUnitFieldDidEndEditing:(AMDoseUnitField *)field{
    self.defaultController.settings.thresholdDoseRate=field.value;
    [[NSNotificationCenter defaultCenter] postNotificationName:AM_SETTINGS_CHANGED_NOTIFICATION object:self];;
}

- (void) setThresholdDoseRateLimit:(float)thresholdDoseRateLimit{
    self.thresholdDoseRateBlinknigView.limit=thresholdDoseRateLimit;
}

- (float) thresholdDoseRateLimit{
    return self.thresholdDoseRateBlinknigView.limit;
}

- (void) setDoseLimit:(float)doseLimit{
    self.thresholdDoseBlinknigView.limit = doseLimit;
}

- (float) doseLimit{
    return self.thresholdDoseBlinknigView.limit;
}

@end
