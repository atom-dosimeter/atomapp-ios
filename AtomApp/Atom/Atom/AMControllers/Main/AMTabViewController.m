//
//  AMTabViewController.m
//  Atom
//
//  Created by denis svinarchuk on 18.07.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMTabViewController.h"
#import "AMUIPreferences.h"

@implementation AMTabViewController

- (void) awakeFromNib{
    [super awakeFromNib];
    self.tabBar.tintColor = AM_UI_TABBAR_TINTCOLOR;
    self.tabBar.barTintColor = AM_UI_BG_COLOR;
    self.tabBar.backgroundColor = AM_UI_BG_COLOR;
}


- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    for (UIViewController* viewController in self.viewControllers){
        NSString *key = viewController.tabBarItem.title;
        viewController.tabBarItem.title = NSLocalizedString(key, @"");
    }
}

@end
