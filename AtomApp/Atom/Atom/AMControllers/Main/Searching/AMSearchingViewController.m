//
//  AMSearchingViewController.m
//  Atom
//
//  Created by denis svinarchuk on 20/10/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMSearchingViewController.h"

@interface AMSearchingViewController ()

@end

@implementation AMSearchingViewController

- (void)viewDidLoad {
    
    self.hasResetButton = NO;
    self.hasIntervalPicker = YES;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.titledView.title = NSLocalizedString(@"Searching", @"Title of searching localized string");
    self.screenView.displayView.sigmaIsHidden = YES;    
}


- (void) didDosimeterUpdate:(AMDosimeter *)dosimeter{
    
    self.screenView.displayView.chartView.currentDosimeter = self.currentDosimeter;
    
    if (!self.isActive) {
        return;
    }
    
    float doseRate = dosimeter.doseSearchedRate;
    float progress = (float)dosimeter.particlesSearchedCount / (float)dosimeter.particlesSearchedMinimumCount;
    float sigma    = dosimeter.statErrorSearchedRate;
    NSTimeInterval startTime = dosimeter.searchingStartTime;
    
    [self updateWithDoseRate:doseRate withSigma:sigma withStartTime:startTime withProgress:progress];
}


@end
