//
//  main.m
//  AtomTagTestOSX
//
//  Created by denis svinarchuk on 27.09.14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMTagDeviceManager.h"


@interface AMTagTest : NSObject <AMTagDeviceManagerProtocol, AMTagDeviceProtocol>

@property (nonatomic,strong) AMTagDeviceManager *tagManager;

- (void) start;

@end


@implementation AMTagTest 


- (id) init{
    self = [super init];
    
    if (self) {
        self.tagManager = [AMTagDeviceManager sharedInstance];
        self.tagManager.delegate = self;
    }
    
    return self;
}

- (void) start{
    [self.tagManager start];
}

- (void) stop{
    [self.tagManager stop];
}

- (void) tagDeviceManager:(AMTagDeviceManager *)manager didActiveFail:(CBCentralManagerState)error{
    NSLog(@"AMTagTest: %li", error);
}


- (void) tagDeviceManager:(AMTagDeviceManager *)manager willConnectWithDevice:(AMTagDevice *)device{
    NSLog(@"AMTagTest: will active...: %@: %@", device.UUIDString, device.name);
    device.delegate = self;
}

- (void) tagDeviceManagerDidUpdateDeviceList:(AMTagDeviceManager *)manager{
    NSLog(@"AMTagTest: did update list: %@", manager.devices);
}


- (void) tagDevice:(AMTagDevice *)device didReady:(BOOL)state error:(NSError *)error{
    NSLog(@"AMTagTest: device: %@: %@ is ready=%i", device.UUIDString, device.name, state);
}

- (void) tagDevice:(AMTagDevice *)device didPreferencesUpdate:(AMTagDevicePreferences *)settings{
    NSLog(@"AMTagTest: device: %@: %@  settings=%@", device.UUIDString, device.name, settings);
}

- (void) tagDevice:(AMTagDevice *)device didMeasurementUpdate:(AMTagDeviceMeasurement *)measurement{
    NSLog(@"AMTagTest: device: %@: %@\n\tmeasurement=%@", device.UUIDString, device.name, measurement);
}

- (void) tagDevice:(AMTagDevice *)device didThresholdUpdate:(AMTagDeviceThreshold *)threshold{
    NSLog(@"AMTagTest: device: %@: \n\tthreshold=%@", device.UUIDString, threshold);
}

- (void) tagDevice:(AMTagDevice *)device didNameUpdate:(AMTagDeviceName *)deviceName{
    NSLog(@"AMTagTest: device: %@: \n\tname=%@", device.UUIDString, deviceName);
}

- (void) tagDeviceDidSynchronize:(AMTagDevice *)device{
}

- (void) tagDeviceWillSynchronize:(AMTagDevice *)device{
}

@end



int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        
        
        AMTagTest *test = [AMTagTest new];
        
        atexit_b(^{
            [test stop];
        });
        
        [test start];
        
        sleep(300);
        
        [test stop];
    }
    return 0;
}
