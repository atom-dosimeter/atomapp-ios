//
//  AMSearchingViewController.h
//  Atom
//
//  Created by denis svinarchuk on 16/10/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AMBaseDosimeterViewController.h"
#import "AMControllerUpdateProtocol.h"
#import "AMBaseDosimeterInfoView.h"

@interface AMBaseDosimeterMainViewController : AMBaseDosimeterViewController  <AMControllerUpdateProtocol,AMBaseDosimeterInfoDelegate>
@property (atomic,readonly)     BOOL isActive;
@property (nonatomic,assign)    BOOL hasIntervalPicker;
@property (nonatomic,assign)    BOOL hasResetButton;
- (void) updateWithDoseRate:(float) doseRate withSigma:(float) sigma withStartTime:(NSTimeInterval)startTime withProgress:(float) progress;

@end
