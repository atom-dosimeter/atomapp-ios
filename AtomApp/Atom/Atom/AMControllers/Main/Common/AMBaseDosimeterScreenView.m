//
//  AMBaseDosimeterView.m
//  Atom
//
//  Created by denis svinarchuk on 16/10/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMBaseDosimeterScreenView.h"
#import "AMUIPreferences.h"

@implementation AMBaseDosimeterScreenView

@synthesize thermometerView=_thermometerView, displayView=_displayView, infoBaseView=_infoBaseView;

- (id) initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self thermometerView];
        [self displayView];
    }
    return self;
}

- (AMThermometerView*) thermometerView{
    if (!_thermometerView){
        
        NSNumber *width = [NSNumber numberWithInt:(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)?120:60];
        NSNumber *pad = [NSNumber numberWithInt:(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)?40:20];
        
        CGRect   frame = CGRectMake(self.bounds.size.width-width.floatValue-20., 0, width.floatValue, self.bounds.size.height);
        //
        // should init with real size
        //
        _thermometerView = [[AMThermometerView alloc] initWithFrame:frame];
        _thermometerView.backgroundColor = AM_UI_DISPLAY_BG_COLOR;
        _thermometerView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self addSubview:_thermometerView];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":_thermometerView}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[view(width)]-(pad)-|" 
                                                                     options:0
                                                                     metrics:@{@"width": width, @"pad": pad}
                                                                       views:@{@"view":_thermometerView}]];
    }
    return _thermometerView;
}

- (UIView*) displayView{
    
    if (!_displayView) {
        _displayView = [[AMBaseDosimeterDisplayView alloc] init];
        _displayView.backgroundColor = AM_UI_BG_COLOR;
        _displayView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self insertSubview:_displayView atIndex:0];
        
        NSNumber *pad_th = [NSNumber numberWithInt:(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)?20:10];
        NSNumber *pad = [NSNumber numberWithInt:(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)?40:20];

        [self addConstraints:[NSLayoutConstraint 
                              constraintsWithVisualFormat:@"V:|-0-[view]-(pad_th)-[info]" 
                              options:0 
                              metrics:@{@"pad_th": pad_th, @"pad": pad} 
                              views:@{@"view":_displayView, @"info":self.infoBaseView}]];        
        [self addConstraints:[NSLayoutConstraint 
                              constraintsWithVisualFormat:@"H:|-(pad)-[view]-(pad_th)-[thermometer]" 
                              options:0 
                              metrics:@{@"pad_th": pad_th, @"pad": pad} 
                              views:@{@"view":_displayView, @"thermometer": self.thermometerView}]];
    }
    
    return _displayView;
}

- (UIView*) infoBaseView{
    if (!_infoBaseView) {
        _infoBaseView = [[UIView alloc] init];
        _infoBaseView.backgroundColor = AM_UI_BG_COLOR;
        _infoBaseView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self insertSubview:_infoBaseView aboveSubview:self.thermometerView];
        
        NSNumber *height = [NSNumber numberWithInt:(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)?420:174];

        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]-0-|" 
                                                                     options:0 
                                                                     metrics:@{@"height": height} 
                                                                       views:@{@"view":_infoBaseView}]];

        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[view]-10-[thermometer]" 
                                                                         options:0 
                                                                         metrics:nil 
                                                                           views:@{@"view":_infoBaseView, @"thermometer": self.thermometerView}]];
            
            
        }
        else
        {
            
            [_infoBaseView.superview addConstraint:[NSLayoutConstraint constraintWithItem:_infoBaseView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.displayView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];

            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[view(280)]" 
                                                                         options:0 
                                                                         metrics:nil 
                                                                           views:@{@"view":_infoBaseView, @"thermometer": self.thermometerView}]];
        }
    }
    return _infoBaseView;
}

@end
