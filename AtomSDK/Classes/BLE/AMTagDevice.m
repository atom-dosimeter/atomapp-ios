//
//  AMTagDevice.m
//  Atom
//
//  Created by denis svinarchuk on 23.08.14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import "AMTagDevice.h"
#import "AMTagDeviceName.h"
#import "CBUUID+UUIDString.h"
#import "AMQueue.h"
#import "AMTagControllerSettings.h"
#import "AMSwiftDevicePreferences.h"
#import "AMSwiftControllerSettings.h"

typedef enum {
    AM_TAG_DEVICE_PLAY_SOUND,
    AM_TAG_DEVICE_RESET_DOSE,
    AM_TAG_DEVICE_SAVE_FLASH,
    AM_TAG_DEVICE_NO_COMMAND
}AMTagDeviceCurrentCommand;


@class AMTagDeviceManager;
@class AMTagDevice;

@interface AMValue : NSObject
@property (nonatomic,strong) NSData *value;
@property (nonatomic,strong) CBUUID *uuid;
@property (nonatomic,assign) BOOL   response;
@end


@implementation AMValue
@end

//
// Tag device settings extention
//
@interface AMTagDeviceSettings()
@property (weak,nonatomic) AMTagDevice *tagDevice;
- (void) updatePreferences;
@end


//
// Tag device extention
//
@interface AMTagDevice() <CBPeripheralDelegate>

/**
 *  BLE charcteristics
 */
@property (nonatomic,readonly) NSArray *characteristics;

/**
 *  BLE central manager.
 *
 */
@property (readonly, nonatomic) CBCentralManager  *centralManager;

/**
 *  BLE Peripheral.
 *
 */
@property (strong, nonatomic) CBPeripheral  *peripheral;

@property (strong, nonatomic) NSString *className;

@property (strong, nonatomic)  AMDevicePreferences *preferences;
@property (strong, nonatomic)  AMTagDeviceSettings *settings;

@property (strong,nonatomic)   AMTagDeviceMeasurement *measurement;
@property (strong,nonatomic)   AMTagDeviceCalibrationConstants *calibration;
@property (strong,nonatomic)   AMDeviceInitialMeasurement      *initial;
@property (strong,nonatomic)   AMTagDeviceName *deviceName;

@property (readonly,nonatomic) AMTagDeviceInfo *deviceInfo;
@property (strong, nonatomic)  AMTagDeviceThreshold *thresholdDefault;

@property (strong, nonatomic)  AMTagDeviceParticles *deviceParticleCounts;

-(void) performDelegate:(SEL)delegate withBlock:(void(^)(void))block;
- (void) writeValue:(NSData*)value forCharacteristicUUID:(CBUUID*)uuid withResponse:(BOOL)response;

@property (assign,atomic) BOOL isSubscribed;
@property (assign,atomic) AMTagDeviceCurrentCommand currentCommand;

@end


//
// AMTagDevice Settings
//
#pragma mark - AMTagDeviceSetting
@implementation AMTagDeviceSettings

@synthesize hasSoundOnImpulse = _hasSoundOnImpulse,
isTotalMuteOn=_isTotalMuteOn,
hasVibrationOnThreshold=_hasVibrationOnThreshold,
hasVibrationOnImpulse=_hasVibrationOnImpulse;

- (void) setHasSoundOnImpulse:(BOOL)hasSoundOnImpulse{
    
    if (hasSoundOnImpulse==_hasSoundOnImpulse) {
        return;
    }
    
    _hasSoundOnImpulse = hasSoundOnImpulse;
    
    AMDevicePreferences *pp = [self.tagDevice.preferences copy];
    
    pp.command = AM_PREFERENCES_COMMAND_SOUNDVIBRO_SETTO;
    pp.parameter1 = hasSoundOnImpulse?1:0;
    pp.parameter2 = 4000;
    pp.parameter3 = self.hasVibrationOnImpulse?50:0;
    
    [self.tagDevice writeValue:pp.data forCharacteristicUUID:[AMDevicePreferences identifier] withResponse:YES];
}


- (BOOL) hasVibrationOnImpulse{
    return _hasVibrationOnImpulse;
}

- (void) setHasVibrationOnImpulse:(BOOL)hasVibrationOnImpulse{
    
    if (hasVibrationOnImpulse==_hasVibrationOnImpulse) {
        return;
    }
    
    _hasVibrationOnImpulse = hasVibrationOnImpulse;
    
    AMDevicePreferences *pp = [self.tagDevice.preferences copy];
    
    pp.command = AM_PREFERENCES_COMMAND_SOUNDVIBRO_SETTO;
    pp.parameter1 = self.hasSoundOnImpulse?1:0;
    pp.parameter2 = 4000;
    pp.parameter3 = hasVibrationOnImpulse?50:0;
    
    if (hasVibrationOnImpulse) {
        [self.tagDevice performDelegate:@selector(tagDevice:didWarn:) withBlock:^{
            [self.tagDevice.delegate tagDevice:self.tagDevice didWarn:NSLocalizedString(@"Warning: this mode increases energy consumption of the device. Do not forget to turn off this one.", @"")];
        }];
    }
    
    [self.tagDevice writeValue:pp.data forCharacteristicUUID:[AMDevicePreferences identifier] withResponse:YES];
}

- (void) updatePreferences{
    
    if ([self.tagDevice.className isEqualToString:AM_DEVICE_TAG]){
        AMTagDevicePreferences *pref = (AMTagDevicePreferences *) self.tagDevice.preferences;
        
        _hasSoundOnImpulse     = pref.soundImpulseDuration>0?YES:NO;
        _hasVibrationOnImpulse = pref.vibrateOnImpulseDuration>0?YES:NO;
        _isTotalMuteOn         = pref.hasAlarmsOnAll==NO && pref.hasAlarmOnImpulse==NO;
    }
    else if ([self.tagDevice.className isEqualToString:AM_DEVICE_SWIFT]
             ||
             [self.tagDevice.className isEqualToString:AM_DEVICE_FAST]
             ){
        AMSwiftDevicePreferences *pref = (AMSwiftDevicePreferences *) self.tagDevice.preferences;
        
        _hasSoundOnImpulse     = pref.soundImpulseDuration>0?YES:NO;
        _hasVibrationOnImpulse = pref.vibrateOnImpulseDuration>0?YES:NO;
        _clicksMode            = pref.clicksMode;
        _accelerometerMode       = pref.accelerometerMode;
        
        //NSLog(@"updatePreferences: [%@:%@] = %@", self.tagDevice.className, self.tagDevice.peripheral, pref);
    }
    
    if ([self.tagDevice.className isEqualToString:AM_DEVICE_FAST]) {
        AMSwiftDevicePreferences *pref = (AMSwiftDevicePreferences *) self.tagDevice.preferences;
        _hasSoundOnImpulse     = pref.soundImpulseDuration>0?YES:NO;
    }
    
    _hasVibrationOnThreshold = self.tagDevice.thresholdDefault.command==AM_THRESHOLD_COMMAND_SOUNDVIBRO_ON;
}

- (double) thresholdDose{
    return self.tagDevice.thresholdDefault.thresholdDose;
}

- (void) setThresholdDose:(double)thresholdDose{
    if (self.tagDevice.thresholdDefault.thresholdDose==thresholdDose) {
        return;
    }
    self.tagDevice.thresholdDefault.thresholdDose = thresholdDose;
    [self.tagDevice writeValue:self.tagDevice.thresholdDefault.data forCharacteristicUUID:[AMTagDeviceThreshold identifier] withResponse:YES];
}

- (double) thresholdDoseRate{
    return self.tagDevice.thresholdDefault.thresholdDoseRate;
}

-(void) setThresholdDoseRate:(double)thresholdDoseRate{
    if (self.tagDevice.thresholdDefault.thresholdDoseRate==thresholdDoseRate) {
        return;
    }
    self.tagDevice.thresholdDefault.thresholdDoseRate=thresholdDoseRate;
    [self.tagDevice writeValue:self.tagDevice.thresholdDefault.data forCharacteristicUUID:[AMTagDeviceThreshold identifier] withResponse:YES];
}

- (void) setHasVibrationOnThreshold:(BOOL)hasVibrationOnThreshold{
    if (_hasVibrationOnThreshold==hasVibrationOnThreshold) {
        return;
    }
    _hasVibrationOnThreshold=hasVibrationOnThreshold;
    self.tagDevice.thresholdDefault.command=hasVibrationOnThreshold?0x22:0x11;
    [self.tagDevice writeValue:self.tagDevice.thresholdDefault.data forCharacteristicUUID:[AMTagDeviceThreshold identifier] withResponse:YES];
}

- (BOOL) hasVibrationOnThreshold{
    return _hasVibrationOnThreshold;
}


- (BOOL) isTotalMuteOn{
    return _isTotalMuteOn;
}

- (void) setIsTotalMuteOn:(BOOL)isTotalMuteOn{
    
    
    if (isTotalMuteOn) {
        [self.tagDevice performDelegate:@selector(tagDevice:didWarn:) withBlock:^{
            [self.tagDevice.delegate tagDevice:self.tagDevice didWarn:NSLocalizedString(@"Warning: this mode turn off all sounds and vibration except device searching tune. Do not forget to turn back this option when you want to use the device standalone.", @"")];
        }];
    }
    
    if (_isTotalMuteOn==isTotalMuteOn) {
        return;
    }
    
    
    _isTotalMuteOn = isTotalMuteOn;
    
    AMDevicePreferences *pp = [self.tagDevice.preferences copy];
    pp.command = isTotalMuteOn?AM_PREFERENCES_COMMAND_TOTALMUTE_ON:AM_PREFERENCES_COMMAND_TOTALMUTE_OFF;
    [self.tagDevice writeValue:pp.data forCharacteristicUUID:[AMDevicePreferences identifier] withResponse:YES];
}


- (void) setClicksMode:(double)clicksMode{
    if (_clicksMode == clicksMode) return;
    
    _clicksMode = clicksMode;
    
    AMSwiftDevicePreferences *pp = [self.tagDevice.preferences copy];
    pp.command = AM_PREFERENCES_COMMAND_CLICKS_MODE;
    pp.parameter1 = _clicksMode?1:0;
    [self.tagDevice writeValue:pp.data forCharacteristicUUID:[AMDevicePreferences identifier] withResponse:YES];
}


- (void) setAccelerometerMode:(BOOL)accelerometerMode {
    
    if (_accelerometerMode == accelerometerMode) return;
    
    _accelerometerMode = accelerometerMode;
    
    AMSwiftDevicePreferences *pp = [self.tagDevice.preferences copy];
    pp.command = AM_PREFERENCES_COMMAND_ACCELEROMETER_MODE;
    pp.parameter1 = _accelerometerMode?1:0;
    [self.tagDevice writeValue:pp.data forCharacteristicUUID:[AMDevicePreferences identifier] withResponse:YES];
}

@end


//
// AMTagDevice
//
#pragma mark - AMTagDevice
@implementation AMTagDevice
{
    NSTimer           *deferredWrite;
    AMQueue           *RSSIList;
    dispatch_queue_t  delegateQueue;
    dispatch_queue_t  writeQueue;
    BOOL              withResponse;
}


@synthesize characteristics=_characteristics;
@synthesize peripheral = _peripheral;
@synthesize UUIDString=_UUIDString, name=_name, isBusy=_isBusy;

+ (NSArray*) servicesUUIDs{
    return @[[CBUUID UUIDWithString:AM_SERVICE_DEVICE_INFO_UUID], [CBUUID UUIDWithString:AM_SERVICE_UUID]];
}

- (id) initWithPeripheral:(CBPeripheral *)peripheral withCentralManager:(CBCentralManager *)centralManager{
    self = [super init];
    
    if (self) {
        self.currentCommand = AM_TAG_DEVICE_NO_COMMAND;
        delegateQueue = dispatch_queue_create("com.radiometer.atom.tag.delegate", DISPATCH_QUEUE_SERIAL);
        writeQueue = dispatch_queue_create("com.radiometer.atom.tag.write.message", DISPATCH_QUEUE_SERIAL);
        RSSIList = [[AMQueue alloc] init];
        RSSIList.maxSize = 20;
        _centralManager = centralManager;
        _peripheral = peripheral;
        _peripheral.delegate = self;
        _deviceInfo = [[AMTagDeviceInfo alloc] init];
        _UUIDString = [peripheral.identifier UUIDString];
        [_peripheral readRSSI];
        
        self.className = AM_DEVICE_UNKNOWN;
        self.peripheralName = peripheral.name;
                
        if (
            [peripheral.name hasPrefix:@"AtomSwift"]
            ){
            self.className = AM_DEVICE_SWIFT;
        }
        else if (
            [peripheral.name hasPrefix:@"AtomFast"]
            ){
            self.className = AM_DEVICE_FAST;
        }
        else if (
                 [peripheral.name hasPrefix:@"Atom"]
                 ){
            self.className = AM_DEVICE_TAG;
        }
    }
    
    return self;
}


-(void) performDelegate:(SEL)delegate withBlock:(void(^)(void))block{
    if (self.delegate && [self.delegate respondsToSelector:delegate]) {
        dispatch_async(delegateQueue, ^{
            block();
        });
    }
}

- (AMTagDeviceSettings*) settings{
    if (!_settings) {
        _settings = [[AMTagDeviceSettings alloc] init];
        _settings.tagDevice = self;
    }
    return _settings;
}

- (NSArray*) particleCounts{
    return self.deviceParticleCounts.particleCounts;
}

- (NSString*) UUIDString{
    return _UUIDString;
}

- (NSString*) nameKey{
    return [NSString stringWithFormat:@"AM_DEVICE_NAME-%@", self.UUIDString];
}

- (NSNumber*) RSSI{
    return self.peripheral.RSSI;
}

- (NSString*) name{
    if (self.deviceName && self.deviceName.name)
        return self.deviceName.name;
    else{
        NSString *cached_name = [[NSUserDefaults standardUserDefaults] stringForKey:[self nameKey]];
        if (cached_name == nil) {
            cached_name = self.peripheral.name;
            [[NSUserDefaults standardUserDefaults] setValue:cached_name forKey:[self nameKey]];
        }
        return cached_name;
    }
}

- (void)setName:(NSString *)name{
    
    [[NSUserDefaults standardUserDefaults] setValue:name forKey:[self nameKey]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if ([self.deviceName.name isEqualToString:name]){
        // nothing to change
        return;
    }
    
    self.deviceName.name = name;
    [self writeValue:self.deviceName.data forCharacteristicUUID:[AMTagDeviceName identifier] withResponse:YES];
}

- (NSArray*) characteristics{
    if (!_characteristics) {
        _characteristics = @[
                             [AMTagDeviceModelString identifier],
                             [AMTagDeviceSerailString identifier],
                             [AMTagDeviceSoftwareVersion identifier],
                             [AMTagDeviceFirmwareVersion identifier],
                             [AMTagDeviceName identifier],
                             [AMTagDeviceParticles identifier],
                             [AMTagDeviceCalibrationConstants identifier],
                             [AMDeviceInitialMeasurement identifier],
                             [AMTagDeviceThreshold identifier],
                             [AMDevicePreferences identifier],
                             [AMTagDeviceMeasurement identifier]
                             ];
    }
    return _characteristics;
}

- (void) setPeripheral:(CBPeripheral *)peripheral{
    _peripheral = peripheral;
    _peripheral.delegate = self;
}

- (void) setPreferences:(AMDevicePreferences *)settings{
    _preferences = settings;
}

- (void) writeValue:(NSData*)value forCharacteristicUUID:(CBUUID*)uuid withResponse:(BOOL)response{
    for (CBService *service in self.peripheral.services) {
        if (service.characteristics != nil) {
            for (CBCharacteristic *characteristic in service.characteristics) {
                if ([characteristic.UUID isEqual:uuid]) {
                    
                    if (response /*&& !(self.currentCommand==AM_TAG_DEVICE_SAVE_FLASH)*/) {
                        _isBusy = YES;
                        [self performDelegate:@selector(tagDeviceWillSynchronize:) withBlock:^{
                            [self.delegate tagDeviceWillSynchronize:self];
                        }];
                    }
                    
                    dispatch_async(writeQueue, ^{
                        [self.peripheral writeValue:value forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
                    });
                    
                    return;
                }
            }
        }
    }
}


- (void) readValueForCharacteristicUUID:(CBUUID*)uuid{
    for (CBService *service in self.peripheral.services) {
        if (service.characteristics != nil) {
            for (CBCharacteristic *characteristic in service.characteristics) {
                if ([characteristic.UUID isEqual:uuid]) {
                    [self.peripheral readValueForCharacteristic:characteristic];
                }
            }
        }
    }
}

- (void) setNotification:(BOOL)flag forCharacteristicUUID:(CBUUID*)uuid{
    
    if (self.isSubscribed==flag)
        return;
    
    self.isSubscribed = flag;
    
    if (self.peripheral.services != nil) {
        for (CBService *service in self.peripheral.services) {
            if (service.characteristics != nil) {
                for (CBCharacteristic *characteristic in service.characteristics) {
                    if (characteristic.isNotifying!=flag && [characteristic.UUID isEqual:uuid]) {
                        [self.peripheral setNotifyValue:flag forCharacteristic:characteristic];
                    }
                }
            }
        }
    }
}

- (void) resetDose{
    dispatch_async(writeQueue, ^{
        
        if (self.currentCommand!=AM_TAG_DEVICE_NO_COMMAND) {
            [self deviceIsBusyMessage];
            return;
        }
        
        self.currentCommand = AM_TAG_DEVICE_RESET_DOSE;
        
        AMDevicePreferences *pp = [self.preferences copy];
        pp.command = AM_PREFERENCES_COMMAND_DOSE_RESET;
        pp.parameter1 = CFSwapInt16(0xACCE);
        
        //
        // reset measurements
        //
        self.initial = nil;
        self.measurement = nil;
        
        
        [self writeValue:pp.data forCharacteristicUUID:[AMDevicePreferences identifier] withResponse:NO];
    });
}

- (void) deviceIsBusyMessage{
    [self performDelegate:@selector(tagDevice:didFail:) withBlock:^{
        NSError *error = [NSError errorWithDomain:@"com.atom.tag.device"
                                             code:0
                                         userInfo:@{
                                                    NSLocalizedDescriptionKey: NSLocalizedString(@"Device are executing command at the moment. You can repeat action after a while...", nil),
                                                    NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Device is busy.", nil)
                                                    }];
        [self.delegate tagDevice:self didFail:error];
    }];
}

- (void) synchronize{
    
    dispatch_async(writeQueue, ^{
        if (self.currentCommand!=AM_TAG_DEVICE_NO_COMMAND) {
            [self deviceIsBusyMessage];
            return;
        }
        
        self.currentCommand = AM_TAG_DEVICE_SAVE_FLASH;
        
        AMDevicePreferences *pp = [self.preferences copy];
        pp.command = AM_PREFERENCES_COMMAND_FLASH_SAVE;
        pp.parameter1 = CFSwapInt16(0xACCE);
        
        [self writeValue:pp.data forCharacteristicUUID:[AMDevicePreferences identifier] withResponse:NO];
        
        //self.currentCommand = AM_TAG_DEVICE_NO_COMMAND;
    });
}


- (void) playSearchSignal{
    dispatch_async(writeQueue, ^{
        
        if (self.currentCommand!=AM_TAG_DEVICE_NO_COMMAND) {
            [self deviceIsBusyMessage];
            return;
        }
        
        self.currentCommand = AM_TAG_DEVICE_PLAY_SOUND;
        
        AMDevicePreferences *p = [self.preferences copy];
        p.command = AM_PREFERENCES_COMMAND_SEARCHING_PLAY;
        p.parameter1 = 0x0;
        [self writeValue:p.data forCharacteristicUUID:[AMDevicePreferences identifier] withResponse:YES];
    });
}


- (void) peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    
    dispatch_async(writeQueue, ^{
        
        if (self.currentCommand == AM_TAG_DEVICE_RESET_DOSE) {
            self.currentCommand = AM_TAG_DEVICE_NO_COMMAND;
            //
            // reread initial measurement
            //
            [self readValueForCharacteristicUUID:[AMDeviceInitialMeasurement identifier]];
            
            return ;
        }
        
        if (error){
            
            [self performDelegate:@selector(tagDevice:didFail:) withBlock:^{
                [self.delegate tagDevice:self didFail:error];
            }];
        }
        
        else {
            
            if (!(self.currentCommand == AM_TAG_DEVICE_SAVE_FLASH)) {
                _isBusy = NO;
                [self performDelegate:@selector(tagDevice:didFail:) withBlock:^{
                    [self.delegate tagDeviceDidSynchronize:self];
                }];
            }
        }
        
        if (self.currentCommand==AM_TAG_DEVICE_NO_COMMAND) {
            //
            // save to flash
            //
            [self synchronize];
        }
        else{
            self.currentCommand = AM_TAG_DEVICE_NO_COMMAND;
        }
    });
}


- (void) discoverServices{
    
    if (self.disconnectionTimer) {
        [self.disconnectionTimer invalidate];
        self.disconnectionTimer = nil;
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(tagDeviceShouldBeDiscovered:)]) {
        if (![self.delegate tagDeviceShouldBeDiscovered:self]) {
            [self cancelConnection];
            [self cleanup];
            return;
        }
    }
    
    [self performDelegate:@selector(tagDeviceWillReady:) withBlock:^{
        [self.delegate tagDeviceWillReady:self];
    }];
    
    [self.peripheral discoverServices:[AMTagDevice servicesUUIDs]];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    
    if (error) {
        [self performDelegate:@selector(tagDevice:didFail:) withBlock:^{
            [self.delegate tagDevice:self didFail:error];
        }];
        [self cleanup];
        return;
    }
    
    [self performDelegate:@selector(tagDevice:didReady:error:) withBlock:^{
        [self.delegate tagDevice:self didReady:YES error:nil];
    }];
    
    for (CBService *service in peripheral.services) {
        // Discover other characteristics
        self.calibration = nil;
        self.measurement = nil;
        self.initial = nil;
        [peripheral discoverCharacteristics:self.characteristics forService:service];
    }
}


- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    
    if (error) {
        [self performDelegate:@selector(tagDevice:didFail:) withBlock:^{
            [self.delegate tagDevice:self didFail:error];
        }];
        [self cleanup];
        return;
    }
    
    for (CBCharacteristic *characteristic in service.characteristics) {
        [peripheral readValueForCharacteristic:characteristic];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    
    if (error) {
        [self performDelegate:@selector(tagDevice:didFail:) withBlock:^{
            [self.delegate tagDevice:self didFail:error];
        }];
        return;
    }
    
    //
    // GAT
    //
    
    if ([characteristic.UUID isEqual:[AMTagDeviceModelString identifier]]){
        [self.deviceInfo readModelStringFromData:characteristic.value];
    }
    
    if ([characteristic.UUID isEqual:[AMTagDeviceSerailString identifier]]){
        [self.deviceInfo readSerialStringFromData:characteristic.value];
    }
    
    if ([characteristic.UUID isEqual:[AMTagDeviceSoftwareVersion identifier]]){
        [self.deviceInfo readSoftwareNumberFromData:characteristic.value];
    }
    
    if ([characteristic.UUID isEqual:[AMTagDeviceFirmwareVersion identifier]]){
        [self.deviceInfo readFirmwareNumberFromData:characteristic.value];
    }
    
    //
    // Initial measurement
    //
    if ([characteristic.UUID isEqual:[AMDeviceInitialMeasurement identifier]]) {
        if (
            [self.className isEqualToString:AM_DEVICE_SWIFT]
            ||
            [self.className isEqualToString:AM_DEVICE_FAST]
            ){
            self.initial = [[AMSwiftDeviceInitialMeasurement alloc] initWithData:characteristic.value];
        }
        else {
            self.initial = [[AMTagDeviceInitialMeasurement alloc] initWithData:characteristic.value];
        }
    }
    
    //
    // Calibration constatnts
    //
    if ([characteristic.UUID isEqual:[AMTagDeviceCalibrationConstants identifier]]) {
        self.calibration = [[AMTagDeviceCalibrationConstants alloc] initWithData:characteristic.value];
    }
    
    //
    // particle counts
    //
    if ([characteristic.UUID isEqual:[AMTagDeviceParticles identifier]]){
        self.deviceParticleCounts = [[AMTagDeviceParticles alloc] initWithData:characteristic.value];
    }
    
    //
    // Changing preferences
    //
    
    //
    // Name
    //
    if ([characteristic.UUID isEqual:[AMTagDeviceName identifier]]){
        
        NSString *name = self.name;
        NSString *pname = peripheral.name;
        
        self.deviceName = [[AMTagDeviceName alloc] initWithCharacteristic:characteristic];
        
        if (![self.deviceName.name isEqualToString:name]) {
            //
            // name is changed by another device
            //
            [self performDelegate:@selector(tagDevice:didChangeFromName:toName:) withBlock:^{
                [self.delegate tagDevice:self didChangeFromName:name toName:self.deviceName.name];
            }];
        }
        
        [self performDelegate:@selector(tagDevice:didNameUpdate:) withBlock:^{
            [self.delegate tagDevice:self didNameUpdate:self.deviceName];
        }];
    }
    
    //
    // Threshold
    //
    if ([characteristic.UUID isEqual:[AMTagDeviceThreshold identifier]]){
        self.thresholdDefault = [[AMTagDeviceThreshold alloc] initWithData:characteristic.value];
        
        [self performDelegate:@selector(tagDevice:didThresholdUpdate:) withBlock:^{
            [self.delegate tagDevice:self didThresholdUpdate:self.thresholdDefault];
        }];
    }
    

    if ([characteristic.UUID isEqual:[AMDevicePreferences identifier]]){
        
        BOOL performeUpdate = self.preferences==nil;
        
        
        if ([self.className isEqualToString:AM_DEVICE_TAG])
            self.preferences = [[AMTagDevicePreferences alloc] initWithData:characteristic.value withDeviceUUIDString:[peripheral.identifier UUIDString]];
        else if (
                 [self.className isEqualToString:AM_DEVICE_SWIFT]
                 ||
                 [self.className isEqualToString:AM_DEVICE_FAST]
                 )
            self.preferences = [[AMSwiftDevicePreferences alloc] initWithData:characteristic.value withDeviceUUIDString:[peripheral.identifier UUIDString]];
        
        [self.settings updatePreferences];
        
        if (performeUpdate) {
            [self performDelegate:@selector(tagDevice:didPreferencesUpdate:) withBlock:^{
                [self.delegate tagDevice:self didPreferencesUpdate:self.preferences];
            }];
        }
    }
    
    
    if (self.calibration!=nil
        &&
        self.initial!=nil
        &&
        self.preferences!=nil
        ) {
        if (self.measurement == nil) {
            //
            // after all subscribe to measurements
            //
            [self setNotification:YES forCharacteristicUUID:[AMTagDeviceMeasurement identifier]];
        }
    }
    
    //
    // Update main measurements
    //
    if ([characteristic.UUID isEqual:[AMTagDeviceMeasurement identifier]] && self.initial && self.calibration) {
        
        if (self.measurement==nil) {
            self.measurement = [[AMTagDeviceMeasurement alloc] initWithData:characteristic.value withCalibration:self.calibration withInitial:self.initial];
            
            if ([self.preferences isKindOfClass:[AMSwiftDevicePreferences class]]) {
                
                AMSwiftDevicePreferences *pref = self.preferences;
                AMSwiftDeviceInitialMeasurement *initial = self.initial;
                
                switch (pref.searchingMode) {
                    case 0:
                        self.measurement.searchingMode = initial.fastImpulsesCount;
                        break;
                    case 1:
                        self.measurement.searchingMode = initial.mediumImpulsesCount;
                        break;
                    case 2:
                        self.measurement.searchingMode = initial.slowImpulsesCount;
                        break;
                    default:
                        self.measurement.searchingMode = initial.mediumImpulsesCount;
                        break;
                }
            }
            else {
                self.measurement.searchingMode = kAMDosimeterSearchModeNormal;
            }
        }
        else{
            [self.measurement readData:characteristic.value];
        }
        
        [self.peripheral readRSSI];
        
        [self performDelegate:@selector(tagDevice:didMeasurementUpdate:) withBlock:^{
            if (self.currentCommand==AM_TAG_DEVICE_NO_COMMAND) {
                [self.delegate tagDevice:self didMeasurementUpdate:self.measurement];
            }
        }];
        
        if (self.measurement.isPreferencesChanged) {
            [self readValueForCharacteristicUUID:[AMDevicePreferences identifier]];
        }
    }
}


- (void) peripheralDidUpdateRSSI:(CBPeripheral *)peripheral error:(NSError *)error{
    
#if DEBUG
    if (peripheral && peripheral.RSSI) {
        [RSSIList push:peripheral.RSSI];
        if (RSSIList.size>0) {
            float summ = 0.0f;
            for (int i=0; i<RSSIList.size; i++) {
                summ+=[[RSSIList objectAtIndex:i] floatValue];
            }
            summ/=RSSIList.size;
            _avgRSSI = [NSNumber numberWithFloat:summ];
        }
    }
#endif
    
    if (error) {
        [self performDelegate:@selector(tagDevice:didFail:) withBlock:^{
            [self.delegate tagDevice:self didFail:error];
        }];
        return;
    }
    
    [self performDelegate:@selector(tagDevicedidUpdateRSSI:) withBlock:^{
        [self.delegate tagDevicedidUpdateRSSI:self];
    }];
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    
    if (error) {
        [self.centralManager cancelPeripheralConnection:self.peripheral];
        [self performDelegate:@selector(tagDevice:didFail:) withBlock:^{
            [self.delegate tagDevice:self didFail:error];
        }];
        return;
    }
    
    if (!characteristic.isNotifying) {
        // Notification has stopped
        
        [self performDelegate:@selector(tagDevice:didFail:) withBlock:^{
            NSString *message = [NSString stringWithFormat:NSLocalizedString(@"Atom Tag %@ (%@) has finished sending notification suddenly. Please, check this device.", nil), self.name, self.UUIDString];
            NSError *finished_error = [NSError errorWithDomain:@"com.atom.tag.device"
                                                          code:0
                                                      userInfo:@{
                                                                 NSLocalizedDescriptionKey: message
                                                                 }];
            [self.delegate tagDevice:self didFail:finished_error];
        }];
    }
}

- (void)cleanup {
    self.currentCommand = AM_TAG_DEVICE_NO_COMMAND;
    
    _isBusy = NO;
    
    // See if we are subscribed to a characteristic on the peripheral
    [self setNotification:NO forCharacteristicUUID:[AMTagDeviceMeasurement identifier]];
    
    self.initial = nil;
    self.measurement = nil;
}

- (void) cancelConnection{
    
    self.currentCommand = AM_TAG_DEVICE_NO_COMMAND;
    _isBusy = NO;
    
    if (self.disconnectionTimer) {
        [self.disconnectionTimer invalidate];
        self.disconnectionTimer = nil;
    }
    [_centralManager cancelPeripheralConnection:self.peripheral];
}

@end
