//
//  AMTagDevice.h
//  Atom
//
//  Created by denis svinarchuk on 23.08.14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreFoundation/CoreFoundation.h>

#if TARGET_OS_IPHONE
#import <CoreBluetooth/CoreBluetooth.h>
#elif TARGET_OS_MAC
#import <IOBluetooth/IOBluetooth.h>
#endif

#import "AMTagDeviceName.h"
#import "AMTagDeviceInfo.h"
#import "AMTagDeviceMeasurement.h"
#import "AMTagDevicePreferences.h"
#import "AMTagDeviceThreshold.h"
#import "AMTagDeviceParticles.h"

#define AM_SERVICE_UUID        @"63462A4A-C28C-4FFD-87A4-2D23A1C72581"
#define AM_DEVICE_UNAVAILABLE_LIMIT 5. // sec

#define AM_DEVICE_SWIFT     @"Swift"
#define AM_DEVICE_FAST      @"Fast"
#define AM_DEVICE_TAG       @"Tag"
#define AM_DEVICE_UNKNOWN   @"UNKNOWN"

@class AMTagDevice;

/**
 *  Delegate a device events to next application level.
 */
@protocol AMTagDeviceProtocol <NSObject>

@required
- (void) tagDevice:(AMTagDevice*)device didMeasurementUpdate:(AMTagDeviceMeasurement*)measurement;
- (void) tagDevice:(AMTagDevice*)device didPreferencesUpdate:(AMTagDevicePreferences*)preferences;
- (void) tagDeviceDidSynchronize:(AMTagDevice *)device;
- (void) tagDeviceWillSynchronize:(AMTagDevice *)device;

@optional

- (BOOL) tagDeviceShouldBeDiscovered:(AMTagDevice*)device;

- (void) tagDeviceWillReady:(AMTagDevice*)device;
- (void) tagDevice:(AMTagDevice*)device didReady:(BOOL)state error:(NSError*)error;
- (void) tagDevicedidUpdateRSSI:(AMTagDevice*)device;
- (void) tagDevice:(AMTagDevice*)device didFail:(NSError*)error;

- (void) tagDevice:(AMTagDevice*)device didThresholdUpdate:(AMTagDeviceThreshold*)threshold;
- (void) tagDevice:(AMTagDevice*)device didNameUpdate:(AMTagDeviceName*)deviceName;

/**
 *  Delegate when name of the device was changed by another Atom Tag but it has not been replaced to local device names cache.
 *
 *  @param device     device
 *  @param deviceName name
 *
 */
- (void) tagDevice:(AMTagDevice *)device didChangeFromName:(NSString*)fromName toName:(NSString*)toName;

- (void) tagDevice:(AMTagDevice*)device didWarn:(NSString*)warnString;

@end

/**
 *  Collection of the device prefered settings.
 */
@interface AMTagDeviceSettings : NSObject

/**
 *  It has sound on particle detection or not.
 */
@property (nonatomic,assign) BOOL hasSoundOnImpulse;

/**
 *  YES if we want to vibrate the device when it detects a particle.
 */
@property (nonatomic,assign) BOOL hasVibrationOnImpulse;

/**
 *  YES if we want to detect overdose with vibration.
 */
@property (nonatomic,assign) BOOL hasVibrationOnThreshold;

/**
 *  YES if we want to turn off any sounds exclude searching tunes.
 */
@property (nonatomic,assign) BOOL isTotalMuteOn;

/**
 *  Threshold of the comulative dose. Threshold sets in case you want to get alert exceeding the dose. It sets in impulses.
 *  Max impulses in year is about seconds per year / 4 = 7 884 000 impulses per year.
 *  Set to 0 to avoid alert signal. Signal can be sound and vibration. If threshold is seted, you can't off sound alert in this version of device software.
 *  Unit: mSv
 */
@property (assign,nonatomic) double thresholdDose;          // mSv

/**
 *  Threshold of the dose rate. It sets in case you want to get alert exceeding the dose rate. 
 *  It sets in impulses per value defined by thresholdDetectionTime property.
 *  thresholdDetectionTime always multiple of 2 seconds. 60 impulses per 16 thresholdDetectionTime is equal 60 impulses per 32 seconds, and equal 60µR/h.
 *  Unit:  µSv/h
 */
@property (assign,nonatomic) double thresholdDoseRate;   // µSv/h

@property (assign,nonatomic) double clicksMode;          // 1:1 , 1:10
@property (assign,nonatomic) BOOL   accelerometerMode;     //

@end


/**
 *  Atom Tag device representaion.
 *
 */
@interface AMTagDevice : NSObject

/**
 *  List services which this device can offer.
 *
 *  @return array of uuids.
 *
 */
+ (NSArray*) servicesUUIDs;

/**
 *  Device preferences and prefered default threshold
 */
@property (readonly,nonatomic) AMTagDeviceSettings             *settings;

/**
 *  Device has calibration metrics. All of them is served
 */
@property (readonly,nonatomic) AMTagDeviceCalibrationConstants *calibration;

/**
 *  Array of last particles detected within time intervals each of them has duration for 2 seconds.
 */
@property (readonly, nonatomic) NSArray *particleCounts;

/**
 *  Atom Tag device name.
 */
@property (strong, nonatomic) NSString *name;

@property (strong, nonatomic) NSString *peripheralName;

/**
 * Atom class Tag/Swift/Fast/...
 */
@property (readonly, nonatomic) NSString *className;


/**
 *  Uniq device identifier.
 *
 */
@property (readonly, nonatomic) NSString *UUIDString;

/**
 *  Connection RSSI level.
 *
 */
@property (readonly, nonatomic) NSNumber *RSSI;

/**
 *  Avarage RSSI for the last 20 updates.
 */
@property (readonly, nonatomic) NSNumber *avgRSSI;

/**
 *  Delegate device protocol.
 */
@property (strong, nonatomic) id<AMTagDeviceProtocol> delegate;

/**
 *  Timer can be associated with the device in a device controller to manage connection/disconnection sessions.
 */
@property (strong, nonatomic) NSTimer* disconnectionTimer;

@property (readonly, atomic) BOOL isBusy;

@property (readonly, nonatomic) CBPeripheral *peripheral;

/**
 *  Create object services.
 *
 *  @param peripheral
 *  @param centralManager
 *
 *  @return object instance
 *
 */
- (id) initWithPeripheral:(CBPeripheral*)peripheral withCentralManager:(CBCentralManager*)centralManager;

/**
 *  Start to descover service.
 *
 */
- (void) discoverServices;

/**
 *  Cleanup device connection.
 *
 */
- (void) cleanup;

/**
 *  Cancel device connection
 *
 */
- (void) cancelConnection;

/**
 *  Write settings to device;
 */
- (void) synchronize;

/**
 *  Play a sound to search a device that was lost, but has connection with the phone.
 */
- (void) playSearchSignal;

/**
 *  Reset device cumulative dose.
 */
- (void) resetDose;

@end
