//
//  AMStatusBarView.m
//  Atom
//
//  Created by denis svinarchuk on 12.07.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMStatusBarView.h"
#import "AMUIPreferences.h"
#import "AMBatteryView.h"
#import "AMConnectionView.h"

typedef  enum {
    
    AM_POWER_ENOUGH,
    AM_POWER_OFF,
    AM_POWER_LOW,
    AM_POWER_VERY_LOW,
    AM_POWER_UNKNOWN
    
} AMPowerStatus;


@interface AMStatusBarView()

@property (nonatomic,strong) AMConnectionView *connectionView;; 
@property (nonatomic,strong) AMBatteryView *batteryView;
@property (nonatomic,strong) UIImageView *backgroundImage;
@property (nonatomic,assign) AMPowerStatus  powerStatus;

@end

@implementation AMStatusBarView
{
    NSTimer *blinkPowerImageTimer;
}

@synthesize powerStatus=_powerStatus;


- (void) setPowerVolume:(float)powerVolume{
    self.batteryView.powerVolume = powerVolume;
}

- (float) powerVolume{
    return self.batteryView.powerVolume;
}

- (void) awakeFromNib{
    [super awakeFromNib];
}

- (void) willMoveToSuperview:(UIView *)newSuperview{
    
    [super willMoveToSuperview:newSuperview];
    self.backgroundColor = [UIColor clearColor];
    NSString *image= @"statusBarBg";
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        image = @"statusBarBg-ipad";
    }
    self.backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:image]];
    [self addSubview:self.backgroundImage];
    
    self.isConnected = NO;
    self.powerStatus = AM_POWER_OFF;
    self.powerVolume = 0.0;
}

- (BOOL) chargerPlugged{
    return self.batteryView.chargerPlugged;
}

- (void) setChargerPlugged:(BOOL)chargerPlugged{
    self.batteryView.chargerPlugged = chargerPlugged;
}

- (AMBatteryView*) batteryView{
    if (_batteryView) {
        return _batteryView;
    }
    
    _batteryView = [[AMBatteryView alloc] initWithFrame:CGRectMake(0, 0, 34, 15)];
    _batteryView.size = CGSizeMake(34, 15);
    _batteryView.translatesAutoresizingMaskIntoConstraints = NO;
    
    
    [self addSubview:_batteryView];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view": _batteryView}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[connectionImage]-10-[view(34)]" options:0 metrics:nil views:@{@"view": _batteryView, @"connectionImage": self.connectionView}]];
        
    return _batteryView;
}

- (AMConnectionView*) connectionView{
    if (_connectionView) {
        return _connectionView;
    }
    
    _connectionView = [[AMConnectionView alloc] init];
    _connectionView.size = CGSizeMake(24, 24);
    _connectionView.translatesAutoresizingMaskIntoConstraints = NO;
        
    [self addSubview:_connectionView];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[view]-5-|" options:0 metrics:nil views:@{@"view": _connectionView}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[view(20)]" options:0 metrics:nil views:@{@"view": _connectionView}]];
    
    return _connectionView;
}

- (void) setPowerStatus:(AMPowerStatus)powerStatus{
    _powerStatus = powerStatus;
    switch (powerStatus) {
        case AM_POWER_ENOUGH:
            self.hasPowerEnough = YES;
            break;                 
        case AM_POWER_LOW:
            self.hasPowerEnough = NO;
            break;                 
        case AM_POWER_VERY_LOW:
            self.hasPowerEnough = NO;
            break;                 
        case AM_POWER_UNKNOWN:
            self.hasPowerEnough = YES;
            break;
        default:
            break;
    }
}


- (void) powerImageBlinks:(NSTimer*)timer{
    [UIView animateWithDuration:0.3 animations:^{
        self.batteryView.alpha = 0.2; 
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.5 animations:^{
            self.batteryView.alpha = 1.0;             
        }];
    }];
}

- (void) setHasPowerEnough:(BOOL)hasPowerEnough{
    _hasPowerEnough = hasPowerEnough;
    
    dispatch_async(dispatch_get_main_queue(), ^{
                
        if (blinkPowerImageTimer) {
            [blinkPowerImageTimer invalidate];
        }

        if (self.powerStatus == AM_POWER_UNKNOWN) {
            return;
        }
        
        if (!_hasPowerEnough && !self.chargerPlugged) {
            blinkPowerImageTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(powerImageBlinks:) userInfo:nil repeats:YES];
        }        
        else
            blinkPowerImageTimer = nil;
    });
    
}

- (void) setIsConnected:(BOOL)isConnected{
    self.connectionView.isConnected = isConnected;
}

@end
