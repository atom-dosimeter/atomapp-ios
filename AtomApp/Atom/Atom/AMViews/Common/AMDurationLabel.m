//
//  AMDurationLabel.m
//  Atom
//
//  Created by denis svinarchuk on 14.09.14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import "AMDurationLabel.h"
#import "NSString+AMUtils.h"

@interface AMDurationLabel()
@property (nonatomic,strong) NSString* dash;
@property (nonatomic,strong) NSString* timeText;
@end

@implementation AMDurationLabel
{
    NSTimer* timeUpdateTimer;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (NSString*) dash{
  return @"-";  
};

- (NSString*) timeText{
    if (self.startTime<=0.0) {
        return self.dash;
    }
    
    return  [NSString durationStringFromStartTime:self.startTime];
}


-(void) setIsActive:(BOOL)isActive{
    
    if (_isActive==isActive) {
        return;
    }
    
    _isActive = isActive;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (timeUpdateTimer) {
            [timeUpdateTimer invalidate];
        }
        if (isActive) {
            timeUpdateTimer  = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timeUpdateTimerCallback:) userInfo:nil repeats:YES];            
        }
    });
}

- (void) timeUpdateTimerCallback:(NSTimer*)timer{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.text = self.timeText;        
    });
}

- (void) setStartTime:(NSTimeInterval)startTime{
    if (_startTime!=startTime || _startTime<=0.0) {
        [self timeUpdateTimerCallback:nil];
    }
    _startTime = startTime;    
    self.isActive = _startTime>0.0?YES:NO;
}

@end
