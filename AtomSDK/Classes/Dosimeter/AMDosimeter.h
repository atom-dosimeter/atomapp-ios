//
//  AMDosimeter.h
//  AMSignalProcessor
//
//  Created by denis svinarchuk on 04.07.14.
//  Copyright (c) 2014 Atom Measurer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMConstants.h"
#import "AMQueue.h"


@interface AMDosimeterSettings : NSObject
@property (assign,nonatomic)   float sensetivity;     // impulses/µR
@property (readonly,nonatomic) float sensetivitySvP;  // nSv/pulse
@property (assign,nonatomic)   float ownBackgound;    // impulses/min.
@property (readonly,nonatomic) float ownBackgoundSvH; // nSv/h.
@property (assign,nonatomic)   float deadTime;        // sec.;
@end


@interface AMDoseRatePoint : NSObject

@property (readonly,nonatomic) NSDate      *time;
@property (readonly,nonatomic) NSUInteger   index;
@property (readonly,nonatomic) float        doseRate;

@end


@class AMDosimeter;

/**
 *  Handles when dose rate computes.
 */ 
@protocol AMDosimeterProtocol <NSObject>

@optional
/**
 *  It handles when a next measurment done.
 *
 *  @param dosimeter the dosimeter instance.
 */
- (void) amDosimeterValuesChanged:(AMDosimeter*)dosimeter;

@end



/**
 * The AMDosimeter class creates a dosimeter type calculator that accumulates particle impulses and computes radiation dose rate.
 */
@interface AMDosimeter : NSObject

/**
 *  Preferences
 */
@property (nonatomic,assign) NSTimeInterval minRefreshTime; // 0 means every time particles received

/**
 *  Dosimeter settings
 */
@property (nonatomic,readonly) AMDosimeterSettings *settings;

/**
 *  Uniq ID.
 *
 */
@property (strong,nonatomic) NSString *UUIDString;

/**
 *  A dosimeter delegate object.
 */
@property (nonatomic,strong) id<AMDosimeterProtocol> delegate;

/**
 * The threshold of numbers of particles that dosimeter should detect to compute dose equivalent rate.
 */
@property (nonatomic,assign) float particlesSearchedMinimumCount;

/**
 *  The current Ambient Dose Equivalent Rate (DER). Accumulated dose rate during time of all measurment.
 */
@property (nonatomic,readonly) float doseEquivalentRate;

/**
 *  Time measurement.
 */
@property (nonatomic,assign)   NSTimeInterval measuringTime;
@property (nonatomic,readonly) NSTimeInterval measuringStartTime;
@property (atomic,readonly)    NSTimeInterval searchingStartTime;

/**
 *  Dose rate is computed during by given searchingModeThreshold numbers of particles which dosimeter detected for searching time.
 *
 */
@property (atomic,readonly) float doseSearchedRate;

/**
 *  Particles are detected by Geiger counter for the last measurment time.
 *
 */
@property (assign,nonatomic) uint64_t particlesEquivalentCount;

/**
 *  Particles are detected by Geiger counter in searching mode.
 *
 */
@property (readonly,nonatomic) NSUInteger particlesSearchedCount; 

/**
 *  Cumulative dose is computed during application is working (after it started to work or reseted).
 */
@property (assign,nonatomic) float cumulativeDose;

/**
 *  The statistical error (sigma) which computes for the Dose Equivalent Rate.
 */
@property (nonatomic,readonly) float statErrorEquivalentRate;

/**
 *  The statistical error which computes n searching measurement mode.
 */
@property (nonatomic,readonly) float statErrorSearchedRate;

/**
 *  User data object associated with the dosimeter.
 */
@property (nonatomic,strong) id userData;

/**
 *  Current interval between dose rate measuring.
 */
@property (nonatomic,assign)   NSTimeInterval doseRateHistoryInterval;

    /**
     * Particles per second.
     */
@property (nonatomic,readonly) float cpsMeasuring;
@property (nonatomic,readonly) float cpsSearching;


/**
 *  Current dose rate history measurements.
 *
 */
@property (nonatomic,readonly) AMQueue *doseRateHistory;

/**
 *  Counts of the history measurement.
 */
@property (nonatomic,assign) NSUInteger doseRateHistoryCount;

/**
 *  Add computing histroy interval
 *
 *  @param interval interval in seconds.
 *
 */
-(void) addDoseRateHistoryInterval:(NSTimeInterval) interval;


/**
 *  Add new detected particles.
 *
 *  @param count count of new particles detected by dosimeter;
 */
- (void) addParticles:(NSInteger)count;
- (void) addParticles:(NSInteger)count withTime:(NSTimeInterval)timeStamp;
- (void) addParticles:(NSInteger)count withCountDuration:(NSTimeInterval)countDuration;
- (void) addParticles:(NSInteger)count withCountDuration:(NSTimeInterval)countDuration withSearchedDoseRate:(float)doseRate;

/**
 *  Dosimeter is activeted to compute
 *
 */
@property (atomic,readonly) BOOL isRunning;

/**
 *  Start to compute dose values. 
 */
- (void) start;

/**
 *  Stop compute.
 */
- (void) stop;

/**
 *  Reset all metrics exclude searching mode.
 */
- (void) reset;

/**
 *  Reset all metrics.
 *
 */
- (void) hardReset;


@end
