//
//  AMTagControllerSettings.m
//  Pods
//
//  Created by denis svinarchuk on 20.11.16.
//
//

#import "AMTagControllerSettings.h"
#import "AMTagDevice.h"
#import "AMTagController.h"

@implementation AMTagControllerSettings

@synthesize tagDevice=_tagDevice, hasVibrationOnThreshold=_hasVibrationOnThreshold, hasSoundOnImpulse=_hasSoundOnImpulse, isEnabled=_isEnabled, tagController=_tagController;

- (id) initWithAMTagDevice:(AMTagDevice *)tagDevice{
    self =  [super initWithUUIDString:tagDevice.UUIDString];
    if (self) {
        self.tagDevice = tagDevice;
    }
    return self;
}

- (void) setTagController:(AMTagController *)tagController{
    _tagController=tagController;
}

- (NSString*) name {
    return self.tagDevice.name;
}

- (void) setName:(NSString *)name{
    self.tagDevice.name = name;
}

- (NSString*) UUIDString{
    return self.tagDevice.UUIDString;
}

- (NSNumber*) thresholdDose{
    
    return [NSNumber numberWithFloat: self.tagDevice.settings.thresholdDose*1000000.0f]; // nSv
}

- (void) setThresholdDose:(NSNumber*)thresholdDose{
    
    if (thresholdDose.floatValue<kAMThresholdDoseMinimum) {
        if (self.tagController.delegate && [self.tagController.delegate respondsToSelector:@selector(amController:didWarn:)]){
            [self.tagController.delegate amController:self.tagController
                                              didWarn: [NSString stringWithFormat:NSLocalizedString(@"You tried to set very low dose threshold. It can't be less then %.3f(mSv)", @""),kAMThresholdDoseMinimum/1000000.0f]];
        }
    }
    else if (thresholdDose.floatValue>kAMThresholdDoseMaximum){
        if (self.tagController.delegate && [self.tagController.delegate respondsToSelector:@selector(amController:didWarn:)]){
            [self.tagController.delegate amController:self.tagController
                                              didWarn: [NSString stringWithFormat:NSLocalizedString(@"You tried to set very high dose threshold. It can't be greate then %.2f(Sv)", @""),kAMThresholdDoseMaximum/1000000000.0f]];
        }
    }
    else
        self.tagDevice.settings.thresholdDose =   thresholdDose.floatValue/1000000.0f; // mSv
}

- (NSNumber*) thresholdDoseRate{
    return [NSNumber numberWithFloat:self.tagDevice.settings.thresholdDoseRate*1000.0f]; // nSv/h
}

- (void) setThresholdDoseRate:(NSNumber*)thresholdDoseRate{
    if (thresholdDoseRate.floatValue<kAMThresholdDoseRateMinimum) {
        if (self.tagController.delegate && [self.tagController.delegate respondsToSelector:@selector(amController:didWarn:)]){
            [self.tagController.delegate amController:self.tagController
                                              didWarn: [NSString stringWithFormat:NSLocalizedString(@"You tried to set very low dose rate threshold. It can't be less then %.0f(nSv)", @""),kAMThresholdDoseRateMinimum]];
        }
    }
    else if (thresholdDoseRate.floatValue>kAMThresholdDoseRateMaximum){
        if (self.tagController.delegate && [self.tagController.delegate respondsToSelector:@selector(amController:didWarn:)]){
            [self.tagController.delegate amController:self.tagController
                                              didWarn: [NSString stringWithFormat:NSLocalizedString(@"You tried to set very high dose rate threshold. It can't be greate then %.2f(mSv)", @""),kAMThresholdDoseRateMaximum/1000000.0f]];
        }
    }
    else
        self.tagDevice.settings.thresholdDoseRate = thresholdDoseRate.floatValue/1000.0f; // µSv/h
}

- (AMBoolean*) hasVibrationOnThreshold{
    return (AMBoolean*)[AMBoolean numberWithBool:self.tagDevice.settings.hasVibrationOnThreshold];
}

- (void) setHasVibrationOnThreshold:(AMBoolean*)isVibrateOn{
    self.tagDevice.settings.hasVibrationOnThreshold=isVibrateOn.boolValue;
}

- (AMBoolean*) hasSoundOnImpulse{
    return  (AMBoolean*)[AMBoolean numberWithBool:self.tagDevice.settings.hasSoundOnImpulse];
};

- (void) setHasSoundOnImpulse:(AMBoolean *)doItSnapSound{
    self.tagDevice.settings.hasSoundOnImpulse = doItSnapSound.boolValue;
}

- (AMBoolean*) hasVibrationOnImpulse{
    return  (AMBoolean*)[AMBoolean numberWithBool:self.tagDevice.settings.hasVibrationOnImpulse];
}

- (void) setHasVibrationOnImpulse:(AMBoolean *)hasVibrationOnImpulse{
    self.tagDevice.settings.hasVibrationOnImpulse = hasVibrationOnImpulse.boolValue;
}

- (AMBoolean*) isTotalMuteOn{
    return (AMBoolean*)[AMBoolean numberWithBool:self.tagDevice.settings.isTotalMuteOn];
}

- (void) setIsTotalMuteOn:(AMBoolean *)isTotalMuteOn{
    self.tagDevice.settings.isTotalMuteOn = isTotalMuteOn.boolValue;
}

- (NSString*) deviceKeyForSettingName:(NSString*)name{
    return [NSString stringWithFormat:@"AM-KEY-%@-%@",self.UUIDString, name];
}

- (AMBoolean*) isEnabled{
    NSString *key =[self deviceKeyForSettingName:@"isEnabled"];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:key]==nil) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:key];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    _isEnabled = (AMBoolean*)[AMBoolean numberWithBool:[[NSUserDefaults standardUserDefaults] boolForKey:key]];
    return  _isEnabled;
}

- (void) setIsEnabled:(AMBoolean *)isEnabled{
    [[NSUserDefaults standardUserDefaults] setBool:isEnabled.boolValue forKey:[self deviceKeyForSettingName:@"isEnabled"]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if (_isEnabled.boolValue!=isEnabled.boolValue && isEnabled.boolValue==NO) {
        [self.tagDevice cancelConnection];
        [self.tagDevice cleanup];
    }
    _isEnabled=isEnabled;
}

- (NSString*) propertyLocalizedTitleAtIndex:(NSInteger)index{
    NSString *p = [super propertyLocalizedTitleAtIndex:index];
    if (p==nil) {
        
        NSString *property_name =self.properties[index];
        if ([property_name isEqualToString:@"hasSoundOnImpulse"]) {
            return NSLocalizedString(@"Make a snapping sound when particle is detected", @"");
        }
        else if ([property_name isEqualToString:@"hasVibrationOnImpulse"]) {
            return NSLocalizedString(@"Vibrate when one particle is detected", @"");
        }
        else if ([property_name isEqualToString:@"hasVibrationOnThreshold"]) {
            return NSLocalizedString(@"Vibrate when detected dose above threshold", @"");
        }
        else if ([property_name isEqualToString:@"isEnabled"]) {
            return NSLocalizedString(@"Enable connection with the Atom Tag when it discovered", @"");
        }
        else if ([property_name isEqualToString:@"isTotalMuteOn"]) {
            return NSLocalizedString(@"Enable mute mode", @"");
        }
    }
    return p;
}

- (void) synchronize{
    [self.tagDevice synchronize];
}


- (NSString*) description{
    return [NSString stringWithFormat:@"%@(%@): thresholdDose = %@, thresholdDoseRate = %@, isVibrateOn = %@",
            self.name,
            self.UUIDString,
            self.thresholdDose,
            self.thresholdDoseRate,
            self.hasVibrationOnThreshold];
}

@end

